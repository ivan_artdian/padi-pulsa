-- MySQL dump 10.13  Distrib 8.0.11, for Linux (x86_64)
--
-- Host: localhost    Database: padi
-- ------------------------------------------------------
-- Server version	8.0.11

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `agent`
--

DROP TABLE IF EXISTS `agent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8 ;
CREATE TABLE `agent` (
  `name` varchar(100) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `phone_no` varchar(15) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `agent_id` varchar(100) NOT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `is_allow_duplicate_email` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`agent_id`),
  CONSTRAINT `agent_group_fk` FOREIGN KEY (`agent_id`) REFERENCES `group` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `agent`
--

LOCK TABLES `agent` WRITE;
/*!40000 ALTER TABLE `agent` DISABLE KEYS */;
INSERT INTO `agent` VALUES ('admin 1','Jl Soekarno Hatta no 1','85523398','admin@mail.com','AGENT_admin_1',1,0),('123','1234224','23424523','432432@gdg.com','AGENT_test',1,0);
/*!40000 ALTER TABLE `agent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `booking_ipl`
--

DROP TABLE IF EXISTS `booking_ipl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8 ;
CREATE TABLE `booking_ipl` (
  `booking_id` varchar(100) NOT NULL,
  `ibs_bill_detail` bigint(20) DEFAULT NULL,
  `trn_reservation_id` bigint(20) DEFAULT NULL,
  `txn_state` varchar(100) DEFAULT NULL,
  `txn_date` date DEFAULT NULL,
  `user_id` varchar(100) DEFAULT NULL,
  `user_email` varchar(100) DEFAULT NULL,
  `txn_amount` varchar(100) DEFAULT NULL,
  `id_pelanggan` varchar(100) DEFAULT NULL,
  `denda` varchar(100) DEFAULT NULL,
  `multi_payment` varchar(100) DEFAULT NULL,
  `response_code` varchar(100) DEFAULT NULL,
  `response_message` varchar(100) DEFAULT NULL,
  `agent_id` varchar(100) DEFAULT NULL,
  `reference1` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`booking_id`),
  KEY `booking_ipl_trn_reservation_fk` (`trn_reservation_id`),
  KEY `booking_ipl_agent_fk` (`agent_id`),
  KEY `booking_ipl_t_ibs_billdetails_fk` (`ibs_bill_detail`),
  CONSTRAINT `booking_ipl_agent_fk` FOREIGN KEY (`agent_id`) REFERENCES `agent` (`agent_id`),
  CONSTRAINT `booking_ipl_t_ibs_billdetails_fk` FOREIGN KEY (`ibs_bill_detail`) REFERENCES `t_ibs_billdetails` (`bills_id`),
  CONSTRAINT `booking_ipl_trn_reservation_fk` FOREIGN KEY (`trn_reservation_id`) REFERENCES `trn_reservation` (`trn_reservation_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `booking_ipl`
--

LOCK TABLES `booking_ipl` WRITE;
/*!40000 ALTER TABLE `booking_ipl` DISABLE KEYS */;
INSERT INTO `booking_ipl` VALUES ('bthttjyt',51,8984827,'Paid','2018-10-05','eded','dede','1000','cfrfr','10','Y','01','success','AGENT_admin_1','dwfefe'),('btyhnr',51,8984827,'Paid','2018-10-05','eded','dede','1000','cfrfr','10','Y','00','success','AGENT_admin_1','dwfefe'),('dbrynrtnm',51,8984827,'Paid','2018-09-05','eded','dede','1000','cfrfr','10','Y','00','success','AGENT_admin_1','dwfefe'),('defce',51,8984827,'Paid','2018-10-05','eded','dede','1000','cfrfr','10','Y','00','success','AGENT_admin_1','dwfefe'),('dfcf',51,8984827,'Paid','2018-10-05','eded','dede','1000','cfrfr','10','Y','00','success','AGENT_admin_1','dwfefe'),('dgbrevw',51,8984827,'Paid','2018-10-05','eded','dede','1000','cfrfr','10','Y','01','success','AGENT_admin_1','dwfefe'),('ent4ene',51,8984827,'Paid','2018-10-05','eded','dede','1000','cfrfr','10','Y','00','success','AGENT_admin_1','dwfefe'),('erbtrerwv',51,8984827,'Paid','2018-10-05','eded','dede','1000','cfrfr','10','Y','01','success','AGENT_admin_1','dwfefe'),('eth54',51,8984827,'Paid','2018-10-05','eded','dede','1000','cfrfr','10','Y','00','success','AGENT_admin_1','dwfefe'),('fdvfdvrev',51,8984827,'Paid','2018-10-05','eded','dede','1000','cfrfr','10','Y','00','success','AGENT_admin_1','dwfefe'),('fdvrfv',51,8984827,'Paid','2018-10-05','eded','dede','1000','cfrfr','10','Y','00','success','AGENT_admin_1','dwfefe'),('gbrbrv',51,8984827,'Paid','2018-09-05','eded','dede','1000','cfrfr','10','Y','00','success','AGENT_admin_1','dwfefe'),('grtbetnyurmn',51,8984827,'Paid','2018-09-05','eded','dede','1000','cfrfr','10','Y','00','success','AGENT_admin_1','dwfefe'),('hrnh4hrt',51,8984827,'Paid','2018-10-05','eded','dede','1000','cfrfr','10','Y','01','success','AGENT_admin_1','dwfefe'),('j6754eh',51,8984827,'Paid','2018-09-05','eded','dede','1000','cfrfr','10','Y','00','success','AGENT_admin_1','dwfefe'),('k54he',51,8984827,'Paid','2018-09-05','eded','dede','1000','cfrfr','10','Y','01','success','AGENT_admin_1','dwfefe'),('m5hr',51,8984827,'Paid','2018-09-05','eded','dede','1000','cfrfr','10','Y','00','success','AGENT_admin_1','dwfefe'),('njytrjr',51,8984827,'Paid','2018-09-05','eded','dede','1000','cfrfr','10','Y','00','success','AGENT_admin_1','dwfefe'),('nrerhge',51,8984827,'Paid','2018-09-05','eded','dede','1000','cfrfr','10','Y','00','success','AGENT_admin_1','dwfefe'),('nrhwrge',51,8984827,'Paid','2018-09-05','eded','dede','1000','cfrfr','10','Y','01','success','AGENT_admin_1','dwfefe'),('nttytrhe',51,8984827,'Paid','2018-09-05','eded','dede','1000','cfrfr','10','Y','00','success','AGENT_admin_1','dwfefe'),('vfdvef',51,8984827,'Paid','2018-10-05','eded','dede','1000','cfrfr','10','Y','00','success','AGENT_admin_1','dwfefe'),('vrvefer',51,8984827,'Paid','2018-10-05','eded','dede','1000','cfrfr','10','Y','00','success','AGENT_admin_1','dwfefe'),('yrhethth',51,8984827,'Paid','2018-09-05','eded','dede','1000','cfrfr','10','Y','00','success','AGENT_admin_1','dwfefe'),('ytrnetdtb',51,8984827,'Paid','2018-10-05','eded','dede','1000','cfrfr','10','Y','01','success','AGENT_admin_1','dwfefe');
/*!40000 ALTER TABLE `booking_ipl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `booking_pulsa`
--

DROP TABLE IF EXISTS `booking_pulsa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8 ;
CREATE TABLE `booking_pulsa` (
  `booking_id` varchar(100) NOT NULL,
  `txn_state` varchar(100) DEFAULT NULL,
  `txn_date` datetime DEFAULT NULL,
  `user_id` varchar(100) DEFAULT NULL,
  `user_email` varchar(100) DEFAULT NULL,
  `txn_amount` varchar(100) DEFAULT NULL,
  `mdn` varchar(100) DEFAULT NULL,
  `product_id` varchar(100) DEFAULT NULL,
  `product_name` varchar(100) DEFAULT NULL,
  `operator` varchar(100) DEFAULT NULL,
  `response_code` varchar(100) DEFAULT NULL,
  `response_message` varchar(100) DEFAULT NULL,
  `trn_reservation_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`booking_id`),
  KEY `booking_pulsa_trn_reservation_FK` (`trn_reservation_id`),
  CONSTRAINT `booking_pulsa_trn_reservation_FK` FOREIGN KEY (`trn_reservation_id`) REFERENCES `trn_reservation` (`trn_reservation_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `booking_pulsa`
--

LOCK TABLES `booking_pulsa` WRITE;
/*!40000 ALTER TABLE `booking_pulsa` DISABLE KEYS */;
INSERT INTO `booking_pulsa` VALUES ('uvK5UmOa','transaction_confirm','2018-09-06 01:52:28','159357','email@mail.com','1000','085290020002','S1','TSEL 1000','Telkomsel','00','Success',NULL);
/*!40000 ALTER TABLE `booking_pulsa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `group`
--

DROP TABLE IF EXISTS `group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8 ;
CREATE TABLE `group` (
  `username` varchar(100) NOT NULL,
  `password` varchar(100) DEFAULT NULL,
  `imageHeader` varchar(100) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `group`
--

LOCK TABLES `group` WRITE;
/*!40000 ALTER TABLE `group` DISABLE KEYS */;
INSERT INTO `group` VALUES ('AGENT_admin_1',NULL,NULL,1),('AGENT_test',NULL,NULL,1),('SUPER_ADMINISTRATOR','SUPER_ADMINISTRATOR','banner.png',1);
/*!40000 ALTER TABLE `group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `group_menu`
--

DROP TABLE IF EXISTS `group_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8 ;
CREATE TABLE `group_menu` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `group_menu`
--

LOCK TABLES `group_menu` WRITE;
/*!40000 ALTER TABLE `group_menu` DISABLE KEYS */;
INSERT INTO `group_menu` VALUES (1,'Admin'),(9,'Parameter'),(12,'Subscriber'),(13,'Partner'),(14,'Transaction');
/*!40000 ALTER TABLE `group_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu`
--

DROP TABLE IF EXISTS `menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8 ;
CREATE TABLE `menu` (
  `menu_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `value` varchar(100) DEFAULT NULL,
  `url` varchar(100) DEFAULT NULL,
  `menu_level` tinyint(4) DEFAULT NULL,
  `group_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`menu_id`),
  KEY `menu_group_menu_fk_idx` (`group_id`),
  CONSTRAINT `menu_group_menu_fk` FOREIGN KEY (`group_id`) REFERENCES `group_menu` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu`
--

LOCK TABLES `menu` WRITE;
/*!40000 ALTER TABLE `menu` DISABLE KEYS */;
INSERT INTO `menu` VALUES (7,'Role','/pages/role-management.xhtml',3,1),(8,'Menu','/pages/menu-management.xhtml',4,1),(10,'User Apps','/pages/user-management.xhtml',1,1),(28,'Group-Menu','/pages/group-menu.xhtml',2,1),(29,'System Parameter','/pages/system-parameter.xhtml',1,9),(41,'Agent','/pages/agent-management.xhtml',5,13),(42,'Register Bill','/pages/bill-invoice-management.xhtml',1,14),(43,'Generate Bill','/pages/invoice-management.xhtml',2,14),(44,'Scheduler','/pages/scheduler-management.xhtml',5,9),(45,'Setting Bill','/pages/system-parameter.xhtml',1,9),(46,'Payment Status','/pages/payment-status.xhtml',15,14);
/*!40000 ALTER TABLE `menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payments`
--

DROP TABLE IF EXISTS `payments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8 ;
CREATE TABLE `payments` (
  `payment_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `payment_amount` decimal(19,2) DEFAULT NULL,
  `payment_code` varchar(40) DEFAULT NULL,
  `payment_date` datetime DEFAULT NULL,
  `payment_status` varchar(10) DEFAULT NULL,
  `payment_type` varchar(50) DEFAULT NULL,
  `payment_type_kai` varchar(20) DEFAULT NULL,
  `booking_id` bigint(20) DEFAULT NULL,
  `trn_reservation_id` bigint(20) DEFAULT NULL,
  `admin_fee_total` decimal(19,2) DEFAULT NULL,
  `extra_fee_total` decimal(19,2) DEFAULT NULL,
  `normal_price_total` decimal(19,2) DEFAULT NULL,
  `card_number` varchar(20) DEFAULT NULL,
  `service_fee_total` decimal(19,2) DEFAULT NULL,
  `payment_code_clickpay` varchar(40) DEFAULT NULL,
  `assurance_charge_tot` decimal(19,2) DEFAULT NULL,
  `payment_3party_Charge_tot` decimal(19,2) DEFAULT NULL,
  `reversal_date` datetime DEFAULT NULL,
  `reversal_code` varchar(40) DEFAULT NULL,
  `payment_code_2` varchar(40) DEFAULT NULL,
  `discount_amount` decimal(19,2) DEFAULT NULL,
  `price_insurance_total` decimal(19,2) DEFAULT NULL,
  `price_tax_total` decimal(19,2) DEFAULT NULL,
  `padipay_code` varchar(40) DEFAULT NULL,
  `book_balance_total` decimal(19,2) DEFAULT NULL,
  `discount_extra_total` decimal(19,2) DEFAULT NULL,
  `discount_total` decimal(19,2) DEFAULT NULL,
  `add_info1` varchar(20) DEFAULT NULL,
  `add_info2` varchar(20) DEFAULT NULL,
  `payment_channel_id` varchar(20) DEFAULT NULL,
  `result_code` varchar(10) DEFAULT NULL,
  `agent_commission_insurance_total` decimal(19,2) DEFAULT NULL,
  `agent_commission_price_total` decimal(19,2) DEFAULT NULL,
  `agent_nta_insurance_total` decimal(19,2) DEFAULT NULL,
  `agent_nta_price_total` decimal(19,2) DEFAULT NULL,
  `commission_insurance_total` decimal(19,2) DEFAULT NULL,
  `commission_price_total` decimal(19,2) DEFAULT NULL,
  `is_paid_by_loyalty` varchar(1) DEFAULT NULL,
  `is_used_disc_pay_channel` varchar(1) DEFAULT NULL,
  `is_used_disc_product` varchar(1) DEFAULT NULL,
  `is_used_loyalty` varchar(1) DEFAULT NULL,
  `is_used_redeem_point` varchar(1) DEFAULT NULL,
  `is_used_voucher` varchar(1) DEFAULT NULL,
  `nta_insurance_total` decimal(19,2) DEFAULT NULL,
  `nta_price_total` decimal(19,2) DEFAULT NULL,
  `paymentStatus` varchar(20) DEFAULT NULL,
  `redeem_points` int(11) DEFAULT NULL,
  `tot_disc_payment_channel` decimal(19,2) DEFAULT NULL,
  `tot_disc_point` decimal(19,2) DEFAULT NULL,
  `tot_disc_Product` decimal(19,2) DEFAULT NULL,
  `tot_disc_voucher` decimal(19,2) DEFAULT NULL,
  `voucher_code` varchar(40) DEFAULT NULL,
  `voucher_value` decimal(19,2) DEFAULT NULL,
  `voucher_value_type` varchar(1) DEFAULT NULL,
  `transfer_points` int(11) DEFAULT NULL,
  `Payment3party_charge_tot` decimal(19,2) DEFAULT NULL,
  `invoice_amount` decimal(19,2) DEFAULT NULL,
  PRIMARY KEY (`payment_id`),
  KEY `FK526A0F2DFA3F4AF0` (`trn_reservation_id`),
  KEY `FK526A0F2D1B9094E3` (`booking_id`),
  KEY `FK526A0F2DB292A0B6` (`trn_reservation_id`),
  KEY `FK526A0F2DEF713D2B` (`trn_reservation_id`),
  KEY `FK526A0F2D19CC9EC5` (`trn_reservation_id`),
  KEY `FK526A0F2D249E98C3` (`trn_reservation_id`),
  KEY `FK526A0F2DFF7132E1` (`trn_reservation_id`),
  KEY `FK526A0F2DDF38554C` (`trn_reservation_id`),
  KEY `FK526A0F2DC731B72D` (`trn_reservation_id`),
  CONSTRAINT `payments_trn_reservation_FK` FOREIGN KEY (`trn_reservation_id`) REFERENCES `trn_reservation` (`trn_reservation_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7824976 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payments`
--

LOCK TABLES `payments` WRITE;
/*!40000 ALTER TABLE `payments` DISABLE KEYS */;
INSERT INTO `payments` VALUES (1514044,960000.00,NULL,NULL,NULL,NULL,NULL,NULL,9487900,0.00,0.00,960000.00,NULL,0.00,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,960000.00,0.00,0.00,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00,NULL,NULL,0.00,0.00,0.00,NULL,NULL,0.00,NULL,NULL,0.00,NULL),(3187552,960000.00,NULL,NULL,NULL,NULL,NULL,NULL,9487899,0.00,0.00,960000.00,NULL,0.00,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,960000.00,0.00,0.00,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00,NULL,NULL,0.00,0.00,0.00,NULL,NULL,0.00,NULL,NULL,0.00,NULL),(6067263,960000.00,NULL,NULL,NULL,NULL,NULL,NULL,9487898,0.00,0.00,960000.00,NULL,0.00,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,960000.00,0.00,0.00,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00,NULL,NULL,0.00,0.00,0.00,NULL,NULL,0.00,NULL,NULL,0.00,NULL),(7824975,1000.00,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00,0.00,1000.00,NULL,0.00,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1000.00,0.00,0.00,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00,NULL,NULL,0.00,0.00,0.00,NULL,NULL,0.00,NULL,NULL,0.00,NULL);
/*!40000 ALTER TABLE `payments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8 ;
CREATE TABLE `role` (
  `role_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(100) DEFAULT NULL,
  `master` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (10,'Super Admin',0),(11,'admin',0);
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_menu`
--

DROP TABLE IF EXISTS `role_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8 ;
CREATE TABLE `role_menu` (
  `role_id` bigint(20) NOT NULL,
  `menu_id` bigint(20) NOT NULL,
  `need_approval` tinyint(1) DEFAULT NULL,
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `role_menu_role_fk` (`role_id`),
  KEY `role_menu_menu_fk` (`menu_id`),
  CONSTRAINT `role_menu_menu_fk` FOREIGN KEY (`menu_id`) REFERENCES `menu` (`menu_id`),
  CONSTRAINT `role_menu_role_fk` FOREIGN KEY (`role_id`) REFERENCES `role` (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=356 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_menu`
--

LOCK TABLES `role_menu` WRITE;
/*!40000 ALTER TABLE `role_menu` DISABLE KEYS */;
INSERT INTO `role_menu` VALUES (10,44,0,343),(10,41,0,344),(10,10,0,345),(10,28,0,346),(10,7,0,347),(10,8,0,348),(11,46,0,352),(11,45,0,353),(11,43,0,354),(11,42,0,355);
/*!40000 ALTER TABLE `role_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `scheduler`
--

DROP TABLE IF EXISTS `scheduler`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8 ;
CREATE TABLE `scheduler` (
  `scheduler_id` varchar(20) NOT NULL,
  `svc_name` varchar(100) DEFAULT NULL,
  `svc_executions` varchar(100) DEFAULT NULL,
  `next_fire` datetime DEFAULT NULL,
  `description` text,
  `period` varchar(100) DEFAULT NULL,
  `agent_id` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`scheduler_id`),
  KEY `scheduler_agent_fk` (`agent_id`),
  CONSTRAINT `scheduler_agent_fk` FOREIGN KEY (`agent_id`) REFERENCES `agent` (`agent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `scheduler`
--

LOCK TABLES `scheduler` WRITE;
/*!40000 ALTER TABLE `scheduler` DISABLE KEYS */;
INSERT INTO `scheduler` VALUES ('mZIciFsr','test send email','SendEmail',NULL,'Desc','0 * * ? * * *',NULL);
/*!40000 ALTER TABLE `scheduler` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `systemparameter`
--

DROP TABLE IF EXISTS `systemparameter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8 ;
CREATE TABLE `systemparameter` (
  `param_Id` int(11) NOT NULL AUTO_INCREMENT,
  `paramname` varchar(50) DEFAULT NULL,
  `paramvalue` varchar(500) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `ispassword` varchar(3) DEFAULT NULL,
  `createdate` datetime DEFAULT NULL,
  `modifydate` datetime DEFAULT NULL,
  `agent_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`param_Id`),
  KEY `systemparameter_agent_fk` (`agent_id`),
  CONSTRAINT `systemparameter_agent_fk` FOREIGN KEY (`agent_id`) REFERENCES `agent` (`agent_id`)
) ENGINE=InnoDB AUTO_INCREMENT=107 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `systemparameter`
--

LOCK TABLES `systemparameter` WRITE;
/*!40000 ALTER TABLE `systemparameter` DISABLE KEYS */;
INSERT INTO `systemparameter` VALUES (80,'EMAIL_API_FROM','ivan.willy.artdian@gmail.com',NULL,NULL,'2018-09-07 13:11:50',NULL,NULL),(81,'EMAIL_API_USERNAME','ivan.willy.artdian@gmail.com',NULL,NULL,'2018-09-07 13:11:50',NULL,NULL),(82,'EMAIL_API_PASSWORD','38FT8$Y2RqK^GUaMh$7k5%@3%g',NULL,NULL,'2018-09-07 13:11:50',NULL,NULL),(83,'MCASH_USER','628993665482',NULL,NULL,'2018-09-07 13:11:50',NULL,NULL),(84,'MCASH_PASSWORD','kashiwagiukon',NULL,NULL,'2018-09-07 13:11:50',NULL,NULL),(85,'PADIPAY_MERCHANT_CODE','padipaytest',NULL,NULL,'2018-09-07 13:11:50',NULL,NULL),(86,'PADIPAY_MERCHANT_PASSWORD','padipaytest',NULL,NULL,'2018-09-07 13:11:50',NULL,NULL),(87,'PADIPAY_HOUR_EXPIRED','1',NULL,NULL,'2018-09-07 13:11:50',NULL,NULL),(88,'PADIPAY_MERCHANT_URL','http://182.23.65.29:8888/padi-pulsa/adapter/mgateway/transactionConfirmResponse',NULL,NULL,'2018-09-07 13:11:50',NULL,NULL),(89,'PADIPAY_HOME_URL','http://localhost:8080',NULL,NULL,'2018-09-07 13:11:50',NULL,NULL),(90,'TRANSACTION_CONFIRM_EMAIL_SUBJECT','Transaction Notification',NULL,NULL,'2018-09-07 13:11:50',NULL,NULL),(91,'CREDENTIAL_CODE','PadiCiti',NULL,NULL,'2018-09-07 13:11:50',NULL,NULL),(92,'CREDENTIAL_PASS','PadiCiti123',NULL,NULL,'2018-09-07 13:11:50',NULL,NULL),(93,'PAYMENT_LIMIT_DATE','1',NULL,NULL,'2018-09-07 13:11:50',NULL,NULL),(94,'ADMIN_FEE','0',NULL,NULL,'2018-09-07 13:11:50',NULL,NULL),(95,'IMAGE_ROOT_PATH','/opt/mcommerce-source/image/mobileimage/',NULL,NULL,'2018-09-07 13:11:50',NULL,NULL),(96,'DEFAULT_CONTEXT_ROOT','https://localhost:8091/padi-pulsa/',NULL,NULL,'2018-09-07 13:11:50',NULL,NULL),(97,'MOBILE_IMAGE_MAP','resources/mobile/',NULL,NULL,'2018-09-07 13:11:50',NULL,NULL),(98,'PADIPAY_URL','https://www.padipay.com/padipay-payment/webapp/home/request.html',NULL,NULL,'2018-09-28 02:03:09',NULL,NULL),(99,'Harga','1000',NULL,NULL,NULL,NULL,'AGENT_admin_1'),(100,'Denda','60000',NULL,NULL,NULL,NULL,'AGENT_admin_1');
/*!40000 ALTER TABLE `systemparameter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_ibs_bill`
--

DROP TABLE IF EXISTS `t_ibs_bill`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8 ;
CREATE TABLE `t_ibs_bill` (
  `bill_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `type` varchar(100) DEFAULT NULL,
  `filename` varchar(100) DEFAULT NULL,
  `valid` tinyint(1) DEFAULT NULL,
  `agent_id` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`bill_id`),
  KEY `t_ibs_bill_agent_fk` (`agent_id`),
  CONSTRAINT `t_ibs_bill_agent_fk` FOREIGN KEY (`agent_id`) REFERENCES `agent` (`agent_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_ibs_bill`
--

LOCK TABLES `t_ibs_bill` WRITE;
/*!40000 ALTER TABLE `t_ibs_bill` DISABLE KEYS */;
INSERT INTO `t_ibs_bill` VALUES (11,'input',NULL,1,'AGENT_admin_1');
/*!40000 ALTER TABLE `t_ibs_bill` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_ibs_billdetails`
--

DROP TABLE IF EXISTS `t_ibs_billdetails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8 ;
CREATE TABLE `t_ibs_billdetails` (
  `bills_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `bill_id` bigint(20) DEFAULT NULL,
  `id_pelanggan` varchar(100) DEFAULT NULL,
  `bln_tagihan` varchar(100) DEFAULT NULL,
  `thn_tagihan` varchar(100) DEFAULT NULL,
  `sts_tagihan` varchar(100) DEFAULT NULL,
  `nama_pelanggan` varchar(100) DEFAULT NULL,
  `no_hp` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `nama_perumahan` varchar(100) DEFAULT NULL,
  `alamat` varchar(100) DEFAULT NULL,
  `no_block` varchar(100) DEFAULT NULL,
  `kel_kec` varchar(100) DEFAULT NULL,
  `kab_kota` varchar(100) DEFAULT NULL,
  `provinsi` varchar(100) DEFAULT NULL,
  `luas_tanah` varchar(100) DEFAULT NULL,
  `amount` varchar(100) DEFAULT NULL,
  `valid` tinyint(1) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`bills_id`),
  KEY `t_ibs_billdetails_t_ibs_bill_fk` (`bill_id`),
  CONSTRAINT `t_ibs_billdetails_t_ibs_bill_fk` FOREIGN KEY (`bill_id`) REFERENCES `t_ibs_bill` (`bill_id`)
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_ibs_billdetails`
--

LOCK TABLES `t_ibs_billdetails` WRITE;
/*!40000 ALTER TABLE `t_ibs_billdetails` DISABLE KEYS */;
INSERT INTO `t_ibs_billdetails` VALUES (51,11,'hgyibub',NULL,NULL,NULL,'khb','kb','kbk','bkb','kb','kbkj','bkj','bkjhbkjbkj','kjjb','400',NULL,NULL,NULL,1),(52,11,'khkhbkbkj',NULL,NULL,NULL,'hkj','hjk','hjk','hjkh','jkh','khkjhkj','kj','bkj','hnjk','330',NULL,NULL,NULL,1),(54,11,'PUT9999',NULL,NULL,NULL,'Nunuh','0813802929302','nunuh.nurjaman@gmail.com','PUSPITALOKA','Ruko Kebayoran Arcade 2','B1','Pondok Aren','Tangerang Selatan','Banten','300',NULL,NULL,'2018-10-09 23:39:23',1),(55,11,'PUT9998',NULL,NULL,NULL,'Nunuh','0813802929302','nunuh.nurjaman@gmail.com','PUSPITALOKA','Ruko Kebayoran Arcade 2','B1','Pondok Aren','Tangerang Selatan','Banten','1',NULL,NULL,'2018-10-09 23:43:45',1);
/*!40000 ALTER TABLE `t_ibs_billdetails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_ibs_invoice`
--

DROP TABLE IF EXISTS `t_ibs_invoice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8 ;
CREATE TABLE `t_ibs_invoice` (
  `bill_invoice_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `bills_id` bigint(20) DEFAULT NULL,
  `bill_periode` varchar(100) DEFAULT NULL,
  `paid_status` varchar(100) DEFAULT NULL,
  `paid_date` datetime DEFAULT NULL,
  `amount` bigint(20) DEFAULT NULL,
  `valid` tinyint(1) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `denda` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`bill_invoice_id`),
  KEY `t_ibs_invoice_t_ibs_billdetails_fk` (`bills_id`),
  CONSTRAINT `t_ibs_invoice_t_ibs_billdetails_fk` FOREIGN KEY (`bills_id`) REFERENCES `t_ibs_billdetails` (`bills_id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_ibs_invoice`
--

LOCK TABLES `t_ibs_invoice` WRITE;
/*!40000 ALTER TABLE `t_ibs_invoice` DISABLE KEYS */;
INSERT INTO `t_ibs_invoice` VALUES (28,51,'05/2018','UnPaid',NULL,400000,1,'2018-10-04 15:50:07',0),(29,51,'01/2018','UnPaid',NULL,400000,1,'2018-10-09 22:36:33',0),(30,51,'01/2018','UnPaid',NULL,400000,1,'2018-10-09 22:37:09',0),(31,52,'01/2018','UnPaid',NULL,330000,1,'2018-10-09 22:37:09',0),(32,51,'01/2020','UnPaid',NULL,400000,1,'2018-10-09 22:46:56',0),(33,51,'01/2021','UnPaid',NULL,400000,1,'2018-10-09 22:57:33',0),(34,51,'12/2018','UnPaid',NULL,400000,1,'2018-10-09 22:58:25',0),(35,51,'01/2019','UnPaid',NULL,400000,1,'2018-10-09 22:58:25',0),(36,51,'02/2019','UnPaid',NULL,400000,1,'2018-10-09 22:58:25',0),(37,51,'03/2019','UnPaid',NULL,400000,1,'2018-10-09 22:58:25',0),(38,51,'04/2019','UnPaid',NULL,400000,1,'2018-10-09 22:58:25',0),(39,51,'05/2019','UnPaid',NULL,400000,1,'2018-10-09 22:58:25',0),(40,51,'06/2019','UnPaid',NULL,400000,1,'2018-10-09 22:58:25',0);
/*!40000 ALTER TABLE `t_ibs_invoice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trn_reservation`
--

DROP TABLE IF EXISTS `trn_reservation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8 ;
CREATE TABLE `trn_reservation` (
  `trn_reservation_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `transaction_code` varchar(40) NOT NULL,
  `trn_status` varchar(10) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `payment_limit_date` datetime DEFAULT NULL,
  `trn_type` varchar(20) DEFAULT NULL,
  `transaction_date` datetime DEFAULT NULL,
  `fail_email_status` varchar(10) DEFAULT NULL,
  `booking_kai_limit_date` datetime DEFAULT NULL,
  `finpay_email_conf_sts` varchar(10) DEFAULT NULL,
  `mitrapay_email_conf_sts` varchar(10) DEFAULT NULL,
  `booking_limit_date` datetime DEFAULT NULL,
  `reservation_channel` varchar(20) DEFAULT NULL,
  `is_double` varchar(1) DEFAULT NULL,
  `transaction_code_old` varchar(40) DEFAULT NULL,
  `reseller_id` bigint(20) DEFAULT NULL,
  `contact_first_name` varchar(50) DEFAULT NULL,
  `contact_last_name` varchar(50) DEFAULT NULL,
  `contact_phone1` varchar(30) DEFAULT NULL,
  `contact_phone2` varchar(30) DEFAULT NULL,
  `reward_point_sts` varchar(1) DEFAULT NULL,
  `contact_email` varchar(40) DEFAULT NULL,
  `device_type` varchar(40) DEFAULT NULL,
  `transactionDateToDate` datetime DEFAULT NULL,
  `voucher_code` varchar(40) DEFAULT NULL,
  `is_take_commission` varchar(1) DEFAULT NULL,
  `get_vou_sts` varchar(255) DEFAULT NULL,
  `time_check_payment_sts` datetime DEFAULT NULL,
  `admin_charge` decimal(19,2) DEFAULT NULL,
  `angsuran` varchar(100) DEFAULT NULL,
  `admin_charge_2` decimal(19,2) DEFAULT NULL,
  `customer_consuming_power` varchar(50) DEFAULT NULL,
  `customer_id` varchar(50) DEFAULT NULL,
  `customer_meter_serial_number` varchar(50) DEFAULT NULL,
  `customer_name` varchar(50) DEFAULT NULL,
  `booking_date` varchar(40) DEFAULT NULL,
  `issued_date_time` datetime DEFAULT NULL,
  `num_code` varchar(40) DEFAULT NULL,
  `input_type` varchar(30) DEFAULT NULL,
  `remark` varchar(500) DEFAULT NULL,
  `unsold1` varchar(255) DEFAULT NULL,
  `unsold2` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`trn_reservation_id`),
  UNIQUE KEY `transaction_code` (`transaction_code`),
  KEY `FK463E5BBD4CC528B1` (`user_id`),
  KEY `FK463E5BBDEF9A786F` (`user_id`),
  KEY `FK463E5BBD44264DF5` (`reseller_id`),
  KEY `FK463E5BBD6275AD8E` (`user_id`),
  KEY `IDX_TRXRESERVATION_TRN_CODE` (`transaction_code`),
  KEY `IDX_TRXRESERVATION_USERID` (`user_id`),
  KEY `FK463E5BBD79E3FC81` (`reseller_id`),
  KEY `FK463E5BBD594E1D32` (`reseller_id`),
  KEY `FK463E5BBD7D7362D9` (`reseller_id`),
  KEY `FK463E5BBD48FC52C6` (`user_id`),
  KEY `FK463E5BBDF5891204` (`reseller_id`),
  KEY `FK463E5BBDB5990F20` (`reseller_id`),
  KEY `FK463E5BBDC0BA360D` (`user_id`),
  KEY `FK463E5BBD5456AAC0` (`reseller_id`),
  KEY `FK463E5BBDED8792C9` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9487901 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trn_reservation`
--

LOCK TABLES `trn_reservation` WRITE;
/*!40000 ALTER TABLE `trn_reservation` DISABLE KEYS */;
INSERT INTO `trn_reservation` VALUES (8984827,'uvK5UmOa','N',159357,'2018-09-06 02:45:05',NULL,'2018-09-06 01:45:05',NULL,NULL,NULL,NULL,'2018-09-06 02:45:05','pulsa',NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(9487898,'1809298440001','N',619068,'2018-09-29 23:56:33',NULL,'2018-09-29 22:56:33',NULL,NULL,NULL,NULL,'2018-09-29 23:56:33','ipl',NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(9487899,'1809295060001','N',619068,'2018-09-29 23:57:42',NULL,'2018-09-29 22:57:42',NULL,NULL,NULL,NULL,'2018-09-29 23:57:42','ipl',NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(9487900,'1809297740001','N',619068,'2018-09-30 00:03:07',NULL,'2018-09-29 23:03:07',NULL,NULL,NULL,NULL,'2018-09-30 00:03:07','ipl',NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `trn_reservation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `unique_number`
--

DROP TABLE IF EXISTS `unique_number`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8 ;
CREATE TABLE `unique_number` (
  `unique_number_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `counter` int(11) DEFAULT NULL,
  `number` varchar(20) DEFAULT NULL,
  `trn_date` varchar(8) DEFAULT NULL,
  PRIMARY KEY (`unique_number_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1934 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `unique_number`
--

LOCK TABLES `unique_number` WRITE;
/*!40000 ALTER TABLE `unique_number` DISABLE KEYS */;
INSERT INTO `unique_number` VALUES (1931,1,'1809298440001',NULL),(1932,1,'1809295060001',NULL),(1933,1,'1809297740001',NULL);
/*!40000 ALTER TABLE `unique_number` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8 ;
CREATE TABLE `user` (
  `username` varchar(100) NOT NULL,
  `password` varchar(4000) DEFAULT NULL,
  `is_login` tinyint(1) DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `user_locale` varchar(100) DEFAULT NULL,
  `role_id` bigint(20) DEFAULT NULL,
  `web_theme` varchar(20) DEFAULT NULL,
  `login_time` datetime DEFAULT NULL,
  `logout_time` datetime DEFAULT NULL,
  PRIMARY KEY (`username`),
  KEY `user_role_fk` (`role_id`),
  CONSTRAINT `user_role_fk` FOREIGN KEY (`role_id`) REFERENCES `role` (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES ('agent_admin','920:7ca653061633e38383ee73424a1110f4:a6df76ce3c1b469107b7b6b1143cef87fe5bd066fbc4cd533b55f71369ab63a5ed680fe1d38871c2ca32390229ee2ffcd9cce81403fba4af95fa9288f94c7992',1,'2018-10-19 16:05:08','en_US',11,'humanity','2018-10-19 16:05:08',NULL),('sys','920:dbc6ee2bc46b110d782040d8c55c6b67:2cc25ae08050979cfd135a31838d83ad02406aad557558b908df2a99981732bba88c46a19c61cd024e27815d035e84e4071182488f31fba6d25f134a598d25ab',0,'2018-10-04 10:59:24','en_US',10,'black-tie','2018-10-04 10:59:24','2018-10-04 10:59:30'),('test','920:b5811028d8f3a3554d2966671aeeae52:21c87aaab75f0d7f686e82bc05de20fddb2a5b13342560de6f598881b628516582e653e2e216ba25668541fe088e49add27c470a07e32c538522f3cb6d762e08',1,'2018-10-04 07:18:58','en_US',11,'humanity','2018-10-04 07:18:58',NULL);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_group`
--

DROP TABLE IF EXISTS `user_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8 ;
CREATE TABLE `user_group` (
  `user_id` varchar(100) NOT NULL,
  `group_id` varchar(100) NOT NULL,
  PRIMARY KEY (`user_id`,`group_id`),
  KEY `user_group_group_fk` (`group_id`),
  CONSTRAINT `user_group_group_fk` FOREIGN KEY (`group_id`) REFERENCES `group` (`username`),
  CONSTRAINT `user_group_user_fk` FOREIGN KEY (`user_id`) REFERENCES `user` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_group`
--

LOCK TABLES `user_group` WRITE;
/*!40000 ALTER TABLE `user_group` DISABLE KEYS */;
INSERT INTO `user_group` VALUES ('agent_admin','AGENT_admin_1'),('test','AGENT_test'),('sys','SUPER_ADMINISTRATOR');
/*!40000 ALTER TABLE `user_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8 ;
CREATE TABLE `users` (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `address` varchar(128) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `full_name` varchar(50) DEFAULT NULL,
  `password` varchar(60) DEFAULT NULL,
  `phone_number` varchar(30) DEFAULT NULL,
  `user_name` varchar(50) DEFAULT NULL,
  `mother_maiden_name` varchar(50) DEFAULT NULL,
  `is_active` char(1) DEFAULT NULL,
  `city_id` bigint(20) DEFAULT NULL,
  `province_id` bigint(20) DEFAULT NULL,
  `sts_email_actived` varchar(10) DEFAULT NULL,
  `sts_email_reset_pass` varchar(10) DEFAULT NULL,
  `temp_password` varchar(60) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `sts_email_info` varchar(10) DEFAULT NULL,
  `title` varchar(15) DEFAULT NULL,
  `user_birthdate` date DEFAULT NULL,
  `gender` varchar(10) DEFAULT NULL,
  `retry_send_mail` varchar(10) DEFAULT NULL,
  `is_locked` char(1) DEFAULT NULL,
  `account_bank` varchar(50) DEFAULT NULL,
  `account_holder_name` varchar(50) DEFAULT NULL,
  `address2` varchar(128) DEFAULT NULL,
  `bank_name` varchar(50) DEFAULT NULL,
  `branch_office` varchar(80) DEFAULT NULL,
  `home_phone` varchar(30) DEFAULT NULL,
  `occupation` varchar(50) DEFAULT NULL,
  `place_of_birth` varchar(40) DEFAULT NULL,
  `post_code` varchar(20) DEFAULT NULL,
  `identity_card_num` varchar(50) DEFAULT NULL,
  `device_type` varchar(40) DEFAULT NULL,
  `app_type` varchar(40) DEFAULT NULL,
  `use_padiair` varchar(40) DEFAULT NULL,
  `use_paditrain` varchar(40) DEFAULT NULL,
  `sts_email_info_padiair` varchar(10) DEFAULT NULL,
  `sts_email_blast_paditrain` varchar(10) DEFAULT NULL,
  `sts_email_blast_padiair` varchar(10) DEFAULT NULL,
  `photo_url` longtext,
  `use_padipay` varchar(40) DEFAULT NULL,
  `account_deposit_num` varchar(50) DEFAULT NULL,
  `deposit_member` varchar(10) DEFAULT NULL,
  `pin_user` varchar(60) DEFAULT NULL,
  `sub_agent_id` bigint(20) DEFAULT NULL,
  `merchant_code` varchar(40) DEFAULT NULL,
  `counter_wrong_pass` int(11) DEFAULT '0',
  `create_date_member` datetime DEFAULT NULL,
  `device_registered` varchar(40) DEFAULT NULL,
  `device_registered_member` varchar(40) DEFAULT NULL,
  `ip_last_used` varchar(30) DEFAULT NULL,
  `ip_registered` varchar(30) DEFAULT NULL,
  `ip_registered_member` varchar(30) DEFAULT NULL,
  `is_email_notif` varchar(1) DEFAULT NULL,
  `is_user_facebook` varchar(1) DEFAULT NULL,
  `is_user_twitter` varchar(1) DEFAULT NULL,
  `last_date_login` datetime DEFAULT NULL,
  `temp_token_member` varchar(10) DEFAULT NULL,
  `token_member_expired` datetime DEFAULT NULL,
  `update_date_member` datetime DEFAULT NULL,
  `reseller_id` bigint(20) DEFAULT NULL,
  `is_member` varchar(1) DEFAULT NULL,
  `count_pin` varchar(2) DEFAULT NULL,
  `status_pin_deposit` varchar(20) DEFAULT NULL,
  `created_date` date DEFAULT NULL,
  `created_time` time DEFAULT NULL,
  `mobile_phone` varchar(255) DEFAULT NULL,
  `update_time` time DEFAULT NULL,
  `check_term_condition` varchar(1) DEFAULT NULL,
  `community` varchar(255) DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `group_id` varchar(255) DEFAULT NULL,
  `heir` varchar(100) DEFAULT NULL,
  `last_name` varchar(80) DEFAULT NULL,
  `member_id` varchar(255) DEFAULT NULL,
  `notification` varchar(255) DEFAULT NULL,
  `religion` varchar(50) DEFAULT NULL,
  `security_answer` varchar(255) DEFAULT NULL,
  `security_question` varchar(255) DEFAULT NULL,
  `type_mobile_payment` varchar(255) DEFAULT NULL,
  `yahoo_id` varchar(80) DEFAULT NULL,
  `phone_number_2` varchar(30) DEFAULT NULL,
  `reward_point_sts` varchar(1) DEFAULT NULL,
  `is_new_reg` varchar(1) DEFAULT NULL,
  `parent_user_name` varchar(50) DEFAULT NULL,
  `sts_sms_activated` varchar(1) DEFAULT NULL,
  `code_activation` varchar(6) DEFAULT NULL,
  `sts_sms_sent` varchar(1) DEFAULT NULL,
  `desc_sms_sent` varchar(255) DEFAULT NULL,
  `desc_email_sent` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=57398712 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (57398648,'Ruko Kebayoran Arcade 2','nunuh.nurjaman@gmail.com','Nunuh','40bd001563085fc35165329ea1ff5c5ecbdbbeef','0813802929302','PUS0001',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-09-30 22:39:44',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(57398649,'Ruko Kebayoran Arcade 2','nunuh.nurjaman@gmail.com','Nunuh2','40bd001563085fc35165329ea1ff5c5ecbdbbeef','0813802929302','PUS0002',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-09-30 22:39:44',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(57398650,'Ruko Kebayoran Arcade 2','nunuh.nurjaman@gmail.com','Nunuh','5A16FC3F8BF4F3A90ACF67D06D06836EABEB519F','0813802929302','PUS0001',NULL,'T',NULL,NULL,NULL,NULL,NULL,'2018-09-30 22:39:44',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(57398651,'Ruko Kebayoran Arcade 2','nunuh.nurjaman@gmail.com','Nunuh2','5A16FC3F8BF4F3A90ACF67D06D06836EABEB519F','0813802929302','PUS0002',NULL,'T',NULL,NULL,NULL,NULL,NULL,'2018-09-30 22:39:44',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(57398652,NULL,NULL,NULL,'5A16FC3F8BF4F3A90ACF67D06D06836EABEB519F',NULL,NULL,NULL,'T',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(57398653,'Ruko Kebayoran Arcade 2','nunuh.nurjaman@gmail.com','Nunuh','5A16FC3F8BF4F3A90ACF67D06D06836EABEB519F','0813802929302','PUS0001',NULL,'T',NULL,NULL,NULL,NULL,NULL,'2018-09-30 22:39:44',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(57398654,'Ruko Kebayoran Arcade 2','nunuh.nurjaman@gmail.com','Nunuh2','5A16FC3F8BF4F3A90ACF67D06D06836EABEB519F','0813802929302','PUS0002',NULL,'T',NULL,NULL,NULL,NULL,NULL,'2018-09-30 22:39:44',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(57398655,NULL,NULL,NULL,'5A16FC3F8BF4F3A90ACF67D06D06836EABEB519F',NULL,NULL,NULL,'T',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(57398656,NULL,NULL,NULL,'5A16FC3F8BF4F3A90ACF67D06D06836EABEB519F',NULL,NULL,NULL,'T',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(57398657,'Ruko Kebayoran Arcade 2','nunuh.nurjaman@gmail.com','Nunuh','5A16FC3F8BF4F3A90ACF67D06D06836EABEB519F','0813802929302','PUS0001',NULL,'T',NULL,NULL,NULL,NULL,NULL,'2018-09-30 22:39:44',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(57398658,'Ruko Kebayoran Arcade 2','nunuh.nurjaman@gmail.com','Nunuh2','5A16FC3F8BF4F3A90ACF67D06D06836EABEB519F','0813802929302','PUS0002',NULL,'T',NULL,NULL,NULL,NULL,NULL,'2018-09-30 22:39:44',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(57398659,NULL,NULL,NULL,'5A16FC3F8BF4F3A90ACF67D06D06836EABEB519F',NULL,NULL,NULL,'T',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(57398660,NULL,NULL,NULL,'5A16FC3F8BF4F3A90ACF67D06D06836EABEB519F',NULL,NULL,NULL,'T',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(57398661,NULL,NULL,NULL,'5A16FC3F8BF4F3A90ACF67D06D06836EABEB519F',NULL,NULL,NULL,'T',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(57398662,'Ruko Kebayoran Arcade 2','nunuh.nurjaman@gmail.com','Nunuh','5A16FC3F8BF4F3A90ACF67D06D06836EABEB519F','0813802929302','PUS0001',NULL,'T',NULL,NULL,NULL,NULL,NULL,'2018-09-30 22:39:44',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(57398663,'Ruko Kebayoran Arcade 2','nunuh.nurjaman@gmail.com','Nunuh2','5A16FC3F8BF4F3A90ACF67D06D06836EABEB519F','0813802929302','PUS0002',NULL,'T',NULL,NULL,NULL,NULL,NULL,'2018-09-30 22:39:44',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(57398664,NULL,NULL,NULL,'5A16FC3F8BF4F3A90ACF67D06D06836EABEB519F',NULL,NULL,NULL,'T',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(57398665,NULL,NULL,NULL,'5A16FC3F8BF4F3A90ACF67D06D06836EABEB519F',NULL,NULL,NULL,'T',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(57398666,NULL,NULL,NULL,'5A16FC3F8BF4F3A90ACF67D06D06836EABEB519F',NULL,NULL,NULL,'T',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(57398667,NULL,NULL,NULL,'5A16FC3F8BF4F3A90ACF67D06D06836EABEB519F',NULL,NULL,NULL,'T',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(57398668,'Ruko Kebayoran Arcade 2','nunuh.nurjaman@gmail.com','Nunuh','5A16FC3F8BF4F3A90ACF67D06D06836EABEB519F','0813802929302','PUS0001',NULL,'T',NULL,NULL,NULL,NULL,NULL,'2018-09-30 22:39:44',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(57398669,'Ruko Kebayoran Arcade 2','nunuh.nurjaman@gmail.com','Nunuh2','5A16FC3F8BF4F3A90ACF67D06D06836EABEB519F','0813802929302','PUS0002',NULL,'T',NULL,NULL,NULL,NULL,NULL,'2018-09-30 22:39:44',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(57398670,NULL,NULL,NULL,'5A16FC3F8BF4F3A90ACF67D06D06836EABEB519F',NULL,NULL,NULL,'T',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(57398671,NULL,NULL,NULL,'5A16FC3F8BF4F3A90ACF67D06D06836EABEB519F',NULL,NULL,NULL,'T',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(57398672,NULL,NULL,NULL,'5A16FC3F8BF4F3A90ACF67D06D06836EABEB519F',NULL,NULL,NULL,'T',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(57398673,NULL,NULL,NULL,'5A16FC3F8BF4F3A90ACF67D06D06836EABEB519F',NULL,NULL,NULL,'T',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(57398674,NULL,NULL,NULL,'5A16FC3F8BF4F3A90ACF67D06D06836EABEB519F',NULL,NULL,NULL,'T',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(57398675,'Ruko Kebayoran Arcade 2','nunuh.nurjaman@gmail.com','Nunuh','5A16FC3F8BF4F3A90ACF67D06D06836EABEB519F','0813802929302','PUS0001',NULL,'T',NULL,NULL,NULL,NULL,NULL,'2018-09-30 22:39:44',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(57398676,'Ruko Kebayoran Arcade 2','nunuh.nurjaman@gmail.com','Nunuh2','5A16FC3F8BF4F3A90ACF67D06D06836EABEB519F','0813802929302','PUS0002',NULL,'T',NULL,NULL,NULL,NULL,NULL,'2018-09-30 22:39:44',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(57398677,NULL,NULL,NULL,'5A16FC3F8BF4F3A90ACF67D06D06836EABEB519F',NULL,NULL,NULL,'T',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(57398678,NULL,NULL,NULL,'5A16FC3F8BF4F3A90ACF67D06D06836EABEB519F',NULL,NULL,NULL,'T',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(57398679,NULL,NULL,NULL,'5A16FC3F8BF4F3A90ACF67D06D06836EABEB519F',NULL,NULL,NULL,'T',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(57398680,NULL,NULL,NULL,'5A16FC3F8BF4F3A90ACF67D06D06836EABEB519F',NULL,NULL,NULL,'T',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(57398681,NULL,NULL,NULL,'5A16FC3F8BF4F3A90ACF67D06D06836EABEB519F',NULL,NULL,NULL,'T',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(57398682,NULL,NULL,NULL,'5A16FC3F8BF4F3A90ACF67D06D06836EABEB519F',NULL,NULL,NULL,'T',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(57398683,'Ruko Kebayoran Arcade 2','nunuh.nurjaman@gmail.com','Nunuh','5A16FC3F8BF4F3A90ACF67D06D06836EABEB519F','0813802929302','PUS0001',NULL,'T',NULL,NULL,NULL,NULL,NULL,'2018-09-30 22:39:44',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(57398684,'Ruko Kebayoran Arcade 2','nunuh.nurjaman@gmail.com','Nunuh2','5A16FC3F8BF4F3A90ACF67D06D06836EABEB519F','0813802929302','PUS0002',NULL,'T',NULL,NULL,NULL,NULL,NULL,'2018-09-30 22:39:44',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(57398685,NULL,NULL,NULL,'5A16FC3F8BF4F3A90ACF67D06D06836EABEB519F',NULL,NULL,NULL,'T',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(57398686,NULL,NULL,NULL,'5A16FC3F8BF4F3A90ACF67D06D06836EABEB519F',NULL,NULL,NULL,'T',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(57398687,NULL,NULL,NULL,'5A16FC3F8BF4F3A90ACF67D06D06836EABEB519F',NULL,NULL,NULL,'T',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(57398688,NULL,NULL,NULL,'5A16FC3F8BF4F3A90ACF67D06D06836EABEB519F',NULL,NULL,NULL,'T',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(57398689,NULL,NULL,NULL,'5A16FC3F8BF4F3A90ACF67D06D06836EABEB519F',NULL,NULL,NULL,'T',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(57398690,NULL,NULL,NULL,'5A16FC3F8BF4F3A90ACF67D06D06836EABEB519F',NULL,NULL,NULL,'T',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(57398691,'uigui','yig','gyuj','5A16FC3F8BF4F3A90ACF67D06D06836EABEB519F','gyg','d3',NULL,'T',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(57398692,'Ruko Kebayoran Arcade 2','nunuh.nurjaman@gmail.com','Nunuh','5A16FC3F8BF4F3A90ACF67D06D06836EABEB519F','0813802929302','PUS0001',NULL,'T',NULL,NULL,NULL,NULL,NULL,'2018-09-30 22:39:44',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(57398693,'Ruko Kebayoran Arcade 2','nunuh.nurjaman@gmail.com','Nunuh2','5A16FC3F8BF4F3A90ACF67D06D06836EABEB519F','0813802929302','PUS0002',NULL,'T',NULL,NULL,NULL,NULL,NULL,'2018-09-30 22:39:44',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(57398694,'gj','hjbh','vjhv','5A16FC3F8BF4F3A90ACF67D06D06836EABEB519F','hjbvb','jhbjh',NULL,'T',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(57398695,'Ruko Kebayoran Arcade 2','nunuh.nurjaman@gmail.com','Nunuh','5A16FC3F8BF4F3A90ACF67D06D06836EABEB519F','0813802929302','PUS0001',NULL,'T',NULL,NULL,NULL,NULL,NULL,'2018-09-30 22:39:44',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(57398696,'Ruko Kebayoran Arcade 2','nunuh.nurjaman@gmail.com','Nunuh2','5A16FC3F8BF4F3A90ACF67D06D06836EABEB519F','0813802929302','PUS0002',NULL,'T',NULL,NULL,NULL,NULL,NULL,'2018-09-30 22:39:44',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(57398697,'gj','hjbh','vjhv','5A16FC3F8BF4F3A90ACF67D06D06836EABEB519F','hjbvb','jhbjh',NULL,'T',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(57398698,'iugi','guig','g','5A16FC3F8BF4F3A90ACF67D06D06836EABEB519F','iugiu','iyygyyi',NULL,'T',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(57398699,'Ruko Kebayoran Arcade 2','nunuh.nurjaman@gmail.com','Nunuh','5A16FC3F8BF4F3A90ACF67D06D06836EABEB519F','0813802929302','PUS0001',NULL,'T',NULL,NULL,NULL,NULL,NULL,'2018-09-30 22:39:44',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(57398700,'Ruko Kebayoran Arcade 2','nunuh.nurjaman@gmail.com','Nunuh2','5A16FC3F8BF4F3A90ACF67D06D06836EABEB519F','0813802929302','PUS0002',NULL,'T',NULL,NULL,NULL,NULL,NULL,'2018-09-30 22:39:44',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(57398701,'gj','hjbh','vjhv','5A16FC3F8BF4F3A90ACF67D06D06836EABEB519F','hjbvb','jhbjh',NULL,'T',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(57398702,'iugi','guig','g2','5A16FC3F8BF4F3A90ACF67D06D06836EABEB519F','iugiu','iyygyyi',NULL,'T',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(57398703,'hj','kh','kjbhk','5A16FC3F8BF4F3A90ACF67D06D06836EABEB519F','bk','565465',NULL,'T',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(57398704,'Ruko Kebayoran Arcade 2','nunuh.nurjaman@gmail.com','Nunuh','5A16FC3F8BF4F3A90ACF67D06D06836EABEB519F','0813802929302','PUS0001',NULL,'T',NULL,NULL,NULL,NULL,NULL,'2018-09-30 22:39:44',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(57398705,'Ruko Kebayoran Arcade 2','nunuh.nurjaman@gmail.com','Nunuh2','5A16FC3F8BF4F3A90ACF67D06D06836EABEB519F','0813802929302','PUS0002',NULL,'T',NULL,NULL,NULL,NULL,NULL,'2018-09-30 22:39:44',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(57398706,'gj','hjbh','vjhv','5A16FC3F8BF4F3A90ACF67D06D06836EABEB519F','hjbvb','jhbjh',NULL,'T',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(57398707,'iugi','guig','g2','5A16FC3F8BF4F3A90ACF67D06D06836EABEB519F','iugiu','iyygyyi',NULL,'T',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(57398708,'uyv','vu','ygv','5A16FC3F8BF4F3A90ACF67D06D06836EABEB519F','gvbyu','vygvyj',NULL,'T',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(57398709,'Ruko Kebayoran Arcade 2','nunuh.nurjaman@gmail.com','Nunuh','5A16FC3F8BF4F3A90ACF67D06D06836EABEB519F','0813802929302','PUT9998',NULL,'T',NULL,NULL,NULL,NULL,NULL,'2018-10-09 23:39:23',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(57398710,'Ruko Kebayoran Arcade 2','nunuh.nurjaman@gmail.com','Nunuh','5A16FC3F8BF4F3A90ACF67D06D06836EABEB519F','0813802929302','PUT9999',NULL,'T',NULL,NULL,NULL,NULL,NULL,'2018-10-09 23:39:23',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(57398711,'Ruko Kebayoran Arcade 2','nunuh.nurjaman@gmail.com','Nunuh','5A16FC3F8BF4F3A90ACF67D06D06836EABEB519F','0813802929302','PUT9998',NULL,'T',NULL,NULL,NULL,NULL,NULL,'2018-10-09 23:43:45',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'padi'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-11-23 10:05:53
