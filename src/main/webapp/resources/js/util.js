function setWidth(){
	var browserwidth=jQuery(window).width();
	var percent25=Math.ceil(browserwidth/(100/25));
			
	//set new widths
	layoutVar.getManager().sizePane('west', percent25);
	layoutVar.getManager().sizePane('east', percent25);

	//resize all panes
	layoutVar.getManager().resizeAll();
}

$(window).load(function(){
//Opera 8.0+
var isOpera = (!!window.opr && !!opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;
// Firefox 1.0+
var isFirefox = typeof InstallTrigger !== 'undefined';
// Safari 3.0+ "[object HTMLElementConstructor]" 
var isSafari = /constructor/i.test(window.HTMLElement) || (function (p) { return p.toString() === "[object SafariRemoteNotification]"; })(!window['safari'] || (typeof safari !== 'undefined' && safari.pushNotification));
// Internet Explorer 6-11
var isIE = /*@cc_on!@*/false || !!document.documentMode;
// Edge 20+
var isEdge = !isIE && !!window.StyleMedia;
// Chrome 1+
var isChrome = !!window.chrome && !!window.chrome.webstore;
// Blink engine detection
var isBlink = (isChrome || isOpera) && !!window.CSS;

var widthMarginForm;
if(isChrome){
	$('.centeredPanel').css('width', 470);
}
	centerForm();
});

$(window).resize(function(){
	centerForm();
});

function centerForm(){
	var heightMargin =  -($('.centeredPanel').height()*55/100);
	var widthMargin=  -($('.centeredPanel').width()/2);
	$('.centeredPanel').css('margin-left',widthMargin);
	$('.centeredPanel').css('margin-top',heightMargin);
	
	var heightMarginForm =  -($('.centeredForm').height()/2);
	var widthMarginForm =  -($('.centeredForm').width()/2);
	$('.centeredForm').css('margin-left',widthMarginForm);
	$('.centeredForm').css('margin-top',heightMarginForm);
}