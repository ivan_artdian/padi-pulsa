function setWidth(){
	var browserwidth=jQuery(window).width();
	var percent25=Math.ceil(browserwidth/(100/25));
			
	// set new widths
	layoutVar.getManager().sizePane('west', percent25);
	layoutVar.getManager().sizePane('east', percent25);

	// resize all panes
	layoutVar.getManager().resizeAll();
}

$(document).ready(function(){
	$(document).ready(function(){
		var browserwidth=jQuery(window).width();
		
		var westWidth = Math.ceil(browserwidth/(100/12));
		var eastLeft = westWidth + 15;
		var eastWidth = browserwidth - eastLeft - 25;
		
		$('.westLayout').css('width',westWidth);
		$('.eastLayout').css('width',eastWidth);
		$('.eastLayout').css('left',eastLeft);
		});
});

$(window).resize(function(){
var browserwidth=jQuery(window).width();

var westWidth = Math.ceil(browserwidth/(100/12));
var eastLeft = westWidth + 15;
var eastWidth = browserwidth - eastLeft - 25;

$('.westLayout').css('width',westWidth);
$('.eastLayout').css('width',eastWidth);
$('.eastLayout').css('left',eastLeft);
});