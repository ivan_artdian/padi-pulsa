package corp.sTech.padi.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

@Entity
@Table(name="t_ibs_bill")
@NamedEntityGraph(name = "TIbsBill.billDetails", attributeNodes = @NamedAttributeNode("billDetails"))
public class TIbsBill implements Serializable {
	private static final long serialVersionUID = 7254360698753939132L;
	@Id
	@Column(name = "bill_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long billId;
	@Column
	private String type;
	@Column
	private String filename;
	@Column
	private boolean valid;
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "agent_id")
	private Agent agent;
	
	@OneToMany(mappedBy = "ibsBill", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
	@OrderBy("createDate DESC")
	private List<TIbsBillDetail> billDetails;
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	public boolean isValid() {
		return valid;
	}
	public void setValid(boolean valid) {
		this.valid = valid;
	}
	public Agent getAgent() {
		return agent;
	}
	public void setAgent(Agent agent) {
		this.agent = agent;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public long getBillId() {
		return billId;
	}
	public void setBillId(long billId) {
		this.billId = billId;
	}
	public List<TIbsBillDetail> getBillDetails() {
		return billDetails;
	}
	public void setBillDetails(List<TIbsBillDetail> billDetails) {
		this.billDetails = billDetails;
	}
}

