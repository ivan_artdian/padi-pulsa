package corp.sTech.padi.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="`menu`")
public class Menu implements Serializable {
	private static final long serialVersionUID = 6793896786924254178L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "menu_id")
	private long menuId;
	@Column
	private String value;
	@Column
	private String url;
	@Column(name = "menu_level")
	private int menuLevel;
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "group_id")
	private GroupMenu groupMenu;

	@OneToMany(mappedBy = "menu", fetch = FetchType.LAZY, cascade = { CascadeType.DETACH, CascadeType.MERGE,
			CascadeType.PERSIST, CascadeType.REFRESH })
	private List<RoleMenu> roleMenus = new ArrayList<RoleMenu>();

	@Transient
	private String urlApproval;

	@Transient
	private String title;

	public long getMenuId() {
		return menuId;
	}

	public void setMenuId(long menuId) {
		this.menuId = menuId;
	}

	public int getMenuLevel() {
		return menuLevel;
	}

	public void setMenuLevel(int menuLevel) {
		this.menuLevel = menuLevel;
	}

	public List<RoleMenu> getRoleMenus() {
		return roleMenus;
	}

	public void setRoleMenus(List<RoleMenu> roleMenus) {
		this.roleMenus = roleMenus;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public boolean equals(Object object) {
		return (object instanceof Menu) && (menuId > -1) ? menuId == ((Menu) object).menuId : (object == this);
	}

	@Override
	public int hashCode() {
		return (menuId > -1) ? (Menu.class.hashCode() + Long.valueOf(menuId).hashCode()) : super.hashCode();
	}

	public GroupMenu getGroupMenu() {
		return groupMenu;
	}

	public void setGroupMenu(GroupMenu groupMenu) {
		this.groupMenu = groupMenu;
	}

	public String getUrlApproval() {
		return url.replace(".xhtml", "-approval.xhtml");
	}

	public void setUrlApproval(String urlApproval) {
		this.urlApproval = urlApproval;
	}

	public String getTitle() {
		return url.replace("/pages/", "").replace(".xhtml", "");
	}
}
