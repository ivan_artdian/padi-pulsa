package corp.sTech.padi.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="`discount`")
public class Discount implements Serializable {
	private static final long serialVersionUID = 1559895780901281036L;
	@Id
	@Column
	private String idDiscount;
	@Column
	private String type;
	@Column
	private String criteria;
	@Column
	private int value;
	@Column
	private int status;
	@Column
	private int quota;
	@Column
	private String tnc;
	@Column
	private String description;

	public String getIdDiscount() {
		return idDiscount;
	}

	public void setIdDiscount(String idDiscount) {
		this.idDiscount = idDiscount;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCriteria() {
		return criteria;
	}

	public void setCriteria(String criteria) {
		this.criteria = criteria;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int getQuota() {
		return quota;
	}

	public void setQuota(int quota) {
		this.quota = quota;
	}

	public String getTnc() {
		return tnc;
	}

	public void setTnc(String tnc) {
		this.tnc = tnc;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idDiscount == null) ? 0 : idDiscount.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Discount other = (Discount) obj;
		if (idDiscount == null) {
			if (other.idDiscount != null)
				return false;
		} else if (!idDiscount.equals(other.idDiscount))
			return false;
		return true;
	}


	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
