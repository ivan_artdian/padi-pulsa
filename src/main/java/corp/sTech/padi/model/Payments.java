package corp.sTech.padi.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="payments")
public class Payments implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="payment_id")
	private int paymentId;

	@Column(name="admin_fee_total", precision=19, scale=2)
	private BigDecimal adminFeeTotal;

	@Column(name="book_balance_total", precision=19, scale=2)
	private BigDecimal bookBalanceTotal;

	@Column(name="discount_extra_total", precision=19, scale=2)
	private BigDecimal discountExtraTotal;

	@Column(name="discount_total", precision=19, scale=2)
	private BigDecimal discountTotal;
	
	@Column(name="nta_price_total", precision=19, scale=2)
	private BigDecimal ntaPriceTotal;

	@Column(name="extra_fee_total", precision=19, scale=2)
	private BigDecimal extraFeeTotal;

	@Column(name="normal_price_total", precision=19, scale=2)
	private BigDecimal normalPriceTotal;

	@Column(name="payment_amount", precision=19, scale=2)
	private BigDecimal paymentAmount;

	@Column(name="Payment3party_charge_tot", precision=19, scale=2)
	private BigDecimal payment3party_charge_tot;

	@Column(name="service_fee_total", precision=19, scale=2)
	private BigDecimal serviceFeeTotal;

	@Column(name="tot_disc_payment_channel", precision=19, scale=2)
	private BigDecimal totDiscPaymentChannel;

	@Column(name="tot_disc_point", precision=19, scale=2)
	private BigDecimal totDiscPoint;

	@Column(precision=19, scale=2)
	private BigDecimal tot_disc_Product;

	@Column(name="voucher_value", precision=19, scale=2)
	private BigDecimal voucherValue;

	@ManyToOne
	@JoinColumn(name="trn_reservation_id")
	private TrnReservation trnReservation;

	public BigDecimal getAdminFeeTotal() {
		return adminFeeTotal;
	}

	public void setAdminFeeTotal(BigDecimal adminFeeTotal) {
		this.adminFeeTotal = adminFeeTotal;
	}

	public BigDecimal getBookBalanceTotal() {
		return bookBalanceTotal;
	}

	public void setBookBalanceTotal(BigDecimal bookBalanceTotal) {
		this.bookBalanceTotal = bookBalanceTotal;
	}

	public BigDecimal getDiscountExtraTotal() {
		return discountExtraTotal;
	}

	public void setDiscountExtraTotal(BigDecimal discountExtraTotal) {
		this.discountExtraTotal = discountExtraTotal;
	}

	public BigDecimal getDiscountTotal() {
		return discountTotal;
	}

	public void setDiscountTotal(BigDecimal discountTotal) {
		this.discountTotal = discountTotal;
	}

	public BigDecimal getNtaPriceTotal() {
		return ntaPriceTotal;
	}

	public void setNtaPriceTotal(BigDecimal ntaPriceTotal) {
		this.ntaPriceTotal = ntaPriceTotal;
	}

	public BigDecimal getExtraFeeTotal() {
		return extraFeeTotal;
	}

	public void setExtraFeeTotal(BigDecimal extraFeeTotal) {
		this.extraFeeTotal = extraFeeTotal;
	}

	public BigDecimal getNormalPriceTotal() {
		return normalPriceTotal;
	}

	public void setNormalPriceTotal(BigDecimal normalPriceTotal) {
		this.normalPriceTotal = normalPriceTotal;
	}

	public BigDecimal getPaymentAmount() {
		return paymentAmount;
	}

	public void setPaymentAmount(BigDecimal paymentAmount) {
		this.paymentAmount = paymentAmount;
	}

	public BigDecimal getPayment3party_charge_tot() {
		return payment3party_charge_tot;
	}

	public void setPayment3party_charge_tot(BigDecimal payment3party_charge_tot) {
		this.payment3party_charge_tot = payment3party_charge_tot;
	}

	public BigDecimal getServiceFeeTotal() {
		return serviceFeeTotal;
	}

	public void setServiceFeeTotal(BigDecimal serviceFeeTotal) {
		this.serviceFeeTotal = serviceFeeTotal;
	}

	public BigDecimal getTotDiscPaymentChannel() {
		return totDiscPaymentChannel;
	}

	public void setTotDiscPaymentChannel(BigDecimal totDiscPaymentChannel) {
		this.totDiscPaymentChannel = totDiscPaymentChannel;
	}

	public BigDecimal getTotDiscPoint() {
		return totDiscPoint;
	}

	public void setTotDiscPoint(BigDecimal totDiscPoint) {
		this.totDiscPoint = totDiscPoint;
	}

	public BigDecimal getTot_disc_Product() {
		return tot_disc_Product;
	}

	public void setTot_disc_Product(BigDecimal tot_disc_Product) {
		this.tot_disc_Product = tot_disc_Product;
	}

	public BigDecimal getVoucherValue() {
		return voucherValue;
	}

	public void setVoucherValue(BigDecimal voucherValue) {
		this.voucherValue = voucherValue;
	}

	public TrnReservation getTrnReservation() {
		return trnReservation;
	}

	public void setTrnReservation(TrnReservation trnReservation) {
		this.trnReservation = trnReservation;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public void setPaymentId(int paymentId) {
		this.paymentId = paymentId;
	}
	
	
}