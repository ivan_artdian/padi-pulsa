package corp.sTech.padi.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.data.domain.Persistable;

@Entity
@Table(name="t_ibs_billdetails")
public class TIbsBillDetail implements Serializable, Persistable{
	private static final long serialVersionUID = 7254360698753939132L;
	@Id
	@Column(name = "bills_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long billsId;
	@ManyToOne
	@JoinColumn(name = "bill_id")
	private TIbsBill ibsBill;
	@Column(name="id_pelanggan")
	private String idPelanggan;
	@Column(name="bln_tagihan")
	private String blnTagihan;
	@Column(name="thn_tagihan")
	private String thnTagihan;
	@Column(name="sts_tagihan")
	private String stsTagihan;
	@Column(name="nama_pelanggan")
	private String namaPelanggan;
	@Column(name="no_hp")
	private String noHP;
	@Column
	private String email;
	@Column(name="nama_perumahan")
	private String namaPerumahan;
	@Column
	private String alamat;
	@Column(name="no_block")
	private String noBlock;
	@Column(name="kel_kec")
	private String kelKec;
	@Column(name="kab_kota")
	private String kabKota;
	@Column(name="provinsi")
	private String provinsi;
	@Column(name="luas_tanah")
	private String luasTanah;
	/*@Column
	private String amount;*/
	@Column(name="create_date")
	private Date createDate;
	
	@Column
	private boolean active;
	
	@Column(name="rtrw")
	private String rtrw;
	
	public long getBillsId() {
		return billsId;
	}
	public void setBillsId(long billsId) {
		this.billsId = billsId;
	}
	public TIbsBill getIbsBill() {
		return ibsBill;
	}
	public void setIbsBill(TIbsBill ibsBill) {
		this.ibsBill = ibsBill;
	}
	public String getIdPelanggan() {
		return idPelanggan;
	}
	public void setIdPelanggan(String idPelanggan) {
		this.idPelanggan = idPelanggan;
	}
	public String getBlnTagihan() {
		return blnTagihan;
	}
	public void setBlnTagihan(String blnTagihan) {
		this.blnTagihan = blnTagihan;
	}
	public String getThnTagihan() {
		return thnTagihan;
	}
	public void setThnTagihan(String thnTagihan) {
		this.thnTagihan = thnTagihan;
	}
	public String getStsTagihan() {
		return stsTagihan;
	}
	public void setStsTagihan(String stsTagihan) {
		this.stsTagihan = stsTagihan;
	}
	public String getNamaPelanggan() {
		return namaPelanggan;
	}
	public void setNamaPelanggan(String namaPelanggan) {
		this.namaPelanggan = namaPelanggan;
	}
	public String getNoHP() {
		return noHP;
	}
	public void setNoHP(String noHP) {
		this.noHP = noHP;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getNamaPerumahan() {
		return namaPerumahan;
	}
	public void setNamaPerumahan(String namaPerumahan) {
		this.namaPerumahan = namaPerumahan;
	}
	public String getAlamat() {
		return alamat;
	}
	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}
	public String getNoBlock() {
		return noBlock;
	}
	public void setNoBlock(String noBlock) {
		this.noBlock = noBlock;
	}
	public String getKelKec() {
		return kelKec;
	}
	public void setKelKec(String kelKec) {
		this.kelKec = kelKec;
	}
	public String getKabKota() {
		return kabKota;
	}
	public void setKabKota(String kabKota) {
		this.kabKota = kabKota;
	}
	public String getProvinsi() {
		return provinsi;
	}
	public void setProvinsi(String provinsi) {
		this.provinsi = provinsi;
	}
	/*public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}*/
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public String getLuasTanah() {
		return luasTanah;
	}
	public void setLuasTanah(String luasTanah) {
		this.luasTanah = luasTanah;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	@Override
	public Serializable getId() {
		return null;
	}
	@Override
	public boolean isNew() {
		return false;
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	public String getRtrw() {
		return rtrw;
	}
	public void setRtrw(String rtrw) {
		this.rtrw = rtrw;
	}

}

