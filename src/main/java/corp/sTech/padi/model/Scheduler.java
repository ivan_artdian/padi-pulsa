package corp.sTech.padi.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="scheduler")
public class Scheduler {

    @Id
    @Column(name="scheduler_id")
    private String schedulerID;

    @Column(name="svc_name")
    private String svcName;

    @Column(name="svc_executions")
    private String svcExecutions;

    @Column(name="next_fire")
    private String nextFire;

    private String description;

    private String period;

    @Column(name="agent_id")
    private String agentId;

    public String getSchedulerID() {
        return schedulerID;
    }

    public void setSchedulerID(String schedulerID) {
        this.schedulerID = schedulerID;
    }

    public String getSvcName() {
        return svcName;
    }

    public void setSvcName(String svcName) {
        this.svcName = svcName;
    }

    public String getSvcExecutions() {
        return svcExecutions;
    }

    public void setSvcExecutions(String svcExecutions) {
        this.svcExecutions = svcExecutions;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public String getNextFire() {
        return nextFire;
    }

    public void setNextFire(String nextFire) {
        this.nextFire = nextFire;
    }
}
