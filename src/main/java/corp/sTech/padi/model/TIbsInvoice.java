package corp.sTech.padi.model;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.transaction.Transactional;

@Entity
@Table(name="t_ibs_invoice")
public class TIbsInvoice implements Serializable {
	private static final long serialVersionUID = 8362356552168986116L;
	@Id
	@Column(name = "bill_invoice_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long billId;
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "bills_id")
	private TIbsBillDetail ibsBillDetail;
	@Column(name="bill_periode")
	private String billPeriode;
	@Column(name="paid_status")
	private String paidStatus;
	@Column(name="paid_date")
	private Date paidDate;
	@Transient
	private String paidDateWeb;
	@Column
	private String amount;
	@Column
	private boolean valid;
	@Column(name="create_date")
	private Date createDate;
	@Column
	private String invoiceCode;
	
	@Column
	private boolean denda;
	public long getBillId() {
		return billId;
	}
	public void setBillId(long billId) {
		this.billId = billId;
	}
	public String getBillPeriode() {
		return billPeriode;
	}
	public void setBillPeriode(String billPeriode) {
		this.billPeriode = billPeriode;
	}
	public String getPaidStatus() {
		return paidStatus;
	}
	public void setPaidStatus(String paidStatus) {
		this.paidStatus = paidStatus;
	}
	public Date getPaidDate() {
		return paidDate;
	}
	public void setPaidDate(Date paidDate) {
		this.paidDate = paidDate;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public boolean isValid() {
		return valid;
	}
	public void setValid(boolean valid) {
		this.valid = valid;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public TIbsBillDetail getIbsBillDetail() {
		return ibsBillDetail;
	}
	public void setIbsBillDetail(TIbsBillDetail ibsBillDetail) {
		this.ibsBillDetail = ibsBillDetail;
	}
	public String getPaidDateWeb() {
		if(paidDate != null) {
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			return sdf.format(paidDate);
		}
		return null;
	}
	
	public void setPaidDateWeb(String paidDateWeb) {
		if(paidDate != null) {
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			this.paidDateWeb = sdf.format(paidDate);
		}
	}
	public boolean isDenda() {
		return denda;
	}
	public void setDenda(boolean denda) {
		this.denda = denda;
	}
	public String getInvoiceCode() {
		return invoiceCode;
	}
	public void setInvoiceCode(String invoiceCode) {
		this.invoiceCode = invoiceCode;
	}
	
	
}

