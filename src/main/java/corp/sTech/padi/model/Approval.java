package corp.sTech.padi.model;

import java.io.Serializable;
import java.util.Date;

import corp.sTech.padi.util.DateUtil;
import corp.sTech.padi.util.SessionMapUtil;

public class Approval implements Serializable {
	private static final long serialVersionUID = 2547140383311476405L;

	private long id;
	private byte[] oldObject;
	private byte[] newObject;
	private String submitter;
	private String approver;
	private Date submitDate;
	private Date approveDate;
	private String taskName;
	private String groupId;
	private String modelName;
	private String urlApproval;
	private String status;

	public Approval() {

	}

	public Approval(String taskName, Class<?> modelClass, String groupId,
			String urlApproval) {
		this.taskName = taskName;
		this.modelName = modelClass.getCanonicalName();
		this.groupId = groupId;
		this.urlApproval = urlApproval;
		this.submitter = SessionMapUtil.getUserSession().getUsername();

		this.submitDate = DateUtil.getCurrentDateZoned();
	}

	public Approval(String taskName, Class<?> modelClass, String groupId, String urlApproval, String modelName) {
		this.taskName = taskName;
		this.modelName = modelClass.getCanonicalName() + modelName;
		this.groupId = groupId;
		this.urlApproval = urlApproval;
		this.submitter = SessionMapUtil.getUserSession().getUsername();

		this.submitDate = DateUtil.getCurrentDateZoned();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public byte[] getOldObject() {
		return oldObject;
	}

	public void setOldObject(byte[] oldObject) {
		this.oldObject = oldObject;
	}

	public byte[] getNewObject() {
		return newObject;
	}

	public void setNewObject(byte[] newObject) {
		this.newObject = newObject;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getSubmitter() {
		return submitter;
	}

	public void setSubmitter(String submitter) {
		this.submitter = submitter;
	}

	public String getApprover() {
		return approver;
	}

	public void setApprover(String approver) {
		this.approver = approver;
	}

	public String getTaskName() {
		return taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public String getModelName() {
		return modelName;
	}

	public void setModelName(String modelName) {
		this.modelName = modelName;
	}

	public String getUrlApproval() {
		return urlApproval;
	}

	public void setUrlApproval(String urlApproval) {
		this.urlApproval = urlApproval;
	}

	public Date getSubmitDate() {
		return submitDate;
	}

	public void setSubmitDate(Date submitDate) {
		this.submitDate = submitDate;
	}

	public Date getApproveDate() {
		return approveDate;
	}

	public void setApproveDate(Date approveDate) {
		this.approveDate = approveDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
