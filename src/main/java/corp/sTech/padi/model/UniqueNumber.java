package corp.sTech.padi.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="unique_number")
public class UniqueNumber implements Serializable{
	private static final long serialVersionUID = 2034603076505163341L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="unique_number_id")
	private Long uniqueNumberId;
	
	@Column(length=20)
	private String number;
	
	@Column(name="trn_date",length=8)
	private String trnDate;
	
	@Column(name="counter")
	private Integer counter;

	public Long getUniqueNumberId() {
		return uniqueNumberId;
	}

	public void setUniqueNumberId(Long uniqueNumberId) {
		this.uniqueNumberId = uniqueNumberId;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getTrnDate() {
		return trnDate;
	}

	public void setTrnDate(String trnDate) {
		this.trnDate = trnDate;
	}

	public Integer getCounter() {
		return counter;
	}

	public void setCounter(Integer counter) {
		this.counter = counter;
	}
	
	
}
