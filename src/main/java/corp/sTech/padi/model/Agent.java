package corp.sTech.padi.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="`agent`")
public class Agent implements Serializable {
	private static final long serialVersionUID = 1559895780901281036L;
	@Id
	@Column(name = "agent_id")
	private String agentId;
	@Column
	private String address;
	@Column
	private String email;
	@Column
	private String name;
	@Column(name = "phone_no")
	private String phoneNo;
	@Column
	private boolean status;
	@Column(name = "is_allow_duplicate_email")
	private boolean allowDuplicateEmail;

	@Transient
	private String agentIdWeb;

	public String getAgentId() {
		return agentId;
	}

	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public boolean isAllowDuplicateEmail() {
		return allowDuplicateEmail;
	}

	public void setAllowDuplicateEmail(boolean allowDuplicateEmail) {
		this.allowDuplicateEmail = allowDuplicateEmail;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((agentId == null) ? 0 : agentId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Agent other = (Agent) obj;
		if (agentId == null) {
			if (other.agentId != null)
				return false;
		} else if (!agentId.equals(other.agentId))
			return false;
		return true;
	}

	public String getAgentIdWeb() {
		if (agentId != null)
			return agentId.replace("AGENT_", "");
		return agentIdWeb;
	}

	public void setAgentIdWeb(String agentIdWeb) {
		this.agentIdWeb = agentIdWeb;
		agentId = "AGENT_" + agentIdWeb;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
