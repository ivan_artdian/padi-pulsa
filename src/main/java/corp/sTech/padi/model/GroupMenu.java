package corp.sTech.padi.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "group_menu")
public class GroupMenu implements Serializable{
	private static final long serialVersionUID = 6323809263749775542L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column
	private long id;
	@Column(name = "group_name")
	private String groupName;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	@Override
	public int hashCode() {
		return (id > -1) ? (GroupMenu.class.hashCode() + Long.valueOf(id).hashCode()) : super.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		return (obj instanceof GroupMenu) && (id > -1) ? id == ((GroupMenu) obj).id : (obj == this);
	}

}
