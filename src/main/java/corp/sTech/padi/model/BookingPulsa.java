package corp.sTech.padi.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import corp.sTech.padi.model.TrnReservation;

@Entity
@Table(name="booking_pulsa")
public class BookingPulsa implements Serializable{
	private static final long serialVersionUID = -4578717749652437872L;
	@Id
	private String booking_id;
	private String txn_state;
	private Date txn_date;
	private String user_id;
	private String user_email;
	private String txn_amount;
	private String mdn;
	private String product_id;
	private String product_name;
	private String operator;
	private String response_code;
	private String response_message;
	
	@ManyToOne
	@JoinColumn(name="trn_reservation_id")
	private TrnReservation trnReservation;
	public String getBooking_id() {
		return booking_id;
	}
	public void setBooking_id(String booking_id) {
		this.booking_id = booking_id;
	}
	public String getTxn_state() {
		return txn_state;
	}
	public void setTxn_state(String txn_state) {
		this.txn_state = txn_state;
	}
	public Date getTxn_date() {
		return txn_date;
	}
	public void setTxn_date(Date txn_date) {
		this.txn_date = txn_date;
	}
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public String getUser_email() {
		return user_email;
	}
	public void setUser_email(String user_email) {
		this.user_email = user_email;
	}
	public String getTxn_amount() {
		return txn_amount;
	}
	public void setTxn_amount(String txn_amount) {
		this.txn_amount = txn_amount;
	}
	public String getMdn() {
		return mdn;
	}
	public void setMdn(String mdn) {
		this.mdn = mdn;
	}
	public String getProduct_id() {
		return product_id;
	}
	public void setProduct_id(String product_id) {
		this.product_id = product_id;
	}
	public String getProduct_name() {
		return product_name;
	}
	public void setProduct_name(String product_name) {
		this.product_name = product_name;
	}
	public String getOperator() {
		return operator;
	}
	public void setOperator(String operator) {
		this.operator = operator;
	}
	public String getResponse_code() {
		return response_code;
	}
	public void setResponse_code(String response_code) {
		this.response_code = response_code;
	}
	public String getResponse_message() {
		return response_message;
	}
	public void setResponse_message(String response_message) {
		this.response_message = response_message;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public TrnReservation getTrnReservation() {
		return trnReservation;
	}
	public void setTrnReservation(TrnReservation trnReservation) {
		this.trnReservation = trnReservation;
	}
	
	
}
