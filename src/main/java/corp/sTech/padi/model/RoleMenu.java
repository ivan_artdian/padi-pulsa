package corp.sTech.padi.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "role_menu")
public class RoleMenu implements Serializable{
	private static final long serialVersionUID = 1854823385502690033L;

	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@JoinColumn(name = "role_id", referencedColumnName = "role_id")
	@ManyToOne
	private Role role;

	@JoinColumn(name = "menu_id", referencedColumnName = "menu_id")
	@ManyToOne
	private Menu menu;

	@Column(name = "need_approval")
	private boolean needApproval;

	public RoleMenu() {

	}

	public RoleMenu(Menu menu, boolean needApproval) {
		this.menu = menu;
		this.needApproval = needApproval;
	}

	public RoleMenu(long id) {
		this.id = id;
		this.needApproval = false;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public Menu getMenu() {
		return menu;
	}

	public void setMenu(Menu menu) {
		this.menu = menu;
	}

	public boolean isNeedApproval() {
		return needApproval;
	}

	public void setNeedApproval(boolean needApproval) {
		this.needApproval = needApproval;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

}
