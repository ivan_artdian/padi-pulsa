package corp.sTech.padi.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity	
@Table(name = "systemparameter")
public class SystemParameter implements Serializable { 
	private static final long serialVersionUID = 1335044879237426512L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "param_Id")
	private long paramId;
	@Column(name = "paramname")
	private String paramName;
	@Column(name = "paramvalue")
	private String paramValue;
	@Column(name = "description")
	private String description;
	@Column(name = "ispassword")
	private String isPassword;
	@Column(name = "createdate")
	private Date createDate;
	@Column(name = "modifydate")
	private Date modifyDate;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "agent_id")
	private Agent agent;
	
	public long getParamId() {
		return paramId;
	}
	public void setParamId(long paramId) {
		this.paramId = paramId;
	}
	public String getParamName() {
		return paramName;
	}
	public void setParamName(String paramName) {
		this.paramName = paramName;
	}
	public String getParamValue() {
		return paramValue;
	}
	public void setParamValue(String paramValue) {
		this.paramValue = paramValue;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getIsPassword() {
		return isPassword;
	}
	public void setIsPassword(String isPassword) {
		this.isPassword = isPassword;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public Date getModifyDate() {
		return modifyDate;
	}
	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public Agent getAgent() {
		return agent;
	}
	public void setAgent(Agent agent) {
		this.agent = agent;
	}

}
