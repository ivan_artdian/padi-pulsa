package corp.sTech.padi.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name="trn_reservation", uniqueConstraints={
	    @UniqueConstraint(columnNames = {"trn_reservation_id", "transaction_code"})
	})
public class TrnReservation implements Serializable{
	private static final long serialVersionUID = -6794311007314056644L;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="trn_reservation_id")
	private int trnReservationId;
	@Column(name="transaction_code")
	private String transactionCode;
	@Column(name="trn_status")
	private String trnStatus;
	@Column(name="user_id")
	private int userId;
	@Column(name="payment_limit_date")
	private Date paymentLimitDate;
	@Column(name="transaction_date")
	private Date transactionDate;
	@Column(name="booking_limit_date")
	private Date bookingLimitDate;
	@Column(name="reservation_channel")
	private String reservationChannel;
	@Column(name="reseller_id")
	private String resellerId;
	@Column(name="device_type")
	private String deviceType;
	
	public String getTransactionCode() {
		return transactionCode;
	}
	public void setTransactionCode(String transactionCode) {
		this.transactionCode = transactionCode;
	}
	public String getTrnStatus() {
		return trnStatus;
	}
	public void setTrnStatus(String trnStatus) {
		this.trnStatus = trnStatus;
	}
	
	public String getReservationChannel() {
		return reservationChannel;
	}
	public void setReservationChannel(String reservationChannel) {
		this.reservationChannel = reservationChannel;
	}
	public String getResellerId() {
		return resellerId;
	}
	public void setResellerId(String resellerId) {
		this.resellerId = resellerId;
	}
	public String getDeviceType() {
		return deviceType;
	}
	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public Date getPaymentLimitDate() {
		return paymentLimitDate;
	}
	public void setPaymentLimitDate(Date paymentLimitDate) {
		this.paymentLimitDate = paymentLimitDate;
	}
	public Date getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}
	public Date getBookingLimitDate() {
		return bookingLimitDate;
	}
	public void setBookingLimitDate(Date bookingLimitDate) {
		this.bookingLimitDate = bookingLimitDate;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public int getTrnReservationId() {
		return trnReservationId;
	}
	public void setTrnReservationId(int trnReservationId) {
		this.trnReservationId = trnReservationId;
	}
}
