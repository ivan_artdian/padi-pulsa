package corp.sTech.padi.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="biller_product")
public class BillerProduct implements Serializable{
	private static final long serialVersionUID = -4578717749652437872L;
	@Id
	private String productId;
	private String modifiedDate;
	private String description;
	private String operator;
	private String amount;
	private String rsPrice;
	private String status;
	private String hargajual;
	private String sourceBiller;
	
	public String getModifiedDate() {
		return modifiedDate;
	}
	public void setModifiedDate(String modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getOperator() {
		return operator;
	}
	public void setOperator(String operator) {
		this.operator = operator;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getRsPrice() {
		return rsPrice;
	}
	public void setRsPrice(String rsPrice) {
		this.rsPrice = rsPrice;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getHargajual() {
		return hargajual;
	}
	public void setHargajual(String hargajual) {
		this.hargajual = hargajual;
	}
	public String getSourceBiller() {
		return sourceBiller;
	}
	public void setSourceBiller(String sourceBiller) {
		this.sourceBiller = sourceBiller;
	}
	
}
