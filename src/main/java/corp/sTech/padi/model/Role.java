package corp.sTech.padi.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "`role`")
public class Role implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 7793813527124727639L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "role_id")
	private long roleId;
	@Column(name = "role_name")
	private String roleName;

	@OneToMany(mappedBy = "role", fetch = FetchType.LAZY)
	private List<User> users = new ArrayList<User>();

	@OneToMany(mappedBy = "role", fetch = FetchType.LAZY)
	private List<RoleMenu> roleMenus = new ArrayList<RoleMenu>();
	
	private boolean master;

	public long getRoleId() {
		return roleId;
	}

	public void setRoleId(long roleId) {
		this.roleId = roleId;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public List<RoleMenu> getRoleMenus() {
		return roleMenus;
	}

	public void setRoleMenus(List<RoleMenu> roleMenus) {
		this.roleMenus = roleMenus;
	}

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public boolean equals(Object object) {
		return (object instanceof Role) && (roleId > -1) ? roleId == ((Role) object).roleId : (object == this);
	}

	@Override
	public int hashCode() {
		return (roleId > -1) ? (Role.class.hashCode() + Long.valueOf(roleId).hashCode()) : super.hashCode();
	}

	public boolean isMaster() {
		return master;
	}

	public void setMaster(boolean master) {
		this.master = master;
	}
}
