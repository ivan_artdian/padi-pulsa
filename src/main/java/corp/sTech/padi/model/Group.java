package corp.sTech.padi.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "`group`")
public class Group implements Serializable{
	private static final long serialVersionUID = 114253972141455247L;
	@Transient
	private String backgroundUrl;
	@Id
	@Column
	private String username;
	@Column
	private String password;

	/*@OneToMany(mappedBy = "group", fetch = FetchType.EAGER)
	private List<GroupPreference> groupPreferences = new ArrayList<>();*/

	@ManyToMany(mappedBy = "groups", fetch = FetchType.EAGER)
	private List<User> users = new ArrayList<User>();

	@Column
	private String imageHeader;
	@Column
	private boolean status;

	@PreRemove
	private void preDelete() {
		for (User user : users) {
			user.getGroups().remove(this);
		}
	}

	public String getBackgroundUrl() {
		return backgroundUrl;
	}

	public void setBackgroundUrl(String backgroundUrl) {
		this.backgroundUrl = backgroundUrl;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}



	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public int hashCode() {
		return (username != null) ? (Group.class.hashCode() + username.hashCode()) : super.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		return (obj instanceof Group) && (username != null) ? username.equals(((Group) obj).username) : (obj == this);
	}

	/*public List<GroupPreference> getGroupPreferences() {
		return groupPreferences;
	}
	
	public void setGroupPreferences(List<GroupPreference> groupPreferences) {
		this.groupPreferences = groupPreferences;
	}*/

	public String getImageHeader() {
		return imageHeader;
	}

	public void setImageHeader(String imageHeader) {
		this.imageHeader = imageHeader;
	}

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}
}
