package corp.sTech.padi.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the booking_ipl database table.
 * 
 */
@Entity
@Table(name="booking_ipl")
@NamedQuery(name="BookingIpl.findAll", query="SELECT b FROM BookingIpl b")
public class BookingIpl implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="booking_id")
	private String bookingId;

	@ManyToOne
	@JoinColumn(name="ibs_bill_detail")
	private TIbsBillDetail TIbsBilldetail;

	private String denda;

	@Column(name="id_pelanggan")
	private String idPelanggan;

	@Column(name="multi_payment")
	private String multiPayment;

	@Column(name="response_code")
	private String responseCode;

	@Column(name="response_message")
	private String responseMessage;

	@Column(name="txn_amount")
	private String txnAmount;

	@Temporal(TemporalType.DATE)
	@Column(name="txn_date")
	private Date txnDate;

	@Column(name="txn_state")
	private String txnState;

	@Column(name="user_email")
	private String userEmail;

	@Column(name="user_id")
	private String userId;

	//bi-directional many-to-one association to Agent
	@ManyToOne
	@JoinColumn(name="agent_id")
	private Agent agent;

	//bi-directional many-to-one association to TrnReservation
	@ManyToOne
	@JoinColumn(name="trn_reservation_id")
	private TrnReservation trnReservation;
	
	@Column
	private String reference1;
	@Column
	private String paymentCodeRefId;
	@Column
	private String paymentChannel;

	public BookingIpl() {
	}

	public String getBookingId() {
		return this.bookingId;
	}

	public void setBookingId(String bookingId) {
		this.bookingId = bookingId;
	}

	public String getDenda() {
		return this.denda;
	}

	public void setDenda(String denda) {
		this.denda = denda;
	}

	public String getIdPelanggan() {
		return this.idPelanggan;
	}

	public void setIdPelanggan(String idPelanggan) {
		this.idPelanggan = idPelanggan;
	}

	public String getMultiPayment() {
		return this.multiPayment;
	}

	public void setMultiPayment(String multiPayment) {
		this.multiPayment = multiPayment;
	}

	public String getResponseCode() {
		return this.responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getResponseMessage() {
		return this.responseMessage;
	}

	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}

	public String getTxnAmount() {
		return this.txnAmount;
	}

	public void setTxnAmount(String txnAmount) {
		this.txnAmount = txnAmount;
	}

	public Date getTxnDate() {
		return this.txnDate;
	}

	public void setTxnDate(Date txnDate) {
		this.txnDate = txnDate;
	}

	public String getTxnState() {
		return this.txnState;
	}

	public void setTxnState(String txnState) {
		this.txnState = txnState;
	}

	public String getUserEmail() {
		return this.userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public String getUserId() {
		return this.userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Agent getAgent() {
		return this.agent;
	}

	public void setAgent(Agent agent) {
		this.agent = agent;
	}

	public TrnReservation getTrnReservation() {
		return this.trnReservation;
	}

	public void setTrnReservation(TrnReservation trnReservation) {
		this.trnReservation = trnReservation;
	}

	public TIbsBillDetail getTIbsBilldetail() {
		return TIbsBilldetail;
	}

	public void setTIbsBilldetail(TIbsBillDetail tIbsBilldetail) {
		TIbsBilldetail = tIbsBilldetail;
	}

	public String getReference1() {
		return reference1;
	}

	public void setReference1(String reference1) {
		this.reference1 = reference1;
	}

	public String getPaymentCodeRefId() {
		return paymentCodeRefId;
	}

	public void setPaymentCodeRefId(String paymentCodeRefId) {
		this.paymentCodeRefId = paymentCodeRefId;
	}

	public String getPaymentChannel() {
		return paymentChannel;
	}

	public void setPaymentChannel(String paymentChannel) {
		this.paymentChannel = paymentChannel;
	}

}