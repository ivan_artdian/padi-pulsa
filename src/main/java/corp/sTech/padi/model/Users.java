package corp.sTech.padi.model;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Time;
import java.util.Date;
import java.math.BigInteger;


@Entity
@Table(name="`users`")
public class Users implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="user_id")
	private BigInteger userId;

	@Column(name="account_bank")
	private String accountBank;

	@Column(name="account_deposit_num")
	private String accountDepositNum;

	@Column(name="account_holder_name")
	private String accountHolderName;

	private String address;

	private String address2;

	@Column(name="app_type")
	private String appType;

	@Column(name="bank_name")
	private String bankName;

	@Column(name="branch_office")
	private String branchOffice;

	@Column(name="check_term_condition")
	private String checkTermCondition;

	@Column(name="city_id")
	private BigInteger cityId;

	@Column(name="code_activation")
	private String codeActivation;

	private String community;

	@Column(name="count_pin")
	private String countPin;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="create_date")
	private Date createDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="create_date_member")
	private Date createDateMember;

	@Temporal(TemporalType.DATE)
	@Column(name="created_date")
	private Date createdDate;

	@Column(name="created_time")
	private Time createdTime;

	@Column(name="deposit_member")
	private String depositMember;

	@Column(name="desc_email_sent")
	private String descEmailSent;

	@Column(name="desc_sms_sent")
	private String descSmsSent;

	@Column(name="device_registered")
	private String deviceRegistered;

	@Column(name="device_registered_member")
	private String deviceRegisteredMember;

	@Column(name="device_type")
	private String deviceType;

	private String email;

	@Column(name="first_name")
	private String firstName;

	@Column(name="full_name")
	private String fullName;

	private String gender;

	@Column(name="group_id")
	private String groupId;

	private String heir;

	@Column(name="home_phone")
	private String homePhone;

	@Column(name="identity_card_num")
	private String identityCardNum;

	@Column(name="ip_last_used")
	private String ipLastUsed;

	@Column(name="ip_registered")
	private String ipRegistered;

	@Column(name="ip_registered_member")
	private String ipRegisteredMember;

	@Column(name="is_active")
	private String isActive;

	@Column(name="is_email_notif")
	private String isEmailNotif;

	@Column(name="is_locked")
	private String isLocked;

	@Column(name="is_member")
	private String isMember;

	@Column(name="is_new_reg")
	private String isNewReg;

	@Column(name="is_user_facebook")
	private String isUserFacebook;

	@Column(name="is_user_twitter")
	private String isUserTwitter;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="last_date_login")
	private Date lastDateLogin;

	@Column(name="last_name")
	private String lastName;

	@Column(name="member_id")
	private String memberId;

	@Column(name="merchant_code")
	private String merchantCode;

	@Column(name="mobile_phone")
	private String mobilePhone;

	@Column(name="mother_maiden_name")
	private String motherMaidenName;

	private String notification;

	private String occupation;

	@Column(name="parent_user_name")
	private String parentUserName;

	private String password;

	@Column(name="phone_number")
	private String phoneNumber;

	@Column(name="phone_number_2")
	private String phoneNumber2;

	@Lob
	@Column(name="photo_url")
	private String photoUrl;

	@Column(name="pin_user")
	private String pinUser;

	@Column(name="place_of_birth")
	private String placeOfBirth;

	@Column(name="post_code")
	private String postCode;

	@Column(name="province_id")
	private BigInteger provinceId;

	private String religion;

	@Column(name="reseller_id")
	private BigInteger resellerId;

	@Column(name="retry_send_mail")
	private String retrySendMail;

	@Column(name="reward_point_sts")
	private String rewardPointSts;

	@Column(name="security_answer")
	private String securityAnswer;

	@Column(name="security_question")
	private String securityQuestion;

	@Column(name="status_pin_deposit")
	private String statusPinDeposit;

	@Column(name="sts_email_actived")
	private String stsEmailActived;

	@Column(name="sts_email_blast_padiair")
	private String stsEmailBlastPadiair;

	@Column(name="sts_email_blast_paditrain")
	private String stsEmailBlastPaditrain;

	@Column(name="sts_email_info")
	private String stsEmailInfo;

	@Column(name="sts_email_info_padiair")
	private String stsEmailInfoPadiair;

	@Column(name="sts_email_reset_pass")
	private String stsEmailResetPass;

	@Column(name="sts_sms_activated")
	private String stsSmsActivated;

	@Column(name="sts_sms_sent")
	private String stsSmsSent;

	@Column(name="sub_agent_id")
	private BigInteger subAgentId;

	@Column(name="temp_password")
	private String tempPassword;

	@Column(name="temp_token_member")
	private String tempTokenMember;

	private String title;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="token_member_expired")
	private Date tokenMemberExpired;

	@Column(name="type_mobile_payment")
	private String typeMobilePayment;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="update_date")
	private Date updateDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="update_date_member")
	private Date updateDateMember;

	@Column(name="update_time")
	private Time updateTime;

	@Column(name="use_padiair")
	private String usePadiair;

	@Column(name="use_padipay")
	private String usePadipay;

	@Column(name="use_paditrain")
	private String usePaditrain;

	@Temporal(TemporalType.DATE)
	@Column(name="user_birthdate")
	private Date userBirthdate;

	@Column(name="user_name")
	private String userName;

	@Column(name="yahoo_id")
	private String yahooId;

	public Users() {
	}

	public String getAccountBank() {
		return this.accountBank;
	}

	public void setAccountBank(String accountBank) {
		this.accountBank = accountBank;
	}

	public String getAccountDepositNum() {
		return this.accountDepositNum;
	}

	public void setAccountDepositNum(String accountDepositNum) {
		this.accountDepositNum = accountDepositNum;
	}

	public String getAccountHolderName() {
		return this.accountHolderName;
	}

	public void setAccountHolderName(String accountHolderName) {
		this.accountHolderName = accountHolderName;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getAddress2() {
		return this.address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getAppType() {
		return this.appType;
	}

	public void setAppType(String appType) {
		this.appType = appType;
	}

	public String getBankName() {
		return this.bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getBranchOffice() {
		return this.branchOffice;
	}

	public void setBranchOffice(String branchOffice) {
		this.branchOffice = branchOffice;
	}

	public String getCheckTermCondition() {
		return this.checkTermCondition;
	}

	public void setCheckTermCondition(String checkTermCondition) {
		this.checkTermCondition = checkTermCondition;
	}

	public BigInteger getCityId() {
		return this.cityId;
	}

	public void setCityId(BigInteger cityId) {
		this.cityId = cityId;
	}

	public String getCodeActivation() {
		return this.codeActivation;
	}

	public void setCodeActivation(String codeActivation) {
		this.codeActivation = codeActivation;
	}

	public String getCommunity() {
		return this.community;
	}

	public void setCommunity(String community) {
		this.community = community;
	}

	public String getCountPin() {
		return this.countPin;
	}

	public void setCountPin(String countPin) {
		this.countPin = countPin;
	}

	public Date getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getCreateDateMember() {
		return this.createDateMember;
	}

	public void setCreateDateMember(Date createDateMember) {
		this.createDateMember = createDateMember;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Time getCreatedTime() {
		return this.createdTime;
	}

	public void setCreatedTime(Time createdTime) {
		this.createdTime = createdTime;
	}

	public String getDepositMember() {
		return this.depositMember;
	}

	public void setDepositMember(String depositMember) {
		this.depositMember = depositMember;
	}

	public String getDescEmailSent() {
		return this.descEmailSent;
	}

	public void setDescEmailSent(String descEmailSent) {
		this.descEmailSent = descEmailSent;
	}

	public String getDescSmsSent() {
		return this.descSmsSent;
	}

	public void setDescSmsSent(String descSmsSent) {
		this.descSmsSent = descSmsSent;
	}

	public String getDeviceRegistered() {
		return this.deviceRegistered;
	}

	public void setDeviceRegistered(String deviceRegistered) {
		this.deviceRegistered = deviceRegistered;
	}

	public String getDeviceRegisteredMember() {
		return this.deviceRegisteredMember;
	}

	public void setDeviceRegisteredMember(String deviceRegisteredMember) {
		this.deviceRegisteredMember = deviceRegisteredMember;
	}

	public String getDeviceType() {
		return this.deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getFullName() {
		return this.fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getGender() {
		return this.gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getGroupId() {
		return this.groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public String getHeir() {
		return this.heir;
	}

	public void setHeir(String heir) {
		this.heir = heir;
	}

	public String getHomePhone() {
		return this.homePhone;
	}

	public void setHomePhone(String homePhone) {
		this.homePhone = homePhone;
	}

	public String getIdentityCardNum() {
		return this.identityCardNum;
	}

	public void setIdentityCardNum(String identityCardNum) {
		this.identityCardNum = identityCardNum;
	}

	public String getIpLastUsed() {
		return this.ipLastUsed;
	}

	public void setIpLastUsed(String ipLastUsed) {
		this.ipLastUsed = ipLastUsed;
	}

	public String getIpRegistered() {
		return this.ipRegistered;
	}

	public void setIpRegistered(String ipRegistered) {
		this.ipRegistered = ipRegistered;
	}

	public String getIpRegisteredMember() {
		return this.ipRegisteredMember;
	}

	public void setIpRegisteredMember(String ipRegisteredMember) {
		this.ipRegisteredMember = ipRegisteredMember;
	}

	public String getIsActive() {
		return this.isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public String getIsEmailNotif() {
		return this.isEmailNotif;
	}

	public void setIsEmailNotif(String isEmailNotif) {
		this.isEmailNotif = isEmailNotif;
	}

	public String getIsLocked() {
		return this.isLocked;
	}

	public void setIsLocked(String isLocked) {
		this.isLocked = isLocked;
	}

	public String getIsMember() {
		return this.isMember;
	}

	public void setIsMember(String isMember) {
		this.isMember = isMember;
	}

	public String getIsNewReg() {
		return this.isNewReg;
	}

	public void setIsNewReg(String isNewReg) {
		this.isNewReg = isNewReg;
	}

	public String getIsUserFacebook() {
		return this.isUserFacebook;
	}

	public void setIsUserFacebook(String isUserFacebook) {
		this.isUserFacebook = isUserFacebook;
	}

	public String getIsUserTwitter() {
		return this.isUserTwitter;
	}

	public void setIsUserTwitter(String isUserTwitter) {
		this.isUserTwitter = isUserTwitter;
	}

	public Date getLastDateLogin() {
		return this.lastDateLogin;
	}

	public void setLastDateLogin(Date lastDateLogin) {
		this.lastDateLogin = lastDateLogin;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMemberId() {
		return this.memberId;
	}

	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}

	public String getMerchantCode() {
		return this.merchantCode;
	}

	public void setMerchantCode(String merchantCode) {
		this.merchantCode = merchantCode;
	}

	public String getMobilePhone() {
		return this.mobilePhone;
	}

	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}

	public String getMotherMaidenName() {
		return this.motherMaidenName;
	}

	public void setMotherMaidenName(String motherMaidenName) {
		this.motherMaidenName = motherMaidenName;
	}

	public String getNotification() {
		return this.notification;
	}

	public void setNotification(String notification) {
		this.notification = notification;
	}

	public String getOccupation() {
		return this.occupation;
	}

	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}

	public String getParentUserName() {
		return this.parentUserName;
	}

	public void setParentUserName(String parentUserName) {
		this.parentUserName = parentUserName;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPhoneNumber() {
		return this.phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getPhoneNumber2() {
		return this.phoneNumber2;
	}

	public void setPhoneNumber2(String phoneNumber2) {
		this.phoneNumber2 = phoneNumber2;
	}

	public String getPhotoUrl() {
		return this.photoUrl;
	}

	public void setPhotoUrl(String photoUrl) {
		this.photoUrl = photoUrl;
	}

	public String getPinUser() {
		return this.pinUser;
	}

	public void setPinUser(String pinUser) {
		this.pinUser = pinUser;
	}

	public String getPlaceOfBirth() {
		return this.placeOfBirth;
	}

	public void setPlaceOfBirth(String placeOfBirth) {
		this.placeOfBirth = placeOfBirth;
	}

	public String getPostCode() {
		return this.postCode;
	}

	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}

	public BigInteger getProvinceId() {
		return this.provinceId;
	}

	public void setProvinceId(BigInteger provinceId) {
		this.provinceId = provinceId;
	}

	public String getReligion() {
		return this.religion;
	}

	public void setReligion(String religion) {
		this.religion = religion;
	}

	public BigInteger getResellerId() {
		return this.resellerId;
	}

	public void setResellerId(BigInteger resellerId) {
		this.resellerId = resellerId;
	}

	public String getRetrySendMail() {
		return this.retrySendMail;
	}

	public void setRetrySendMail(String retrySendMail) {
		this.retrySendMail = retrySendMail;
	}

	public String getRewardPointSts() {
		return this.rewardPointSts;
	}

	public void setRewardPointSts(String rewardPointSts) {
		this.rewardPointSts = rewardPointSts;
	}

	public String getSecurityAnswer() {
		return this.securityAnswer;
	}

	public void setSecurityAnswer(String securityAnswer) {
		this.securityAnswer = securityAnswer;
	}

	public String getSecurityQuestion() {
		return this.securityQuestion;
	}

	public void setSecurityQuestion(String securityQuestion) {
		this.securityQuestion = securityQuestion;
	}

	public String getStatusPinDeposit() {
		return this.statusPinDeposit;
	}

	public void setStatusPinDeposit(String statusPinDeposit) {
		this.statusPinDeposit = statusPinDeposit;
	}

	public String getStsEmailActived() {
		return this.stsEmailActived;
	}

	public void setStsEmailActived(String stsEmailActived) {
		this.stsEmailActived = stsEmailActived;
	}

	public String getStsEmailBlastPadiair() {
		return this.stsEmailBlastPadiair;
	}

	public void setStsEmailBlastPadiair(String stsEmailBlastPadiair) {
		this.stsEmailBlastPadiair = stsEmailBlastPadiair;
	}

	public String getStsEmailBlastPaditrain() {
		return this.stsEmailBlastPaditrain;
	}

	public void setStsEmailBlastPaditrain(String stsEmailBlastPaditrain) {
		this.stsEmailBlastPaditrain = stsEmailBlastPaditrain;
	}

	public String getStsEmailInfo() {
		return this.stsEmailInfo;
	}

	public void setStsEmailInfo(String stsEmailInfo) {
		this.stsEmailInfo = stsEmailInfo;
	}

	public String getStsEmailInfoPadiair() {
		return this.stsEmailInfoPadiair;
	}

	public void setStsEmailInfoPadiair(String stsEmailInfoPadiair) {
		this.stsEmailInfoPadiair = stsEmailInfoPadiair;
	}

	public String getStsEmailResetPass() {
		return this.stsEmailResetPass;
	}

	public void setStsEmailResetPass(String stsEmailResetPass) {
		this.stsEmailResetPass = stsEmailResetPass;
	}

	public String getStsSmsActivated() {
		return this.stsSmsActivated;
	}

	public void setStsSmsActivated(String stsSmsActivated) {
		this.stsSmsActivated = stsSmsActivated;
	}

	public String getStsSmsSent() {
		return this.stsSmsSent;
	}

	public void setStsSmsSent(String stsSmsSent) {
		this.stsSmsSent = stsSmsSent;
	}

	public BigInteger getSubAgentId() {
		return this.subAgentId;
	}

	public void setSubAgentId(BigInteger subAgentId) {
		this.subAgentId = subAgentId;
	}

	public String getTempPassword() {
		return this.tempPassword;
	}

	public void setTempPassword(String tempPassword) {
		this.tempPassword = tempPassword;
	}

	public String getTempTokenMember() {
		return this.tempTokenMember;
	}

	public void setTempTokenMember(String tempTokenMember) {
		this.tempTokenMember = tempTokenMember;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Date getTokenMemberExpired() {
		return this.tokenMemberExpired;
	}

	public void setTokenMemberExpired(Date tokenMemberExpired) {
		this.tokenMemberExpired = tokenMemberExpired;
	}

	public String getTypeMobilePayment() {
		return this.typeMobilePayment;
	}

	public void setTypeMobilePayment(String typeMobilePayment) {
		this.typeMobilePayment = typeMobilePayment;
	}

	public Date getUpdateDate() {
		return this.updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public Date getUpdateDateMember() {
		return this.updateDateMember;
	}

	public void setUpdateDateMember(Date updateDateMember) {
		this.updateDateMember = updateDateMember;
	}

	public Time getUpdateTime() {
		return this.updateTime;
	}

	public void setUpdateTime(Time updateTime) {
		this.updateTime = updateTime;
	}

	public String getUsePadiair() {
		return this.usePadiair;
	}

	public void setUsePadiair(String usePadiair) {
		this.usePadiair = usePadiair;
	}

	public String getUsePadipay() {
		return this.usePadipay;
	}

	public void setUsePadipay(String usePadipay) {
		this.usePadipay = usePadipay;
	}

	public String getUsePaditrain() {
		return this.usePaditrain;
	}

	public void setUsePaditrain(String usePaditrain) {
		this.usePaditrain = usePaditrain;
	}

	public Date getUserBirthdate() {
		return this.userBirthdate;
	}

	public void setUserBirthdate(Date userBirthdate) {
		this.userBirthdate = userBirthdate;
	}

	public String getUserName() {
		return this.userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getYahooId() {
		return this.yahooId;
	}

	public void setYahooId(String yahooId) {
		this.yahooId = yahooId;
	}

	public BigInteger getUserId() {
		return userId;
	}

	public void setUserId(BigInteger userId) {
		this.userId = userId;
	}

}