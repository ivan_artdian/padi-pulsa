package corp.sTech.padi.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.FastDateFormat;

public class DateUtils extends org.apache.commons.lang3.time.DateUtils
{
	public static final String DEFAULT_DATE_PATTERN = "dd-MM-yyyy";
	public static final String DDMMYY_DATE_PATTERN = "dd/MM/yy";
	public static final String DDMMMYYYY_DATE_PATTERN = "dd MMM yyyy";
	public static final String MMDDYYYY_DATE_PATTERN = "MM/dd/yyyy";
	public static final String YYYYMMDD_DATE_PATTERN = "yyyy-MM-dd";
	public static final String DEFAULT_TIME_PATTERN = "HH:mm:ss";
	public static final String yyyyMMdd_DATE_PATTERN = "yyyyMMdd";
	public static final String yyMMdd_DATE_PATTERN = "yyMMdd";
	public static final String yyyyMMddhhmmss_DATE_PATTERN = "yyyyMMddHHmmss";
	public static final String hhmmss_DATE_PATTERN = "HHmmss";
	public static final String DDMMMYYYYHHmmss_DATE_PATTERN = "dd-MM-yyyy HH:mm:ss";
	public static final String DDMMMMYYYYHHmmss_DATE_PATTERN = "dd-MMM-yyyy HH:mm:ss"; //18-JAN-2013 23:46:39
	public static final String DDMMMYYYYHHmmss_DATE_PATTERN2 = "dd/MM/yyyy HH:mm:ss";
	public static final String MMMYYYY_DATE_PATTERN = "MMM yyyy";
	public static final String MM_DATE_PATTERN = "MM";
	public static final String YYYY_DATE_PATTERN = "yyyy";
	public static final String DEFAULT_DATE_PATTERN2 = "dd/MM/yyyy";
	public static final String YYYY_MM_DD_DATE_PATTERN = "yyyy-MM-dd";
	public static final String MMYYYY_MONTH_YEAR_PATTERN = "MM/yyyy";
	public static final String hhmm_DATE_PATTERN = "HHmm";
	public static final String YYYYMMddHHmmss_DATE_PATTERN = "yyyy-MM-dd HH:mm:ss";
	
	/**
	 * Constructor.
	 */
	protected DateUtils()
	{
		super();
	}
	
	// =========================================================================
	// Helpers (Protected)
	// =========================================================================
	
	/**
	 * Return the current Locale for date-operation without specified pattern.
	 * @return current Locale
	 */
	protected static Locale getCurrentLocale()
	{
		//return Locale.getDefault();
		return org.springframework.context.i18n.LocaleContextHolder.getLocale();
	}
	
	
	// =========================================================================
	// Commons Date Utilities methods.
	// =========================================================================
	
	/**
	 * Format a Date to String representation using default date pattern
	 * {@value #DEFAULT_DATE_PATTERN}
	 * 
	 * @param date A date to convert
	 * @return a string representation of the date
	 * @see DateFormatUtils#format(java.util.Date, java.lang.String)
	 * @see #format(java.util.Date, java.lang.String)
	 */
	public static final String format(Date date)
	{
		return DateFormatUtils.format(date, DEFAULT_DATE_PATTERN);
	}
	
	/**
	 * Format a Date to String representation using date pattern specified.
	 * 
	 * @param date A date to convert
	 * @return a string representation of the date
	 * @see DateFormatUtils#format(java.util.Date, java.lang.String)
	 * @see #format(Date)
	 */
	public static final String format(Date date, String pattern)
	{
		return DateFormatUtils.format(date, pattern);
	}
	
	// =========================================================================
	// bHeritage Custom Date Utilities methods
	// =========================================================================

	public final static Date getFirstDateOfThisMonth() {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.DAY_OF_MONTH, 1);
		return cal.getTime();
	}
	
	public final static Date getLastDateOfThisMonth() {
		 Calendar cal = Calendar.getInstance();
		 cal.set(Calendar.DATE, cal.getActualMaximum(Calendar.DATE));
		 cal.set(Calendar.HOUR_OF_DAY, 23);
		 cal.set(Calendar.MINUTE, 59);
		 cal.set(Calendar.SECOND, 59);
		return cal.getTime();
	}
	
	/**
	 * @return String of the first date for current month in dd-MM-yyyy format
	 */
	public final static String getStringOfMonthFirstDate() {
		//return format( getFirstDateOfThisMonth() );
		return FastDateFormat.getInstance(DEFAULT_DATE_PATTERN).format(getFirstDateOfThisMonth());
	}
	
	/**
	 * @return String of the last date for current month in dd-MM-yyyy format
	 */
	public final static String getStringOfMonthLastDate() {
		//return format( getLastDateOfThisMonth() );
		return FastDateFormat.getInstance(DEFAULT_DATE_PATTERN).format(getLastDateOfThisMonth());
	}
	

	public final static synchronized Date stringDDMMYYYYToDate(String tgl) throws ParseException {
		return new SimpleDateFormat(DEFAULT_DATE_PATTERN).parse(tgl);
	}
	
	public final static synchronized Date stringHHmmToDate(String tgl) throws ParseException {
		return new SimpleDateFormat(hhmm_DATE_PATTERN).parse(tgl);
	}
	
	public final static synchronized Date stringHHmmssToDate(String tgl) throws ParseException {
		return new SimpleDateFormat(hhmmss_DATE_PATTERN).parse(tgl);
	}
	
	public final static synchronized Date stringDDMMMYYYYHHmmssToDate(String tgl) throws ParseException {
		return new SimpleDateFormat(DDMMMYYYYHHmmss_DATE_PATTERN).parse(tgl);
	}
	
	public final static synchronized Date stringDDMMMYYYYHHmmssToDate2(String tgl) throws ParseException {
		return new SimpleDateFormat(DDMMMYYYYHHmmss_DATE_PATTERN2).parse(tgl);
	}	
	
	public final static synchronized Date stringDDMONTHYYYYHHmmssToDate(String tgl) throws ParseException {
		return new SimpleDateFormat(DDMMMMYYYYHHmmss_DATE_PATTERN).parse(tgl);
	}
	
	public final static synchronized Date stringyyyyMMddhhmmssToDate(String tgl) throws ParseException {
		return new SimpleDateFormat(yyyyMMddhhmmss_DATE_PATTERN).parse(tgl);
	}
	
	
	public final static synchronized Date stringDDMMYYToDate(String tgl) throws ParseException {
		return new SimpleDateFormat(DDMMYY_DATE_PATTERN).parse(tgl);
	}	
	
	public final static synchronized Date stringYYYYMMDDToDate(String tgl) throws ParseException {
		return new SimpleDateFormat(YYYYMMDD_DATE_PATTERN).parse(tgl);
	}
	
	public final static synchronized Date stringMMDDYYYYToDate(String tgl) throws ParseException {
		return new SimpleDateFormat(MMDDYYYY_DATE_PATTERN).parse(tgl);
	}
	
	public final static synchronized Date stringDDMMMYYYYToDate(String tgl) throws ParseException {
		return new SimpleDateFormat(DDMMMYYYY_DATE_PATTERN).parse(tgl);
	}
	
	public final static synchronized Date stringyyyyMMddToDate(String tgl) throws ParseException {
		return new SimpleDateFormat(yyyyMMdd_DATE_PATTERN).parse(tgl);
	}	
	
	public final static synchronized Date stringMMYYYYToDate(String tgl) throws ParseException {
		return new SimpleDateFormat(MMYYYY_MONTH_YEAR_PATTERN).parse(tgl);
	}

	/**
	 * 
	 * @param date
	 * @return String of date in dd-MM-yyyy format
	 */
	public static String dateToDDMMYYYYString(Date date) {
		if (date != null) {
			return FastDateFormat.getInstance(DEFAULT_DATE_PATTERN).format(date);
		}
		return null;
	}
	
	public static String dateToDDMMYYYYString2(Date date) {
		if (date != null) {
			return FastDateFormat.getInstance(
					DEFAULT_DATE_PATTERN2).format(date);
		}
		return null;
	}
	
	
	/**
	 * 
	 * @param date
	 * @return String of date in YYYYMMDD format
	 */
	public static String dateToyyyyMMddString(Date date) {
		if (date != null) {
			return FastDateFormat.getInstance(yyyyMMdd_DATE_PATTERN).format(date);
		}
		return null;
	}
	
	public static String dateToYYYYMMDDString(Date date) {
		if (date != null) {
			return FastDateFormat.getInstance(YYYYMMDD_DATE_PATTERN).format(date);
		}
		return null;
	}
	
	
	
	public static String dateToyyyy_MM_ddString(Date date) {
		if (date != null) {
			return FastDateFormat.getInstance(YYYY_MM_DD_DATE_PATTERN).format(date);
		}
		return null;
	}
	
	
	
	public static String dateToyyMMddString(Date date) {
		if (date != null) {
			return FastDateFormat.getInstance(yyMMdd_DATE_PATTERN).format(date);
		}
		return null;
	}
	
	
	/**
	 * 
	 * @param date
	 * @return String of date in YYYYMMDDhhmmss format
	 */
	public static String dateToyyyyMMddhhmmssString(Date date) {
		if (date != null) {
			return FastDateFormat.getInstance(yyyyMMddhhmmss_DATE_PATTERN).format(date);
		}
		return null;
	}	
	
	public static String dateToDDMMMMYYYYHHmmssString(Date date) {
		if (date != null) {
			return FastDateFormat.getInstance(DDMMMMYYYYHHmmss_DATE_PATTERN).format(date);
		}
		return null;
	}
	
	public static String dateToDDMMYYYYHHmmssString(Date date) {
		if (date != null) {
			return FastDateFormat.getInstance(DDMMMYYYYHHmmss_DATE_PATTERN2).format(date);
		}
		return null;
	}
	
	
	public static String dateToMMMYYYYString(Date date) {
		if (date != null) {
			return FastDateFormat.getInstance(MMMYYYY_DATE_PATTERN).format(date);
		}
		return null;
	}	
	
	
	public static String dateToMMYYYYString(Date date) {
		if (date != null) {
			return FastDateFormat.getInstance(MMYYYY_MONTH_YEAR_PATTERN).format(date);
		}
		return null;
	}
	
	
	public static String dateToHHMMSSString(Date date) {
		if (date != null) {
			return FastDateFormat.getInstance(DEFAULT_TIME_PATTERN).format(date);
		}
		return null;
	}	
	
	public static String dateToMMString(Date date) {
		if (date != null) {
			return FastDateFormat.getInstance(MM_DATE_PATTERN).format(date);
		}
		return null;
	}	
	
	public static String dateToHHmmString(Date date) {
		if (date != null) {
			return FastDateFormat.getInstance(hhmm_DATE_PATTERN).format(date);
		}
		return null;
	}	
	
	
	public static String dateToYYYYString(Date date) {
		if (date != null) {
			return FastDateFormat.getInstance(YYYY_DATE_PATTERN).format(date);
		}
		return null;
	}	
	
	
	
	/**
	 * 
	 * @param date
	 * @return String of date in hhmmss format
	 */
	public static String dateTohhmmssString(Date date) {
		if (date != null) {
			return FastDateFormat.getInstance(hhmmss_DATE_PATTERN).format(date);
		}
		return null;
	}
	
	
	/**
	 * 
	 * @param startTime String in HH:MM:ss format
	 * @return Date
	 * @throws ParseException 
	 */
	public static Date getTimeFromStringFormat(String startTime) throws ParseException {
		return getTimeFromStringFormat(null, startTime);
	}
	
	/**
	 * 
	 * @param currentDate
	 * @param startTime String in HH:MM:ss format
	 * @return Date
	 * @throws ParseException 
	 */
	public static Date getTimeFromStringFormat(Date currentDate, String startTime) throws ParseException {
		Calendar calendar = Calendar.getInstance();
		if(currentDate != null) calendar.setTime(currentDate);
		
		if(startTime == null) return calendar.getTime(); //return (HH:MM:ss) with (00:00:00)
		
		String[] splitTime = startTime.split(":");
		
		if (splitTime.length != 3) throw new ParseException("Incorrect time format (HH:MM:ss)", 0); 
		
		calendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(splitTime[0]));
		calendar.set(Calendar.MINUTE, Integer.parseInt(splitTime[1]));
		calendar.set(Calendar.SECOND, Integer.parseInt(splitTime[2]));
		
		return calendar.getTime();
	}

	/**
	 * 
	 * @param dateWithTime
	 * @return String
	 */
	public static String getTimeFromDate(Date dateWithTime) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(dateWithTime);
		
		String hour = cal.get(Calendar.HOUR_OF_DAY) < 10 ? "0" + cal.get(Calendar.HOUR_OF_DAY) : "" + cal.get(Calendar.HOUR_OF_DAY);
		String minute = cal.get(Calendar.MINUTE) < 10 ? "0" + cal.get(Calendar.MINUTE) : "" + cal.get(Calendar.MINUTE);
		String second = cal.get(Calendar.SECOND) < 10 ? "0" + cal.get(Calendar.SECOND) : "" + cal.get(Calendar.SECOND);
		
		return String.format("%s:%s:%s", hour, minute, second);
	}
	
	public static String getTimeFromDateSSS(Date dateWithTime) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(dateWithTime);
		
		String hour = cal.get(Calendar.HOUR_OF_DAY) < 10 ? "0" + cal.get(Calendar.HOUR_OF_DAY) : "" + cal.get(Calendar.HOUR_OF_DAY);
		String minute = cal.get(Calendar.MINUTE) < 10 ? "0" + cal.get(Calendar.MINUTE) : "" + cal.get(Calendar.MINUTE);
		String second = cal.get(Calendar.SECOND) < 10 ? "0" + cal.get(Calendar.SECOND) : "" + cal.get(Calendar.SECOND);
		
		return String.format("%s%s%s", hour, minute, second);
	}
	
	public static int getCountDays(Date a, Date b){
		 Calendar start = Calendar.getInstance();		 
		 Calendar end = Calendar.getInstance();
		
		int difference = 0;
		int countDay = 0;
 
		if (a.compareTo(b) < 0){
			start.setTime(a);
			end.setTime(b);
		}else{
			start.setTime(b);
			end.setTime(a);
		}
 
		//membandingkan tahun date a dan tahun date b
		while (start.get(Calendar.YEAR) != end.get(Calendar.YEAR)){
			difference = 365 * (end.get(Calendar.YEAR) - start.get(Calendar.YEAR));
			countDay += difference;
			start.add(Calendar.DAY_OF_YEAR, difference);
		}
		if (start.get(Calendar.DAY_OF_YEAR) != end.get(Calendar.DAY_OF_YEAR)){
			difference = end.get(Calendar.DAY_OF_YEAR) - start.get(Calendar.DAY_OF_YEAR);
			countDay += difference;
			start.add(Calendar.DAY_OF_YEAR, difference);
		}
		return(countDay);
	}
	
//	CONVERT JAM DI REDBUS
	public static String convertTime(String time) {
		double hasil = Integer.parseInt(time) / 60;
		String jam = "" + hasil;
		for (int y = 0; y < jam.length(); y++) {
			if (jam.length() == 1) {
				jam = jam.replaceAll(jam, "0" + jam + ":00");
			} else if (jam.length() == 2) {
				jam = jam.replaceAll(jam, jam + ":00");
			} else if (jam.length() == 3) {
				jam = jam.replaceAll(jam, "0" + jam.substring(0, 1) + ":" + jam.substring(2, 3) + "0");
			} else if (jam.length() == 4) {
				if (jam.substring(2, 3).equalsIgnoreCase(".")) {
					jam = jam.replaceAll(jam, jam.substring(0, 2) + ":" + jam.substring(3, 4) + "0");// 18.5
				} else {
					jam = jam.replaceAll(jam, "0" + jam.substring(0, 1) + ":" + jam.substring(2, 4));// 8.15
				}
			} else if (jam.length() == 5) {
				jam = jam.replaceAll(jam, jam.substring(0, 2) + ":" + jam.substring(3, 5));
			}
		}
		return jam;
	}
	
	public static String dateToYYYYMMddHHmmssString(Date date) {
		if (date != null) {
			return FastDateFormat.getInstance(YYYYMMddHHmmss_DATE_PATTERN).format(date);
		}
		return null;
	}
	
	public static String addDateString(String dateNow, int addDate) throws ParseException{
		Calendar cal = Calendar.getInstance();
		cal.setTime(stringYYYYMMDDToDate(dateNow));
		cal.add(Calendar.DATE, addDate);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String tgl = sdf.format(cal.getTime());
		return tgl;
	}
	
//	public static void main(String[] ar) throws ParseException{
////		System.out.println("vvvvvvvv : "+convertTime("1490"));//1440 --> 24 jm 1380 23 jm
//		Calendar cal = Calendar.getInstance();
//		cal.setTime(getLastDateOfThisMonth());
////		cal = getLastDateOfThisMonth();
//		System.out.println("============== "+cal.getTime());
//		cal.add(Calendar.DATE,1);
//		System.out.println("==============2 "+cal.getTime());
//
//		addDateString("2017-08-27");
//	
//		System.out.println("modulus ::: "+1500%1500);
//	}
}
