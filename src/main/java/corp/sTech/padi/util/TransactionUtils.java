package corp.sTech.padi.util;

import corp.sTech.padi.modelApi.MResponse;
import corp.sTech.padi.modelProcessors.MMetadata;
import corp.sTech.padi.modelProcessors.MRawData;
import corp.sTech.padi.parameter.SystemParameter;

public class TransactionUtils {
	
	public static String maskString(int startLen, int endLen, String data, char mask){
        int total = data.length();
        
        int masklen = total-(startLen + endLen) ;
        StringBuffer maskedbuf = new StringBuffer(data.substring(0,startLen));
        for(int i=0;i<masklen;i++) {
            maskedbuf.append(mask);
        }
        
        maskedbuf.append(data.substring(startLen+masklen, total));
        String masked = maskedbuf.toString();
        return masked;
    }
	
	public static MMetadata initMetadata(String txnName, String request){
		MMetadata metadata = new MMetadata();
		metadata.setTxnIdentifier(RandomString.generateRandomString());
		metadata.setTxnName(txnName);
		metadata.setTimeIn(DateUtil.getCurrentDateString(SystemParameter.DATE_FORMAT));
		metadata.setTxnFlag("true");
		
		if (request!=null) {
			MRawData mRawData = new MRawData();
			mRawData.setRawRequest(request);
			metadata.setmRawData(mRawData);
		}
		
		return metadata;
	}
	
	public static MMetadata constructResponse(MMetadata metadata){
		MResponse mResponse = metadata.getmResponse();
		
		mResponse.setSctlID(metadata.getTxnIdentifier());
		if(mResponse.getTrxTime() == null)
			mResponse.setTrxTime(DateUtil.getCurrentDateString(SystemParameter.DATE_FORMAT));
		MRawData mRawData = metadata.getmRawData();
		if (mRawData==null) mRawData = new MRawData();
		
		mRawData.setRawResponse(JSONUtils.objectToJSONString(mResponse));
		
		metadata.setmRawData(mRawData);
		metadata.setmResponse(mResponse);
		metadata.setTxnStatusCode(mResponse.getResponseCode());
		metadata.setTxnStatusMessage(mResponse.getResponseMessage());
		
		return metadata;
	}
}
