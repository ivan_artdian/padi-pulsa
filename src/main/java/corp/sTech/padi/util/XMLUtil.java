package corp.sTech.padi.util;

import java.io.StringReader;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

public class XMLUtil {
	public static String mapToXmlString(Map<String, String> map, String rootName) {
		try {
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

			// root elements
			Document doc = docBuilder.newDocument();
			Element rootElement = doc.createElement(rootName);
			doc.appendChild(rootElement);

			for (String key : map.keySet()) {
				String value = map.get(key);

				Element element = doc.createElement(key);
				element.appendChild(doc.createTextNode(value));
				rootElement.appendChild(element);
			}

			TransformerFactory tf = TransformerFactory.newInstance();
			Transformer transformer = tf.newTransformer();
			transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
			StringWriter writer = new StringWriter();
			transformer.transform(new DOMSource(doc), new StreamResult(writer));
			String output = writer.getBuffer().toString().replaceAll("\n|\r", "");
			return output;
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (TransformerConfigurationException e) {
			e.printStackTrace();
		} catch (TransformerException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static Map<String, String> xmlStringToMap(String xmlString) {
		Document doc = convertStringToDocument(xmlString);
		doc.getDocumentElement().normalize();
		
		Map<String, String> map = new HashMap<>();
		try {
			if (doc.hasChildNodes()) {
				map = parseXml(doc.getChildNodes());
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
		return map;
	}

	private static Document convertStringToDocument(String xmlStr) {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder;
		try {
			builder = factory.newDocumentBuilder();
			Document doc = builder.parse(new InputSource(new StringReader(xmlStr)));
			return doc;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private static Map<String, String> parseXml(NodeList nodeList) {
		Map<String, String> map = new HashMap<>();
		for (int count = 0; count < nodeList.getLength(); count++) {
			Node tempNode = nodeList.item(count);

			// make sure it's element node.
			if (tempNode.getNodeType() == Node.ELEMENT_NODE) {
				if (tempNode.hasChildNodes() && tempNode.getFirstChild().getNodeType() != Node.TEXT_NODE) {
					// loop again if has child nodes
					map = parseXml(tempNode.getChildNodes());
				}else if (tempNode.hasChildNodes() && tempNode.getFirstChild().getNodeType() == Node.TEXT_NODE) {
					String key = tempNode.getNodeName();
					String value = tempNode.getFirstChild().getNodeValue();
					
					map.put(key, value);
				}
			}
		}
		
		return map;
	}
}
