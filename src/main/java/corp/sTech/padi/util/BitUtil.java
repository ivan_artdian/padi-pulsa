package corp.sTech.padi.util;

import java.math.BigInteger;

import org.apache.commons.lang3.StringUtils;

public class BitUtil {
	public static String decToHex(int decimal) {
		String hex = Integer.toHexString(decimal);
		return hex;
	}

	public static String asciiToBin(String ascii) {
		char[] chars = ascii.toCharArray();
		StringBuffer bin = new StringBuffer();
		for (int i = 0; i < chars.length; i++) {
			String binaryString = StringUtils.leftPad(Integer.toBinaryString(chars[i]), 8, "0");
			bin.append(binaryString);
		}
		return bin.toString();
	}

	public static String hexToBinary(String hex) {
		String bin = new BigInteger(hex, 16).toString(2);
		return bin;
	}

	public static void xor(int dec1, int dec2) {
		String hex1 = decToHex(dec1);
		String hex2 = decToHex(dec2);

		BigInteger bigInteger1 = new BigInteger(hex1, 16);
		BigInteger bigInteger2 = new BigInteger(hex2, 16);

		BigInteger bigIntegerXor = bigInteger1.xor(bigInteger2);
	}

	public static String xor(String dec1, String dec2) {
		String hex1 = asciiToBin(dec1);
		String hex2 = asciiToBin(dec2);

		BigInteger bigInteger1 = new BigInteger(hex1, 2);
		BigInteger bigInteger2 = new BigInteger(hex2, 2);

		BigInteger bigIntegerXor = bigInteger1.xor(bigInteger2);
		String result = StringUtils.leftPad(bigIntegerXor.toString(2), 80, "0");
		
		return result;
	}

	public static void main(String[] args) {
		BitUtil.xor("1910015678", "8765abcdef");
		
		System.out.println(Long.MAX_VALUE);
	}
}
