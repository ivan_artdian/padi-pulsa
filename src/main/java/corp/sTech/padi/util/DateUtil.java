package corp.sTech.padi.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang3.time.DateFormatUtils;

public class DateUtil {
	public static Date getCurrentDateZoned() {
		Calendar cal = Calendar.getInstance();
		// cal.add(Calendar.HOUR, 7);
		Date dateZoned = cal.getTime();
		return dateZoned;
	}
	
	public static String getCurrentDateString(String formatDate){
		DateFormat dateFormat = new SimpleDateFormat(formatDate);
		Date date = new Date();
		return dateFormat.format(date);
	}
	
	public static String dateToString(Date date){
		return DateFormatUtils.format(date, "dd-MM-yyyy HH:mm:ss");
	}
	
	public static String dateToString(Date date, String formatDate){
		return DateFormatUtils.format(date, formatDate);
	}
	
	public static Date stringToDate(String date){
		try {
			return new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
	}
	public static Date stringToDate(String date, String format){
		try {
			return new SimpleDateFormat(format).parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static String getNextHour(int addHour){
		DateFormat dateFormat = new SimpleDateFormat("HH:mm");
		Calendar cal = Calendar.getInstance();
		
		cal = Calendar.getInstance();
		cal.add(Calendar.HOUR, addHour);
		return dateFormat.format(cal.getTime());
	}
	
	public static void main(String[] args){
//		System.out.println(DateUtil.getCurrentDateString("ddMMyyyy HHmmss"));
//		System.out.println(dateToString(getCurrentDateZoned()));
		System.out.println(stringToDate("11:11", "HH:mm"));
	}

	
}
