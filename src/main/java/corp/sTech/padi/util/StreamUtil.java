package corp.sTech.padi.util;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.FilenameUtils;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

import corp.sTech.padi.parameter.SystemParameter;
import corp.sTech.padi.repository.SystemParameterRepository;

public class StreamUtil {

//	private static SystemParameterService systemParameterService;

	private static SystemParameterRepository systemParameterRepository;
	
	public static String doUpload(FileUploadEvent event) {
		return doUpload(event, SessionMapUtil.getGroupNameSession());
	}

	public static String doUpload(FileUploadEvent event, String groupName) {
		UploadedFile uploadedPhoto = event.getFile();

		// String rootPath = BussinessParameter.mobileImageFile;
		String rootPath = systemParameterRepository.paramValueOf(SystemParameter.IMAGE_ROOT_PATH);
		String folderPath = rootPath+ File.separator + groupName + File.separator;
		File folder = new File(folderPath);
		if (!folder.exists()) {
			boolean success = folder.mkdirs();
			if (!success) {
				return null;
			}
		}
		byte[] bytes = null;
		File file = null;
		try {
			if (null != uploadedPhoto) {
				bytes = uploadedPhoto.getContents();
				String filename = FilenameUtils.getName(uploadedPhoto.getFileName());
				BufferedOutputStream stream;
				file = new File(folder.getAbsolutePath() + File.separator + filename);
				stream = new BufferedOutputStream(new FileOutputStream(file));
				stream.write(bytes);
				stream.close();
			} else {
				System.out.println("======================= file null");
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return (null == file ? null : FilenameUtils.getName(file.getAbsolutePath()));
	}

	public static String uploadPdf(String groupName) {
		String rootPath = systemParameterRepository.paramValueOf(SystemParameter.IMAGE_ROOT_PATH);
		String folderPath = rootPath + groupName + File.separator;
		File folder = new File(folderPath);
		if (!folder.exists()) {
			boolean success = folder.mkdirs();
			if (!success) {
				return null;
			}
		}
		return folderPath;
		
	}
	public static String doUploadByte(String groupName, String type, String id, String filename, byte[] content) {
		String rootPath = systemParameterRepository.paramValueOf(SystemParameter.IMAGE_ROOT_PATH);
		String folderPath = rootPath + groupName + File.separator + type + File.separator + id + File.separator;
		File folder = new File(folderPath);
		if (!folder.exists()) {
			boolean success = folder.mkdirs();
			if (!success) {
				return null;
			}
		}
		File file = null;
		try {
			if (null != content) {
				BufferedOutputStream stream;
				file = new File(folder.getAbsolutePath() + File.separator + filename);
				stream = new BufferedOutputStream(new FileOutputStream(file));
				stream.write(content);
				stream.close();
			} else {
				System.out.println("======================= content null");
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return (null == file ? null : FilenameUtils.getName(file.getAbsolutePath()));
	}

	public static InputStream getInputStream(FileUploadEvent event) {
		try {
			InputStream inputstream = event.getFile().getInputstream();
			return inputstream;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static SystemParameterRepository getSystemParameterRepository() {
		return systemParameterRepository;
	}

	public static void setSystemParameterRepository(SystemParameterRepository systemParameterRepository) {
		StreamUtil.systemParameterRepository = systemParameterRepository;
	}

//	public static SystemParameterService getSystemParameterService() {
//		return systemParameterService;
//	}
//
//	public static void setSystemParameterService(SystemParameterService systemParameterService) {
//		StreamUtil.systemParameterService = systemParameterService;
//	}
}
