package corp.sTech.padi.util;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.message.BasicNameValuePair;

public class UrlEncoder {
	public static String encodedUrlParam(String url, Map<String, String> params) {
		if (!url.endsWith("?"))
			url += "?";

		List<NameValuePair> urlParams = new LinkedList<NameValuePair>();

		Set<String> keySet = params.keySet();
		for (String key : keySet) {
			urlParams.add(new BasicNameValuePair(key, params.get(key)));
		}

		String paramString = URLEncodedUtils.format(urlParams, "utf-8");

		url += paramString;
		return url;
	}
	
	public static void main(String[] args) {
		Map<String, String> params = new HashMap<>();
		params.put("padipaySignature", "padipaySignature");
		params.put("transactionTime", "transactionTime");
		params.put("expirationTime", "expirationTime");
		params.put("merchantCode", "merchantCode");
		params.put("invoiceCode", "invoiceCode");
		params.put("padipayCode", "padipayCode");
		params.put("amount", "amount");
		params.put("miscFee", "miscFee");
		params.put("totalPayments", "totalPayments");
		params.put("resultCode", "resultCode");
		params.put("resultDesc", "resultDesc");
		params.put("paymentCodeRefId", "paymentCodeRefId");
		params.put("paymentChannelId", "paymentChannelId");
		params.put("paymentChannel", "paymentChannel");
		params.put("addInfo1", "addInfo1");
		params.put("addInfo2", "addInfo2");
		
		/*String signature = null;
		try {
			signature = HashUtil.md5("62895632449140PRODUCTCYRUSKU20171223091000033728");
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		params.put("signature", signature);*/
		
		String encodedUrlParam = encodedUrlParam("https://127.0.0.1:8081/PPOB/adapter/mgateway/topupBalanceResponse", params );
		System.out.println(encodedUrlParam);
	}
}
