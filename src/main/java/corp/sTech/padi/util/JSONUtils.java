package corp.sTech.padi.util;

import java.io.IOException;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import corp.sTech.padi.modelApi.MRequest;

public class JSONUtils {

	public static String objectToJSONString(Object object) {
		ObjectMapper mapper = new ObjectMapper();
		mapper.enable(SerializationFeature.INDENT_OUTPUT);
		mapper.setSerializationInclusion(Include.NON_NULL);
		String jsonInString = null;
		try {
			jsonInString = mapper.writeValueAsString(object);
		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return jsonInString;
	}

	public static String prettyJsonString(String input) {
		ObjectMapper mapper = new ObjectMapper();
		Object json;
		try {
			json = mapper.readValue(input, Object.class);
			String indented = objectToJSONString(json);
			return indented;
		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static Object jsonStringToObject(String jsonInString, Class objectClass) {
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

		Object obj = null;
		try {
			obj = mapper.readValue(jsonInString, objectClass);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return obj;
	}

	public static Object jsonStringToListObject(String jsonInString, Class objectClass) {
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		
		List<Object> obj = null;
		try {
			obj = (List<Object>) mapper.readValue(jsonInString, mapper.getTypeFactory().constructCollectionType(List.class, objectClass));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return obj;
	}
	
	
	public static void main(String[] args) {
		MRequest mRequest = new MRequest();
		mRequest.setCredentialCode("PadiCiti");
		mRequest.setCredentialPass("PadiCiti123");
		mRequest.setMdn("085290020002");
		mRequest.setTrxID("uvK5UmOa");
		mRequest.setLanguageVer("ID");
		mRequest.setUserID("UserId");
		mRequest.setEmail("email@mail.com");
		         
		System.out.println(objectToJSONString(mRequest));
	}
}
