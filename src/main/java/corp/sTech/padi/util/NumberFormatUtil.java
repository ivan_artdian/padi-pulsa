package corp.sTech.padi.util;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;

public class NumberFormatUtil {
	public static String formatNumber(Object input) {
		try {
			if(input instanceof String) {
				input = Long.valueOf((String) input);
			}
			return NumberFormat.getNumberInstance(new Locale("ID")).format(input);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "0";
	}

	public static String formatNumber(Object input, boolean currency, boolean cent) {
		String inputFormat = formatNumber(input);
		if(currency) {
			input = "Rp " + inputFormat;
		}

		if (cent) {
			input = input + ",00";
		}
		return (String) input;
	}

	public static int parseNumberToInt(String input) {
		if (input != null && !input.isEmpty()) {
			try {
				input = input.toUpperCase();
				input = input.replaceAll("IDR", "");
				input = input.replaceAll("RP", "");
				input = input.replaceAll("Rp", "");
				input = input.replaceAll(",00", "");
				input = input.replaceAll(" ", "");

				return (NumberFormat.getNumberInstance(new Locale("ID")).parse(input)).intValue();
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		return 0;
	}

	public static void main(String[] args) {
		//		int parseNumberToInt = parseNumberToInt("IDR 5.500");
		System.out.println(formatNumber("5500"));
	}
}
