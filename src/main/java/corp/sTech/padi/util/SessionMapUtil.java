package corp.sTech.padi.util;

import java.util.List;
import java.util.Map;

import javax.faces.context.FacesContext;

import corp.sTech.padi.model.Agent;
import corp.sTech.padi.model.Group;
import corp.sTech.padi.model.Menu;
import corp.sTech.padi.model.User;


public class SessionMapUtil {
	private static Map<String, Object> getSessionMap() {
		return FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
	}

	public static void put(String key, Object value) {
		Map<String, Object> sessionMap = getSessionMap();
		sessionMap.put(key, value);
	}

	private static Object getSession(String sessionName) {
		Map<String, Object> sessionMap = getSessionMap();
		return sessionMap.get(sessionName);
	}
	
	public static Menu getSelectedMenu() {
		return (Menu) getSession("selectedMenu");
	}

	public static String getUrlApproval() {
		Menu selectedMenu = getSelectedMenu();
		if (null == selectedMenu) {
			return null;
		}
		return selectedMenu.getUrlApproval();
	}

	public static User getUserSession() {
		User user = (User) getSession("user");
		return user;
	}
	
	public static Group getGroupSession() {
		List<Group> groups = getUserSession().getGroups();
		if (groups.size() <= 0) {
			return null;
		} else {
			return groups.get(0);
		}
	}

	public static Agent getAgentSession() {
		Agent agent = (Agent) getSession("agent");
		return agent;
	}

	public static String getGroupNameSession() {
		Group group = getGroupSession();
		if (null == group) {
			return null;
		} else {
			return group.getUsername();
		}
	}

	public static String getTransferContent() {
		return (String) getSession("transferContent");
	}

	public static boolean isFromMenuButton() {
		Boolean fromMenuButton = (Boolean) getSession("fromMenuButton");
		return fromMenuButton;
	}

	public static boolean isSuperAdministrator(){
		return (boolean) getSession("isSuperAdministrator");
	}
}
