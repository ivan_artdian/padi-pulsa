package corp.sTech.padi.util;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.ws.rs.core.MultivaluedMap;

import com.sun.jersey.core.util.MultivaluedMapImpl;

public class ReflectionUtil {
	private static List<Field> findFields(Object obj, String fieldPrefix) {
		List<Field> matchFields = new ArrayList<>();
		Field[] declaredFields = obj.getClass().getDeclaredFields();
		for (Field field : declaredFields) {
			if (field.getName().toUpperCase().startsWith(fieldPrefix.toUpperCase())) {
				matchFields.add(field);
			}
		}

		return matchFields;
	}

	private static List<Object> getFieldValue(Object obj, List<Field> fields, boolean constantOnly) {
		List<Object> fieldValues = new ArrayList<>();
		try {
			for (Field field : fields) {
				if (constantOnly) {
					fieldValues.add(field.get(null));
				} else {
					if (field.isAccessible()) {
						fieldValues.add(field.get(obj));
					}
				}
			}
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}

		return fieldValues;
	}

	private static List<Object> getValueFromField(Object obj, String fieldPrefix, boolean constantOnly) {
		return getFieldValue(obj, findFields(obj, fieldPrefix), constantOnly);
	}

	public static List<String> getValueStringFromField(Class cls, String fieldPrefix) {
		try {
			Object newInstance = cls.newInstance();

			return getValueStringFromField(newInstance, fieldPrefix, true);
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static List<String> getValueStringFromFieldExcept(Class cls, String fieldPrefix) {
		try {
			Object newInstance = cls.newInstance();
			List<Field> allField= Arrays.asList(newInstance.getClass().getDeclaredFields())
					.stream()
					.filter(item -> !item.getName().toUpperCase().startsWith(fieldPrefix.toUpperCase()))
					.collect(Collectors.toList());
			
			List<String> filteredField = getFieldValue(newInstance, allField, true)
					.stream()
					.map(item -> item.toString())
					.collect(Collectors.toList());
			
			return filteredField;
		}  catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		
		return null;
	}

	private static List<String> getValueStringFromField(Object obj, String fieldPrefix, boolean constantOnly) {
		List<String> stringValues = new ArrayList<>();
		List<Object> values = getValueFromField(obj, fieldPrefix, constantOnly);
		for (Object value : values) {
			stringValues.add(value.toString());
		}

		return stringValues;
	}

	public static List<String> getValueStringFromField(Object obj, String fieldPrefix) {
		return getValueStringFromField(obj, fieldPrefix, false);
	}

	public static void setFieldValue(Object object, String fieldName, Object value) {
		try {
			Field field = object.getClass().getDeclaredField(fieldName);
			if (field != null) {
				field.setAccessible(true);
				field.set(object, value);
			}

		} catch (NoSuchFieldException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static MultivaluedMap<String, String> queryAsString(Object obj) {
		try {
			Field[] declaredFields = obj.getClass().getDeclaredFields();
			MultivaluedMap<String, String> map = new MultivaluedMapImpl();
			for (Field field : declaredFields) {
				field.setAccessible(true);
				String a = (String) field.get(obj);
				map.putSingle(field.getName(), String.valueOf(a));
			}
			return map;
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		return null;
	}
}
