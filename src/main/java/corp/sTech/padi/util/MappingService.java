package corp.sTech.padi.util;

public class MappingService {

	public static String mappingMDNtoOperator(String mdn, String category, String plan) {
		System.out.println("Operator type:"+mdn.substring(0, 4));
		
		if("prepaid".equals(plan)) {
			if("pulsa".equals(category)) {
				switch (mdn.substring(0, 4)) {
				
				case "0811": return "TELKOMSEL";
				case "0812": return "TELKOMSEL";
				case "0813": return "TELKOMSEL";
				case "0821": return "TELKOMSEL";
				case "0822": return "TELKOMSEL";
				case "0823": return "TELKOMSEL";
				case "0852": return "TELKOMSEL";
				case "0853": return "TELKOMSEL";
				case "0851": return "TELKOMSEL";
				case "0855": return "INDOSAT";
				case "0856": return "INDOSAT";
				case "0857": return "INDOSAT";
				case "0858": return "INDOSAT";
				case "0814": return "INDOSAT";
				case "0815": return "INDOSAT";
				case "0816": return "INDOSAT";
				case "0817": return "XL";
				case "0818": return "XL";
				case "0819": return "XL";
				case "0859": return "XL";
				case "0877": return "XL";
				case "0878": return "XL";
				case "0838": return "AXIS";
				case "0831": return "AXIS";
				case "0832": return "AXIS";
				case "0833": return "AXIS";
				case "0896": return "Three";
				case "0897": return "Three";
				case "0898": return "Three";
				case "0899": return "Three";

				default: return "Unknown";
				
				}
			}else if("data".equals(category)) {
				switch (mdn.substring(0, 4)) {
				
				case "0811": return "TELKOMSEL DATA";
				case "0812": return "TELKOMSEL DATA";
				case "0813": return "TELKOMSEL DATA";
				case "0821": return "TELKOMSEL DATA";
				case "0822": return "TELKOMSEL DATA";
				case "0823": return "TELKOMSEL DATA";
				case "0852": return "TELKOMSEL DATA";
				case "0853": return "TELKOMSEL DATA";
				case "0851": return "TELKOMSEL DATA";
				case "0855": return "INDOSAT DATA";
				case "0856": return "INDOSAT DATA";
				case "0857": return "INDOSAT DATA";
				case "0858": return "INDOSAT DATA";
				case "0814": return "INDOSAT DATA";
				case "0815": return "INDOSAT DATA";
				case "0816": return "INDOSAT DATA";
				case "0817": return "XL DATA";
				case "0818": return "XL DATA";
				case "0819": return "XL DATA";
				case "0859": return "XL DATA";
				case "0877": return "XL DATA";
				case "0878": return "XL DATA";
				case "0838": return "AXIS DATA";
				case "0831": return "AXIS DATA";
				case "0832": return "AXIS DATA";
				case "0833": return "AXIS DATA";
				case "0896": return "Three Data";
				case "0897": return "Three Data";
				case "0898": return "Three Data";
				case "0899": return "Three Data";

				default: return "Unknown";
				
				}
			}
		} else if("postpaid".equals(plan)) {
			return "TAGIHAN";
		}
		return "Unknown";
	}
	
	public static String reservationChannelOf(String operator) {
		if(operator != null) {
			if(operator.toUpperCase().contains("DATA")) {
				return "data";
			}
			return "pulsa";
		}
		
		return null;
	}
}
