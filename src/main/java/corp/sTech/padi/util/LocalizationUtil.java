package corp.sTech.padi.util;

import java.text.MessageFormat;
import java.util.ResourceBundle;

import javax.faces.context.FacesContext;

public class LocalizationUtil {
	public static String getI18nValue(String propertiesName) {
		String value = "";
		try {
		    ResourceBundle bundle = ResourceBundle.getBundle("internationalization.i18n", FacesContext.getCurrentInstance().getViewRoot().getLocale());
		    value = bundle.getString(propertiesName);
		} catch (Exception e) {
		    System.out.println("EXCEPTION: " + e);
		    value = "error get i18n value";
		}
		return value;
	}

	public static String getI18nValue(String propertiesName, Object param[]) {
		String text = getI18nValue(propertiesName);

		MessageFormat messageFormat = new MessageFormat(text);
		String value = messageFormat.format(param, new StringBuffer(), null).toString();
		return value;
	}
}
