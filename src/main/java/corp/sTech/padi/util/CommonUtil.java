package corp.sTech.padi.util;

import java.io.PrintStream;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.MessageDigest;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Random;
import java.util.StringTokenizer;
import java.util.Vector;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class CommonUtil {
	public static final String SAMA_DENGAN = "";
	static Log log = LogFactory.getFactory().getInstance(CommonUtil.class);
	static String[] m = {"January", "February", "March", "April", "May", "June", "July", "August", "September",
			"October", "November", "December"};

	private static String algorithm = "DESede";
	private static Key key = null;
	private static Cipher cipher = null;

	public static String digest(String plain) throws Exception {
		if (plain == null)
			return null;
		try {
			MessageDigest sha = MessageDigest.getInstance("SHA-1");
			sha.update(plain.getBytes("UTF-16"));
			byte[] bs = sha.digest();
			StringBuffer res = new StringBuffer();
			for (int ix = 0; ix < bs.length; ++ix) {
				byte b = bs[ix];
				int i;
				if (b > 0)
					i = b;
				else
					i = 256 + b;
				int d = i / 16;
				if (d > 9)
					res.append((char) (65 + d - 10));
				else
					res.append((char) (48 + d));
				d = i % 16;
				if (d > 9)
					res.append((char) (65 + d - 10));
				else
					res.append((char) (48 + d));
			}
			return res.toString();
		} catch (Exception ex) {
			throw new Exception("Digest Exception", ex);
		}
	}

	public static String getMedia(byte[] b) throws Exception {
		StringBuffer out = new StringBuffer();
		out.append(new String(b, "ISO-8859-1"));
		return out.toString();
	}

	public static String randomin(int minLeft, int lenLeft, int minRight, int lenRight, boolean all) {
		String[] numeric = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"};
		String[] numAlpha = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f", "g", "h",
				"i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"};
		String[] data;
		if (all)
			data = numAlpha;
		else
			data = numeric;

		Random generator = new Random();
		StringBuffer result = new StringBuffer();

		int len = minLeft + generator.nextInt(lenLeft);
		for (int i = 0; i < len; ++i) {
			int idx = generator.nextInt(data.length);
			result.append(data[idx]);
			idx = generator.nextInt(data.length);
		}

		result.append("");

		len = minRight + generator.nextInt(lenRight);
		for (int i = 0; i < len; ++i) {
			int idx = generator.nextInt(data.length);
			result.append(data[idx]);
			idx = generator.nextInt(data.length);
		}

		return result.toString();
	}

	public static String generateRandomPassword() {
		return randomin(3, 1, 3, 2, true);
	}

	public static String generateRandomPassCode() {
		return randomin(2, 1, 2, 2, true);
	}

	public static String htmlEntities(String value) {
		String result = value;
		try {
			String[][] tagArray = {{"<", "&lt;"}, {">", "&gt;"}, {"\\[b\\]", "<b>"}, {"\\[li\\]", "<li>"},
					{"\\[/b\\]", "</b>"}, {"\\[i\\]", "<i>"}, {"\\[/i\\]", "</i>"}, {"\\[ul\\]", "<ul>"},
					{"\\[ul type=\"1\"\\]", "<ul type=\"1\">"}, {"\\[ul type=\"a\"\\]", "<ul type=\"a\">"},
					{"\\[ul type=\"A\"\\]", "<ul type=\"A\">"}, {"\\[/ul\\]", "</ul>"}, {"\\[center\\]", "<center>"},
					{"\\[/center\\]", "</center>"}, {"\\[br\\]", "<br>"}, {"\\[/br\\]", "</br>"}, {"\\[u\\]", "<u>"},
					{"\\[/u\\]", "</u>"}};
			for (int i = 0; i < tagArray.length; ++i)
				result = result.replaceAll(tagArray[i][0], tagArray[i][1]);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	public static String htmlUser(String value) {
		String result = value;
		try {
			String[][] tagArray = {{"<", "\\["}, {">", "\\]"}, {"</", "\\[/"}};
			for (int i = 0; i < tagArray.length; ++i)
				result = result.replaceAll(tagArray[i][0], tagArray[i][1]);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	public static boolean isSameActionClass(String previous, String next) {
		boolean b = false;

		if ((previous != null) && (previous.length() > 0) && (next != null) && (next.length() > 0) && (previous
				.substring(0, previous.indexOf("_")).equalsIgnoreCase(next.substring(0, next.indexOf("_")))))
			b = true;

		return b;
	}

	public static String getStringFromDate(Date input, String format) {
		try {
			SimpleDateFormat sdf = new SimpleDateFormat(format);
			return sdf.format(input);
		} catch (Exception e) {
		}
		return null;
	}

	public static String getStringFromTime(Time input, String format) {
		try {
			SimpleDateFormat sdf = new SimpleDateFormat(format);
			return sdf.format(input);
		} catch (Exception e) {
		}
		return null;
	}

	public static Calendar getCalendarFromString(String input, String format) {
		Calendar calendar = new GregorianCalendar();
		try {
			SimpleDateFormat sdf = new SimpleDateFormat(format);
			calendar.setTime(sdf.parse(input));
		} catch (Exception e) {
			return null;
		}
		return calendar;
	}

	public static Date getDateFromString(String input, String format) {
		Calendar calendar = new GregorianCalendar();
		try {
			SimpleDateFormat sdf = new SimpleDateFormat(format);
			calendar.setTime(sdf.parse(input));
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return calendar.getTime();
	}

	public static boolean alreadyContain(long[] ls, long l) {
		for (int i = 0; i < ls.length; ++i) {
			if (ls[i] == l)
				return true;
		}
		return false;
	}

	public static Vector<String> monthList() {
		Vector strings = new Vector();
		for (int i = 0; i < m.length; ++i) {
			strings.add(m[i]);
		}
		return strings;
	}

	public static int getMonthIndex(String string) {
		for (int i = 0; i < m.length; ++i) {
			if (string.equalsIgnoreCase(m[i])) {
				return i;
			}
		}
		return -1;
	}

	public static String getMonth(int index) {
		return m[index];
	}

	public static boolean[] convertFromString(boolean[] bs, String readonlyTabs) {
		if ((readonlyTabs != null) && (readonlyTabs.length() > 0)) {
			StringTokenizer stringTokenizer = new StringTokenizer(readonlyTabs, ",");
			while (stringTokenizer.hasMoreTokens())
				try {
					int j = Integer.parseInt(stringTokenizer.nextToken());
					bs[(j - 1)] = true;
				} catch (Exception localException) {
				}
		}
		return bs;
	}

	public static boolean isNumeric(String str) {
		for (int i = 0; i < str.length(); ++i) {
			if (!(Character.isDigit(str.charAt(i))))
				return false;
		}
		return true;
	}

	public static Date joinDateAndTime(Date date, Time time) {
		Calendar c1 = new GregorianCalendar();
		Calendar c2 = new GregorianCalendar();
		c1.setTime(date);
		c2.setTime(time);
		c1.set(10, c2.get(10));
		c1.set(12, c2.get(12));
		c1.set(13, c2.get(13));
		return c1.getTime();
	}

	public static void main(String[] args) {
		System.out.println("test : " + randomin(3, 1, 3, 1, false));
	}

	public static byte[] encrypt(String input)
			throws InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
		cipher.init(1, key);
		byte[] inputBytes = input.getBytes();
		return cipher.doFinal(inputBytes);
	}

	public static String decrypt(byte[] encryptionBytes)
			throws InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
		cipher.init(2, key);
		byte[] recoveredBytes = cipher.doFinal(encryptionBytes);
		String recovered = new String(recoveredBytes);
		return recovered;
	}

	public static void setUp() throws Exception {
		key = KeyGenerator.getInstance(algorithm).generateKey();
		cipher = Cipher.getInstance(algorithm);
	}
}