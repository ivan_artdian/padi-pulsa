package corp.sTech.padi.util;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.context.FacesContext;

public class MessageUtil {
	private static final Severity INFO = FacesMessage.SEVERITY_INFO;
	private static final Severity ERROR = FacesMessage.SEVERITY_ERROR;

	public static void addMessageInfo(String keyMessage) {
		String message = LocalizationUtil.getI18nValue(keyMessage);
		showMessage(message, INFO);
	}

	public static void addMessageError(String keyMessage) {
		String message = LocalizationUtil.getI18nValue(keyMessage);
		showMessage(message, ERROR);
	}

	public static void addMessageInfo(String keyMessage, String param[]) {
		String message = LocalizationUtil.getI18nValue(keyMessage, param);
		showMessage(message, INFO);
	}

	public static void addMessageError(String keyMessage, String param[]) {
		String message = LocalizationUtil.getI18nValue(keyMessage, param);
		showMessage(message, ERROR);
	}
	
	public static void addMessageInfoAdditional(String additionalMessage) {
		String message = additionalMessage;
		showMessage(message, INFO);
	}
	
	public static void addMessageErrorAdditional(String additionalMessage) {
		String message = additionalMessage;
		showMessage(message, INFO);
	}

	private static void showMessage(String message, Severity severity) {
		try {
			if (message.equals("error get i18n value")) {
				severity = ERROR;
			}
			FacesContext.getCurrentInstance().addMessage("growl",
					new FacesMessage(severity, severity.toString().split(" ")[0], message));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
