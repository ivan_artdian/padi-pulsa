package corp.sTech.padi.bean;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import corp.sTech.padi.model.Approval;
import corp.sTech.padi.model.SystemParameter;
import corp.sTech.padi.service.SystemParameterService;
import corp.sTech.padi.service.TaskService;
import corp.sTech.padi.util.MessageUtil;
import corp.sTech.padi.util.SessionMapUtil;

@ManagedBean
@ViewScoped
public class SourceBillerHandler {
	private SystemParameter systemParameter;
	private SystemParameter transSystemParameter;
	private String sysParamBiller;
	
	@ManagedProperty(value = "#{systemParameterService}")
	private SystemParameterService service; 

	@ManagedProperty(value = "#{taskService}")
	private TaskService taskService;
	private final String[] messageParams = new String[]{"SystemParameter"};
	
	private String biller;
	
	@PostConstruct
	public void init(){
		systemParameter = service.findSystemParameter("SourceBiller");
		biller = systemParameter.getParamValue();
		
		newTransSystem();
	}
	
	public void getSysParamName(String paramName){
		service.findSystemParameter(paramName);
	}
	
	public void editSystemParameter(){
		try {
			
			systemParameter.setParamValue(biller);;

			saveTask("Edit System Parameter", systemParameter, systemParameter);
			init();
		} catch (Exception e) {
			MessageUtil.addMessageError("common.message.error.edit", messageParams);
			return;
		}
	}
	
	public void newTransSystem(){
		transSystemParameter = new SystemParameter();
	}
	
	
	private boolean saveTask(String taskName, Object oldData, Object newData){
		Approval approval = new Approval(taskName, SystemParameter.class, SessionMapUtil.getGroupNameSession(),
				SessionMapUtil.getUrlApproval());
		return taskService.saveTask(oldData == null ? new SystemParameter() : oldData,
				newData == null ? new SystemParameter() : newData, approval);		
	}
	
	public void preAddSystemParameter() {
		transSystemParameter = new SystemParameter();
	}


	public SystemParameter getTransSystemParameter() { 
		return transSystemParameter;
	}


	public void setTransSystemParameter(SystemParameter transSystemParameter) {
		this.transSystemParameter = transSystemParameter;
	}


	public SystemParameterService getService() {
		return service;
	}


	public void setService(SystemParameterService service) {
		this.service = service;
	}


	public TaskService getTaskService() {
		return taskService;
	}


	public void setTaskService(TaskService taskService) {
		this.taskService = taskService;
	}

	public String[] getMessageParams() {
		return messageParams;
	}

	public String getSysParamBiller() {
		return sysParamBiller;
	}

	public void setSysParamBiller(String sysParamBiller) {
		this.sysParamBiller = sysParamBiller;
	}

	public SystemParameter getSystemParameter() {
		return systemParameter;
	}

	public void setSystemParameter(SystemParameter systemParameter) {
		this.systemParameter = systemParameter;
	}

	public String getBiller() {
		return biller;
	}

	public void setBiller(String biller) {
		this.biller = biller;
	}
}
