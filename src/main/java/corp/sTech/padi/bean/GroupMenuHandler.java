package corp.sTech.padi.bean;

import corp.sTech.padi.model.Approval;
import corp.sTech.padi.model.GroupMenu;
import corp.sTech.padi.service.GroupMenuService;
import corp.sTech.padi.service.TaskService;
import corp.sTech.padi.util.MessageUtil;
import corp.sTech.padi.util.SessionMapUtil;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

@ManagedBean
@ViewScoped
public class GroupMenuHandler {
	
	private List<GroupMenu> groupMenu;
	private GroupMenu transGroupMenu;
	
	@ManagedProperty(value = "#{groupMenuService}")
	private GroupMenuService service;
	
	@ManagedProperty(value = "#{taskService}")
	private TaskService taskService;
	private final String[] messageParams = new String[]{"GroupMenu"};
	
	private List<GroupMenu> availGroupMenus =  new ArrayList<>();
	
	@PostConstruct
	public void init(){
		groupMenu = getAllGroupMenu();
		newTransGroupMenu();
	}
	
	public void addGroupMenu(){
		try{
			saveTask("Add Menu", null, transGroupMenu);
			init();
		} catch (Exception e) {
			MessageUtil.addMessageError("common.message.error.add", messageParams);
			return;
		}
	}
	
	public void deleteGroupMenu(){
		try {
			saveTask("Delete Menu", transGroupMenu, null);
			init();
		} catch (Exception e) {
			MessageUtil.addMessageError("common.message.error.delete", messageParams);
			return;
		}
	}
	
	public void editGroupMenu(){
		try {
			GroupMenu oldData = service.findOne(transGroupMenu);
			saveTask("Edit Menu", oldData, transGroupMenu);
			init();
		} catch (Exception e) {
			MessageUtil.addMessageError("common.message.error.delete", messageParams);
			return;
		}
	}
	
	public void newTransGroupMenu(){
		transGroupMenu = new GroupMenu();
	}
	
	private boolean saveTask(String taskName, Object oldData, Object newData){
		Approval approval = new Approval(taskName, GroupMenu.class, SessionMapUtil.getGroupNameSession(),
				SessionMapUtil.getUrlApproval());
		return taskService.saveTask(oldData == null ? new GroupMenu() : oldData,
				newData == null ? new GroupMenu() : newData, approval);		
	}
	
	public void preAddGroupMenu() {
		transGroupMenu = new GroupMenu();
	}
	
	private List<GroupMenu> getAllGroupMenu(){
		return service.findAll();
	}

	public List<GroupMenu> getGroupMenu() {
		return groupMenu;
	}

	public void setGroupMenu(List<GroupMenu> groupMenu) {
		this.groupMenu = groupMenu;
	}

	public GroupMenu getTransGroupMenu() {
		return transGroupMenu;
	}

	public void setTransGroupMenu(GroupMenu transGroupMenu) {
		this.transGroupMenu = transGroupMenu;
	}

	public GroupMenuService getService() {
		return service;
	}

	public void setService(GroupMenuService service) {
		this.service = service;
	}

	public TaskService getTaskService() {
		return taskService;
	}

	public void setTaskService(TaskService taskService) {
		this.taskService = taskService;
	}

	public List<GroupMenu> getAvailGroupMenus() {
		return availGroupMenus;
	}

	public void setAvailGroupMenus(List<GroupMenu> availGroupMenus) {
		this.availGroupMenus = availGroupMenus;
	}

	public String[] getMessageParams() {
		return messageParams;
	}
	
	
	

}
