package corp.sTech.padi.bean;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import corp.sTech.padi.model.Approval;
import corp.sTech.padi.model.GroupMenu;
import corp.sTech.padi.model.Menu;
import corp.sTech.padi.service.MenuManagementService;
import corp.sTech.padi.service.TaskService;
import corp.sTech.padi.util.MessageUtil;
import corp.sTech.padi.util.SessionMapUtil;

@ManagedBean
@ViewScoped
public class MenuManagementHandler {

	private List<Menu> menus;
	private Menu transMenu;

	@ManagedProperty(value = "#{menuManagementService}")
	private MenuManagementService service;
	@ManagedProperty(value = "#{taskService}")
	private TaskService taskService;
	private final String[] messageParams = new String[] { "Menu" };

	private List<GroupMenu> availGroupMenu = new ArrayList<>();

	@PostConstruct
	public void init() {
		menus = getAllMenus();
		newTransMenu();
		
		fillAvailGroupMenu();
	}

	private void fillAvailGroupMenu() {
		availGroupMenu = service.findAllGroupMenu();
	}

	public void addMenu() {
		try {
			saveTask("Add Menu", null, transMenu);
			init();
		} catch (Exception e) {
			MessageUtil.addMessageError("common.message.error.add", messageParams);
			return;
		}
	}

	public void deleteMenu() {
		try {
			saveTask("Delete Menu", transMenu, null);
			init();
		} catch (Exception e) {
			MessageUtil.addMessageError("common.message.error.delete", messageParams);
			return;
		}
	}

	public void editMenu() {
		try {
			Menu oldData = service.findOne(transMenu);
			saveTask("Edit Menu", oldData, transMenu);
			init();
		} catch (Exception e) {
			MessageUtil.addMessageError("common.message.error.edit", messageParams);
			return;
		}
	}

	public void newTransMenu() {
		transMenu = new Menu();
		transMenu.setMenuLevel(1);
	}

	private boolean saveTask(String taskName, Object oldData, Object newData) {
		Approval approval = new Approval(taskName, Menu.class, SessionMapUtil.getGroupNameSession(),
				SessionMapUtil.getUrlApproval());
		return taskService.saveTask(oldData == null ? new Menu() : oldData, newData == null ? new Menu() : newData,
				approval);
	}

	private List<Menu> getAllMenus() {
		return service.findAll();
	}

	public List<Menu> getMenus() {
		return menus;
	}

	public void setMenus(List<Menu> menus) {
		this.menus = menus;
	}

	public MenuManagementService getService() {
		return service;
	}

	public void setService(MenuManagementService service) {
		this.service = service;
	}

	public Menu getTransMenu() {
		return transMenu;
	}

	public void setTransMenu(Menu transMenu) {
		this.transMenu = transMenu;
	}

	public TaskService getTaskService() {
		return taskService;
	}

	public void setTaskService(TaskService taskService) {
		this.taskService = taskService;
	}

	public List<GroupMenu> getAvailGroupMenu() {
		return availGroupMenu;
	}

	public void setAvailGroupMenu(List<GroupMenu> availGroupMenu) {
		this.availGroupMenu = availGroupMenu;
	}
}
