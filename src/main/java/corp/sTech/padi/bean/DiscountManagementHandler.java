package corp.sTech.padi.bean;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import corp.sTech.padi.model.Approval;
import corp.sTech.padi.model.Discount;
import corp.sTech.padi.service.DiscountManagementService;
import corp.sTech.padi.service.TaskService;
import corp.sTech.padi.util.MessageUtil;
import corp.sTech.padi.util.RandomString;
import corp.sTech.padi.util.SessionMapUtil;


@ManagedBean
@ViewScoped
public class DiscountManagementHandler {
	private List<Discount> discounts;
	private Discount transDiscount;
	
	@ManagedProperty(value = "#{discountManagementService}")
	private DiscountManagementService service;
	private final String[] messageParams = new String[] { "Discount" };

	@ManagedProperty(value = "#{taskService}")
	private TaskService taskService;
	
	@PostConstruct
	public void init() {
		discounts = getAllDiscounts();
		newTransDiscount();
	}

	public void newTransDiscount() {
		transDiscount = new Discount();
	}
	
	public void preAddDiscount() {
		newTransDiscount();
	}

	public void addDiscount() {
		try {
			transDiscount.setIdDiscount(RandomString.generateRandomString());
			transDiscount.setStatus(1);
			transDiscount.setQuota(0);
			transDiscount.setTnc("Undefine");
			
			if (transDiscount.getCriteria()==null) transDiscount.setCriteria("0");
			
			saveTask("Add Discount", null, transDiscount);
			init();
		} catch (Exception e) {
			MessageUtil.addMessageError("common.message.error.add", messageParams);
			return;
		}
	}

	public void editDiscount() {
		try {
			Discount oriDiscount = service.getDiscount(transDiscount);

			saveTask("Edit Discount", oriDiscount, transDiscount);
			init();
		} catch (Exception e) {
			e.printStackTrace();
			MessageUtil.addMessageError("common.message.error.edit", messageParams);
			return;
		}
	}

	public void preEditDiscount(Discount discount) {
		transDiscount = discount;
	}

	public void DeleteDiscount() {
		try {
			Discount newTransDiscount = transDiscount;
			newTransDiscount.setStatus(0);
			saveTask("Delete Discount", transDiscount, newTransDiscount);
			init();
		} catch (Exception e) {
			MessageUtil.addMessageError("common.message.error.delete", messageParams);
			return;
		}
	}

	public void preDeleteDiscount(Discount discount) {
		transDiscount = discount;
	}

	private boolean saveTask(String taskName, Object oldData, Object newData) {
		Approval approval = new Approval(taskName, Discount.class, SessionMapUtil.getGroupNameSession(),
				SessionMapUtil.getUrlApproval());
		return taskService.saveTask(oldData == null ? new Discount() : oldData, newData == null ? new Discount() : newData,
				approval);
	}

	private List<Discount> getAllDiscounts() {
		return service.getAllDiscounts();
	}
	
	public List<Discount> getDiscounts() {
		return discounts;
	}

	public void setDiscounts(List<Discount> discounts) {
		this.discounts = discounts;
	}

	public Discount getTransDiscount() {
		return transDiscount;
	}

	public void setTransDiscount(Discount transDiscount) {
		this.transDiscount = transDiscount;
	}

	public DiscountManagementService getService() {
		return service;
	}

	public void setService(DiscountManagementService service) {
		this.service = service;
	}

	public String[] getMessageParams() {
		return messageParams;
	}

	public TaskService getTaskService() {
		return taskService;
	}

	public void setTaskService(TaskService taskService) {
		this.taskService = taskService;
	}
}
