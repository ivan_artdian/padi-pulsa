package corp.sTech.padi.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import corp.sTech.padi.model.Approval;
import corp.sTech.padi.model.Menu;
import corp.sTech.padi.model.Role;
import corp.sTech.padi.model.RoleMenu;
import corp.sTech.padi.modelConverter.MenuConverter;
import corp.sTech.padi.service.RoleManagementService;
import corp.sTech.padi.service.TaskService;
import corp.sTech.padi.util.MessageUtil;
import corp.sTech.padi.util.SessionMapUtil;

@ManagedBean
@ViewScoped
public class RoleManagementHandler implements Serializable {
	private static final long serialVersionUID = -7011448344156880913L;
	private List<Role> roles;
	private Role transRole;
	private RoleMenu transRoleMenu;
	private List<RoleMenu> roleMenus;

	private List<Menu> availableMenu;

	@ManagedProperty(value = "#{roleManagementService}")
	private RoleManagementService service;

	@ManagedProperty(value = "#{menuConverter}")
	private MenuConverter menuConverter;

	private final String[] messageParam = { "Role" };

	@ManagedProperty(value = "#{taskService}")
	private TaskService taskService;

	@PostConstruct
	public void init() {
		roles = getAllRoles();
		newTransRole();
		newTransRoleMenu();

		roleMenus = new ArrayList<RoleMenu>();
		availableMenu = new ArrayList<Menu>();
	}

	public void loadMenu(Role role) {
		transRole = role;
		roleMenus = service.getAssignRoleMenu(role);
		List<Menu> assignedMenu = new ArrayList<Menu>();
		for (RoleMenu roleMenu : roleMenus) {
			assignedMenu.add(roleMenu.getMenu());
		}

		availableMenu = service.loadAllMenu();
		availableMenu.removeAll(assignedMenu);
	}

	public void addRole() {
		try {
			saveTask("Add Role", null, transRole);
			init();
		} catch (Exception e) {
			MessageUtil.addMessageError("common.message.error.add", messageParam);
		}
	}

	public void editRole() {
		try {
			/*service.deleteAllRoleMenuByRole(transRole);
			service.saveRoleMenus(roleMenus);
			
			transRole.setRoleMenus(roleMenus);
			*/
			Role oriRole = service.findWithRoleMenuByRoleIdEagerly(transRole.getRoleId());
			transRole.setRoleMenus(roleMenus);
			saveTask("Edit Role", oriRole, transRole);
			init();
		} catch (Exception e) {
			MessageUtil.addMessageError("common.message.error.edit", messageParam);
		}
	}

	public void deleteRole() {
		try {
			transRole.setRoleMenus(roleMenus);
			saveTask("Delete Role", transRole, null);

			init();
		} catch (Exception e) {
			MessageUtil.addMessageError("common.message.error.delete", messageParam);
		}
	}

	private boolean saveTask(String taskName, Object oldData, Object newData) {
		Approval approval = new Approval(taskName, Role.class, SessionMapUtil.getGroupNameSession(),
				SessionMapUtil.getUrlApproval());
		return taskService.saveTask(oldData == null ? new Role() : oldData, newData == null ? new Role() : newData,
				approval);
	}

	public void addRoleMenusItem() {
		RoleMenu roleMenu = service.setRoleMenu(transRole);
		roleMenus.add(0, roleMenu);
	}

	public void newTransRole() {
		transRole = new Role();
	}

	public void newTransRoleMenu() {
		transRoleMenu = new RoleMenu();
	}
	
	public void preAddRole(){
		transRole = new Role();
	}

	public void preEditRM(RoleMenu roleMenu) {
		transRoleMenu = roleMenu;
	}

	public void preEditRole(Role role) {
		transRole = role;
	}

	public void deleteRM() {
		roleMenus.remove(transRoleMenu);
	}

	public void preDeleteRole(Role role) {
		transRole = role;
	}

	public String returnEmpty() {
		return "";
	}

	private List<Role> getAllRoles() {
		return service.findAll();
	}

	public List<Role> getRoles() {
		return roles;
	}

	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}

	public RoleManagementService getService() {
		return service;
	}

	public void setService(RoleManagementService service) {
		this.service = service;
	}

	public Role getTransRole() {
		return transRole;
	}

	public void setTransRole(Role transRole) {
		this.transRole = transRole;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public List<RoleMenu> getRoleMenus() {
		return roleMenus;
	}

	public void setRoleMenus(List<RoleMenu> roleMenus) {
		this.roleMenus = roleMenus;
	}

	public List<Menu> getAvailableMenu() {
		return availableMenu;
	}

	public void setAvailableMenu(List<Menu> availableMenu) {
		this.availableMenu = availableMenu;
	}

	public RoleMenu getTransRoleMenu() {
		return transRoleMenu;
	}

	public void setTransRoleMenu(RoleMenu transRoleMenu) {
		this.transRoleMenu = transRoleMenu;
	}

	public MenuConverter getMenuConverter() {
		return menuConverter;
	}

	public void setMenuConverter(MenuConverter menuConverter) {
		this.menuConverter = menuConverter;
	}

	public TaskService getTaskService() {
		return taskService;
	}

	public void setTaskService(TaskService taskService) {
		this.taskService = taskService;
	}
}
