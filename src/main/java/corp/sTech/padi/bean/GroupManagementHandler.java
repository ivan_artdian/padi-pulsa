package corp.sTech.padi.bean;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.primefaces.event.FileUploadEvent;

import corp.sTech.padi.model.Approval;
import corp.sTech.padi.model.Group;
import corp.sTech.padi.parameter.ActiveParam;
import corp.sTech.padi.service.GroupManagementService;
import corp.sTech.padi.service.TaskService;
import corp.sTech.padi.util.MessageUtil;
import corp.sTech.padi.util.SessionMapUtil;
import corp.sTech.padi.util.StreamUtil;

@ManagedBean
@ViewScoped
public class GroupManagementHandler {

	private List<Group> groups;
	private Group transGroup;

	@ManagedProperty(value = "#{groupManagementService}")
	private GroupManagementService service;
	private final String[] messageParams = new String[] { "Group" };

	@ManagedProperty(value = "#{taskService}")
	private TaskService taskService;

	@PostConstruct
	public void init() {
		groups = getAllGroups();
		newTransGroup();
	}

	public boolean isGroupDepedency(Group group) {
		return service.isGroupDepedency(group);
	}

	public void uploadImageHeader(FileUploadEvent event) {
		String filename = StreamUtil.doUpload(event, transGroup.getUsername());
		transGroup.setImageHeader(filename);
	}

	public void addGroup() {
		try {
			transGroup.setPassword("");
			transGroup.setStatus(ActiveParam.ACTIVE);
			saveTask("Add Group", null, transGroup);
			init();
		} catch (Exception e) {
			MessageUtil.addMessageError("common.message.error.add", messageParams);
		}
	}

	public void deleteGroup() {
		try {
			saveTask("Delete Group", transGroup, null);
			init();
		} catch (Exception e) {
			MessageUtil.addMessageError("common.message.error.delete", messageParams);
		}
	}

	public void newTransGroup() {
		transGroup = new Group();
	}

	public void preAddGroup() {
		transGroup = new Group();
	}

	public void preEditGroup(Group group) {
		transGroup = group;
	}

	public void preDeleteGroup(Group group) {
		transGroup = group;
	}

	public String editGroup() {
		try {
			Group oriGroup = service.getOneGroup(transGroup);

			saveTask("Edit Group", oriGroup, transGroup);
			init();
		} catch (Exception e) {
			MessageUtil.addMessageError("common.message.error.edit", messageParams);
		}
		return "/pages/main-page.xhtml?faces-redirect=true";
	}

	private boolean saveTask(String taskName, Object oldData, Object newData) {
		Approval approval = new Approval(taskName, Group.class, SessionMapUtil.getGroupNameSession(),
				SessionMapUtil.getUrlApproval());
		return taskService.saveTask(oldData == null ? new Group() : oldData, newData == null ? new Group() : newData,
				approval);
	}

	private List<Group> getAllGroups() {
		return service.getAllGroups();
	}

	public List<Group> getGroups() {
		return groups;
	}

	public void setGroups(List<Group> groups) {
		this.groups = groups;
	}

	public Group getTransGroup() {
		return transGroup;
	}

	public void setTransGroup(Group transGroup) {
		this.transGroup = transGroup;
	}

	public GroupManagementService getService() {
		return service;
	}

	public void setService(GroupManagementService service) {
		this.service = service;
	}

	public TaskService getTaskService() {
		return taskService;
	}

	public void setTaskService(TaskService taskService) {
		this.taskService = taskService;
	}

	public String[] getMessageParams() {
		return messageParams;
	}
}
