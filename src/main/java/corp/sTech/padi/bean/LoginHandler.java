package corp.sTech.padi.bean;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

import corp.sTech.padi.model.Agent;
import corp.sTech.padi.model.User;
import corp.sTech.padi.parameter.SystemParameter;
import corp.sTech.padi.service.LoginService;
import corp.sTech.padi.util.MessageUtil;
import corp.sTech.padi.util.SecureLoginUtil;
import corp.sTech.padi.util.SessionMapUtil;
import corp.sTech.padi.util.TransactionUtils;

@ManagedBean
@SessionScoped
public class LoginHandler {

	private String username;
	private String password;

	@ManagedProperty(value = "#{loginService}")
	private LoginService loginService;

	/************ skip group login ****************/
	/*
	 * @ManagedProperty(value = "#{inititiatorHandler}") private InititiatorHandler
	 * inititiatorHandler;
	 */

	private String backgroundUrl;

	@PostConstruct
	public void init() {
		/************ skip group login ****************/
		// backgroundUrl = inititiatorHandler.getGroup().getBackgroundUrl();
	}

	public String login() {
		/************ skip group login ****************/
		// User user = loginService.doLogin(username, password,
		// inititiatorHandler.getGroup());
		// User user = loginService.doLogin(username, password);
		try {
			User user = loginService.findUser(username);
			if (null != user) {
				String storedPassword = user.getPassword();
				boolean valid = SecureLoginUtil.validatePassword(password, storedPassword);
				if (valid) {
					if (user.isLogin()) {
						MessageUtil.addMessageError("common.message.error.userStillLogin");
						// disable on debug
						// return "";
					}
					user.setLogin(true);
					user.setLastLogin(new Date());
					user.setLoginTime(new Date());
					user.setLogoutTime(null);
					user = loginService.updateLogin(user);
					SessionMapUtil.put("user", user);
					SessionMapUtil.put("agent", loginService.getAgentByGroup(SessionMapUtil.getGroupSession()));
					SessionMapUtil.put("isSuperAdministrator",
							SessionMapUtil.getGroupNameSession().equals("SUPER_ADMINISTRATOR"));
				} else {
					MessageUtil.addMessageError("common.message.error.incorrectPassword");
					return "";
				}
			} else {
				MessageUtil.addMessageError("common.message.error.userNotFound");
				return "";
			}
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (InvalidKeySpecException e) {
			e.printStackTrace();
		}
		return "/pages/main-page.xhtml?faces-redirect=true";
	}

	public void logout() {
		User user = SessionMapUtil.getUserSession();
		user.setLogin(false);
		user.setLogoutTime(new Date());
		loginService.updateLogin(user);
		Agent agent = SessionMapUtil.getAgentSession();
		loginService.doLogout();
	}

	public String getBackgroundUrl() {
		return backgroundUrl;
	}

	public void setBackgroundUrl(String backgroundUrl) {
		this.backgroundUrl = backgroundUrl;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public LoginService getLoginService() {
		return loginService;
	}

	public void setLoginService(LoginService loginService) {
		this.loginService = loginService;
	}

	/************ skip group login ****************/
	/*
	 * public InititiatorHandler getInititiatorHandler() { return
	 * inititiatorHandler; }
	 * 
	 * public void setInititiatorHandler(InititiatorHandler inititiatorHandler) {
	 * this.inititiatorHandler = inititiatorHandler; }
	 */
}
