package corp.sTech.padi.bean;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import corp.sTech.padi.model.Approval;
import corp.sTech.padi.model.SystemParameter;
import corp.sTech.padi.service.SystemParameterService;
import corp.sTech.padi.service.TaskService;
import corp.sTech.padi.util.MessageUtil;
import corp.sTech.padi.util.ReflectionUtil;
import corp.sTech.padi.util.SessionMapUtil;

@ManagedBean
@ViewScoped
public class SystemParameterHandler {
	
	private List<SystemParameter> systemParameter;
	private SystemParameter transSystemParameter;
	private List<String> availSystemParameter;
	
	@ManagedProperty(value = "#{systemParameterService}")
	private SystemParameterService service; 

	@ManagedProperty(value = "#{taskService}")
	private TaskService taskService;
	private final String[] messageParams = new String[]{"SystemParameter"};
	
	private List<SystemParameter> availGroupSystemParameter = new ArrayList<>();
	
	private String harga;
	private String denda;
	
	@PostConstruct
	public void init(){
		systemParameter = getAllSystemParameter();  
		System.out.println(systemParameter.size());
		
		for (SystemParameter sp : systemParameter) {
			if(corp.sTech.padi.parameter.SystemParameter.IPL_PARAMATER_HARGA.equals(sp.getParamName())) {
				harga = sp.getParamValue();
			}else if (corp.sTech.padi.parameter.SystemParameter.IPL_PARAMATER_DENDA.equals(sp.getParamName())){
				denda = sp.getParamValue();
			}
		}
		
		newTransSystem();
		newAvailSystemParameter();
	}
	
	public void getSysParamName(String paramName){
		service.findSystemParameter(paramName);
	}
	
	
	public void addSystemParameter(){
		try { 
			List<SystemParameter> systemParameters = new ArrayList<>();
			SystemParameter spHarga = new SystemParameter();
			spHarga.setParamName(corp.sTech.padi.parameter.SystemParameter.IPL_PARAMATER_HARGA);
			spHarga.setParamValue(harga);
			spHarga.setAgent(SessionMapUtil.getAgentSession());
			systemParameters.add(spHarga);

			SystemParameter spDenda = new SystemParameter();
			spDenda.setParamName(corp.sTech.padi.parameter.SystemParameter.IPL_PARAMATER_DENDA);
			spDenda.setParamValue(denda);
			spDenda.setAgent(SessionMapUtil.getAgentSession());
			systemParameters.add(spDenda);
			
			saveTask("Add System", null, systemParameters);
			init();
		} catch (Exception e) {
			MessageUtil.addMessageError("common.message.error.add", messageParams);
			return;
		}
	}
	
	public void deleteSystemParameter(){
		try {
			List<SystemParameter> systemParameters = systemParameter;
			SystemParameter spHarga = new SystemParameter();
			spHarga.setParamName(corp.sTech.padi.parameter.SystemParameter.IPL_PARAMATER_HARGA);
			spHarga.setParamValue(harga);
			spHarga.setAgent(SessionMapUtil.getAgentSession());
			systemParameters.add(spHarga);

			SystemParameter spDenda = new SystemParameter();
			spDenda.setParamName(corp.sTech.padi.parameter.SystemParameter.IPL_PARAMATER_DENDA);
			spDenda.setParamValue(denda);
			spDenda.setAgent(SessionMapUtil.getAgentSession());
			systemParameters.add(spDenda);
			
			saveTask("Delete System", systemParameters, null);
			init();
		} catch (Exception e) {
			MessageUtil.addMessageError("common.message.error.delete", messageParams);
			return;
		}
	}
	
	public void editSystemParameter(){
		try {
			List<SystemParameter> systemParameters = systemParameter;
			for (SystemParameter systemParameter : systemParameters) {
				if(systemParameter.getParamName().equals(corp.sTech.padi.parameter.SystemParameter.IPL_PARAMATER_HARGA)) {
					systemParameter.setParamValue(harga);
				}else if (systemParameter.getParamName().equals(corp.sTech.padi.parameter.SystemParameter.IPL_PARAMATER_DENDA)) {
					systemParameter.setParamValue(denda);
				}
			}
			
			
//			SystemParameter oldData = service.findOne(transSystemParameter);

			saveTask("Edit System Parameter", systemParameters, systemParameters);
			init();
		} catch (Exception e) {
			MessageUtil.addMessageError("common.message.error.edit", messageParams);
			return;
		}
	}
	
	public void newTransSystem(){
		transSystemParameter = new SystemParameter();
	}
	
	public void newAvailSystemParameter() {
		this.availSystemParameter = ReflectionUtil.getValueStringFromField(corp.sTech.padi.parameter.SystemParameter.class, "IPL_PARAMETER");
	}
	
	private boolean saveTask(String taskName, Object oldData, Object newData){
		Approval approval = new Approval(taskName, SystemParameter.class, SessionMapUtil.getGroupNameSession(),
				SessionMapUtil.getUrlApproval());
		return taskService.saveTask(oldData == null ? new SystemParameter() : oldData,
				newData == null ? new SystemParameter() : newData, approval);		
	}
	
	public void preAddSystemParameter() {
		transSystemParameter = new SystemParameter();
	}
		
	private List<SystemParameter> getAllSystemParameter() {
		return service.findAll();
	}
	

	public List<SystemParameter> getSystemParameter() { 
		return systemParameter;
	}


	public void setSystemParameter(List<SystemParameter> systemParameter) {
		this.systemParameter = systemParameter;
	}


	public SystemParameter getTransSystemParameter() { 
		return transSystemParameter;
	}


	public void setTransSystemParameter(SystemParameter transSystemParameter) {
		this.transSystemParameter = transSystemParameter;
	}


	public SystemParameterService getService() {
		return service;
	}


	public void setService(SystemParameterService service) {
		this.service = service;
	}


	public TaskService getTaskService() {
		return taskService;
	}


	public void setTaskService(TaskService taskService) {
		this.taskService = taskService;
	}


	public List<SystemParameter> getAvailGroupSystemParameter() {
		return availGroupSystemParameter;
	}


	public void setAvailGroupSystemParameter(
			List<SystemParameter> availGroupSystemParameter) {
		this.availGroupSystemParameter = availGroupSystemParameter;
	}


	public String[] getMessageParams() {
		return messageParams;
	}

	public List<String> getAvailSystemParameter() {
		return availSystemParameter;
	}

	public void setAvailSystemParameter(List<String> availSystemParameter) {
		this.availSystemParameter = availSystemParameter;
	}

	public String getHarga() {
		return harga;
	}

	public void setHarga(String harga) {
		this.harga = harga;
	}

	public String getDenda() {
		return denda;
	}

	public void setDenda(String denda) {
		this.denda = denda;
	}
}
