package corp.sTech.padi.bean;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import com.lowagie.text.BadElementException;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;

import corp.sTech.padi.model.Approval;
import corp.sTech.padi.model.TIbsBill;
import corp.sTech.padi.model.TIbsBillDetail;
import corp.sTech.padi.model.TIbsInvoice;
import corp.sTech.padi.parameter.ActiveParam;
import corp.sTech.padi.service.BillInvoiceManagementService;
import corp.sTech.padi.service.TaskService;
import corp.sTech.padi.util.MessageUtil;
import corp.sTech.padi.util.NumberFormatUtil;
import corp.sTech.padi.util.RandomString;
import corp.sTech.padi.util.SessionMapUtil;

@ManagedBean
@ViewScoped
public class InvoiceManagementHandler {

	private List<TIbsInvoice> invoices;
	private Map<String, List<TIbsInvoice>> invoicesMap = new HashMap<>();
	private TIbsInvoice transInvoice;
	private TIbsBillDetail transIbsBillDetail;

	@ManagedProperty(value = "#{billInvoiceManagementService}")
	private BillInvoiceManagementService service;
	@ManagedProperty(value = "#{taskService}")
	private TaskService taskService;
	private final String[] messageParams = new String[] { "Invoice" };

	private String fromMonth;
	private String toMonth;
	private String fromYear;
	private String toYear;

	private List<String> availDate = new ArrayList<>();
	private List<String> availIdPelanggan = new ArrayList<>();
	private List<String> selectedIdPelanggan = new ArrayList<>();

	private boolean checked = false;
	
	private List<TIbsBillDetail> dataTagihanPelanggan;
	private List<TIbsBillDetail> selectedDataTagihanPelanggan;

	@PostConstruct
	public void init() {
		invoices = getAllInvoices();
		mappingInvoice();
		newTransInvoice();
		transIbsBillDetail = new TIbsBillDetail();

		get10YearLater();
		fillAvailIdPelanggan();
		fillDataTagihanPelanggan();

		selectedIdPelanggan = new ArrayList<>();
	}

	private void mappingInvoice() {
		List<TIbsInvoice> data;
		for (TIbsInvoice ibsInvoice : invoices) {
			List<TIbsInvoice> invoices = invoicesMap.get(ibsInvoice.getBillPeriode());
			if (invoices == null) {
				data = new ArrayList<>();
			} else {
				data = invoices;
			}
			data.add(ibsInvoice);
			invoicesMap.put(ibsInvoice.getBillPeriode(), data);
		}

		if (invoicesMap.isEmpty()) {
			invoicesMap.put("No Record Found", null);
		}
	}

	public void checkAll() {
		if (checked) {
			selectedIdPelanggan = availIdPelanggan;
		} else {
			selectedIdPelanggan = new ArrayList<>();
		}
	}

	private void fillAvailIdPelanggan() {
		availIdPelanggan = service.getAllIdPelanggan();
	}
	
	private void fillDataTagihanPelanggan() {
		dataTagihanPelanggan = new ArrayList<TIbsBillDetail>();
		
		dataTagihanPelanggan = service.getAllBillDetails();
		
		System.out.println("Juancukkk "+dataTagihanPelanggan.size());
	}

	private void get10YearLater() {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		for (int i = 0; i < 10; i++) {
			availDate.add("" + calendar.get(Calendar.YEAR));
			calendar.add(Calendar.YEAR, 1);
		}
	}

	public void addInvoice() {
		boolean stop = false;
		String fromPeriode = fromMonth + "/" + fromYear;
		String toPeriode = toMonth + "/" + toYear;
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/yyyy");
		Date periode;
		Date finalPeriode;
		List<TIbsInvoice> tIbsInvoices = new ArrayList<>();
		Calendar calendar = Calendar.getInstance();
		try {
			calendar.setTime(simpleDateFormat.parse(fromPeriode));
			periode = calendar.getTime();
			finalPeriode = simpleDateFormat.parse(toPeriode);
		} catch (ParseException e) {
			e.printStackTrace();
			periode = new Date();
			finalPeriode = new Date();
		}

		try {
			/*
			 * List<TIbsBill> ibsBills = service.getAllIbsBills(); List<TIbsBillDetail>
			 * billDetails = null; if(ibsBills != null && ibsBills.size() > 0) { TIbsBill
			 * tIbsBill = ibsBills.get(0); billDetails = tIbsBill.getBillDetails(); }
			 */
			List<TIbsBillDetail> billDetails = null;
//			billDetails = service.getAllIbsBillDetailByIdPelanggan(selectedIdPelanggan);
			billDetails = selectedDataTagihanPelanggan;

			String hargaPerMeter = service.getHarga();
			if (hargaPerMeter == null) {
				throw new Exception("amount agent null");
			}

			int i = 0;
			while (!stop && billDetails != null && billDetails.size() > 0) {
				if (!periode.after(finalPeriode) && !finalPeriode.before(periode)) {
					for (TIbsBillDetail tIbsBillDetail : billDetails) {
						boolean isDuplicateBill = service.checkDuplicateBill(tIbsBillDetail, simpleDateFormat.format(periode));
						
						if(!isDuplicateBill) {
							TIbsInvoice invoice = new TIbsInvoice();
							invoice.setBillId(RandomString.generateRandom(8));
							invoice.setIbsBillDetail(tIbsBillDetail);
							invoice.setBillPeriode(simpleDateFormat.format(periode));
							invoice.setPaidStatus("UnPaid");
							invoice.setValid(ActiveParam.ACTIVE);
							invoice.setCreateDate(new Date());
	
							invoice.setAmount(String.valueOf(NumberFormatUtil.parseNumberToInt(hargaPerMeter)
									* NumberFormatUtil.parseNumberToInt(tIbsBillDetail.getLuasTanah())));
							tIbsInvoices.add(invoice);
						}
					}
					calendar.add(Calendar.MONTH, 1);
					periode = calendar.getTime();
					i++;
				} else {
					stop = true;
				}
			}

			saveTask("Add Invoice", null, tIbsInvoices);
			init();
		} catch (Exception e) {
			System.out.println(e.getStackTrace());
			MessageUtil.addMessageError("common.message.error.add", messageParams);
			e.printStackTrace();
			return;
		}
	}

	public void deleteInvoice() {
		try {
			saveTask("Delete Invoice", transInvoice, null);
			init();
		} catch (Exception e) {
			MessageUtil.addMessageError("common.message.error.delete", messageParams);
			return;
		}
	}

	public void editInvoice() {
		try {
			transInvoice.setBillPeriode(fromMonth+"/"+fromYear);
			TIbsInvoice oldData = service.findOneInvoice(transInvoice);
			saveTask("Edit Invoice", oldData, transInvoice);
//			init();
		} catch (Exception e) {
			MessageUtil.addMessageError("common.message.error.edit", messageParams);
			return;
		}
	}

	public void newTransInvoice() {
		transInvoice = new TIbsInvoice();

		fromMonth = null;
		toMonth = null;
		fromYear = null;
		toYear = null;

		selectedIdPelanggan = new ArrayList<>();
		checked = false;
	}
	
	public void preEditInvoice(TIbsInvoice ibsInvoice) {
		transInvoice = ibsInvoice;
		String[] billPeriode = ibsInvoice.getBillPeriode().split("/");
		fromMonth = billPeriode[0];
		fromYear = billPeriode[1];
	}

	private boolean saveTask(String taskName, Object oldData, Object newData) {
		Approval approval = new Approval(taskName, TIbsInvoice.class, SessionMapUtil.getGroupNameSession(),
				SessionMapUtil.getUrlApproval());
		return taskService.saveTask(oldData == null ? new TIbsInvoice() : oldData,
				newData == null ? new TIbsInvoice() : newData, approval);
	}
	
	 
    public void preProcessPDF(Object document) throws IOException, BadElementException, DocumentException {
        Document pdf = (Document) document;
        pdf.open();
         
        pdf.add(new Paragraph("Daftar Tagihan IPL"));
        pdf.add(new Paragraph("\n"));
    }

	private List<TIbsInvoice> getAllInvoices() {
		return service.getAllInvoice();
	}

	public List<TIbsInvoice> getInvoices() {
		return invoices;
	}

	public void setInvoices(List<TIbsInvoice> invoices) {
		this.invoices = invoices;
	}

	public TIbsInvoice getTransInvoice() {
		return transInvoice;
	}

	public void setTransInvoice(TIbsInvoice transInvoice) {
		this.transInvoice = transInvoice;
	}

	public BillInvoiceManagementService getService() {
		return service;
	}

	public void setService(BillInvoiceManagementService service) {
		this.service = service;
	}

	public TaskService getTaskService() {
		return taskService;
	}

	public void setTaskService(TaskService taskService) {
		this.taskService = taskService;
	}

	public String[] getMessageParams() {
		return messageParams;
	}

	public String getFromMonth() {
		return fromMonth;
	}

	public void setFromMonth(String fromMonth) {
		this.fromMonth = fromMonth;
	}

	public String getToMonth() {
		return toMonth;
	}

	public void setToMonth(String toMonth) {
		this.toMonth = toMonth;
	}

	public String getFromYear() {
		return fromYear;
	}

	public void setFromYear(String fromYear) {
		this.fromYear = fromYear;
	}

	public String getToYear() {
		return toYear;
	}

	public void setToYear(String toYear) {
		this.toYear = toYear;
	}

	public List<String> getAvailDate() {
		return availDate;
	}

	public void setAvailDate(List<String> availDate) {
		this.availDate = availDate;
	}

	public List<String> getSelectedIdPelanggan() {
		return selectedIdPelanggan;
	}

	public void setSelectedIdPelanggan(List<String> selectedIdPelanggan) {
		this.selectedIdPelanggan = selectedIdPelanggan;
	}

	public List<String> getAvailIdPelanggan() {
		return availIdPelanggan;
	}

	public void setAvailIdPelanggan(List<String> availIdPelanggan) {
		this.availIdPelanggan = availIdPelanggan;
	}

	public boolean isChecked() {
		return checked;
	}

	public void setChecked(boolean checked) {
		this.checked = checked;
	}

	public Map<String, List<TIbsInvoice>> getInvoicesMap() {
		return invoicesMap;
	}

	public void setInvoicesMap(Map<String, List<TIbsInvoice>> invoicesMap) {
		this.invoicesMap = invoicesMap;
	}

	public TIbsBillDetail getTransIbsBillDetail() {
		return transIbsBillDetail;
	}

	public void setTransIbsBillDetail(TIbsBillDetail transIbsBillDetail) {
		this.transIbsBillDetail = transIbsBillDetail;
	}

	public List<TIbsBillDetail> getDataTagihanPelanggan() {
		return dataTagihanPelanggan;
	}

	public void setDataTagihanPelanggan(List<TIbsBillDetail> dataTagihanPelanggan) {
		this.dataTagihanPelanggan = dataTagihanPelanggan;
	}

	public List<TIbsBillDetail> getSelectedDataTagihanPelanggan() {
		return selectedDataTagihanPelanggan;
	}

	public void setSelectedDataTagihanPelanggan(List<TIbsBillDetail> selectedDataTagihanPelanggan) {
		this.selectedDataTagihanPelanggan = selectedDataTagihanPelanggan;
	}
	
	

}
