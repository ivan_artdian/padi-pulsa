package corp.sTech.padi.bean;

import corp.sTech.padi.model.Approval;
import corp.sTech.padi.model.BillerProduct;
import corp.sTech.padi.service.BillerProductService;
import corp.sTech.padi.service.TaskService;
import corp.sTech.padi.util.MessageUtil;
import corp.sTech.padi.util.SessionMapUtil;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

@ManagedBean
@ViewScoped
public class BillerProductHandler {
	
	private List<BillerProduct> billerProducts;
	private BillerProduct transBillerProduct;
	
	@ManagedProperty(value = "#{billerProductsService}")
	private BillerProductService service;
	
	@ManagedProperty(value = "#{taskService}")
	private TaskService taskService;
	private final String[] messageParams = new String[]{"BillerProduct"};
	
	private List<BillerProduct> availBillerProducts =  new ArrayList<>();
	
	@PostConstruct
	public void init(){
		billerProducts = getAllBillerProduct();
		newTransBillerProduct();
	}
	
	public void addBillerProduct(){
		try{
			saveTask("Add BillerProduct", null, transBillerProduct);
			init();
		} catch (Exception e) {
			MessageUtil.addMessageError("common.message.error.add", messageParams);
			return;
		}
	}
	
	public void deleteBillerProduct(){
		try {
			saveTask("Delete BillerProduct", transBillerProduct, null);
			init();
		} catch (Exception e) {
			MessageUtil.addMessageError("common.message.error.delete", messageParams);
			return;
		}
	}
	
	public void editBillerProduct(){
		try {
			BillerProduct oldData = service.findOne(transBillerProduct);
			saveTask("Edit BillerProduct", oldData, transBillerProduct);
			init();
		} catch (Exception e) {
			MessageUtil.addMessageError("common.message.error.delete", messageParams);
			return;
		}
	}
	
	public void newTransBillerProduct(){
		transBillerProduct = new BillerProduct();
	}
	
	private boolean saveTask(String taskName, Object oldData, Object newData){
		Approval approval = new Approval(taskName, BillerProduct.class, SessionMapUtil.getGroupNameSession(),
				SessionMapUtil.getUrlApproval());
		return taskService.saveTask(oldData == null ? new BillerProduct() : oldData,
				newData == null ? new BillerProduct() : newData, approval);		
	}
	
	public void preAddBillerProduct() {
		transBillerProduct = new BillerProduct();
	}
	
	private List<BillerProduct> getAllBillerProduct(){
		return service.findAll();
	}

	public List<BillerProduct> getBillerProduct() {
		return billerProducts;
	}

	public void setBillerProduct(List<BillerProduct> billerProducts) {
		this.billerProducts = billerProducts;
	}

	public BillerProduct getTransBillerProduct() {
		return transBillerProduct;
	}

	public void setTransBillerProduct(BillerProduct transBillerProduct) {
		this.transBillerProduct = transBillerProduct;
	}

	public BillerProductService getService() {
		return service;
	}

	public void setService(BillerProductService service) {
		this.service = service;
	}

	public TaskService getTaskService() {
		return taskService;
	}

	public void setTaskService(TaskService taskService) {
		this.taskService = taskService;
	}

	public List<BillerProduct> getAvailBillerProducts() {
		return availBillerProducts;
	}

	public void setAvailBillerProducts(List<BillerProduct> availBillerProducts) {
		this.availBillerProducts = availBillerProducts;
	}

	public String[] getMessageParams() {
		return messageParams;
	}

}
