package corp.sTech.padi.bean;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;

import corp.sTech.padi.model.Approval;
import corp.sTech.padi.model.TIbsBill;
import corp.sTech.padi.model.TIbsBillDetail;
import corp.sTech.padi.parameter.ActiveParam;
import corp.sTech.padi.parameter.SystemParameter;
import corp.sTech.padi.service.BillInvoiceManagementService;
import corp.sTech.padi.service.TaskService;
import corp.sTech.padi.util.MessageUtil;
import corp.sTech.padi.util.NumberFormatUtil;
import corp.sTech.padi.util.RandomString;
import corp.sTech.padi.util.SessionMapUtil;
import corp.sTech.padi.util.StreamUtil;

@ManagedBean
@ViewScoped
public class BillInvoiceManagementHandler {
	private TIbsBill transBill;
	private TIbsBillDetail transBillDetail;

	@ManagedProperty(value = "#{billInvoiceManagementService}")
	private BillInvoiceManagementService service;
	@ManagedProperty(value = "#{taskService}")
	private TaskService taskService;

	private final String[] messageParams = new String[] { "Bill Invoice" };
	private final String[] messageParamsContent = new String[] { "Bill Detail" };
	private InputStream stream;

	private List<TIbsBill> ibsBills = new ArrayList<>();
	private List<TIbsBillDetail> ibsBillsDetails = new ArrayList<>();
	private List<TIbsBillDetail> transIbsBillsDetails = new ArrayList<>();

	@PostConstruct
	public void init() {
		newTransTIbsBill();
		ibsBills = getAllIbsBills();
		
		assignBillDetail();
	}

	private void assignBillDetail() {
		transBill = (ibsBills != null && ibsBills.size()>0) ? ibsBills.get(0) : null;
		if(transBill != null) {
			ibsBillsDetails = transBill.getBillDetails();
		}
	}

	public void postEdit() {
		ibsBills = getAllIbsBills();
		assignBillDetail();
	}

	private List<TIbsBill> getAllIbsBills() {
		List<TIbsBill> bills = service.getAllIbsBills();
		return bills;
	}

	public void addBillInvoice() {
		try {
			TIbsBill tIbsBill = service.findByAgent(SessionMapUtil.getAgentSession());
			List<TIbsBillDetail> ibsBillDetails = new ArrayList<>();
			if(tIbsBill != null) {
				transBill.setBillId(tIbsBill.getBillId());
//				ibsBillDetails = tIbsBill.getBillDetails();
			}
			transBill.setType(SystemParameter.IBS_BILL_TYPE_UPLOAD);
			transBill.setAgent(SessionMapUtil.getAgentSession());
			transBill.setValid(ActiveParam.ACTIVE);
			
			String filename = transBill.getFilename().toLowerCase();
			Workbook workbook = null;
			if (filename.endsWith("xls")) {
				workbook = new HSSFWorkbook(stream);
			} else if (filename.endsWith("xlsx")) {
				workbook = new XSSFWorkbook(stream);
			}
			Sheet sheet = workbook.getSheetAt(0);

			int billkeyColumnIndex = 0;
			int amountColumnIndex = 0;

			Iterator<Row> iterator = sheet.iterator();
			TIbsBillDetail ibsBillDetail = null;

//			String amount = service.getHarga();
//			if(amount == null) {
//				throw new Exception("amount agent null");
//			}
			
			String duplicateIdPelanggan = "";
			int duplicateCount = 0;

			boolean formatMatch = true;
			while (iterator.hasNext()) {
				System.out.println("===============");
				
				boolean fieldEmpty = false;
				boolean billDetailExist = false;
				Row row = iterator.next();
				int ibsBillDetailIndex = 0;
				
				if(row.getRowNum() == 0) {
					Iterator<Cell> cellIterator = row.cellIterator();
					
					while (cellIterator.hasNext() && formatMatch) {
						Cell cell = cellIterator.next();
						DataFormatter formatter = new DataFormatter();
	
						String value = formatter.formatCellValue(cell);
						System.out.println("value"+value);
						if(value != null && !value.isEmpty()) {
							int columnIndex = cell.getColumnIndex();
	
							ibsBillDetailIndex++;
							switch (columnIndex) {
								case 0:
									if(!"id_pelanggan".equals(value)) {
										formatMatch = false;
									}
									break;
								case 1:
									if(!"nam_pelanggan".equals(value)) {
										formatMatch = false;
									}
									break;
								case 2:
									if(!"no_hp".equals(value)) {
										formatMatch = false;
									}
									break;
								case 3:
									if(!"email".equals(value)) {
										formatMatch = false;
									}
									break;
								case 4:
									if(!"nama_perumahan".equals(value)) {
										formatMatch = false;
									}
									break;
								case 5:
									if(!"Alamat".equals(value)) {
										formatMatch = false;
									}
									break;
								case 6:
									if(!"no_block".equals(value)) {
										formatMatch = false;
									}
									break;
								case 7:
									if(!"Kel/Kec".equals(value)) {
										formatMatch = false;
									}
									break;
								case 8:
									if(!"Kab/Kota".equals(value)) {
										formatMatch = false;
									}
									break;
								case 9:
									if(!"Provinsi".equals(value)) {
										formatMatch = false;
									}
									break;
								case 10:
									if(!"luas_tanah".equals(value)) {
										formatMatch = false;
									}
									break;
								case 11:
									if(!"rtrw".equals(value)) {
										formatMatch = false;
									}
									break;
							default:
								System.out.println("default");
								formatMatch = false;
								break;
							}
						}else {
							System.out.println("else");
							formatMatch = false;
						}
					}
				}
				
				if (row.getRowNum() > 0 && formatMatch) {
					ibsBillDetail = new TIbsBillDetail();
					ibsBillDetail.setIbsBill(transBill);

					boolean nullOrEmptyIdPelanggan = true;
					Iterator<Cell> cellIterator = row.cellIterator();
					while (cellIterator.hasNext()) {
						Cell cell = cellIterator.next();
						DataFormatter formatter = new DataFormatter();

						String value = formatter.formatCellValue(cell);
						System.out.print("["+value+"] ");
						int columnIndex = cell.getColumnIndex();
						if(((value != null && !value.isEmpty()) || columnIndex == 2 || columnIndex == 3) && !fieldEmpty && !billDetailExist) {
							// content
	
							ibsBillDetailIndex++;
							switch (columnIndex) {
							// 0			1			2			3			4				5		6		7				8		9			10		11			12			13				
							// id_pelanggan	bln_tagihan	thn_tagihan	sts_tagihan	nama_pelanggan	no_hp	email	nama_perumahan	Alamat	no_block	Kel/Kec	Kab/Kota	Provinsi	luas_tanah
							case 0:
								nullOrEmptyIdPelanggan = false;
								boolean idPelangganExist = service.checkExistingIdPelanggan(value);
								if(idPelangganExist) {
									billDetailExist = true;
									duplicateCount++;
									duplicateIdPelanggan = duplicateIdPelanggan + ", "+value;
								}else {
									ibsBillDetail.setIdPelanggan(value);
								}
								
								break;
							/*case 1:
								ibsBillDetail.setBlnTagihan(value);
								break;
							case 2:
								ibsBillDetail.setThnTagihan(value);
								break;*/
							/*case 1:
								ibsBillDetail.setStsTagihan(value);
								break;*/
							case 1:
								ibsBillDetail.setNamaPelanggan(value);
								break;
							case 2:
								ibsBillDetail.setNoHP(value);
								break;
							case 3:
								ibsBillDetail.setEmail(value);
								break;
							case 4:
								ibsBillDetail.setNamaPerumahan(value);
								break;
							case 5:
								ibsBillDetail.setAlamat(value);
								break;
							case 6:
								ibsBillDetail.setNoBlock(value);
								break;
							case 7:
								ibsBillDetail.setKelKec(value);
								break;
							case 8:
								ibsBillDetail.setKabKota(value);
								break;
							case 9:
								ibsBillDetail.setProvinsi(value);
								break;
							case 10:
								ibsBillDetail.setLuasTanah(value);
								break;
							case 11:
								ibsBillDetail.setRtrw(value);
								break;
	
							default:
								break;
							}
						}else {
							fieldEmpty = true;
						}
					}
					ibsBillDetail.setCreateDate(new Date());
					ibsBillDetail.setActive(ActiveParam.ACTIVE);
//					ibsBillDetail.setAmount(String.valueOf(NumberFormatUtil.parseNumberToInt(amount) * NumberFormatUtil.parseNumberToInt(ibsBillDetail.getLuasTanah())));
					
					if(!fieldEmpty && !billDetailExist && !nullOrEmptyIdPelanggan) {
						ibsBillDetails.add(ibsBillDetail);
					}
				}
			}
			duplicateIdPelanggan = duplicateIdPelanggan.replaceFirst(", ", "");

			workbook.close();
			stream.close();

//			transBill.setBillDetails(ibsBillDetails);
			if(formatMatch) {
				boolean success = saveTaskDetail("Add Bill Detail", null, ibsBillDetails);
				if(success && duplicateCount > 0) {
					String msg = "Success add "+ibsBillDetails.size()+" data, fail add "+duplicateCount+" data ("+duplicateIdPelanggan+")";
					MessageUtil.addMessageInfoAdditional(msg);
				}else if(success) {
					MessageUtil.addMessageInfo("common.message.info.add", messageParamsContent);
				}
			}else {
				String msg = "Format Upload File Not Match";
				MessageUtil.addMessageErrorAdditional(msg);
			}
			init();
		} catch (Exception e) {
			e.printStackTrace();
			MessageUtil.addMessageError("common.message.error.add", messageParams);
			return;
		}
	}
	
	public void addBillInvoiceInput() {
		try {
			TIbsBill ibsBill = new TIbsBill();
			TIbsBill tIbsBill = service.findByAgent(SessionMapUtil.getAgentSession());
			
			if(tIbsBill != null) {
				ibsBill = tIbsBill;
//				transIbsBillsDetails.addAll(tIbsBill.getBillDetails());
			}
			ibsBill.setType(SystemParameter.IBS_BILL_TYPE_INPUT);
			ibsBill.setAgent(SessionMapUtil.getAgentSession());
			ibsBill.setValid(ActiveParam.ACTIVE);
			
			/*String amount = service.getHarga();
			if(amount == null) {
				throw new Exception("amount agent null");
			}*/
			
			String duplicateIdPelanggan = "";
			int duplicateCount = 0;
			List<TIbsBillDetail> data = new ArrayList<>();
			
			for (TIbsBillDetail tIbsBillDetail : transIbsBillsDetails) {
				String idPelanggan = tIbsBillDetail.getIdPelanggan();
				boolean idPelangganExist = service.checkExistingIdPelanggan(idPelanggan);
				if(idPelangganExist) {
					duplicateCount++;
					duplicateIdPelanggan = duplicateIdPelanggan + ", "+idPelanggan;
				}else {
					tIbsBillDetail.setIbsBill(ibsBill);
					tIbsBillDetail.setActive(ActiveParam.ACTIVE);
					data.add(tIbsBillDetail);
				}
				
//				tIbsBillDetail.setAmount(String.valueOf(NumberFormatUtil.parseNumberToInt(amount) * NumberFormatUtil.parseNumberToInt(tIbsBillDetail.getLuasTanah())));
			}
			duplicateIdPelanggan = duplicateIdPelanggan.replaceFirst(", ", "");
			
//				ibsBill.setBillDetails(transIbsBillsDetails);
			boolean success = saveTaskDetail("Add Bill Detail", null, data);
			if(success && duplicateCount > 0) {
				String msg = "Success add "+data.size()+" data, fail add "+duplicateCount+" data ("+duplicateIdPelanggan+")";
				MessageUtil.addMessageInfoAdditional(msg);
			}else if(success) {
				MessageUtil.addMessageInfo("common.message.info.add", messageParamsContent);
			}
			init();
		} catch (Exception e) {
			e.printStackTrace();
			MessageUtil.addMessageError("common.message.error.add", messageParams);
			return;
		}
	}

	public void deleteBillInvoice() {
		try {
			TIbsBill oldData = service.findOne(transBill);
			transBill.setValid(ActiveParam.INACTIVE);
			saveTask("Delete Bill Invoice", oldData, transBill);
			init();
		} catch (Exception e) {
			MessageUtil.addMessageError("common.message.error.delete", messageParams);
			return;
		}
	}

	public void deleteBillInvoiceContent() {
		try {
			transBillDetail.setActive(ActiveParam.INACTIVE);
			saveTaskDetail("Delete Bill Detail", null, transBillDetail);
			init();
		} catch (Exception e) {
			e.printStackTrace();
			MessageUtil.addMessageError("common.message.error.delete", messageParamsContent);
			return;
		}
	}

	public void editBillInvoice() {
		try {
			TIbsBill oldData = service.findOne(transBill);
			// updateBillCommerceStatus(transBill);
			saveTask("Edit Bill Invoice", oldData, transBill);
			// init();
			postEdit();
		} catch (Exception e) {
			MessageUtil.addMessageError("common.message.error.edit", messageParams);
			return;
		}
	}

	public void editBillInvoiceContent() {
		try {
			TIbsBill oldData = service.findOne(transBill);
			saveTask("Edit Bill Invoice Content", oldData, transBill);
			// init();
			postEdit();
		} catch (Exception e) {
			e.printStackTrace();
			MessageUtil.addMessageError("common.message.error.edit", messageParamsContent);
			return;
		}
	}

	public void preAddBillInvoice() {
		newTransTIbsBill();
	}
	public void preAddBillInvoiceInput() {
		newTransTIbsBill();
		transIbsBillsDetails = new ArrayList<>();
		
		addTransBillDetail();
		
//		return "/pages/bill-invoice-management-input.xhtml";
	}
	
	public void addTransBillDetail() {
		TIbsBillDetail billDetail = new TIbsBillDetail();
		billDetail.setBillsId(RandomString.generateRandom(8));
		billDetail.setAlamat(null);
		billDetail.setBlnTagihan(null);
		billDetail.setEmail(null);
		billDetail.setIdPelanggan(null);
		billDetail.setKabKota(null);
		billDetail.setKelKec(null);
		billDetail.setLuasTanah(null);
		billDetail.setNamaPelanggan(null);
		billDetail.setNoBlock(null);
		billDetail.setNoHP(null);
		billDetail.setProvinsi(null);
		billDetail.setStsTagihan(null);
		billDetail.setThnTagihan(null);
		billDetail.setRtrw(null);
		transIbsBillsDetails.add(billDetail);
	}

	public void preEditBillInvoice(TIbsBill ibsBill) {
		transBill = service.findOne(ibsBill);
	}
	
	public void preEditBillsDetail(TIbsBillDetail billDetail) {
		transBillDetail = billDetail;
	}

	public void preEditBillInvoiceContent(TIbsBillDetail ibsBillInfo) {
		transBillDetail = ibsBillInfo;
	}

	public void preDeleteBillInvoice(TIbsBill ibsBill) {
		transBill = service.findOne(ibsBill);
	}

	public void preDeleteBillInvoiceContent(TIbsBillDetail ibsBillInfo) {
		transBillDetail = ibsBillInfo;
	}
	public void preDeleteBillsDetail(TIbsBillDetail billDetail) {
		transBillDetail = billDetail;
	}

	public void newTransTIbsBill() {
		transBill = new TIbsBill();
		List<TIbsBillDetail> billInfos = new ArrayList<>();
		transBillDetail = new TIbsBillDetail();
	}

	private boolean saveTask(String taskName, Object oldData, Object newData) {
		Approval approval = new Approval(taskName, TIbsBill.class, SessionMapUtil.getGroupNameSession(),
				SessionMapUtil.getUrlApproval());
		return taskService.saveTask(oldData == null ? new TIbsBill() : oldData,
				newData == null ? new TIbsBill() : newData, approval);
	}
	private boolean saveTaskDetail(String taskName, Object oldData, Object newData) {
		Approval approval = new Approval(taskName, TIbsBillDetail.class, SessionMapUtil.getGroupNameSession(),
				SessionMapUtil.getUrlApproval());
		return taskService.saveTask(oldData == null ? new TIbsBillDetail() : oldData,
				newData == null ? new TIbsBillDetail() : newData, approval);
	}

	public void uploadFile(FileUploadEvent event) {
		stream = StreamUtil.getInputStream(event);
		transBill.setFilename(event.getFile().getFileName());
	}

	public void onSelectBill(SelectEvent event) {
		transBill = service.getDetailById(((TIbsBill) event.getObject()).getBillId());
		ibsBillsDetails = transBill.getBillDetails();
	}

	public TIbsBill getTransBill() {
		return transBill;
	}

	public void setTransBill(TIbsBill transBill) {
		this.transBill = transBill;
	}

	public BillInvoiceManagementService getService() {
		return service;
	}

	public void setService(BillInvoiceManagementService service) {
		this.service = service;
	}

	public TaskService getTaskService() {
		return taskService;
	}

	public void setTaskService(TaskService taskService) {
		this.taskService = taskService;
	}

	public InputStream getStream() {
		return stream;
	}

	public void setStream(InputStream stream) {
		this.stream = stream;
	}

	public List<TIbsBill> getIbsBills() {
		return ibsBills;
	}

	public void setIbsBills(List<TIbsBill> ibsBills) {
		this.ibsBills = ibsBills;
	}

	public String[] getMessageParams() {
		return messageParams;
	}

	public TIbsBillDetail getTransBillInfo() {
		return transBillDetail;
	}

	public void setTransBillInfo(TIbsBillDetail transBillInfo) {
		this.transBillDetail = transBillInfo;
	}

	public List<TIbsBillDetail> getIbsBillsDetails() {
		return ibsBillsDetails;
	}

	public void setIbsBillsDetails(List<TIbsBillDetail> ibsBillsDetails) {
		this.ibsBillsDetails = ibsBillsDetails;
	}

	public List<TIbsBillDetail> getTransIbsBillsDetails() {
		return transIbsBillsDetails;
	}

	public void setTransIbsBillsDetails(List<TIbsBillDetail> transIbsBillsDetails) {
		this.transIbsBillsDetails = transIbsBillsDetails;
	}

	public TIbsBillDetail getTransBillDetail() {
		return transBillDetail;
	}

	public void setTransBillDetail(TIbsBillDetail transBillDetail) {
		this.transBillDetail = transBillDetail;
	}
}
