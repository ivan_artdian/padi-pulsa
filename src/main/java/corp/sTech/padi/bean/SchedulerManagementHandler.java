package corp.sTech.padi.bean;

import static org.quartz.JobKey.jobKey;
import static org.quartz.TriggerBuilder.newTrigger;
import static org.quartz.TriggerKey.triggerKey;

import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;

import org.quartz.CronScheduleBuilder;
import org.quartz.Job;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.TriggerKey;
import org.quartz.impl.StdSchedulerFactory;
import org.reflections.Reflections;
import org.springframework.util.StringUtils;

import corp.sTech.padi.model.Agent;
import corp.sTech.padi.model.Approval;
import corp.sTech.padi.model.Scheduler;
import corp.sTech.padi.service.SchedulerManagementService;
import corp.sTech.padi.service.TaskService;
import corp.sTech.padi.util.MessageUtil;
import corp.sTech.padi.util.RandomString;
import corp.sTech.padi.util.SessionMapUtil;

@ManagedBean
@ApplicationScoped
public class SchedulerManagementHandler {

	private static final String JOB_PACKAGE = "corp.sTech.padi.scheduler";
	private List<Scheduler> schedulerList;
	private Scheduler scheduler;
	private List<Agent> agents;
	private Agent agent;
	private List<String> secondsAndMinutes;
	private List<String> selectedSecond;
	private List<String> selectedMinutes;
	private List<String> hours;
	private List<String> selectedHours;
	private List<String> days;
	private List<String> selectedDays;
	private List<String> months;
	private List<String> selectedMonths;
	private List<String> jobs;
	private org.quartz.Scheduler qScheduler;
	private boolean started;

	@ManagedProperty(value = "#{schedulerManagementService}")
	private SchedulerManagementService service;

	@ManagedProperty(value = "#{taskService}")
	private TaskService taskService;

	private final String[] messageParams = new String[] { "Scheduler" };

	@PostConstruct
	public void init() {
		schedulerList = getAllScheduler();
		agents = getAllAgent();
		agent = new Agent();
		newScheduler();
		newSeconds();
		newHours();
		newDays();
		newMonths();
		newJob();

		try {
			qScheduler = StdSchedulerFactory.getDefaultScheduler();
			qScheduler.start();

			// register all job
			for (Scheduler scheduler : schedulerList) {
				addJob(scheduler);
			}

			started = true;
		} catch (SchedulerException e) {
			e.printStackTrace();
		}
	}

	@PreDestroy
	public void destroy() {
		try {
			qScheduler.shutdown();
		} catch (SchedulerException e) {
			e.printStackTrace();
		}
	}

	public void preAddScheduler() {
		newScheduler();
		this.selectedSecond = new ArrayList<>();
		this.selectedMinutes = new ArrayList<>();
		this.selectedDays = new ArrayList<>();
		this.selectedHours = new ArrayList<>();
		this.selectedMonths = new ArrayList<>();
		agent = new Agent();
	}

	public void preEditScheduler(Scheduler s) {
		scheduler = s;
//        this.agent = service.findAgentByAgentId(scheduler.getAgentId());

		String[] allPeriod = scheduler.getPeriod().split(" ");
		String[] allPeriod0 = allPeriod[0].split(",");
		String[] allPeriod1 = allPeriod[0].split(",");
		String[] allPeriod2 = allPeriod[0].split(",");
		String[] allPeriod3 = allPeriod[0].split(",");
		this.selectedSecond = "*".equals(allPeriod0[0]) ? secondsAndMinutes : Arrays.asList(allPeriod0);
		this.selectedMinutes = "*".equals(allPeriod1[0]) ? secondsAndMinutes : Arrays.asList(allPeriod1);
		this.selectedHours = "*".equals(allPeriod2[0]) ? hours : Arrays.asList(allPeriod2);
		this.selectedDays = "?".equals(allPeriod3[0]) ? days : Arrays.asList(allPeriod3);

		String[] monthData = allPeriod[4].split(",");
		List<String> month = new ArrayList<>();
		for (String item : monthData) {
			String findMonth = months.stream().filter(x -> x.toUpperCase().startsWith(item)).findFirst().orElse(null);
			month.add(findMonth);
		}

		this.selectedMonths = "*".equals(monthData[0]) ? month : new ArrayList<>();
	}

	public void preDeleteScheduler(Scheduler s) {
		scheduler = s;
	}

	private void newScheduler() {
		scheduler = new Scheduler();
		scheduler.setSchedulerID(RandomString.generateRandomString(8));
	}

	private void newSeconds() {
		secondsAndMinutes = new ArrayList<>();
		int SECOND_AND_MINUTES = 60;
		for (int i = 0; i < SECOND_AND_MINUTES; i++) {
			secondsAndMinutes.add(String.valueOf(i));
		}
	}

	private void newHours() {
		hours = new ArrayList<>();
		int HOURS_IN_DAYS = 24;
		for (int i = 0; i < HOURS_IN_DAYS; i++) {
			hours.add(String.valueOf(i));
		}
	}

	private void newDays() {
		days = new ArrayList<>();
		int DAYS_IN_MONTH = 31;
		for (int i = 1; i <= DAYS_IN_MONTH; i++) {
			days.add(String.valueOf(i));
		}
	}

	private void newMonths() {
		months = new ArrayList<>();
		String[] month = new DateFormatSymbols().getMonths();

		months.addAll(Arrays.stream(month).filter(x -> !"".equals(x)).collect(Collectors.toList()));
	}

	private void newJob() {
		jobs = new ArrayList<>();
		Reflections reflections = new Reflections(JOB_PACKAGE);

		Set<Class<? extends Job>> allClasses = reflections.getSubTypesOf(org.quartz.Job.class);

		for (Class<? extends Job> cl : allClasses) {
			System.out.println(cl.getSimpleName());
			jobs.add(cl.getSimpleName());
		}
	}

	/*
	 * Transaction
	 */
	private void saveTask(String taskName, Object oldData, Object newData) {
		Approval approval = new Approval(taskName, Scheduler.class, SessionMapUtil.getGroupNameSession(),
				SessionMapUtil.getUrlApproval());

		taskService.saveTask(oldData, newData, approval);
	}

	private String generateCronCommand() {
		String second = StringUtils.collectionToDelimitedString(selectedSecond, ",");
		if("".equals(second)) second = "*";
		String minute = StringUtils.collectionToDelimitedString(selectedMinutes, ",");
		if("".equals(minute)) minute = "*";
		String hour = StringUtils.collectionToDelimitedString(selectedHours, ",");
		if("".equals(hour)) hour = "*";
		String dayOfMonth = StringUtils.collectionToDelimitedString(selectedDays, ",");
		if("".equals(dayOfMonth)) dayOfMonth = "?";
		List<String> changedSelectedMonth = selectedMonths.stream().map(x -> {
			String uppercase = x.toUpperCase();

			return uppercase.substring(0, 3);
		}).collect(Collectors.toList());
		String month = StringUtils.collectionToDelimitedString(changedSelectedMonth, ",");
		if("".equals(month)) month = "*";
		String dayOfWeek = "*";
		String year = "*";

		return second.concat(" " + minute).concat(" " + hour).concat(" " + dayOfMonth).concat(" " + month)
				.concat(" " + dayOfWeek).concat(" " + year);
	}

	private void addJob(Scheduler scheduler) {

		String jobID = generateJobId(scheduler);
		String triggerID = generateTriggerId(scheduler);
		try {
			Class jobClass = Class.forName(JOB_PACKAGE + "." + scheduler.getSvcExecutions());
			JobDetail job = JobBuilder.newJob(jobClass).withIdentity(jobID, triggerID).build();

			Trigger trigger = newTrigger().withIdentity(triggerID, jobID)
					.withSchedule(CronScheduleBuilder.cronSchedule(scheduler.getPeriod())).build();

			qScheduler.scheduleJob(job, trigger);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private String generateTriggerId(Scheduler scheduler) {
		return scheduler.getSchedulerID() + "-" + scheduler.getSvcName() + "-" + scheduler.getSvcExecutions();
	}

	private String generateJobId(Scheduler scheduler) {
		return scheduler.getSvcExecutions() + "-" + scheduler.getSvcName() + "-" + scheduler.getSchedulerID();
	}

	public void stopJob(Scheduler scheduler) {
		String jobID = generateJobId(scheduler);
		String triggerID = generateTriggerId(scheduler);
		TriggerKey triggerKey = triggerKey(triggerID, jobID);

		try {
			qScheduler.unscheduleJob(triggerKey);
		} catch (SchedulerException e) {
			e.printStackTrace();
		}
	}
	
	public void updateTrigger(Scheduler scheduler) {
		try {
			String jobID = generateJobId(scheduler);
			String triggerID = generateTriggerId(scheduler);
			Trigger oldTrigger = qScheduler.getTrigger(triggerKey(triggerID, jobID));

			TriggerBuilder tb = oldTrigger.getTriggerBuilder();
			Trigger newTrigger = tb.withIdentity(triggerID, jobID)
					.withSchedule(CronScheduleBuilder.cronSchedule(scheduler.getPeriod())).build();

			qScheduler.rescheduleJob(oldTrigger.getKey(), newTrigger);
		} catch (SchedulerException e) {
			e.printStackTrace();
		}
	}

	public void startScheduler() {
		init();
	}

	private void deleteJob(Scheduler scheduler) {
		String jobID = generateJobId(scheduler);
		String triggerID = generateTriggerId(scheduler);
		try {
			qScheduler.deleteJob(jobKey(jobID, triggerID));
		} catch (SchedulerException se) {
			se.printStackTrace();
		}
	}

	public void shutdown() {
		destroy();
		started = false;
	}

	public boolean isJobRunning(Scheduler scheduler) {
		boolean status = false;
		try {
			String jobID = generateJobId(scheduler);
			String triggerID = generateTriggerId(scheduler);

			JobDetail jobDetail = qScheduler.getJobDetail(new JobKey(jobID, triggerID));

			if (jobDetail != null) {
				List<? extends Trigger> triggers = qScheduler.getTriggersOfJob(jobDetail.getKey());

				for (Trigger trigger : triggers) {
					Trigger.TriggerState triggerState = qScheduler.getTriggerState(trigger.getKey());
					if (Trigger.TriggerState.NORMAL.equals(triggerState)) {
						status = true;
					}
				}
			}

			return status;
		} catch (SchedulerException se) {
			se.printStackTrace();
		}
		return status;
	}

	public void addScheduler() {
		try {
			String cronCommand = generateCronCommand();
//            scheduler.setAgentId(agent.getAgentId());
			scheduler.setPeriod(cronCommand);

			saveTask("Add Scheduler", null, scheduler);
			init();
		} catch (Exception e) {
			e.printStackTrace();
			MessageUtil.addMessageError("common.message.error.add", messageParams);
		}
	}

	public void editScheduler() {
		try {
			Scheduler oldData = service.findScheduler(scheduler);
			scheduler.setPeriod(generateCronCommand());
//            scheduler.setAgentId(agent.getAgentId());

			saveTask("Edit Scheduler", oldData, scheduler);
//			addJob(scheduler);
			updateTrigger(scheduler);
			init();
		} catch (Exception e) {
			MessageUtil.addMessageError("common.message.error.edit", messageParams);
		}
	}

	public void deleteScheduler() {
		try {
			saveTask("Delete Scheduler", scheduler, null);
			deleteJob(scheduler);
			init();
		} catch (Exception e) {
			MessageUtil.addMessageError("common.message.error.delete", messageParams);
		}
	}

	/*
	 * Getter and Setter
	 */
	private List<Scheduler> getAllScheduler() {
		return service.getAllScheduler();
	}

	private List<Agent> getAllAgent() {
		return service.getAllAgent();
	}

	public List<Scheduler> getSchedulerList() {
		return schedulerList;
	}

	public void setSchedulerList(List<Scheduler> schedulerList) {
		this.schedulerList = schedulerList;
	}

	public Scheduler getScheduler() {
		return scheduler;
	}

	public void setScheduler(Scheduler scheduler) {
		this.scheduler = scheduler;
	}

	public SchedulerManagementService getService() {
		return service;
	}

	public void setService(SchedulerManagementService service) {
		this.service = service;
	}

	public List<Agent> getAgents() {
		return agents;
	}

	public void setAgents(List<Agent> agents) {
		this.agents = agents;
	}

	public Agent getAgent() {
		return agent;
	}

	public void setAgent(Agent agent) {
		this.agent = agent;
	}

	public List<String> getSecondsAndMinutes() {
		return secondsAndMinutes;
	}

	public void setSecondsAndMinutes(List<String> secondsAndMinutes) {
		this.secondsAndMinutes = secondsAndMinutes;
	}

	public List<String> getSelectedSecond() {
		return selectedSecond;
	}

	public void setSelectedSecond(List<String> selectedSecond) {
		this.selectedSecond = selectedSecond;
	}

	public List<String> getHours() {
		return hours;
	}

	public void setHours(List<String> hours) {
		this.hours = hours;
	}

	public List<String> getDays() {
		return days;
	}

	public void setDays(List<String> days) {
		this.days = days;
	}

	public List<String> getMonths() {
		return months;
	}

	public void setMonths(List<String> months) {
		this.months = months;
	}

	public List<String> getSelectedHours() {
		return selectedHours;
	}

	public void setSelectedHours(List<String> selectedHours) {
		this.selectedHours = selectedHours;
	}

	public List<String> getSelectedDays() {
		return selectedDays;
	}

	public void setSelectedDays(List<String> selectedDays) {
		this.selectedDays = selectedDays;
	}

	public List<String> getSelectedMonths() {
		return selectedMonths;
	}

	public void setSelectedMonths(List<String> selectedMonths) {
		this.selectedMonths = selectedMonths;
	}

	public List<String> getSelectedMinutes() {
		return selectedMinutes;
	}

	public void setSelectedMinutes(List<String> selectedMinutes) {
		this.selectedMinutes = selectedMinutes;
	}

	public List<String> getJobs() {
		return jobs;
	}

	public void setJobs(List<String> jobs) {
		this.jobs = jobs;
	}

	public TaskService getTaskService() {
		return taskService;
	}

	public void setTaskService(TaskService taskService) {
		this.taskService = taskService;
	}

	public boolean isStarted() {
		return started;
	}

	public void setStarted(boolean started) {
		this.started = started;
	}
}
