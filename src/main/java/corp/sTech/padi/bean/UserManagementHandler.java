package corp.sTech.padi.bean;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import corp.sTech.padi.model.Approval;
import corp.sTech.padi.model.Group;
import corp.sTech.padi.model.Role;
import corp.sTech.padi.model.Theme;
import corp.sTech.padi.model.User;
import corp.sTech.padi.modelConverter.GroupConverter;
import corp.sTech.padi.modelConverter.RoleConverter;
import corp.sTech.padi.service.TaskService;
import corp.sTech.padi.service.UserManagementService;
import corp.sTech.padi.util.MessageUtil;
import corp.sTech.padi.util.SecureLoginUtil;
import corp.sTech.padi.util.SessionMapUtil;

@ManagedBean
@ViewScoped
public class UserManagementHandler {

	private List<User> users;
	private User transUser;

	@ManagedProperty(value = "#{userManagementService}")
	private UserManagementService service;
	private final String[] messageParams = new String[] { "User" };
	private List<Role> availRoles;
	private List<Group> availGroups;

	private Group transGroup;

	@ManagedProperty(value = "#{taskService}")
	private TaskService taskService;

	private List<Theme> themes;

	@PostConstruct
	public void init() {
		users = getAllUsers();
		newTransUser();

		availRoles = getAllRoles();
		availGroups = getAllGroups();

		themes = service.fillThemes();

		newTransgroup();
	}

	public void newTransgroup() {
		transGroup = new Group();
	}

	private List<Group> getAllGroups() {
		return service.getAllGroups();
	}

	private List<Role> getAllRoles() {
		return service.findAllRole();
	}

	public void addUser() {
		try {
			transUser.setPassword(SecureLoginUtil.generateStorngPasswordHash(transUser.getPassword()));
			List<Group> groups = new ArrayList<>();
			groups.add(transGroup);
			transUser.setGroups(groups);

			saveTask("Add User", null, transUser);
			init();
		} catch (Exception e) {
			MessageUtil.addMessageError("common.message.error.add", messageParams);
		}
	}

	public void deleteUser() {
		try {
			saveTask("Delete User", transUser, null);
			init();
		} catch (Exception e) {
			MessageUtil.addMessageError("common.message.error.delete", messageParams);
		}
	}

	public void newTransUser() {
		transUser = new User();
	}

	public String editUser() {
		try {
			User oriUser = service.findUserWithRoleEagerLy(transUser);
			if (!oriUser.getPassword().equals(transUser.getPassword()))
				transUser.setPassword(SecureLoginUtil.generateStorngPasswordHash(transUser.getPassword()));

			if (transUser.getGroups() != null && transUser.getGroups().size() > 0) {
				transUser.getGroups().set(0, transGroup);
			} else {
				List<Group> groups = new ArrayList<>();
				groups.add(transGroup);
				transUser.setGroups(groups);
			}
			saveTask("Edit User", oriUser, transUser);
			init();
		} catch (Exception e) {
			MessageUtil.addMessageError("common.message.error.edit", messageParams);
		}
		return "/pages/main-page.xhtml?faces-redirect=true";
	}

	private boolean saveTask(String taskName, Object oldData, Object newData) {
		Approval approval = new Approval(taskName, User.class, SessionMapUtil.getGroupNameSession(),
				SessionMapUtil.getUrlApproval());
		return taskService.saveTask(oldData == null ? new User() : oldData, newData == null ? new User() : newData,
				approval);
	}

	private List<User> getAllUsers() {
		if (SessionMapUtil.isFromMenuButton()) {
			return service.findAllWithRoleEagerly();
		} else {
			User user = SessionMapUtil.getUserSession();
			User userWithRoleEagerLy = service.findUserWithRoleEagerLy(user);
			List<User> users = new ArrayList<>();
			users.add(userWithRoleEagerLy);
			return users;
		}
	}

	public void preAddUser() {
		transUser = new User();
	}

	public void preEditUser(User user) {
		transUser = user;
		transGroup = (user.getGroups() != null && user.getGroups().size() > 0 ? user.getGroups().get(0) : null);
	}

	public void preDeleteUser(User user) {
		transUser = user;
	}

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	public UserManagementService getService() {
		return service;
	}

	public void setService(UserManagementService service) {
		this.service = service;
	}

	public User getTransUser() {
		return transUser;
	}

	public void setTransUser(User transUser) {
		this.transUser = transUser;
	}

	public List<Role> getAvailRoles() {
		return availRoles;
	}

	public void setAvailRoles(List<Role> availRoles) {
		this.availRoles = availRoles;
	}

	public TaskService getTaskService() {
		return taskService;
	}

	public void setTaskService(TaskService taskService) {
		this.taskService = taskService;
	}

	public List<Theme> getThemes() {
		return themes;
	}

	public void setThemes(List<Theme> themes) {
		this.themes = themes;
	}

	public String[] getMessageParams() {
		return messageParams;
	}

	public List<Group> getAvailGroups() {
		return availGroups;
	}

	public void setAvailGroups(List<Group> availGroups) {
		this.availGroups = availGroups;
	}

	public Group getTransGroup() {
		return transGroup;
	}

	public void setTransGroup(Group transGroup) {
		this.transGroup = transGroup;
	}

}
