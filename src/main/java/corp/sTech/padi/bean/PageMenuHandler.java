package corp.sTech.padi.bean;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

import corp.sTech.padi.model.Menu;
import corp.sTech.padi.model.User;
import corp.sTech.padi.parameter.SystemParameter;
import corp.sTech.padi.repository.SystemParameterRepository;
import corp.sTech.padi.service.PageMenuService;
import corp.sTech.padi.util.SessionMapUtil;

@ManagedBean
@SessionScoped
public class PageMenuHandler {
	private List<Menu> menus = new ArrayList<Menu>();
	private List<String> groupMenus = new ArrayList<>();
	private Set<String> groupMenuSet = new LinkedHashSet<>();

	private Menu selectedMenu;
	private int selectedGroup = -1;

	@ManagedProperty(value = "#{pageMenuService}")
	private PageMenuService service;

	@ManagedProperty(value = "#{systemParameterRepository}")
	private SystemParameterRepository systemParameterRepository;
	
	@PostConstruct
	public void init() {
		User user = SessionMapUtil.getUserSession();

		menus = service.getAllMenu(user.getRole());
		for (Menu menu : menus) {
			if (menu.getGroupMenu() != null)
				groupMenuSet.add(menu.getGroupMenu().getGroupName());
		}
		groupMenus.addAll(groupMenuSet);
	}

	public String getImageHeader() {
//		return BussinessParameter.mobileImagePublic + SessionMapUtil.getGroupNameSession() + "/"
//				+ SessionMapUtil.getGroupSession().getImageHeader();
		/*String result = systemParameterRepository.paramValueOf(SystemParameter.DEFAULT_CONTEXT_ROOT)+File.separator+systemParameterRepository.paramValueOf(SystemParameter.MOBILE_IMAGE_MAP)+File.separator+ SessionMapUtil.getGroupNameSession() +File.separator
				+ SessionMapUtil.getGroupSession().getImageHeader();*/
		String result = systemParameterRepository.paramValueOf(SystemParameter.DEFAULT_CONTEXT_ROOT)+File.separator+systemParameterRepository.paramValueOf(SystemParameter.MOBILE_IMAGE_MAP)+File.separator+"image_header.jpeg";
		return result;
	}

	public String doClick(Menu selectedMenu) {
		// this.selectedMenu = selectedMenu;
		SessionMapUtil.put("selectedMenu", selectedMenu);
		SessionMapUtil.put("fromMenuButton", true);
		selectedGroup = groupMenus.indexOf(selectedMenu.getGroupMenu().getGroupName());
		// SessionMapUtil.put("urlApproval", selectedMenu.getUrlApproval());
		return "/pages/main-page.xhtml?faces-redirect=true";
	}
	
	public String doClickHome() {
		// this.selectedMenu = selectedMenu;
		SessionMapUtil.put("selectedMenu", null);
		SessionMapUtil.put("fromMenuButton", true);
		return "/pages/main-page.xhtml?faces-redirect=true";
	}

	public String fromMenuButton() {
		Menu menu = service.getMenuByUrlLike("%user-management%");
		SessionMapUtil.put("selectedMenu", menu);
		SessionMapUtil.put("fromMenuButton", false);
		return "/pages/main-page.xhtml?faces-redirect=true";
	}

	public List<Menu> getMenus() {
		return menus;
	}

	public void setMenus(List<Menu> menus) {
		this.menus = menus;
	}

	public Menu getSelectedMenu() {
		return selectedMenu;
	}

	public void setSelectedMenu(Menu selectedMenu) {

		this.selectedMenu = selectedMenu;
	}

	public PageMenuService getService() {
		return service;
	}

	public void setService(PageMenuService service) {
		this.service = service;
	}

	public List<String> getGroupMenus() {
		return groupMenus;
	}

	public void setGroupMenus(List<String> groupMenus) {
		this.groupMenus = groupMenus;
	}

	public int getSelectedGroup() {
		return selectedGroup;
	}

	public void setSelectedGroup(int selectedGroup) {
		this.selectedGroup = selectedGroup;
	}

	public SystemParameterRepository getSystemParameterRepository() {
		return systemParameterRepository;
	}

	public void setSystemParameterRepository(SystemParameterRepository systemParameterRepository) {
		this.systemParameterRepository = systemParameterRepository;
	}
}
