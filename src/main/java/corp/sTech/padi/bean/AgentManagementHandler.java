package corp.sTech.padi.bean;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.management.Notification;

import org.primefaces.event.FileUploadEvent;

import corp.sTech.padi.model.Agent;
import corp.sTech.padi.model.Approval;
import corp.sTech.padi.parameter.ActiveParam;
import corp.sTech.padi.service.AgentManagementService;
import corp.sTech.padi.service.TaskService;
import corp.sTech.padi.util.MessageUtil;
import corp.sTech.padi.util.SessionMapUtil;


@ManagedBean
@ViewScoped
public class AgentManagementHandler {
	private List<Agent> agents;
	private Agent transAgent;
	
	@ManagedProperty(value = "#{agentManagementService}")
	private AgentManagementService service;
	private final String[] messageParams = new String[] { "Agent" };

	@ManagedProperty(value = "#{taskService}")
	private TaskService taskService;
	
	@PostConstruct
	public void init() {
		agents = getAllAgents();
		newTransAgent();
	}

	public void newTransAgent() {
		transAgent = new Agent();
	}
	
	public void preAddAgent() {
		newTransAgent();
	}

	public void addAgent() {
		try {
			transAgent.setStatus(ActiveParam.ACTIVE);
			saveTask("Add Agent", null, transAgent);
			init();
		} catch (Exception e) {
			MessageUtil.addMessageError("common.message.error.add", messageParams);
			return;
		}
	}

	public void editAgent() {
		try {
			Agent oriAgent = service.getAgent(transAgent);

			saveTask("Edit Agent", oriAgent, transAgent);
			init();
		} catch (Exception e) {
			e.printStackTrace();
			MessageUtil.addMessageError("common.message.error.edit", messageParams);
			return;
		}
	}

	public void preEditAgent(Agent agent) {
		transAgent = agent;
	}

	public void DeleteAgent() {
		try {
			Agent newTransAgent = transAgent;
			newTransAgent.setStatus(ActiveParam.INACTIVE);
			saveTask("Delete Agent", transAgent, newTransAgent);
			init();
		} catch (Exception e) {
			MessageUtil.addMessageError("common.message.error.delete", messageParams);
			return;
		}
	}

	public void preDeleteAgent(Agent agent) {
		transAgent = agent;
	}

	private boolean saveTask(String taskName, Object oldData, Object newData) {
		Approval approval = new Approval(taskName, Agent.class, SessionMapUtil.getGroupNameSession(),
				SessionMapUtil.getUrlApproval());
		return taskService.saveTask(oldData == null ? new Agent() : oldData, newData == null ? new Agent() : newData,
				approval);
	}

	private List<Agent> getAllAgents() {
		return service.getAllAgents();
	}

	public List<Agent> getAgents() {
		return agents;
	}

	public void setAgents(List<Agent> agents) {
		this.agents = agents;
	}

	public Agent getTransAgent() {
		return transAgent;
	}

	public void setTransAgent(Agent transAgent) {
		this.transAgent = transAgent;
	}

	public AgentManagementService getService() {
		return service;
	}

	public void setService(AgentManagementService service) {
		this.service = service;
	}

	public String[] getMessageParams() {
		return messageParams;
	}

	public TaskService getTaskService() {
		return taskService;
	}

	public void setTaskService(TaskService taskService) {
		this.taskService = taskService;
	}
}
