package corp.sTech.padi.bean;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.BarChartModel;
import org.primefaces.model.chart.CategoryAxis;
import org.primefaces.model.chart.ChartSeries;
import org.primefaces.model.chart.LineChartModel;

import corp.sTech.padi.model.BookingIpl;
import corp.sTech.padi.parameter.ResponseCode;
import corp.sTech.padi.service.TransactionSummaryService;

@ManagedBean
@SessionScoped
public class TransactionSummaryHandler {
	public class Data {
		int transactionCount;
		int successCount;
		int failCount;
	}

	private LineChartModel lineModel;
	private BarChartModel barModel;
	
	@ManagedProperty(value = "#{transactionSummaryService}")
	private TransactionSummaryService service;
	private List<BookingIpl> bookingIpls;
	private ChartSeries transactionChart;
	private ChartSeries successChart;
	private ChartSeries failChart;
	
	
	@PostConstruct
	public void init() {
		bookingIpls = service.getAllBookingIpl();
		
		transactionChart = new ChartSeries();
		successChart = new ChartSeries();
		successChart.setLabel("Success");
		failChart = new ChartSeries();
		failChart.setLabel("Fail");
        
		DateFormat sdf = new SimpleDateFormat("MM/yyyy");
		Collections.sort(bookingIpls, (p1, p2) -> (p1.getTxnDate()).compareTo(p2.getTxnDate()));
		
        Map<String, Data> bookingIplMap = new LinkedHashMap<>();
		for (BookingIpl bookingIpl : bookingIpls) {
			Data data = new Data();
			String key = sdf.format(bookingIpl.getTxnDate());
			
			if(bookingIplMap.get(key) != null) {
				data = bookingIplMap.get(key);
			}
			
			data.transactionCount++;
			if(ResponseCode.success.equals(bookingIpl.getResponseCode())){
				data.successCount++;
			}else {
				data.failCount++;
			}
			
			bookingIplMap.put(key, data);
		}
		
		int maxTransactionCount = 0;
		int maxStatusCount = 0;
		for (String key : bookingIplMap.keySet()) {
			Data data = bookingIplMap.get(key);
			
			transactionChart.set(key, data.transactionCount);
			successChart.set(key, data.successCount);
			failChart.set(key, data.failCount);
			
			if(data.transactionCount > maxTransactionCount) {
				maxTransactionCount = data.transactionCount;
			}
			
			if(data.successCount > maxStatusCount) {
				maxStatusCount = data.successCount;
			}
			
			if(data.failCount > maxStatusCount) {
				maxStatusCount = data.failCount;
			}
		}
		
		createLineModel();
        createBarModel();
        
        Axis transactionAxis = lineModel.getAxis(AxisType.Y);
        Axis statusAxis = barModel.getAxis(AxisType.Y);
        
        transactionAxis.setMax(((maxTransactionCount + Math.round((0.1 * maxTransactionCount)) + 3) / 4 ) * 4);
        statusAxis.setMax(((maxStatusCount + Math.round((0.1 * maxStatusCount)) + 3) / 4 ) * 4);
	}

	private void createLineModel() {
		lineModel = initCategoryModel();
		lineModel.setTitle("Transaction");
		lineModel.setShowPointLabels(true);
		lineModel.setDatatipFormat("%n%d");
		lineModel.setShowDatatip(false);
		lineModel.getAxes().put(AxisType.X, new CategoryAxis("Periode"));
		
		Axis yAxis = lineModel.getAxis(AxisType.Y);
		yAxis.setMin(0);
		yAxis.setMax(2000);
	}
	
	private void createBarModel() {
        barModel = initBarModel();
         
        barModel.setTitle("Status");
        barModel.setLegendPosition("ne");
        barModel.setShowPointLabels(true);
        barModel.setDatatipFormat("%n%d");
        barModel.setShowDatatip(false);
         
        Axis xAxis = barModel.getAxis(AxisType.X);
         
        Axis yAxis = barModel.getAxis(AxisType.Y);
        yAxis.setMin(0);
        yAxis.setMax(2000);
    }
	
	private BarChartModel initBarModel() {
        BarChartModel model = new BarChartModel();
 
        model.addSeries(successChart);
        model.addSeries(failChart);
         
        return model;
    }

	private LineChartModel initCategoryModel() {
		LineChartModel model = new LineChartModel();

		model.addSeries(transactionChart);

		return model;
	}

	public LineChartModel getLineModel() {
		return lineModel;
	}

	public void setLineModel(LineChartModel lineModel) {
		this.lineModel = lineModel;
	}

	public BarChartModel getBarModel() {
		return barModel;
	}

	public void setBarModel(BarChartModel barModel) {
		this.barModel = barModel;
	}

	public TransactionSummaryService getService() {
		return service;
	}

	public void setService(TransactionSummaryService service) {
		this.service = service;
	}

	public List<BookingIpl> getBookingIpls() {
		return bookingIpls;
	}

	public void setBookingIpls(List<BookingIpl> bookingIpls) {
		this.bookingIpls = bookingIpls;
	}

	public ChartSeries getTransactionChart() {
		return transactionChart;
	}

	public void setTransactionChart(ChartSeries transactionChart) {
		this.transactionChart = transactionChart;
	}

	public ChartSeries getSuccessChart() {
		return successChart;
	}

	public void setSuccessChart(ChartSeries successChart) {
		this.successChart = successChart;
	}

	public ChartSeries getFailChart() {
		return failChart;
	}

	public void setFailChart(ChartSeries failChart) {
		this.failChart = failChart;
	}

	
}
