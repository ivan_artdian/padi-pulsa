package corp.sTech.padi.modelProcessors;

import corp.sTech.padi.modelApi.MRequest;
import corp.sTech.padi.modelApi.MResponse;

public class MMetadata {
	
	private String txnIdentifier;
	private String txnName;
	private String timeIn;
	private String timeOut;
	private String timeProcessing;
	private String txnStatusCode;
	private String txnStatusMessage;
	private String txnFlag;
	private String txnType;
	private String svcType;

	private MRequest mRequest;
	private MResponse mResponse;
	private MRawData mRawData;
	public String getTxnIdentifier() {
		return txnIdentifier;
	}
	public void setTxnIdentifier(String txnIdentifier) {
		this.txnIdentifier = txnIdentifier;
	}
	public String getTxnName() {
		return txnName;
	}
	public void setTxnName(String txnName) {
		this.txnName = txnName;
	}
	public String getTimeIn() {
		return timeIn;
	}
	public void setTimeIn(String timeIn) {
		this.timeIn = timeIn;
	}
	public String getTimeOut() {
		return timeOut;
	}
	public void setTimeOut(String timeOut) {
		this.timeOut = timeOut;
	}
	public String getTimeProcessing() {
		return timeProcessing;
	}
	public void setTimeProcessing(String timeProcessing) {
		this.timeProcessing = timeProcessing;
	}
	public String getTxnStatusCode() {
		return txnStatusCode;
	}
	public void setTxnStatusCode(String txnStatusCode) {
		this.txnStatusCode = txnStatusCode;
	}
	public String getTxnStatusMessage() {
		return txnStatusMessage;
	}
	public void setTxnStatusMessage(String txnStatusMessage) {
		this.txnStatusMessage = txnStatusMessage;
	}
	public String getTxnFlag() {
		return txnFlag;
	}
	public void setTxnFlag(String txnFlag) {
		this.txnFlag = txnFlag;
	}
	public String getTxnType() {
		return txnType;
	}
	public void setTxnType(String txnType) {
		this.txnType = txnType;
	}
	public String getSvcType() {
		return svcType;
	}
	public void setSvcType(String svcType) {
		this.svcType = svcType;
	}
	public MRequest getmRequest() {
		return mRequest;
	}
	public void setmRequest(MRequest mRequest) {
		this.mRequest = mRequest;
	}
	public MResponse getmResponse() {
		return mResponse;
	}
	public void setmResponse(MResponse mResponse) {
		this.mResponse = mResponse;
	}
	public MRawData getmRawData() {
		return mRawData;
	}
	public void setmRawData(MRawData mRawData) {
		this.mRawData = mRawData;
	}
	
	
}
