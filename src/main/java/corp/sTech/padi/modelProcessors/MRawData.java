package corp.sTech.padi.modelProcessors;

public class MRawData {

	private String rawRequest;
	private String rawResponse;
	
	public String getRawRequest() {
		return rawRequest;
	}
	public void setRawRequest(String rawRequest) {
		this.rawRequest = rawRequest;
	}
	public String getRawResponse() {
		return rawResponse;
	}
	public void setRawResponse(String rawResponse) {
		this.rawResponse = rawResponse;
	}
}
