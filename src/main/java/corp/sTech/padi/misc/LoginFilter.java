package corp.sTech.padi.misc;

import java.io.IOException;

import javax.faces.application.ResourceHandler;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class LoginFilter implements Filter {
	private static final String AJAX_REDIRECT_XML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
			+ "<partial-response><redirect url=\"%s\"></redirect></partial-response>";

	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest request = (HttpServletRequest) req;
		HttpServletResponse response = (HttpServletResponse) res;
		HttpSession session = request.getSession(false);
		String loginURL = request.getContextPath() + "/faces/pages/login.xhtml";
		String mainPageUrl = request.getContextPath() + "/faces/pages/main-page.xhtml";
		String mainPageRoot = request.getContextPath() + "/faces";
		String mobileRenderPath = "/mobileRender/";

		boolean loggedIn = (session != null) && (session.getAttribute("user") != null);
		boolean loginRequest = request.getRequestURI().equals(loginURL);
		boolean resourceRequest = request.getRequestURI()
				.startsWith(mainPageRoot + ResourceHandler.RESOURCE_IDENTIFIER + "/");
		boolean ajaxRequest = "partial/ajax".equals(request.getHeader("Faces-Request"));
		boolean mainPage = request.getRequestURI().equals(mainPageUrl);

		boolean apiPath = !request.getRequestURI().startsWith(mainPageRoot);
		// boolean apiPath = true;
		boolean mobileRender = request.getRequestURI().contains(mobileRenderPath);
		if (loginRequest || resourceRequest || (loggedIn && mainPage) || apiPath
				|| mobileRender) {

			if (!resourceRequest) {
				response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
				response.setHeader("Pragma", "no-cache");
				response.setDateHeader("Expires", 0);
			}

			chain.doFilter(request, response);
		} else if (ajaxRequest) {
			response.setContentType("text/xml");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().printf(AJAX_REDIRECT_XML, loginURL);
		} else {
			response.sendRedirect(loginURL);
		}
	}

	public void init(FilterConfig config) throws ServletException {

	}

	public void destroy() {

	}
}