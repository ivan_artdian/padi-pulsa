package corp.sTech.padi.scheduler;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import corp.sTech.padi.api.EmailAPI;
import corp.sTech.padi.model.TIbsBillDetail;
import corp.sTech.padi.model.TIbsInvoice;
import corp.sTech.padi.repository.TIbsInvoiceRepository;
import corp.sTech.padi.util.NumberFormatUtil;

public class SendEmail implements Job{

	private static TIbsInvoiceRepository ibsInvoiceRepository;
	
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		System.out.println("send mail");
		
		String body = "Anda mempunyai tagihan bulan ini sebesar : ";
		
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/yyyy");
		String billPeriode = simpleDateFormat.format(new Date());
		List<TIbsInvoice> ibsInvoices = ibsInvoiceRepository.findByBillPeriode(billPeriode);
		for (TIbsInvoice tIbsInvoice : ibsInvoices) {
			if("UnPaid".equals(tIbsInvoice.getPaidStatus())) {
				new Thread( () -> {
					TIbsBillDetail ibsBillDetail = tIbsInvoice.getIbsBillDetail();
					EmailAPI.sendMail(ibsBillDetail.getEmail(), "Info Tagihan", body + NumberFormatUtil.formatNumber(tIbsInvoice.getAmount(), true, true), null);
				}).start();
			}
		}
		
	}

	public static TIbsInvoiceRepository getIbsInvoiceRepository() {
		return ibsInvoiceRepository;
	}

	public static void setIbsInvoiceRepository(TIbsInvoiceRepository ibsInvoiceRepository) {
		SendEmail.ibsInvoiceRepository = ibsInvoiceRepository;
	}

}
