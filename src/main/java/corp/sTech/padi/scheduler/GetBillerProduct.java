package corp.sTech.padi.scheduler;

import java.util.ArrayList;
import java.util.List;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import corp.sTech.padi.api.Mcash;
import corp.sTech.padi.model.BillerProduct;
import corp.sTech.padi.modelApi.Product;
import corp.sTech.padi.parameter.SystemParameter;
import corp.sTech.padi.repository.BillerProductRepository;
import corp.sTech.padi.repository.SystemParameterRepository;
import corp.sTech.padi.util.DateUtil;
import corp.sTech.padi.util.JSONUtils;

public class GetBillerProduct implements Job{

	private static BillerProductRepository billerProductRepository;
	
	private SystemParameterRepository systemParameterRepository;
	
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		mCashProduct();
	}
	
	private void mCashProduct() {
		String username = systemParameterRepository.paramValueOf(SystemParameter.MCASH_USER);
		String password = systemParameterRepository.paramValueOf(SystemParameter.MCASH_PASSWORD);
		String response = Mcash.cekHargaDanStatus(null, username, password, null);

		@SuppressWarnings("unchecked")
		List<Product> products = (List<Product>) JSONUtils.jsonStringToListObject(response, Product.class);
		
		List<BillerProduct> billerProducts = new ArrayList<BillerProduct>();
		
		for (int i = 0; i < products.size(); i++) {
			BillerProduct billerProduct = new BillerProduct();
			
			billerProduct.setProductId(products.get(i).getProductId());
			billerProduct.setModifiedDate(DateUtil.getCurrentDateString("ddMMyyyy"));
			billerProduct.setDescription(products.get(i).getDescription());
			billerProduct.setOperator(products.get(i).getOperator());
			billerProduct.setAmount(products.get(i).getAmount());
			billerProduct.setRsPrice(products.get(i).getRsPrice());
			billerProduct.setStatus(products.get(i).getStatus());
//			billerProduct.setHargajual(hargajual);
			billerProduct.setSourceBiller("MCash");
			
			billerProducts.add(billerProduct);
			
		}
		billerProductRepository.save(billerProducts);
	}

	public static BillerProductRepository getBillerProductRepository() {
		return billerProductRepository;
	}

	public static void setBillerProductRepository(BillerProductRepository billerProductRepository) {
		GetBillerProduct.billerProductRepository = billerProductRepository;
	}

}
