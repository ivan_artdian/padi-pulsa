package corp.sTech.padi.scheduler;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import corp.sTech.padi.model.BookingIpl;
import corp.sTech.padi.parameter.ResponseCode;
import corp.sTech.padi.parameter.ResponseMessage;
import corp.sTech.padi.repository.BookingIplRepository;


public class ExpiredPayment implements Job{

	private static BookingIplRepository bookingIplRepository;
	
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		
		List<BookingIpl> bookingIpls = bookingIplRepository.findByResponseCode(ResponseCode.onProcess);
		
		for (int i = 0; i < bookingIpls.size(); i++) {
			try {
				Date datetxn = bookingIpls.get(i).getTxnDate();
				
				LocalDateTime startDate = datetxn.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
			    LocalDateTime endDate = LocalDateTime.now();

			    long numberOfHours = Duration.between(startDate, endDate).toHours();
			    
			    if (numberOfHours >=24) {
					BookingIpl bookingIpl = new BookingIpl();
					bookingIpl = bookingIpls.get(i);
					
					bookingIpl.setResponseCode(ResponseCode.TransactionExpired);
					bookingIpl.setResponseMessage(ResponseMessage.TransactionExpired);
					
					bookingIplRepository.save(bookingIpl);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}
		
	}

	public static BookingIplRepository getBookingIplRepository() {
		return bookingIplRepository;
	}

	public static void setBookingIplRepository(BookingIplRepository bookingIplRepository) {
		ExpiredPayment.bookingIplRepository = bookingIplRepository;
	}

}
