package corp.sTech.padi.adapter;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import corp.sTech.padi.mobileprocessor.MProcessor;
import corp.sTech.padi.modelApi.MRequest;
import corp.sTech.padi.modelApi.MResponse;
import corp.sTech.padi.modelProcessors.MMetadata;
import corp.sTech.padi.parameter.MobileServiceName;
import corp.sTech.padi.util.JSONUtils;
import corp.sTech.padi.util.TransactionUtils;

@Path("/mgateway")
@Component
public class MRestAdapter {
	@Autowired
	private MProcessor mprocessor;

	@POST
	@Path("/productList")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public MResponse productList(MRequest mRequest) {
		// for temporary
		String request = convertRequest(mRequest);

		MMetadata metadata = new MMetadata();

		metadata = TransactionUtils.initMetadata(MobileServiceName.productList, request);
		metadata = mprocessor.mobileProcess(metadata);

		return metadata.getmResponse();
	}
	
	@POST
	@Path("/transactionInquiry")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public MResponse transactionInquiry(MRequest mRequest) {
		// for temporary
		String request = convertRequest(mRequest);
		
		MMetadata metadata = new MMetadata();
		
		metadata = TransactionUtils.initMetadata(MobileServiceName.transactionInquiry, request);
		metadata = mprocessor.mobileProcess(metadata);
		
		return metadata.getmResponse();
	}
	
	@POST
	@Path("/iplBillInquiry")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public MResponse iplBillInquiry(MRequest mRequest) {
		// for temporary
		String request = convertRequest(mRequest);
		
		MMetadata metadata = new MMetadata();
		
		metadata = TransactionUtils.initMetadata(MobileServiceName.iplBillInquiry, request);
		metadata = mprocessor.mobileProcess(metadata);
		
		return metadata.getmResponse();
	}
	
	@POST
	@Path("/transactionConfirm")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public MResponse transactionConfirm(MRequest mRequest) {
		// for temporary
		String request = convertRequest(mRequest);
		
		MMetadata metadata = new MMetadata();
		
		metadata = TransactionUtils.initMetadata(MobileServiceName.transactionConfirm, request);
		metadata = mprocessor.mobileProcess(metadata);
		
		return metadata.getmResponse();
	}
	
	@POST
	@Path("/iplBillConfirm")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public MResponse iplBillConfirm(MRequest mRequest) {
		// for temporary
		String request = convertRequest(mRequest);
		
		MMetadata metadata = new MMetadata();
		
		metadata = TransactionUtils.initMetadata(MobileServiceName.iplBillConfirm, request);
		metadata = mprocessor.mobileProcess(metadata);
		
		return metadata.getmResponse();
	}
	
	@POST
	@Path("/transactionConfirmResponse")
	@Produces(MediaType.TEXT_PLAIN)
	public String transactionConfirmResponse(@FormParam("padipaySignature") String padipaySignature,
			@FormParam("transactionTime") String transactionTime,
			@FormParam("expirationTime") String expirationTime,
			@FormParam("merchantCode") String merchantCode,
			@FormParam("invoiceCode") String invoiceCode,
			@FormParam("padipayCode") String padipayCode,
			@FormParam("amount") String amount,
			@FormParam("miscFee") String miscFee,
			@FormParam("totalPayments") String totalPayments,
			@FormParam("resultCode") String resultCode,
			@FormParam("resultDesc") String resultDesc,
			@FormParam("paymentCodeRefId") String paymentCodeRefId,
			@FormParam("paymentChannelId") String paymentChannelId,
			@FormParam("paymentChannel") String paymentChannel,
			@FormParam("addInfo1") String addInfo1,
			@FormParam("addInfo2") String addInfo2) {
		MMetadata metadata = new MMetadata();

		MRequest mRequest = new MRequest();
		mRequest.setPadipaySignature(padipaySignature);
		mRequest.setTransactionTime(transactionTime);
		mRequest.setExpirationTime(expirationTime);
		mRequest.setMerchantCode(merchantCode);
		mRequest.setInvoiceCode(invoiceCode);
		mRequest.setPadipayCode(padipayCode);
		mRequest.setAmount(amount);
		mRequest.setMiscFee(miscFee);
		mRequest.setTotalPayments(totalPayments);
		mRequest.setResultCode(resultCode);
		mRequest.setResultDesc(resultDesc);
		mRequest.setPaymentCodeRefId(paymentCodeRefId);
		mRequest.setPaymentChannelId(paymentChannelId);
		mRequest.setPaymentChannel(paymentChannel);
		mRequest.setAddInfo1(addInfo1);
		mRequest.setAddInfo2(addInfo2);
		
		String request = convertRequest(mRequest);
		
		metadata = TransactionUtils.initMetadata(MobileServiceName.transactionConfirmResponse, request);
		metadata = mprocessor.mobileProcess(metadata);

		return metadata.getmResponse().getResponseCode();
	}
	
	@POST
	@Path("/iplBillHistory")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public MResponse iplBillHistory(MRequest mRequest) {
		// for temporary
		String request = convertRequest(mRequest);
		
		MMetadata metadata = new MMetadata();
		
		metadata = TransactionUtils.initMetadata(MobileServiceName.iplBillHistory, request);
		metadata = mprocessor.mobileProcess(metadata);
		
		return metadata.getmResponse();
	}
	
	@POST
	@Path("/transactionBillHistory")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public MResponse transactionBillHistory(MRequest mRequest) {
		// for temporary
		String request = convertRequest(mRequest);
		
		MMetadata metadata = new MMetadata();
		
		metadata = TransactionUtils.initMetadata(MobileServiceName.transactionBillHistory, request);
		metadata = mprocessor.mobileProcess(metadata);
		
		return metadata.getmResponse();
	}
	
	@POST
	@Path("/favList")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public MResponse favList(MRequest mRequest) {
		// for temporary
		String request = convertRequest(mRequest);
		
		MMetadata metadata = new MMetadata();
		
		metadata = TransactionUtils.initMetadata(MobileServiceName.favList, request);
		metadata = mprocessor.mobileProcess(metadata);
		
		return metadata.getmResponse();
	}
	
	@POST
	@Path("/favAdd")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public MResponse favAdd(MRequest mRequest) {
		// for temporary
		String request = convertRequest(mRequest);
		
		MMetadata metadata = new MMetadata();
		
		metadata = TransactionUtils.initMetadata(MobileServiceName.favAdd, request);
		metadata = mprocessor.mobileProcess(metadata);
		
		return metadata.getmResponse();
	}
	
	@POST
	@Path("/favUpdate")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public MResponse favUpdate(MRequest mRequest) {
		// for temporary
		String request = convertRequest(mRequest);
		
		MMetadata metadata = new MMetadata();
		
		metadata = TransactionUtils.initMetadata(MobileServiceName.favUpdate, request);
		metadata = mprocessor.mobileProcess(metadata);
		
		return metadata.getmResponse();
	}
	
	@POST
	@Path("/favDelete")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public MResponse favDelete(MRequest mRequest) {
		// for temporary
		String request = convertRequest(mRequest);
		
		MMetadata metadata = new MMetadata();
		
		metadata = TransactionUtils.initMetadata(MobileServiceName.favDelete, request);
		metadata = mprocessor.mobileProcess(metadata);
		
		return metadata.getmResponse();
	}
	
	private String convertRequest(MRequest mRequest) {
		String request = JSONUtils.objectToJSONString(mRequest);

		return request;
	}
}
