package corp.sTech.padi.modelConverter;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;

import corp.sTech.padi.model.Group;
import corp.sTech.padi.service.ConverterService;

@ManagedBean(name = "groupConverter")
@ViewScoped
public class GroupConverter implements Converter {

	@ManagedProperty(value = "#{converterService}")
	private ConverterService service;

	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if (value == null || value.isEmpty()) {
			return null;
		}

		List<Group> groups = service.findGroupByGroupId(value);
		return (groups == null ? null : groups.get(0));
	}

	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value == null) {
			return "";
		}

		if (value instanceof Group) {
			return ((Group) value).getUsername();
		} else {
			throw new ConverterException(new FacesMessage(value + " is not a valid Group"));
		}
	}

	public ConverterService getService() {
		return service;
	}

	public void setService(ConverterService service) {
		this.service = service;
	}

}
