package corp.sTech.padi.modelConverter;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;

import corp.sTech.padi.model.Menu;
import corp.sTech.padi.service.ConverterService;

@ManagedBean(name = "menuConverter")
@ViewScoped
public class MenuConverter implements Converter {

	@ManagedProperty(value = "#{converterService}")
	private ConverterService service;

	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if (value == null || value.isEmpty()) {
			return null;
		}

		List<Menu> menus = service.findMenuByMenuId(value);
		return (menus == null ? null : menus.get(0));
	}

	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value == null) {
			return "";
		}

		if (value instanceof Menu) {
			return String.valueOf(((Menu) value).getMenuId());
		} else {
			throw new ConverterException(new FacesMessage(value + " is not a valid Menu"));
		}
	}

	public ConverterService getService() {
		return service;
	}

	public void setService(ConverterService service) {
		this.service = service;
	}

}
