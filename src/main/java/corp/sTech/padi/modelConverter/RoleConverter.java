package corp.sTech.padi.modelConverter;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;

import corp.sTech.padi.model.Role;
import corp.sTech.padi.service.ConverterService;

@ManagedBean(name = "roleConverter")
@ViewScoped
public class RoleConverter implements Converter {

	@ManagedProperty(value = "#{converterService}")
	private ConverterService service;

	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if (value == null || value.isEmpty()) {
			return null;
		}

		List<Role> roles = service.findRoleByRoleId(value);
		return (roles == null ? null : roles.get(0));
	}

	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value == null) {
			return "";
		}

		if (value instanceof Role) {
			return String.valueOf(((Role) value).getRoleId());
		} else {
			throw new ConverterException(new FacesMessage(value + " is not a valid Role"));
		}
	}

	public ConverterService getService() {
		return service;
	}

	public void setService(ConverterService service) {
		this.service = service;
	}

}
