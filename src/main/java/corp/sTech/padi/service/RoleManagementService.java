package corp.sTech.padi.service;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import corp.sTech.padi.model.Menu;
import corp.sTech.padi.model.Role;
import corp.sTech.padi.model.RoleMenu;
import corp.sTech.padi.repository.MenuRepository;
import corp.sTech.padi.repository.RoleMenuRepository;
import corp.sTech.padi.repository.RoleRepository;
import corp.sTech.padi.util.DateUtil;

@Component
public class RoleManagementService implements Serializable{
	private static final long serialVersionUID = 627478830096863820L;

	@Autowired
	private RoleRepository roleRepository;

	@Autowired
	private MenuRepository menuRepository;
	
	@Autowired
	private RoleMenuRepository roleMenuRepository;

	public Role save(Role role) {
		Role role2 = null;
		try {
			role2 = roleRepository.save(role);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return role2;
	}

	public List<Role> findAll() {
		return (List<Role>) roleRepository.findAll();
	}

	public void delete(Role role) {
		roleRepository.delete(role);
	}
	
	public List<Menu> loadAllMenu() {
		try {
			List<Menu> result = (List<Menu>) menuRepository.findAll();
			return result;
		} catch (Exception e) {
			return null;
		}
	}

	public List<RoleMenu> getAssignRoleMenu(Role role) {
		Role result = roleRepository.findWithRoleMenuByRoleIdEagerly(role.getRoleId());
		return result.getRoleMenus();
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public RoleMenu setRoleMenu(Role role) {
		RoleMenu result = null;

		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
		String dateString = StringUtils.rightPad(sdf.format(DateUtil.getCurrentDateZoned()), 17, '0');
		try {
			Menu menu = new Menu();
			menu.setMenuId(Long.valueOf(dateString));
			RoleMenu roleMenu = new RoleMenu();

			roleMenu.setId((roleMenuRepository.findTop1ByOrderByIdDesc() == null ? 0
					: roleMenuRepository.findTop1ByOrderByIdDesc().get(0).getId()) + 1);
			roleMenu.setRole(role);
			roleMenu.setMenu(menu);
			roleMenu.setNeedApproval(false);
			result = roleMenu;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}

	public List<RoleMenu> saveRoleMenus(List<RoleMenu> roleMenus) {
		return (List<RoleMenu>) roleMenuRepository.save(roleMenus);
	}

	public void deleteAllRoleMenuByRole(Role role) {
		roleMenuRepository.deleteByRole(role);
	}

	public Role findWithRoleMenuByRoleIdEagerly(long roleId) {
		return roleRepository.findWithRoleMenuByRoleIdEagerly(roleId);
	}
}
