package corp.sTech.padi.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import corp.sTech.padi.model.Agent;
import corp.sTech.padi.model.Group;
import corp.sTech.padi.repository.AgentRepository;
import corp.sTech.padi.repository.GroupRepository;
import corp.sTech.padi.util.SessionMapUtil;

@Component
public class AgentManagementService {
	@Autowired
	private AgentRepository agentRepository;

	@Autowired
	private GroupRepository groupRepository;

	public Agent saveAgent(Agent agent) {
		return agentRepository.save(agent);
	}

	public List<Agent> getAllAgents() {
		// return (List<Agent>) agentRepository.findByStatus(true);
		if (SessionMapUtil.isSuperAdministrator()) {
			return (List<Agent>) agentRepository.findAll();
		}
		return (List<Agent>) agentRepository.findByAgentId(SessionMapUtil.getAgentSession().getAgentId());
	}

	public Group saveGroup(Group group) {
		return groupRepository.save(group);
	}

	public Agent getAgent(Agent agent) {
		return agentRepository.findOne(agent.getAgentId());
	}
}
