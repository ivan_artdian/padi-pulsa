package corp.sTech.padi.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import corp.sTech.padi.model.GroupMenu;
import corp.sTech.padi.repository.GroupMenuRepository;

@Component
public class GroupMenuService {
	
	@Autowired
	private GroupMenuRepository groupMenuRepository;
	
	public GroupMenu save(GroupMenu groupMenu){
		return groupMenuRepository.save(groupMenu);
	}
	
	public List<GroupMenu> findAll(){
		return (List<GroupMenu>) groupMenuRepository.findAll();
	}
	
	public void delete(GroupMenu groupMenu){
		groupMenuRepository.delete(groupMenu);
	}
	
	public GroupMenu findOne(GroupMenu groupMenu){
		return groupMenuRepository.findOne(groupMenu.getId());
	}

}
