package corp.sTech.padi.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import corp.sTech.padi.model.Group;
import corp.sTech.padi.repository.AgentRepository;
import corp.sTech.padi.repository.GroupRepository;

@Component
public class GroupManagementService {
	@Autowired
	private GroupRepository groupRepository;

	@Autowired
	private AgentRepository agentRepository;

	/*@Autowired
	private GroupPreferenceRepository groupPreferenceRepository;
	*/
	public List<Group> getAllGroups() {
		return (List<Group>) groupRepository.findAll();
	}

	/*public Group findGroupWithGroupPreferenceEagerly(Group group) {
		return groupRepository.findGroupWithGroupPreferenceEagerly(group.getUsername());
	}*/

	public Group getOneGroup(Group group) {
		return groupRepository.findOne(group.getUsername());
	}

	public boolean isGroupDepedency(Group group) {
		boolean isSuperAdministrator = group.getUsername().equals("SUPER_ADMINISTRATOR");
		boolean isAgent = agentRepository.exists(group.getUsername());

		return isSuperAdministrator || isAgent;
	}
}
