package corp.sTech.padi.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import corp.sTech.padi.model.BillerProduct;
import corp.sTech.padi.repository.BillerProductRepository;

@Component
public class BillerProductService {
	
	@Autowired
	private BillerProductRepository billerProductRepository;;
	
	public BillerProduct save(BillerProduct billerProduct){
		return billerProductRepository.save(billerProduct);
	}
	
	public List<BillerProduct> findAll(){
		return (List<BillerProduct>) billerProductRepository.findAll();
	}
	
	public void delete(BillerProduct billerProduct){
		billerProductRepository.delete(billerProduct);
	}
	
	public BillerProduct findOne(BillerProduct billerProduct){
		return billerProductRepository.findOne(billerProduct.getProductId());
	}

}
