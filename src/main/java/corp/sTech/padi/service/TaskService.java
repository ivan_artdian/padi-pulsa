package corp.sTech.padi.service;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import corp.sTech.padi.api.EmailAPI;
import corp.sTech.padi.model.Agent;
import corp.sTech.padi.model.Approval;
import corp.sTech.padi.model.Discount;
import corp.sTech.padi.model.Group;
import corp.sTech.padi.model.GroupMenu;
import corp.sTech.padi.model.Menu;
import corp.sTech.padi.model.Role;
import corp.sTech.padi.model.RoleMenu;
import corp.sTech.padi.model.Scheduler;
import corp.sTech.padi.model.SystemParameter;
import corp.sTech.padi.model.TIbsBill;
import corp.sTech.padi.model.TIbsBillDetail;
import corp.sTech.padi.model.TIbsInvoice;
import corp.sTech.padi.model.User;
import corp.sTech.padi.model.Users;
import corp.sTech.padi.parameter.ActiveParam;
import corp.sTech.padi.parameter.ModelNameParam;
import corp.sTech.padi.repository.AgentRepository;
import corp.sTech.padi.repository.DiscountRepository;
import corp.sTech.padi.repository.GroupMenuRepository;
import corp.sTech.padi.repository.GroupRepository;
import corp.sTech.padi.repository.MenuRepository;
import corp.sTech.padi.repository.RoleMenuRepository;
import corp.sTech.padi.repository.RoleRepository;
import corp.sTech.padi.repository.SchedulerRepository;
import corp.sTech.padi.repository.SystemParameterRepository;
import corp.sTech.padi.repository.TIbsBillDetailRepository;
import corp.sTech.padi.repository.TIbsBillRepository;
import corp.sTech.padi.repository.TIbsInvoiceRepository;
import corp.sTech.padi.repository.UserRepository;
import corp.sTech.padi.repository.UsersRepository;
import corp.sTech.padi.util.CommonUtil;
import corp.sTech.padi.util.MessageUtil;
import corp.sTech.padi.util.NumberFormatUtil;
import corp.sTech.padi.util.RandomString;
import corp.sTech.padi.util.SecureLoginUtil;
import corp.sTech.padi.util.SessionMapUtil;

@Component
public class TaskService {
	@Autowired
	private RoleMenuRepository roleMenuRepository;

	@Autowired
	private MenuRepository menuRepository;

	@Autowired
	private AgentRepository agentRepository;

	@Autowired
	private GroupRepository groupRepository;

	@Autowired
	private RoleRepository roleRepository;

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private UsersRepository usersRepository;

	@Autowired
	private SystemParameterRepository systemParameterRepository;

	@Autowired
	private GroupMenuRepository groupMenuRepository;
	
	@Autowired
	private TIbsBillRepository ibsBillRepository;
	
	@Autowired
	private TIbsBillDetailRepository ibsBillDetailRepository;
	
	@Autowired
	private TIbsInvoiceRepository ibsInvoiceRepository;

	@Autowired
	private SchedulerRepository schedulerRepository;

	@Autowired
	private DiscountRepository discountRepository;
	
	public boolean saveTask(Object oldData, Object newData, Approval approval) {
		try {
			boolean success = false;

			success = doTask(oldData, newData, approval);
			return success;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public boolean doTask(Object oldData, Object newData, Approval approval) {
		boolean success = false;
		String taskName = approval.getTaskName().toUpperCase();
		if (taskName.contains("ADD")) {
			String[] params = new String[] { StringUtils.removeStartIgnoreCase(taskName, "add ") };
			success = doTaskAdd(oldData, newData, approval.getModelName());
			if (success) {
				if(!approval.getModelName().equals(ModelNameParam.T_IBS_BILLDETAIL))
					MessageUtil.addMessageInfo("common.message.info.add", params);
			} else {
				MessageUtil.addMessageError("common.message.error.add", params);
			}
		} else if (taskName.contains("EDIT")) {
			String[] params = new String[] { StringUtils.removeStartIgnoreCase(taskName, "edit ") };
			success = doTaskEdit(oldData, newData, approval.getModelName());
			if (success) {
				MessageUtil.addMessageInfo("common.message.info.edit", params);
			} else {
				MessageUtil.addMessageError("common.message.error.edit", params);
			}
		} else if (taskName.contains("DELETE")) {
			String[] params = new String[] { StringUtils.removeStartIgnoreCase(taskName, "delete ") };
			success = doTaskDelete(oldData, newData, approval.getModelName());
			if (success) {
				MessageUtil.addMessageInfo("common.message.info.delete", params);
			} else {
				MessageUtil.addMessageError("common.message.error.delete", params);
			}
		}

		return success;
	}

	private boolean doTaskAdd(Object oldData, Object newData, String modelName) {
		/*********** modify this to task add approval ***********/
		boolean success = false;
		try {
			if (modelName.equals(ModelNameParam.MENU)) {
				menuRepository.save((Menu) newData);
			} else if (modelName.equals(ModelNameParam.DISCOUNT)) {
				discountRepository.save((Discount) newData);
			} else if (modelName.equals(ModelNameParam.AGENT)) {
				Agent agent = (Agent) newData;
				Group group = new Group();
				group.setUsername(agent.getAgentId());
				group.setStatus(ActiveParam.ACTIVE);
				group.setImageHeader("WhatsApp Image 2018-09-28 at 11.23.41.jpeg");
				groupRepository.save(group);

				Agent savedAgent = agentRepository.save(agent);
				
				User user = new User();
				user.setUsername(savedAgent.getAgentIdWeb());
				user.setPassword(SecureLoginUtil.generateStorngPasswordHash("123"));
				user.setUserLocale("en_US");
				user.setWebTheme("bluesky");
				
				List<Group> groups = new ArrayList<>();
				groups.add(group);
				user.setGroups(groups);
				
				Role role = new Role();
				role.setRoleId(11);
				user.setRole(role);
				
				userRepository.save(user);
			} else if (modelName.equals(ModelNameParam.ROLE)) {
				Role role = (Role) newData;
				
				roleRepository.save(role);
			} else if (modelName.equals(ModelNameParam.USER)) {
				userRepository.save((User) newData);
				SessionMapUtil.put("isSuperAdministrator",
						SessionMapUtil.getGroupNameSession().equals("SUPER_ADMINISTRATOR"));
			} else if (modelName.equals(ModelNameParam.GROUP)) {
				groupRepository.save((Group) newData);
			} else if (modelName.equals(ModelNameParam.SYSTEM_PARAMETER)) {
				systemParameterRepository.save((List<SystemParameter>) newData);

//				systemParamUtil.refresh();
			} else if (modelName.equals(ModelNameParam.GROUP_MENU)) {
				groupMenuRepository.save((GroupMenu) newData);
			} else if (modelName.equals(ModelNameParam.T_IBS_BILL)) {
				TIbsBill ibsBill = (TIbsBill) newData;
				ibsBillRepository.save(ibsBill);
				TIbsBill bill = ibsBillRepository.findOneWithBillInfosByBillId(ibsBill.getBillId());
				List<TIbsBillDetail> billDetails = bill.getBillDetails();
				for (TIbsBillDetail tIbsBillDetail : billDetails) {
					Users users = new Users();
//					users.setUserId(BigInteger.valueOf(RandomString.generateRandom(8)));
					// sts tagihan belum ada
					users.setFullName(tIbsBillDetail.getNamaPelanggan());
					users.setUserName(tIbsBillDetail.getIdPelanggan());
//					users.setPassword(DigestUtils.sha1Hex("123"));
					users.setPassword(CommonUtil.digest("123"));
					users.setPhoneNumber(tIbsBillDetail.getNoHP());
					users.setEmail(tIbsBillDetail.getEmail());
					// nama perumahan belum ada
					users.setAddress(tIbsBillDetail.getAlamat());
					// no block belum ada
					// city perlu id?
					// provinsi perlu id?
					// luas tanah belum ada
					users.setCreateDate(tIbsBillDetail.getCreateDate());
					users.setIsActive("T");
					users.setIsMember("Y");
					usersRepository.save(users);
				}
			} else if (modelName.equals(ModelNameParam.T_IBS_INVOICE)) {
				List<TIbsInvoice> invoices = (List<TIbsInvoice>) ibsInvoiceRepository.save((List<TIbsInvoice>) newData);
				new Thread( () -> {
					if(invoices != null && !invoices.isEmpty()) {
						// send email
						
						Map<String, List<TIbsInvoice>> invoicesMap = new HashMap<>();
						for (TIbsInvoice invoice : invoices) {
							String idPelanggan = invoice.getIbsBillDetail().getIdPelanggan();
							List<TIbsInvoice> value = new ArrayList<>();
							if(invoicesMap.get(idPelanggan) != null) {
								value = invoicesMap.get(idPelanggan);
							}
							value.add(invoice);
							invoicesMap.put(idPelanggan, value);
						}
						
						for (String key : invoicesMap.keySet()) {
							List<TIbsInvoice> list = invoicesMap.get(key);
							
							TIbsBillDetail billDetail = list.get(0).getIbsBillDetail();
							
							String msgDetail = "";
							
							for (TIbsInvoice invoice : list) {
								billDetail = invoice.getIbsBillDetail();
								
								msgDetail = msgDetail + "                                 <tr>\n" + 
														"                                    <td><font color=\"#FFFFFF\" style=\"font-family:'cambria','hoefler text','liberation serif','times','times new roman',serif\">"+invoice.getBillPeriode()+"</font></td>\n" + 
														"                                    <td><font color=\"#FFFFFF\" style=\"font-family:'cambria','hoefler text','liberation serif','times','times new roman',serif\">"+invoice.getAmount()+"</font></td>\n" + 
														"                                 </tr>\n";
							}
							
							String idPelanggan = billDetail.getIdPelanggan();
							String namaPelanggan = billDetail.getNamaPelanggan();
							String noHP = billDetail.getNoHP();
							String email = billDetail.getEmail();
							String noBlock = billDetail.getNoBlock();
							String kelKec = billDetail.getKelKec();
							String kabKota = billDetail.getKabKota();
							String provinsi = billDetail.getProvinsi();
							String luasTanah = billDetail.getLuasTanah();
							
								String body = "<div>\n" + 
										"   <table border=\"1\" frame=\"box\" width=\"800\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\">\n" + 
										"      <tbody>\n" + 
										"         <tr>\n" + 
										"            <td>\n" + 
										"               <table width=\"800\" cellpadding=\"0\" cellspacing=\"0\">\n" + 
										"                  <tbody>\n" + 
										"                     <tr>\n" + 
										"                        <td>\n" + 
										"                           <table width=\"800\">\n" + 
										"                              <tbody>\n" + 
										"                                 <tr>\n" + 
										"                                    <td><img src=\"https://ci3.googleusercontent.com/proxy/BFOjmM-7RAoAMa1dKzHUJnlHX9tyXCcndkvnBj-EquNgpvmCdmnjj9kE2gvtRK1KQ4SFWwqibjcziDvlpVYhmzITn6DkoCZ9Ai2i=s0-d-e1-ft#https://www.padiciti.com/mail/image/logo-padiciti.png\" class=\"CToWUd\"></td>\n" + 
										"                                    <td align=\"right\">\n" + 
										"                                    </td>\n" + 
										"                                 </tr>\n" + 
										"                              </tbody>\n" + 
										"                           </table>\n" + 
										"                        </td>\n" + 
										"                     </tr>\n" + 
										"                     <tr>\n" + 
										"                        <td>\n" + 
										"                           <table>\n" + 
										"                              <tbody>\n" + 
										"                                 <tr>\n" + 
										"                                    <td style=\"font-family:'cambria','hoefler text','liberation serif','times','times new roman',serif\">\n" + 
										"                                       <b>\n" + 
										"                                          <center>Tagihan IPL</center>\n" + 
										"                                       </b>\n" + 
										"                                       <br>\n" + 
										"                                       <p>Kepada Pelanggan&nbsp;<strong>Padiciti</strong>&nbsp;Yang Terhormat,<br>\n" + 
										"                                          Bpk/Ibu&nbsp;<strong>"+namaPelanggan+"</strong>\n" + 
										"                                       </p>\n" + 
										"                                       Terima kasih atas kepercayaan Anda menggunakan&nbsp;<strong>Padiciti</strong>&nbsp;untuk melakukan pembayaran tagihan IPL.\n" + 
										"                                       Dengan ini kami informasikan bahwa Anda memiliki <b>Tagihan IPL</b> dengan data sebagai berikutt:                 \n" + 
										"                                    </td>\n" + 
										"                                 </tr>\n" + 
										"                              </tbody>\n" + 
										"                           </table>\n" + 
										"                        </td>\n" + 
										"                     </tr>\n" + 
										"                     <tr bgcolor=\"#69bd4c\">\n" + 
										"                        <td>\n" + 
										"                           <table width=\"458\">\n" + 
										"                              <tbody>\n" + 
										"                                 <tr>\n" + 
										"                                    <td width=\"199\"><font color=\"#FFFFFF\" style=\"font-family:'cambria','hoefler text','liberation serif','times','times new roman',serif\">Id Pelanggan</font></td>\n" + 
										"                                    <td width=\"231\"><font color=\"#FFFFFF\" style=\"font-family:'cambria','hoefler text','liberation serif','times','times new roman',serif\"><strong>"+idPelanggan+"</strong></font></td>\n" + 
										"                                 </tr>\n" + 
										"                                 <tr>\n" + 
										"                                    <td><font color=\"#FFFFFF\" style=\"font-family:'cambria','hoefler text','liberation serif','times','times new roman',serif\">Nama Pelanggan</font></td>\n" + 
										"                                    <td><font color=\"#FFFFFF\" style=\"font-family:'cambria','hoefler text','liberation serif','times','times new roman',serif\">"+namaPelanggan+"</font></td>\n" + 
										"                                 </tr>\n" + 
										"                                 <tr>\n" + 
										"                                    <td><font color=\"#FFFFFF\" style=\"font-family:'cambria','hoefler text','liberation serif','times','times new roman',serif\">No HP</font></td>\n" + 
										"                                    <td><font color=\"#FFFFFF\" style=\"font-family:'cambria','hoefler text','liberation serif','times','times new roman',serif\">"+noHP+"</font></td>\n" + 
										"                                 </tr>\n" + 
										"                                 <tr>\n" + 
										"                                    <td><font color=\"#FFFFFF\" style=\"font-family:'cambria','hoefler text','liberation serif','times','times new roman',serif\">Email</font></td>\n" + 
										"                                    <td><font color=\"#FFFFFF\" style=\"font-family:'cambria','hoefler text','liberation serif','times','times new roman',serif\">"+email+"</font></td>\n" + 
										"                                 </tr>\n" + 
										"                                 <tr>\n" + 
										"                                    <td><font color=\"#FFFFFF\" style=\"font-family:'cambria','hoefler text','liberation serif','times','times new roman',serif\">No Block</font></td>\n" + 
										"                                    <td><font color=\"#FFFFFF\" style=\"font-family:'cambria','hoefler text','liberation serif','times','times new roman',serif\">"+noBlock+"</font></td>\n" + 
										"                                 </tr>\n" + 
										"                                 <tr>\n" + 
										"                                    <td><font color=\"#FFFFFF\" style=\"font-family:'cambria','hoefler text','liberation serif','times','times new roman',serif\">Kel / Kec</font></td>\n" + 
										"                                    <td><font color=\"#FFFFFF\" style=\"font-family:'cambria','hoefler text','liberation serif','times','times new roman',serif\">"+kelKec+"</font></td>\n" + 
										"                                 </tr>\n" + 
										"                                 <tr>\n" + 
										"                                    <td><font color=\"#FFFFFF\" style=\"font-family:'cambria','hoefler text','liberation serif','times','times new roman',serif\">Kab / Kota</font></td>\n" + 
										"                                    <td><font color=\"#FFFFFF\" style=\"font-family:'cambria','hoefler text','liberation serif','times','times new roman',serif\">"+kabKota+"</font></td>\n" + 
										"                                 </tr>\n" + 
										"                                 <tr>\n" + 
										"                                    <td><font color=\"#FFFFFF\" style=\"font-family:'cambria','hoefler text','liberation serif','times','times new roman',serif\">Provinsi</font></td>\n" + 
										"                                    <td><font color=\"#FFFFFF\" style=\"font-family:'cambria','hoefler text','liberation serif','times','times new roman',serif\">"+provinsi+"</font></td>\n" + 
										"                                 </tr>\n" + 
										"                                 <tr>\n" + 
										"                                    <td><font color=\"#FFFFFF\" style=\"font-family:'cambria','hoefler text','liberation serif','times','times new roman',serif\">Luas Tanah</font></td>\n" + 
										"                                    <td><font color=\"#FFFFFF\" style=\"font-family:'cambria','hoefler text','liberation serif','times','times new roman',serif\">"+luasTanah+"</font></td>\n" + 
										"                                 </tr>\n" + 
										"                                  <tr>\n" + 
										"                                    <td>&nbsp;\n" + 
										"                                    </td>\n" + 
										"                                 </tr>\n" + 
										"                                 <tr>\n" + 
										"                                    <td><font color=\"#FFFFFF\" style=\"font-family:'cambria','hoefler text','liberation serif','times','times new roman',serif\">Daftar Tagihan</font></td>\n" + 
										"                                 </tr>\n" + msgDetail +
										"                              </tbody>\n" + 
										"                           </table>\n" + 
										"                        </td>\n" + 
										"                     </tr>\n" + 
										"                     <tr>\n" + 
										"                        <td>&nbsp;\n" + 
										"                        </td>\n" + 
										"                     </tr>\n" + 
										"                     <tr>\n" + 
										"                        <td>\n" + 
										"                           <table width=\"80%\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" border=\"1\">\n" + 
										"                              <tbody>\n" + 
										"								  <tr>\n" +
										"									 <td align=\"center\">\n" +
										"										<p style=\"font-family:'cambria','hoefler text','liberation serif','times','times new roman',serif\"> <font color=\"#000000\">Silakan download <b>Padibill</b> untuk detail informasi dan cara melakukan pembayaran</font><br><br>\n" +
										"									 		<img src=\"https://www.padiciti.com/mail/image/logo-download.png\" width=\"30%\" height=\"30%\">\n" +
										"										</p>\n" + 
										"									 </td>\n" +
										"								  </tr>\n" +
										"                                 <tr>\n" + 
										"                                    <td align=\"center\" bgcolor=\"#F0F0EE\">\n" + 
										"                                       <p style=\"font-family:'cambria','hoefler text','liberation serif','times','times new roman',serif\"><font color=\"#000000\">Jika membutuhkan bantuan, Anda dapat menghubungi kami di:</font><br><br>\n" + 
										"                                          <img src=\"https://www.padiciti.com/mail/image/logo-paguyuban.jpg\" width=\"18%\" height=\"18%\" class=\"CToWUd\">\n" +  
										"                                       </p>\n" + 
										"                                       <p style=\"font-family:'cambria','hoefler text','liberation serif','times','times new roman',serif\"><span style=\"color:#000000\"><font color=\"#000000\"><font size=\"-1\"></font><br>\n" + 
										"                                          Customer Service (Jam Kantor/Kerja) : 081280910021 <br>\n" + 
										"                                          Email: <a href=\"mailto:puspitaloka.bsd@gmail.com\" target=\"_blank\">puspitaloka.bsd@gmail.com</a><br>\n" + 
										"                                          Website: <a href=\"https://www.padiciti.com\" target=\"_blank\" data-saferedirecturl=\"https://www.google.com/url?q=https://www.padiciti.com&amp;source=gmail&amp;ust=1539170109532000&amp;usg=AFQjCNGmJtcWOqmrXGKgA42XzxiSlgdXuA\">www.padiciti.com</a></font></span><br>\n" + 
										"                                       </p>\n" + 
										"                                    </td>\n" + 
										"                                 </tr>\n" + 
										"                              </tbody>\n" + 
										"                           </table>\n" + 
										"                           <br>\n" + 
										"                        </td>\n" + 
										"                     </tr>\n" + 
										"                  </tbody>\n" + 
										"               </table>\n" + 
										"            </td>\n" + 
										"         </tr>\n" + 
										"      </tbody>\n" + 
										"   </table>\n" + 
										"   <div class=\"yj6qo\"></div>\n" + 
										"   <div class=\"adL\">\n" + 
										"   </div>\n" + 
										"</div>";
								EmailAPI.sendMail(email, "Info Tagihan", body, null);
						}
					}
				}).start();
			} else if (modelName.equals(ModelNameParam.SCHEDULER)) {
				schedulerRepository.save((Scheduler) newData);
			} else if (modelName.equals(ModelNameParam.T_IBS_BILLDETAIL)) {
				List<TIbsBillDetail> ibsBillDetails = ((List<TIbsBillDetail>) newData);
				if (ibsBillDetails != null && ibsBillDetails.size() > 0) {
					TIbsBill ibsBill = ibsBillDetails.get(0).getIbsBill();
					long billId = ibsBill.getBillId();
					if(billId <= 0) {
						ibsBill = ibsBillRepository.save(ibsBill);
						
					}
					
					for (TIbsBillDetail tIbsBillDetail : ibsBillDetails) {
						tIbsBillDetail.setIbsBill(ibsBill);
//						tIbsBillDetail.setAmount(String.valueOf(NumberFormatUtil.parseNumberToInt(amount) * NumberFormatUtil.parseNumberToInt(tIbsBillDetail.getLuasTanah())));
					}
					ibsBillDetailRepository.save(ibsBillDetails);
				}
				
				for (TIbsBillDetail tIbsBillDetail : ibsBillDetails) {
					if(tIbsBillDetail.getEmail() != null && !tIbsBillDetail.getEmail().isEmpty()) {
						Users users = new Users();
	//					users.setUserId(BigInteger.valueOf(RandomString.generateRandom(8)));
						// sts tagihan belum ada
						users.setFullName(tIbsBillDetail.getNamaPelanggan());
						users.setUserName(tIbsBillDetail.getIdPelanggan());
	//					users.setPassword(DigestUtils.sha1Hex("123"));
						users.setPassword(CommonUtil.digest("123"));
						String noHP = tIbsBillDetail.getNoHP();
						users.setPhoneNumber(noHP.length() > 30 ? noHP.substring(0,30) : noHP);
						String email = tIbsBillDetail.getEmail();
						users.setEmail(email.length() > 50 ? email.substring(0,30) : email);
						// nama perumahan belum ada
						users.setAddress(tIbsBillDetail.getAlamat());
						// no block belum ada
						// city perlu id?
						// provinsi perlu id?
						// luas tanah belum ada
						users.setCreateDate(tIbsBillDetail.getCreateDate());
						users.setIsActive("T");
						users.setIsMember("Y");
						
						List<Users> usersExistings = usersRepository.findByEmail(email);
						if(usersExistings != null && usersExistings.size() > 0) {
							Users usersExisting = usersExistings.get(0);
							usersExisting.setUserName(tIbsBillDetail.getIdPelanggan());
							usersRepository.save(usersExisting);
						}else {
							usersRepository.save(users);
						}
					}
				}
			}
			/*else if (modelName.equals(ModelNameParam.TRAVEL_CONFIG)) {
				travelConfigRepository.save((TravelConfig) newData); 
				}*/
			else {
				return false;
			}
			success = true;
		} catch (Exception e) {
			e.printStackTrace();
			success = false;
		}
		return success;
	}

	private boolean doTaskEdit(Object oldData, Object newData, String modelName) {
		/*********** modify this to task edit approval ***********/
		boolean success = false;
		try {
			if (modelName.equals(ModelNameParam.MENU)) {
				menuRepository.save((Menu) newData);
			} else if (modelName.equals(ModelNameParam.DISCOUNT)) {
				discountRepository.save((Discount) newData);
			} else if (modelName.equals(ModelNameParam.AGENT)) {
				Agent agent = (Agent) newData;
				agentRepository.save(agent);

				Group group = groupRepository.findOne(agent.getAgentId());
				if (group != null) {
					group.setStatus(agent.isStatus());
					groupRepository.save(group);
				}
			} else if (modelName.equals(ModelNameParam.ROLE)) {
				Role role = (Role) newData;
				Role oldRole = (Role) oldData;

				List<RoleMenu> roleMenus = role.getRoleMenus();
				List<RoleMenu> oldRoleMenus = oldRole.getRoleMenus();
				for (RoleMenu roleMenu : oldRoleMenus) {
					roleMenuRepository.delete(roleMenu.getId());
				}
				List<RoleMenu> newRoleMenus = (List<RoleMenu>) roleMenuRepository.save(roleMenus);

				role.setRoleMenus(newRoleMenus);
				roleRepository.save(role);
			} else if (modelName.equals(ModelNameParam.USER)) {
				userRepository.save((User) newData);

				if (((User) newData).getUsername().equals(SessionMapUtil.getUserSession().getUsername()))
					SessionMapUtil.put("user", (User) newData);

				SessionMapUtil.put("isSuperAdministrator",
						SessionMapUtil.getGroupNameSession().equals("SUPER_ADMINISTRATOR"));
			} else if (modelName.equals(ModelNameParam.GROUP)) {
				Group group = groupRepository.save((Group) newData);
				User userSession = SessionMapUtil.getUserSession();

				List<Group> groups = new ArrayList<>();
				groups.add(group);
				userSession.setGroups(groups);
				User savedUser = userRepository.save(userSession);

				SessionMapUtil.put("user", savedUser);
			} else if (modelName.equals(ModelNameParam.SYSTEM_PARAMETER)) {
				systemParameterRepository.save((List<SystemParameter>) newData);

//				systemParamUtil.refresh();
			} else if (modelName.equals(ModelNameParam.GROUP_MENU)) {
				groupMenuRepository.save((GroupMenu) newData);
			} else if (modelName.equals(ModelNameParam.T_IBS_INVOICE)) {
				ibsInvoiceRepository.save((TIbsInvoice) newData);
			} else if (modelName.equals(ModelNameParam.SCHEDULER)) {
				schedulerRepository.save((Scheduler) newData);
			} else if (modelName.equals(ModelNameParam.T_IBS_BILL)) {
				TIbsBill ibsBill = (TIbsBill) newData;
				ibsBillRepository.save(ibsBill);
			}
			/*else if (modelName.equals(ModelNameParam.TRAVEL_CONFIG)) {
				travelConfigRepository.save((TravelConfig) newData);
				}*/
			else {
				return false;
			}
			success = true;
		} catch (Exception e) {
			e.printStackTrace();
			success = false;
		}
		return success;
	}

	private boolean doTaskDelete(Object oldData, Object newData, String modelName) {
		/*********** modify this to task delete approval ***********/
		boolean success = false;
		try {
			if (modelName.equals(ModelNameParam.MENU)) {
				Menu menu = (Menu) oldData;
				roleMenuRepository.deleteByMenu(menu);

				menuRepository.delete(menu);
			} else if (modelName.equals(ModelNameParam.AGENT)) {
				Agent agent = agentRepository.save((Agent) newData);

				Group group = groupRepository.findOne(agent.getAgentId());
				if (group != null) {
					group.setStatus(agent.isStatus());
					groupRepository.save(group);
				}
			} else if (modelName.equals(ModelNameParam.ROLE)) {
				Role oldRole = (Role) oldData;

				roleMenuRepository.deleteByRole(oldRole);
				oldRole.setRoleMenus(null);
				roleRepository.delete(oldRole);
			} else if (modelName.equals(ModelNameParam.USER)) {
				User user = (User) oldData;

				List<Group> groups = new ArrayList<>();
				user.setGroups(groups);
				userRepository.save(user);

				userRepository.delete(user);
			} else if (modelName.equals(ModelNameParam.GROUP)) {
				Group group = (Group) oldData;

				groupRepository.delete(group);
			} else if (modelName.equals(ModelNameParam.SYSTEM_PARAMETER)) {
				systemParameterRepository.delete((List<SystemParameter>) oldData);

//				systemParamUtil.refresh();
			} else if (modelName.equals(ModelNameParam.GROUP_MENU)) {
				groupMenuRepository.delete((GroupMenu) oldData);
			} else if (modelName.equals(ModelNameParam.T_IBS_INVOICE)) {
				ibsInvoiceRepository.delete((List<TIbsInvoice>) oldData);
			} else if (modelName.equals(ModelNameParam.SCHEDULER)) {
				schedulerRepository.delete((Scheduler) oldData);
			} else if (modelName.equals(ModelNameParam.DISCOUNT)) {
				discountRepository.delete((Discount) oldData);
			} else if (modelName.equals(ModelNameParam.T_IBS_BILLDETAIL)) {
				TIbsBillDetail ibsBillDetail = (TIbsBillDetail) newData;
				ibsBillDetailRepository.save(ibsBillDetail);
			}
			/* else if (modelName.equals(ModelNameParam.TRAVEL_CONFIG)) {
				travelConfigRepository.delete((TravelConfig) oldData);
			}*/
			else {
				return false;
			}
			success = true;
		} catch (Exception e) {
			e.printStackTrace();
			success = false;
		}
		return success;
	}

	public RoleMenuRepository getRoleMenuRepository() {
		return roleMenuRepository;
	}

	public void setRoleMenuRepository(RoleMenuRepository roleMenuRepository) {
		this.roleMenuRepository = roleMenuRepository;
	}

	public MenuRepository getMenuRepository() {
		return menuRepository;
	}

	public void setMenuRepository(MenuRepository menuRepository) {
		this.menuRepository = menuRepository;
	}

	public DiscountRepository getDiscountRepository() {
		return discountRepository;
	}

	public void setDiscountRepository(DiscountRepository discountRepository) {
		this.discountRepository = discountRepository;
	}
	
	
}
