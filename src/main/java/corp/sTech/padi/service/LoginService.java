package corp.sTech.padi.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import corp.sTech.padi.model.Agent;
import corp.sTech.padi.model.Group;
import corp.sTech.padi.model.Menu;
import corp.sTech.padi.model.Role;
import corp.sTech.padi.model.RoleMenu;
import corp.sTech.padi.model.User;
import corp.sTech.padi.repository.AgentRepository;
import corp.sTech.padi.repository.GroupRepository;
import corp.sTech.padi.repository.RoleMenuRepository;
import corp.sTech.padi.repository.UserRepository;

@Component
public class LoginService {
	@Autowired
	private UserRepository userRepository;

	@Autowired
	private GroupRepository groupRepository;

	@Autowired
	private RoleMenuRepository roleMenuRepository;

	@Autowired
	private AgentRepository agentRepository;

	public User doLogin(String username, String password, Group group) {
		User user = userRepository.findByUsernameAndPasswordAndGroups(username, password, group);
		return user;
	}

	public User doLogin(String username, String password) {
		/*lazy*/
		// User user = userRepository.findByUsernameAndPassword(username,
		// password);
		User user = userRepository.findRoleByUsernameAndPasswordEagerLy(username, password);
		return user;
	}

	public User findUser(String username) {
		return userRepository.findByUsername(username);
	}

	public User findUserEagerly(String username) {
		return userRepository.findByUserWithRoleEagerly(username);
	}

	public void doLogout() {
		ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
		externalContext.invalidateSession();

		try {
			externalContext.redirect(externalContext.getRequestContextPath());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public List<Menu> getAllMenu(Role role){
		List<RoleMenu> roleMenus = roleMenuRepository.findByRole(role);
		List<Menu> result = new ArrayList<Menu>();
		for (RoleMenu roleMenu : roleMenus) {
			result.add(roleMenu.getMenu());
		}
		return result;
	}

	public User updateLogin(User user) {
		User userLogin = userRepository.save(user);
		return findUser(userLogin.getUsername());
	}

	public Agent getAgentByGroup(Group group) {
		List<Agent> agents = agentRepository.findByAgentId(group.getUsername());
		if (agents != null && agents.size() > 0) {
			return agents.get(0);
		} else {
			return null;
		}
	}
}
