package corp.sTech.padi.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import corp.sTech.padi.model.Agent;
import corp.sTech.padi.model.BookingIpl;
import corp.sTech.padi.repository.BookingIplRepository;
import corp.sTech.padi.util.SessionMapUtil;

@Component
public class TransactionSummaryService {

	@Autowired
	private BookingIplRepository bookingIplRepository;
	
	public List<BookingIpl> getAllBookingIpl() {
		Agent agent = SessionMapUtil.getAgentSession();
		List<BookingIpl> bookingIpls = bookingIplRepository.findByAgent(agent);
		return bookingIpls;
	}
}
