package corp.sTech.padi.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import corp.sTech.padi.model.Discount;
import corp.sTech.padi.repository.DiscountRepository;

@Component
public class DiscountManagementService {
	@Autowired
	private DiscountRepository discountRepository;

	public List<Discount> getAllDiscounts() {
		return (List<Discount>) discountRepository.findAll();
	}

	public Discount saveDiscount(Discount discount) {
		return discountRepository.save(discount);
	}
	
	public Discount getDiscount(Discount discount) {
		return discountRepository.findOne(discount.getIdDiscount());
	}
}
