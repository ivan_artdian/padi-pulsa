package corp.sTech.padi.service;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import corp.sTech.padi.model.Agent;
import corp.sTech.padi.model.Scheduler;
import corp.sTech.padi.repository.AgentRepository;
import corp.sTech.padi.repository.SchedulerRepository;

@Component
public class SchedulerManagementService implements Serializable {

    @Autowired
    private SchedulerRepository schedulerRepository;

    @Autowired
    private AgentRepository agentRepository;

    public List<Scheduler> getAllScheduler() {
        return (List<Scheduler>) schedulerRepository.findAll();
    }

    public List<Agent> getAllAgent() {
        return (List<Agent>) agentRepository.findAll();
    }

    public Agent findAgentByAgentId(String agentID) {
        return agentRepository.findOne(agentID);
    }

    public Scheduler findScheduler(Scheduler s) {
        return schedulerRepository.findOne(s.getSchedulerID());
    }
}
