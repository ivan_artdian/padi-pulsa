package corp.sTech.padi.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import corp.sTech.padi.model.Agent;
import corp.sTech.padi.model.SystemParameter;
import corp.sTech.padi.repository.SystemParameterRepository;
import corp.sTech.padi.util.SessionMapUtil;

@Component 
public class SystemParameterService {
	
	@Autowired 
	private SystemParameterRepository systemParameterRepository;
	
	public SystemParameter save(SystemParameter systemParameter){
		return systemParameterRepository.save(systemParameter);
	}
	
	public List<SystemParameter> findAll(){
		Agent agent = SessionMapUtil.getAgentSession();
		System.out.println(agent.getAgentId());
		return (List<SystemParameter>) systemParameterRepository.findByAgent(agent);
	}
	
	public void delete(SystemParameter systemParameter) {
		systemParameterRepository.delete(systemParameter);
	}

	public SystemParameter findOne(SystemParameter systemParameter) {
		return systemParameterRepository.findOne(systemParameter.getParamId());
	}
	
	public SystemParameter findSystemParameter(String paramName) {
		return systemParameterRepository.findByParamName(paramName);
	}
	
}