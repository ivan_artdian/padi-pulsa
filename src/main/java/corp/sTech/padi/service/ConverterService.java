package corp.sTech.padi.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import corp.sTech.padi.model.Agent;
import corp.sTech.padi.model.Group;
import corp.sTech.padi.model.GroupMenu;
import corp.sTech.padi.model.Menu;
import corp.sTech.padi.model.Role;
import corp.sTech.padi.repository.AgentRepository;
import corp.sTech.padi.repository.GroupMenuRepository;
import corp.sTech.padi.repository.GroupRepository;
import corp.sTech.padi.repository.MenuRepository;
import corp.sTech.padi.repository.RoleRepository;

@Component
public class ConverterService {
	@Autowired
	private RoleRepository roleRepository;
	
	@Autowired
	private MenuRepository menuRepository;

	@Autowired
	private GroupMenuRepository groupMenuRepository;
	
	@Autowired
	private AgentRepository agentRepository;
	
	@Autowired
	private GroupRepository groupRepository;
	
	public List<Role> findRoleByRoleId(String roleId) {
		return roleRepository.findByRoleId(Long.valueOf(roleId));
	}

	public List<Menu> findMenuByMenuId(String menuId) {
		return menuRepository.findByMenuId(Long.valueOf(menuId));
	}

	public List<GroupMenu> findGroupMenuByGroupMenuId(long groupMenuId) {
		return groupMenuRepository.findById(groupMenuId);
	}
	
	public List<Agent> findAgentByAgentId(String agentId) {
		return agentRepository.findByAgentId(agentId);
	}
	
	public List<Group> findGroupByGroupId(String groupId) {
		return groupRepository.findByUsername(groupId);
	}
}
