package corp.sTech.padi.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import corp.sTech.padi.model.Agent;
import corp.sTech.padi.model.TIbsBill;
import corp.sTech.padi.model.TIbsBillDetail;
import corp.sTech.padi.model.TIbsInvoice;
import corp.sTech.padi.model.Users;
import corp.sTech.padi.parameter.SystemParameter;
import corp.sTech.padi.repository.SystemParameterRepository;
import corp.sTech.padi.repository.TIbsBillDetailRepository;
import corp.sTech.padi.repository.TIbsBillRepository;
import corp.sTech.padi.repository.TIbsInvoiceRepository;
import corp.sTech.padi.repository.UsersRepository;
import corp.sTech.padi.util.SessionMapUtil;

@Component
public class BillInvoiceManagementService {

	@Autowired
	private TIbsBillRepository ibsBillRepository;
	
	@Autowired
	private TIbsBillDetailRepository ibsBillDetailRepository;
	
	@Autowired
	private TIbsInvoiceRepository ibsInvoiceRepository;
	
	@Autowired
	private SystemParameterRepository systemParameterRepository;
	
	@Autowired
	private UsersRepository usersRepository;

	public List<TIbsBill> getAllIbsBills() {
		Agent agent = SessionMapUtil.getAgentSession();
		return (List<TIbsBill>) ibsBillRepository.findByAgent(agent);
	}

	public TIbsBill findOne(TIbsBill bill) {
		return ibsBillRepository.findOne(bill.getBillId());
	}
	
	public TIbsBill findByAgent(Agent agent) {
		return ibsBillRepository.findTop1ByAgent(agent);
	}

	public TIbsBill getDetailById(long id) {
		return ibsBillRepository.findOneWithBillInfosByBillId(id);
	}

	public List<TIbsInvoice> getAllInvoiceBYAgent(Agent agent) {
		return ibsInvoiceRepository.findByIbsBillDetail_IbsBill_Agent(agent);
	}
	
	public List<TIbsInvoice> getAllInvoice() {
		boolean superAdministrator = SessionMapUtil.isSuperAdministrator();
		if (superAdministrator) {
			return (List<TIbsInvoice>) ibsInvoiceRepository.findByOrderByCreateDateDesc();
		}
		
		Agent agent = SessionMapUtil.getAgentSession();
		return ibsInvoiceRepository.findByIbsBillDetail_IbsBill_AgentOrderByCreateDateDesc(agent);
	}
	
	public TIbsInvoice findOneInvoice(TIbsInvoice ibsInvoice) {
		return ibsInvoiceRepository.findOne(ibsInvoice.getBillId());
	}
	
	public String getHarga() {
		Agent agent = SessionMapUtil.getAgentSession();
		corp.sTech.padi.model.SystemParameter systemParameter = systemParameterRepository.findByParamNameAndAgent(SystemParameter.IPL_PARAMATER_HARGA, agent);
		if(systemParameter != null) {
			return systemParameter.getParamValue();
		}
		return null;
	}
	
	public String getDenda() {
		Agent agent = SessionMapUtil.getAgentSession();
		corp.sTech.padi.model.SystemParameter systemParameter = systemParameterRepository.findByParamNameAndAgent(SystemParameter.IPL_PARAMATER_DENDA, agent);
		if(systemParameter != null) {
			return systemParameter.getParamValue();
		}
		return null;
	}
	
	public List<String> getAllIdPelanggan(){
		Agent agent = SessionMapUtil.getAgentSession();
		List<TIbsBillDetail> list = ibsBillDetailRepository.findByIbsBill_Agent(agent);
		List<String> idPelanggans = new ArrayList<>();
		for (TIbsBillDetail tIbsBillDetail : list) {
			if(tIbsBillDetail.isActive())
				idPelanggans.add(tIbsBillDetail.getIdPelanggan());
		}
		return idPelanggans;
		
	}
	
	public List<TIbsBillDetail> getAllBillDetails(){
		Agent agent = SessionMapUtil.getAgentSession();
		List<TIbsBillDetail> list = ibsBillDetailRepository.findByIbsBill_Agent(agent);
		List<TIbsBillDetail> activeBillDetails = new ArrayList<>();
		for (TIbsBillDetail tIbsBillDetail : list) {
			if(tIbsBillDetail.isActive())
				activeBillDetails.add(tIbsBillDetail);
		}
		return activeBillDetails;
		
	}
	
	public List<TIbsBillDetail> getAllIbsBillDetailByIdPelanggan(List<String> idPelanggans){
		List<TIbsBillDetail> list = ibsBillDetailRepository.findByIdPelangganIn(idPelanggans);
		return list;
		
	}
	
	public boolean checkExistingIdPelanggan(String idPelanggan) {
		TIbsBillDetail billDetail = ibsBillDetailRepository.findByIdPelanggan(idPelanggan);
		return billDetail != null;
	}
	
	public boolean checkDuplicateBill(TIbsBillDetail ibsBillDetail, String billPeriode) {
		List<TIbsInvoice> list = ibsInvoiceRepository.findByIbsBillDetailAndBillPeriode(ibsBillDetail, billPeriode);
		if(list != null && list.size() > 0) {
			return true;
		}
		return false;
	}
}
