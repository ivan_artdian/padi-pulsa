package corp.sTech.padi.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import corp.sTech.padi.model.GroupMenu;
import corp.sTech.padi.model.Menu;
import corp.sTech.padi.model.Role;
import corp.sTech.padi.model.RoleMenu;
import corp.sTech.padi.repository.GroupMenuRepository;
import corp.sTech.padi.repository.MenuRepository;
import corp.sTech.padi.repository.RoleMenuRepository;

@Component
public class PageMenuService {
	@Autowired
	private RoleMenuRepository roleMenuRepository;

	/*@Autowired
	private GroupPreferenceRepository groupPreferenceRepository;*/

	@Autowired
	private GroupMenuRepository groupMenuRepository;

	@Autowired
	private MenuRepository menuRepository;

	public List<Menu> getAllMenu(Role role) {
		List<RoleMenu> roleMenus = roleMenuRepository.findByRole(role);
		List<Menu> result = new ArrayList<Menu>();
		for (RoleMenu roleMenu : roleMenus) {
			result.add(roleMenu.getMenu());
		}
		Collections.sort(result,
				(o1, o2) -> Integer.valueOf(o1.getMenuLevel()).compareTo(Integer.valueOf(o2.getMenuLevel())));
		return result;
	}
	/*
		public String getPreferenceValue(Group group, String preferenceKey) {
			Preference preference = new Preference();
			preference.setKey(preferenceKey);
			GroupPreference groupPreference = groupPreferenceRepository.findByGroupAndPreference(group, preference);
			return groupPreference == null ? "" : groupPreference.getValue();
		}*/

	public List<GroupMenu> getAllGroupAndMenu() {
		return (List<GroupMenu>) groupMenuRepository.findAll();
	}

	public Menu getMenuByUrlLike(String url) {
		return menuRepository.findByUrlLike(url);
	}
}
