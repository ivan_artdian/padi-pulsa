package corp.sTech.padi.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import corp.sTech.padi.model.GroupMenu;
import corp.sTech.padi.model.Menu;
import corp.sTech.padi.repository.GroupMenuRepository;
import corp.sTech.padi.repository.MenuRepository;

@Component
public class MenuManagementService {
	@Autowired
	private MenuRepository menuRepository;

	@Autowired
	private GroupMenuRepository groupMenuRepository;

	public Menu save(Menu menu) {
		return menuRepository.save(menu);
	}

	public List<Menu> findAll() {
		return (List<Menu>) menuRepository.findAll();
	}

	public void delete(Menu menu) {
		menuRepository.delete(menu);
	}

	public Menu findOne(Menu menu) {
		return menuRepository.findOne(menu.getMenuId());
	}

	public List<GroupMenu> findAllGroupMenu() {
		return (List<GroupMenu>) groupMenuRepository.findAll();
	}
}
