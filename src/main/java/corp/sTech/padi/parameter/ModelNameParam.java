package corp.sTech.padi.parameter;

public class ModelNameParam {

	private static final String PACKAGE_SEPARATOR = ".";
	private static final String PACKAGE_NAME = "corp.sTech.padi.model" + PACKAGE_SEPARATOR;
	/*********************** Class Name **************************************/
	public static final String MENU = PACKAGE_NAME + "Menu";
	public static final String MOBILE_THEME = PACKAGE_NAME + "MobileTheme";
	public static final String AGENT = PACKAGE_NAME + "Agent";
	public static final String NOTIFICATION = PACKAGE_NAME + "Notification";
	public static final String SUBSCRIBER = PACKAGE_NAME +"Subscriber";
	public static final String PARTNER = PACKAGE_NAME + "Partner";
	public static final String ROLE = PACKAGE_NAME + "Role";
	public static final String USER = PACKAGE_NAME + "User";
	public static final String MAP_LOCATION = PACKAGE_NAME + "MapLocation";
	public static final String PROMO = PACKAGE_NAME + "Promo";
	public static final String INFORMATIONAL = PACKAGE_NAME + "Informational";
	public static final String MOBILE_MENU = PACKAGE_NAME + "MobileMenu";
	public static final String MOBILE_MENU_LIST = MOBILE_MENU + "List";
	public static final String MOBILE_PAGE = PACKAGE_NAME + "MobilePage";
	public static final String MOBILE_INPUT = PACKAGE_NAME + "MobileInput";
	public static final String TERM_CONDITION = PACKAGE_NAME + "TermCondition";
	public static final String TCommerceBillInfo = PACKAGE_NAME + "TCommerceBillInfo";
	public static final String TCommerceBillpayment = PACKAGE_NAME + "TCommerceBillpayment";
	public static final String TCommerceBills = PACKAGE_NAME + "TCommerceBills";
	public static final String BUS_SCHEDULLE = PACKAGE_NAME + "BusSchedulle";
	public static final String GROUP = PACKAGE_NAME + "Group";
	public static final String SYSTEM_PARAMETER = PACKAGE_NAME + "SystemParameter";
	public static final String MESSAGE_CONFIG = PACKAGE_NAME + "MessageConfig";
	public static final String GROUP_MENU = PACKAGE_NAME + "GroupMenu";
	public static final String T_IBS_BILL = PACKAGE_NAME + "TIbsBill";
	public static final String T_IBS_BILLDETAIL = PACKAGE_NAME + "TIbsBillDetail";
	public static final String T_IBS_BILLINFO = PACKAGE_NAME + "TIbsBillInfo";
	public static final String PAYMENT_MECHANISM = PACKAGE_NAME + "PaymentMechanism";
	public static final String HARMONIZED_CONFIG = PACKAGE_NAME + "HarmonizedConfig";
	public static final String PAYMENT_METHOD = PACKAGE_NAME + "PaymentMethod";
	public static final String APP_TXN_LOG = PACKAGE_NAME + "AppTxnLog";
	public static final String APP_STEP_LOG = PACKAGE_NAME + "AppStepLog";
	public static final String APP_DETAIL_LOG = PACKAGE_NAME + "AppDetailLog";
	public static final String RESPONSE_CODE_CONFIG = PACKAGE_NAME + "ResponseCodeConfig";
	public static final String MOBILE_MENU_FAVOURITE = PACKAGE_NAME + "MobileMenuFavourite";
	public static final String COUPON = PACKAGE_NAME + "Coupon";
	public static final String FAQ = PACKAGE_NAME + "Faq";
	public static final String STORE = PACKAGE_NAME + "Store";
	public static final String TRAVEL = PACKAGE_NAME + "Travel";
	public static final String TRAVEL_CONFIG = PACKAGE_NAME + "TravelConfig";
	public static final String BADAN_ZAKAT = PACKAGE_NAME + "BadanZakat";
	public static final String DOA = PACKAGE_NAME + "Doa";
	public static final String BUSSINESS_PACKAGE = PACKAGE_NAME + "BussinessPackage";
	public static final String MERCHANT = PACKAGE_NAME + "Merchant";
	public static final String BUSSINESS_TOUR = PACKAGE_NAME + "BussinessTour";
	public static final String TOUR_PARTICIPANTS = PACKAGE_NAME + "TourParticipants";
	public static final String BUSSINESS_ZIS = PACKAGE_NAME + "BussinessZis";
	public static final String MESSAGE_TEMPLATE = PACKAGE_NAME + "MessageTemplate";
	public static final String MESSAGE_CONTENT = PACKAGE_NAME + "MessageContent";
	public static final String TOUR_SCHEDULE = PACKAGE_NAME + "TourSchedule";
	public static final String TOUR_ITINERARY = PACKAGE_NAME + "TourItinerary";
	public static final String ANNOUNCE = PACKAGE_NAME + "Announce";
	public static final String FACILITY = PACKAGE_NAME + "Facility";
	public static final String TRANSPORTATION = PACKAGE_NAME + "Transportation";
    public static final String MOBILE_APP_VERSION = PACKAGE_NAME + "MobileAppVersion";
    public static final String AIRPORT = PACKAGE_NAME + "Airport";
    public static final String T_IBS_INVOICE = PACKAGE_NAME + "TIbsInvoice";
    public static final String SCHEDULER = PACKAGE_NAME + "Scheduler";
    public static final String DISCOUNT = PACKAGE_NAME + "Discount";
	/*********************** Class Name **************************************/
	
}
