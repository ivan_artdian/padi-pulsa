package corp.sTech.padi.parameter;

public class ResponseMessage {
	public static String success="Success";
	public static String failedProductList="Gagal Mendapat Daftar Produk";
	public static String TransactionInquiryNotFound="Produk Tidak Ditemukan";
	public static String TransactionInquiryNotActive="Produk Tidak Aktif";
	public static String TransactionConfirmFailConstructUrl="Gagal Membuat Padipay URL";
	public static String CredentialInvalid="Username / Password Tidak Sesuai";
	public static String TransactionHistoryEmpty="Riwayat Transaksi Kosong";
	public static String TransactionExpired="Expired";
	public static String failedFavouritesNull="Favourite Product Anda Kosong";
	public static String failedAddFavourite="Failed Add Favourite";
	
}
