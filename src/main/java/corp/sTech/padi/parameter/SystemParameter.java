package corp.sTech.padi.parameter;

public class SystemParameter {
//	public static final int EMAIL_API_PORT = 465;
//	public static final int EMAIL_API_PORT = 587;
	public static final String EMAIL_API_PORT = "EMAIL_API_PORT";
//	public static final String EMAIL_API_HOST = "smtp.gmail.com";
	public static final String EMAIL_API_HOST = "EMAIL_API_HOST";
//	public static final String EMAIL_API_FROM = "stech.developer1@gmail.com";
	public static final String EMAIL_API_FROM = "EMAIL_API_FROM";
	public static final boolean EMAIL_API_AUTH = true;
//	public static final String EMAIL_API_USERNAME = "stech.developer1@gmail.com";
	public static final String EMAIL_API_USERNAME = "EMAIL_API_USERNAME";
//	public static final String EMAIL_API_PASSWORD = "stech123";
	public static final String EMAIL_API_PASSWORD = "EMAIL_API_PASSWORD";
//	public static final Protocol EMAIL_API_PROTOCOL = Protocol.SMTPS;
	public static final Protocol EMAIL_API_PROTOCOL = Protocol.TLS;
	public static final boolean EMAIL_API_DEBUG = true;
	public static final String EMAIL_API_TO = "stech.developer1@gmail.com";
	public static final String DATE_FORMAT = "ddMMyyyyHHmmss"; 
	
//	public static final String MCASH_USER = "628993665482";
	public static final String MCASH_USER = "MCASH_USER";
//	public static final String MCASH_PASSWORD = "kashiwagiukon";
	public static final String MCASH_PASSWORD = "MCASH_PASSWORD";
//	public static final String PADIPAY_URL = "https://www.padipay.com/padipay-payment/webapp/home/request.html";
	public static final String PADIPAY_URL = "PADIPAY_URL";
//	public static final String PADIPAY_MERCHANT_CODE = "padipaytest";
	public static final String PADIPAY_MERCHANT_CODE = "PADIPAY_MERCHANT_CODE";
//	public static final String PADIPAY_MERCHANT_PASSWORD = "padipaytest";
	public static final String PADIPAY_MERCHANT_PASSWORD = "PADIPAY_MERCHANT_PASSWORD";
//	public static final int PADIPAY_HOUR_EXPIRED = 1;
	public static final String PADIPAY_HOUR_EXPIRED = "PADIPAY_HOUR_EXPIRED";
	public static final String PADIPAY_DATE_FORMAT = "yyyyMMdd-HHmmss";
//	public static final String PADIPAY_MERCHANT_URL = "http://localhost:8080/adapter/mgateway/topupBalanceResponse";
	public static final String PADIPAY_MERCHANT_URL = "PADIPAY_MERCHANT_URL";
//	public static final String PADIPAY_HOME_URL = "http://localhost:8080";
	public static final String PADIPAY_HOME_URL = "PADIPAY_HOME_URL";
//	public static final String TRANSACTION_CONFIRM_EMAIL_SUBJECT= "Transaction Notification";
	public static final String TRANSACTION_CONFIRM_EMAIL_SUBJECT= "TRANSACTION_CONFIRM_EMAIL_SUBJECT";
//	public static final String CREDENTIAL_CODE= "PadiCiti";
	public static final String CREDENTIAL_CODE= "CREDENTIAL_CODE";
//	public static final String CREDENTIAL_PASS= "PadiCiti123";
	public static final String CREDENTIAL_PASS= "CREDENTIAL_PASS";
//	public static final int PAYMENT_LIMIT_DATE = 1;
	public static final String PAYMENT_LIMIT_DATE = "PAYMENT_LIMIT_DATE";
//	public static final int ADMIN_FEE = 0;
	public static final String ADMIN_FEE = "ADMIN_FEE";
	public static final String PADIPAY_RESULT_SUCCESS = "00";
	public static final String IMAGE_ROOT_PATH = "IMAGE_ROOT_PATH";
	public static final String DEFAULT_CONTEXT_ROOT = "DEFAULT_CONTEXT_ROOT";
	public static final String MOBILE_IMAGE_MAP = "MOBILE_IMAGE_MAP";
	public static final String IBS_BILL_TYPE_UPLOAD = "upload";
	public static final String IBS_BILL_TYPE_INPUT = "input";
	
	public static final String IPL_PARAMATER_HARGA = "Harga";
	public static final String IPL_PARAMATER_DENDA = "Denda";
}
