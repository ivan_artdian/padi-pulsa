package corp.sTech.padi.parameter;

public enum Protocol {
	SMTP, SMTPS, TLS
}
