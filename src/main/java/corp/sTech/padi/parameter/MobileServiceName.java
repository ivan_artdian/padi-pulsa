package corp.sTech.padi.parameter;

public class MobileServiceName {
	public static String productList = "productList";
	public static String transactionInquiry = "transactionInquiry";
	public static String iplBillInquiry = "iplBillInquiry";
	public static String transactionConfirm = "transactionConfirm";
	public static String iplBillConfirm = "iplBillConfirm";
	public static String iplBillHistory = "iplBillHistory";
	public static String transactionConfirmResponse = "transactionConfirmResponse";
	public static String transactionBillHistory = "transactionBillHistory";
	public static String favList = "favList";
	public static String favAdd = "favAdd";
	public static String favUpdate = "favUpdate";
	public static String favDelete = "favDelete";
}
