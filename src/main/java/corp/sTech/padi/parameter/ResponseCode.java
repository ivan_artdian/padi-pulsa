package corp.sTech.padi.parameter;

public class ResponseCode {
	public static String success="00";
	public static String onProcess="01";
	public static String failedProductList="01";
	public static String TransactionInquiryNotFound="02";
	public static String TransactionInquiryNotActive="03";
	public static String TransactionConfirmFailConstructUrl="04";
	public static String CredentialInvalid="05";
	public static String TransactionFailed="06";
	public static String TransactionHistoryEmpty="07";
	public static String TransactionExpired="08";
	public static String failedFavouritesNull="09";
	public static String failedAddFavourite="10";
	
	public static String retrievePaymentResponseSuccess="00";
	public static String retrievePaymentResponseRejected="01";
	public static String padipayResponseSuccess="00";
	public static String mcashResponseSuccess="0";
}
