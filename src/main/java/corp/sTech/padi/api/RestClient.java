package corp.sTech.padi.api;

import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class RestClient {
	public static String doPostXMLRequest(String resource, Object objectRequest) {
		return doPost(resource, objectRequest, MediaType.APPLICATION_XML);
	}

	public static String doPostFormUrlEncodedRequest(String resource, Object objectRequest) {
		return doPost(resource, objectRequest, MediaType.APPLICATION_FORM_URLENCODED);
	}
	
	public static String doPostFormUrlEncodedRequestAcceptJSON(String resource, Object objectRequest) {
		return doPost(resource, objectRequest, MediaType.APPLICATION_FORM_URLENCODED, MediaType.APPLICATION_JSON);
	}

	public static String doGetFormUrlEncodedRequest(String resource, Object objectRequest) {
		return doGet(resource, objectRequest, MediaType.APPLICATION_FORM_URLENCODED, MediaType.APPLICATION_JSON);
	}

	private static String doPost(String resource, Object objectRequest, String mediaType) {
		Client client = Client.create();
		WebResource webResource = client.resource(resource);

		if (objectRequest == null) {
			webResource.header("Content-Length", 0);
		}

		ClientResponse response = webResource.accept(mediaType).type(mediaType).post(ClientResponse.class,
				objectRequest);

		if (response.getStatus() != 200) {
			System.err.println("Failed : HTTP error code : " + response.getStatus());
			return null;
		}

		String responseString = response.getEntity(String.class);
		return responseString;
	}
	
	private static String doPost(String resource, Object objectRequest, String mediaType, String acceptMediaType) {
		Client client = Client.create();
		WebResource webResource = client.resource(resource);
		
		if (objectRequest == null) {
			webResource.header("Content-Length", 0);
		}
		
		ClientResponse response = webResource.accept(acceptMediaType).type(mediaType).post(ClientResponse.class,
				objectRequest);
		
		if (response.getStatus() != 200) {
			System.err.println("Failed : HTTP error code : " + response.getStatus());
			return null;
		}
		
		String responseString = response.getEntity(String.class);
		return responseString;
	}

	private static String doGet(String resource, Object objectRequest, String mediaType) {
		Client client = Client.create();
		WebResource webResource = client.resource(resource);

		if (objectRequest == null) {
			webResource.header("Content-Length", 0);
		}

		ClientResponse response = webResource.accept(mediaType).type(mediaType).get(ClientResponse.class);

		if (response.getStatus() != 200) {
			System.err.println("Failed : HTTP error code : " + response.getStatus());
			return null;
		}

		String responseString = response.getEntity(String.class);
		return responseString;
	}
	private static String doGet(String resource, Object objectRequest, String mediaTypeRequest, String mediaTypeAccept) {
		Client client = Client.create();
		WebResource webResource = client.resource(resource);
		
		if (objectRequest == null) {
			webResource.header("Content-Length", 0);
		}
		
		ClientResponse response = webResource.accept(mediaTypeAccept).type(mediaTypeRequest).get(ClientResponse.class);
		
		if (response.getStatus() != 200) {
			System.err.println("Failed : HTTP error code : " + response.getStatus());
			return null;
		}
		
		String responseString = response.getEntity(String.class);
		return responseString;
	}

}
