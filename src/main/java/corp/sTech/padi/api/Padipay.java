package corp.sTech.padi.api;

import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.ws.rs.core.MultivaluedMap;

import corp.sTech.padi.parameter.ResponseCode;
import corp.sTech.padi.util.HashUtil;
import corp.sTech.padi.util.UrlEncoder;

public class Padipay {
	public static String getUrlRedirectForward(String merchantCode, String merchantPass, String invoiceCode,
			String amount, String transactionTime, String expirationTime, String merchantReturnUrl,
			String customerFullName, String customerEmail, String customerPhoneNumber, String customerCountry,
			String callBackUrl, String url) {
		Map<String, String> params = new HashMap<>();
		params.put("merchantCode", merchantCode);
		params.put("merchantPass", merchantPass);
		params.put("invoiceCode", invoiceCode);
		params.put("amount", amount);
		params.put("transactionTime", transactionTime);
		params.put("expirationTime", expirationTime);
		params.put("merchantReturnUrl", merchantReturnUrl);
		params.put("languageVer", "ID");
		params.put("customerFullName", customerFullName);
		params.put("customerEmail", customerEmail);
		params.put("customerPhoneNumber", customerPhoneNumber);
		params.put("customerCountry", customerCountry);
		
		String padipaySignature = constructPadipaySignature(merchantCode, merchantPass, invoiceCode, amount,
				expirationTime);

		params.put("padipaySignature", padipaySignature);
		params.put("callBackUrl", callBackUrl);

		String urlEncoded = UrlEncoder
				.encodedUrlParam(url, params);
		return urlEncoded;
	}
	
	public static String getPaymentStatus(String merchantCode, String merchantPass, String invoiceCode, String url) {
		Map<String, String> params = new HashMap<>();
		params.put("merchantCode", merchantCode);
		params.put("merchantPass", merchantPass);
		params.put("invoiceCode", invoiceCode);
		
		String urlEncoded = UrlEncoder.encodedUrlParam(url, params);
		
		String response = RestClient.doPostFormUrlEncodedRequestAcceptJSON(urlEncoded, null);
		
		return response;
	}

	public static String constructPadipaySignature(String merchantCode, String merchantPass, String invoiceCode,
			String amount, String expirationTime) {
		String valueParam1 = merchantCode + "%" + merchantPass + "%" + invoiceCode + "%" + amount + "%"
				+ expirationTime;

		String valueParam2 = valueParam1.toUpperCase();
		String hashSignature = "";
		try {
			hashSignature = HashUtil.sha256(valueParam2);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}

		String padipaySignature = hashSignature.toUpperCase();
		return padipaySignature;
	}

	public static String retrievePaymentResponse(final MultivaluedMap<String, String> formParams) {
		if (formParams != null) {
			String responseCode = ResponseCode.retrievePaymentResponseSuccess;
			Set<String> keySet = formParams.keySet();
			for (String key : keySet) {
				String value = formParams.getFirst(key);
				System.out.println("==================== " + key + ", " + value);
				if("resultCode".equalsIgnoreCase(key)) {
					if("00".equals(value)) {
						// success
						return responseCode;
					}else {
						return ResponseCode.retrievePaymentResponseRejected;
					}
				}
			}
		}else {
			return ResponseCode.retrievePaymentResponseRejected;
		}
		return ResponseCode.retrievePaymentResponseRejected;
	}
}
