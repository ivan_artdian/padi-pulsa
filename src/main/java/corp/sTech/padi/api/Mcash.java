package corp.sTech.padi.api;

import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ws.rs.core.MultivaluedMap;

import com.sun.jersey.core.util.MultivaluedMapImpl;

import corp.sTech.padi.modelApi.Product;
import corp.sTech.padi.parameter.SystemParameter;
import corp.sTech.padi.util.BitUtil;
import corp.sTech.padi.util.HashUtil;
import corp.sTech.padi.util.JSONUtils;
import corp.sTech.padi.util.XMLUtil;

public class Mcash {
	public static String topup(String product, String userid, String password, String msisdn, String partnerTrxId, String url) {
		Map<String, String> elements = new HashMap<>();
		String command = "TOPUP";
		elements.put("command", command);
		constructElementRequest(product, userid, password, msisdn, partnerTrxId, elements);

		String xmlString = XMLUtil.mapToXmlString(elements, "evoucher");
		System.out.println(xmlString);
		
		String response = RestClient.doPostXMLRequest(url, xmlString);
		System.out.println(response);
		
		return response;
	}

	public static String cekTagihan(String product, String userid, String password, String msisdn, String partnerTrxId, String url) {
		Map<String, String> elements = new HashMap<>();
		String command = "CEK";
		elements.put("command", command);
		constructElementRequest(product, userid, password, msisdn, partnerTrxId, elements);

		String xmlString = XMLUtil.mapToXmlString(elements, "evoucher");
		System.out.println(xmlString);
		
		String response = RestClient.doPostXMLRequest(url, xmlString);
		System.out.println(response);
		
		return response;
	}

	public static String bayarTagihan(String product, String userid, String password, String msisdn,
			String partnerTrxId, String amount, String url) {
		Map<String, String> elements = new HashMap<>();
		String command = "PAY";
		elements.put("command", command);
		constructElementRequest(product, userid, password, msisdn, partnerTrxId, elements);
		elements.put("amount", amount);

		String xmlString = XMLUtil.mapToXmlString(elements, "evoucher");
		System.out.println(xmlString);
		
		String response = RestClient.doPostXMLRequest(url, xmlString);
		System.out.println(response);
		
		return response;
	}

	public static String cekHargaDanStatus(String productId, String username, String password, String operator) {
		String command = "PRODUCTCYRUSKU";
		
		MultivaluedMap formData = new MultivaluedMapImpl();
		
		Map<String, String> params = new HashMap<>();
		
		formData.add("command", command);
		formData.add("username", username);
		DateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		String time = sdf.format(new Date());
		formData.add("time", time);
		String signature = constructSignMD5(username, command, time, password);
		formData.add("signature", signature);
		if (productId != null)
			formData.add("product_id", productId);
		if (operator != null)
			formData.add("operator", operator);
		
		String response = RestClient.doPostFormUrlEncodedRequestAcceptJSON("https://cyrusku.cyruspad.com/interkoneksi/productcyrusku.asp", formData);		
		System.out.println(response);
		return response;
	}

	private static void constructElementRequest(String product, String userid, String password, String msisdn,
			String partnerTrxId, Map<String, String> elements) {
		elements.put("product", product);
		elements.put("userid", userid);
		DateFormat sdf = new SimpleDateFormat("HHmmss");
		String time = sdf.format(new Date());
		elements.put("time", time);
		elements.put("msisdn", msisdn);
		elements.put("partner_trxid", partnerTrxId);

		String signature = constructSign(time, msisdn, password);
		elements.put("signature", signature);
	}

	public static String retrieveSN(String message) {
		String[] messages = message.split("S/N ");
		String sn = messages[1];
		System.out.println(sn);
		return sn;
	}

	public static void retrieveReversal(final MultivaluedMap<String, String> formParams) {
		if (formParams != null) {
			Set<String> keySet = formParams.keySet();
			for (String key : keySet) {
				String value = formParams.getFirst(key);
			}
		} else {

		}
	}

	private static String constructSign(String time, String msisdn, String password) {
		StringBuilder builder = new StringBuilder();
		String last4digitMsisdn = msisdn.substring(msisdn.length() - 4, msisdn.length());
		String data1 = time + last4digitMsisdn;
		String reverseLast4DigitMsisdn = new StringBuilder(last4digitMsisdn).reverse().toString();
		String data2 = reverseLast4DigitMsisdn + password.substring(0, 6);
		String xorData = BitUtil.xor(data1, data2);
		
		byte[] xorBytes = binaryToByteArray(xorData);
		
		String result = Base64.getEncoder().encodeToString(xorBytes);
		System.out.println(result);
		return result;
	}

	private static byte[] binaryToByteArray(String bin) {
		byte[] bytes = new byte[10];
		int j = 0;
		
		for (int i = 0; i < bin.length(); i += 8) {
			String bits = bin.substring(i, i+8);
			byte b = Byte.parseByte(bits, 2);
			bytes[j] = b;
			j++;
		}
		
		return bytes;
	}

	private static String constructSignMD5(String username, String command, String time, String password) {
		try {
			String input = username + command + time + password;
			String md5 = HashUtil.md5(input);
			return md5;
			
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static String stringToBinary(String text)
	 {
	     String bString="";
	     String temp="";
	     for(int i=0;i<text.length();i++)
	     {
	         temp=Integer.toBinaryString(text.charAt(i));
	         for(int j=temp.length();j<8;j++)
	         {
	             temp="0"+temp;
	         }
	         bString+=temp;
	     }
	     return bString;
	 }
	 public static String BinaryToString(String binaryCode)
	 {
	     String[] code = binaryCode.split("(?<=\\G........)");
	     String word="";
	     for(int i=0;i<code.length;i++)
	     {
	    	 word+= (char)Integer.parseInt(code[i],2);
	     }
	     return word;
	 }
}
                