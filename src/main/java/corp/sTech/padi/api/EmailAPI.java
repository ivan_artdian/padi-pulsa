package corp.sTech.padi.api;

import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import corp.sTech.padi.parameter.Protocol;
import corp.sTech.padi.parameter.SystemParameter;
import corp.sTech.padi.repository.SystemParameterRepository;
import corp.sTech.padi.util.NumberFormatUtil;

public class EmailAPI {
	private final static String port = SystemParameter.EMAIL_API_PORT;
	private final static String host = SystemParameter.EMAIL_API_HOST;
	private final static String from = SystemParameter.EMAIL_API_FROM;
	private final static boolean auth = SystemParameter.EMAIL_API_AUTH;
	private final static String username = SystemParameter.EMAIL_API_USERNAME;
	private final static String password = SystemParameter.EMAIL_API_PASSWORD;
	private final static Protocol protocol = SystemParameter.EMAIL_API_PROTOCOL;
	private final static boolean debug = SystemParameter.EMAIL_API_DEBUG;
	
	private static SystemParameterRepository systemParameterRepository;
	
	public static void sendMail(String to, String subject, String body, List<String> attachmentNames) {
		Properties props = fillProperties();

		Authenticator authenticator = fillAuthenticator(props);

		Session session = assignSession(props, authenticator);

		if (attachmentNames != null && attachmentNames.size() > 0) {
			sendMessage(to, subject, body, attachmentNames, session);
		} else {
			sendMessage(to, subject, body, session);
		}
	}

	private static void sendMessage(String to, String subject, String body, Session session) {
		MimeMessage message = new MimeMessage(session);
		Multipart multipart = new MimeMultipart();
		BodyPart messageBodyPart = new MimeBodyPart();

		try {
			message.setFrom(new InternetAddress(systemParameterRepository.paramValueOf(from)));
			InternetAddress[] address = { new InternetAddress(to) };
			message.setRecipients(Message.RecipientType.TO, address);
			message.setSubject(subject);
			message.setSentDate(new Date());
//			message.setText(body);
			messageBodyPart.setContent(body, "text/html; charset=utf-8");
			multipart.addBodyPart(messageBodyPart);
			message.setContent(multipart);
			Transport.send(message);
		} catch (MessagingException ex) {
			ex.printStackTrace();
		}
	}

	private static void sendMessage(String to, String subject, String body, List<String> attachmentNames,
			Session session) {
		MimeMessage message = new MimeMessage(session);
		Multipart multipart = new MimeMultipart();
		BodyPart messageBodyPart = new MimeBodyPart();

		try {
			message.setFrom(new InternetAddress(systemParameterRepository.paramValueOf(from)));
			InternetAddress[] address = InternetAddress.parse(to);
			message.setRecipients(Message.RecipientType.TO, address);
			message.setSubject(subject);
			message.setSentDate(new Date());

			for (String filename : attachmentNames) {
				addAttachment(multipart, filename);
			}
//			messageBodyPart.setText(body);
			messageBodyPart.setContent(body, "text/html; charset=utf-8");
			multipart.addBodyPart(messageBodyPart);
			message.setContent(multipart);
			Transport.send(message);
		} catch (MessagingException ex) {
			ex.printStackTrace();
		}
	}

	private static Session assignSession(Properties props, Authenticator authenticator) {
		Session session = Session.getInstance(props, authenticator);
		// session.setDebug(debug);
		return session;
	}

	private static Authenticator fillAuthenticator(Properties props) {
		Authenticator authenticator = null;
		if (auth) {
			props.put("mail.smtp.auth", true);
			authenticator = new Authenticator() {
				private PasswordAuthentication pa = new PasswordAuthentication(systemParameterRepository.paramValueOf(username), systemParameterRepository.paramValueOf(password));

				@Override
				public PasswordAuthentication getPasswordAuthentication() {
					return pa;
				}
			};
		}
		return authenticator;
	}

	private static Properties fillProperties() {
		Properties props = new Properties();
		props.put("mail.smtp.host", systemParameterRepository.paramValueOf(host));
		props.put("mail.smtp.port", NumberFormatUtil.parseNumberToInt(systemParameterRepository.paramValueOf(port)));
		switch (protocol) {
		case SMTPS:
			props.put("mail.smtp.ssl.enable", true);
			break;
		case TLS:
			props.put("mail.smtp.starttls.enable", true);
			props.put("mail.smtp.starttls.required", true);
			break;
		}
		
		System.out.println("===="+systemParameterRepository.paramValueOf(host)+" "+NumberFormatUtil.parseNumberToInt(systemParameterRepository.paramValueOf(port))+" "+protocol+" "+systemParameterRepository.paramValueOf(from)+" "+systemParameterRepository.paramValueOf(username)+" "+systemParameterRepository.paramValueOf(password));
//		props.put("mail.smtp.socketFactory.fallback", "true");
//		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.ssl.trust", "*");
		
//		props.put("mail.smtp.timeout", 25000);
//		props.put("mail.smtps.starttls.enable", true);
//		props.put("mail.smtps.auth", true);
		return props;
	}

	private static void addAttachment(Multipart multipart, String filename) {
		try {
			DataSource source = new FileDataSource(filename);
			BodyPart messageBodyPart = new MimeBodyPart();
			messageBodyPart.setDataHandler(new DataHandler(source));
			messageBodyPart.setFileName(filename);
			multipart.addBodyPart(messageBodyPart);
		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}

	public static SystemParameterRepository getSystemParameterRepository() {
		return systemParameterRepository;
	}

	public static void setSystemParameterRepository(SystemParameterRepository systemParameterRepository) {
		EmailAPI.systemParameterRepository = systemParameterRepository;
	}
}
