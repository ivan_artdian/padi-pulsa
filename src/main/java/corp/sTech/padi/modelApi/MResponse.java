package corp.sTech.padi.modelApi;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import corp.sTech.padi.model.Favourite;

public class MResponse {
	private String trxID;
	private List<String> trxIDs;
	private String sctlID;
	private String responseCode;
	private String responseMessage;
	private List<Product> products;
	private Product product;
	private String padipaySignature;
	private String merchantCode;
	private String merchantPass;
	private String amount;
	private String trxTime;
	private String expirationTime;
	private String merchantReturnURL;
	private String padipayReqURL;
	@JsonProperty("id_pelanggan")
	private String idPelanggan;
	private List<Bill> bills;
	private List<TxnProductHistory> txnProductHistories;
	private List<Favourite> favourites;
	private String denda;
	@JsonProperty("total_amount")
	private String totalAmount;
	
	private String invoiceCode;
	private String transactionTime;
	
	private String discount;
	private String discountCriteria;
	private String pembayaran;
	private String totalTagihan;
	
	
	public String getInvoiceCode() {
		return invoiceCode;
	}
	public void setInvoiceCode(String invoiceCode) {
		this.invoiceCode = invoiceCode;
	}
	public String getTransactionTime() {
		return transactionTime;
	}
	public void setTransactionTime(String transactionTime) {
		this.transactionTime = transactionTime;
	}
	public String getTrxID() {
		return trxID;
	}
	public void setTrxID(String trxID) {
		this.trxID = trxID;
	}
	public String getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}
	public String getResponseMessage() {
		return responseMessage;
	}
	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}
	public List<Product> getProducts() {
		return products;
	}
	public void setProducts(List<Product> products) {
		this.products = products;
	}
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
	public String getPadipaySignature() {
		return padipaySignature;
	}
	public void setPadipaySignature(String padipaySignature) {
		this.padipaySignature = padipaySignature;
	}
	public String getMerchantCode() {
		return merchantCode;
	}
	public void setMerchantCode(String merchantCode) {
		this.merchantCode = merchantCode;
	}
	public String getMerchantPass() {
		return merchantPass;
	}
	public void setMerchantPass(String merchantPass) {
		this.merchantPass = merchantPass;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getTrxTime() {
		return trxTime;
	}
	public void setTrxTime(String trxTime) {
		this.trxTime = trxTime;
	}
	public String getExpirationTime() {
		return expirationTime;
	}
	public void setExpirationTime(String expirationTime) {
		this.expirationTime = expirationTime;
	}
	public String getMerchantReturnURL() {
		return merchantReturnURL;
	}
	public void setMerchantReturnURL(String merchantReturnURL) {
		this.merchantReturnURL = merchantReturnURL;
	}
	public String getPadipayReqURL() {
		return padipayReqURL;
	}
	public void setPadipayReqURL(String padipayReqURL) {
		this.padipayReqURL = padipayReqURL;
	}
	public String getSctlID() {
		return sctlID;
	}
	public void setSctlID(String sctlID) {
		this.sctlID = sctlID;
	}
	public String getIdPelanggan() {
		return idPelanggan;
	}
	public void setIdPelanggan(String idPelanggan) {
		this.idPelanggan = idPelanggan;
	}
	public String getDenda() {
		return denda;
	}
	public void setDenda(String denda) {
		this.denda = denda;
	}
	public String getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(String totalAmount) {
		this.totalAmount = totalAmount;
	}
	public List<Bill> getBills() {
		return bills;
	}
	public void setBills(List<Bill> bills) {
		this.bills = bills;
	}
	public List<String> getTrxIDs() {
		return trxIDs;
	}
	public void setTrxIDs(List<String> trxIDs) {
		this.trxIDs = trxIDs;
	}
	public String getDiscount() {
		return discount;
	}
	public void setDiscount(String discount) {
		this.discount = discount;
	}
	public String getPembayaran() {
		return pembayaran;
	}
	public void setPembayaran(String pembayaran) {
		this.pembayaran = pembayaran;
	}
	public String getTotalTagihan() {
		return totalTagihan;
	}
	public void setTotalTagihan(String totalTagihan) {
		this.totalTagihan = totalTagihan;
	}
	public String getDiscountCriteria() {
		return discountCriteria;
	}
	public void setDiscountCriteria(String discountCriteria) {
		this.discountCriteria = discountCriteria;
	}
	public List<TxnProductHistory> getTxnProductHistories() {
		return txnProductHistories;
	}
	public void setTxnProductHistories(List<TxnProductHistory> txnProductHistories) {
		this.txnProductHistories = txnProductHistories;
	}
	public List<Favourite> getFavourites() {
		return favourites;
	}
	public void setFavourites(List<Favourite> favourites) {
		this.favourites = favourites;
	}
}
