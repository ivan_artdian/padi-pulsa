package corp.sTech.padi.modelApi;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Bill {
	private String nama;
	private String periode;
	private String alamat;
	@JsonProperty("ipl_name")
	private String iplName;
	@JsonProperty("kode_blok")
	private String kodeBlok;
	@JsonProperty("luas_tanah")
	private String luasTanah;
	private String amount;
	private String status;
	private List<String> reference1s;
	private String reference1;
	private String denda;
	private long billId;
	private String paymentCodeRefId;
	private String paymentChannel;
	public String getNama() {
		return nama;
	}
	public void setNama(String nama) {
		this.nama = nama;
	}
	public String getPeriode() {
		return periode;
	}
	public void setPeriode(String periode) {
		this.periode = periode;
	}
	public String getAlamat() {
		return alamat;
	}
	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}
	public String getIplName() {
		return iplName;
	}
	public void setIplName(String iplName) {
		this.iplName = iplName;
	}
	public String getKodeBlok() {
		return kodeBlok;
	}
	public void setKodeBlok(String kodeBlok) {
		this.kodeBlok = kodeBlok;
	}
	public String getLuasTanah() {
		return luasTanah;
	}
	public void setLuasTanah(String luasTanah) {
		this.luasTanah = luasTanah;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getReference1() {
		return reference1;
	}
	public void setReference1(String reference1) {
		this.reference1 = reference1;
	}
	public List<String> getReference1s() {
		return reference1s;
	}
	public void setReference1s(List<String> reference1s) {
		this.reference1s = reference1s;
	}
	public String getDenda() {
		return denda;
	}
	public void setDenda(String denda) {
		this.denda = denda;
	}
	public long getBillId() {
		return billId;
	}
	public void setBillId(long billId) {
		this.billId = billId;
	}
	public String getPaymentCodeRefId() {
		return paymentCodeRefId;
	}
	public void setPaymentCodeRefId(String paymentCodeRefId) {
		this.paymentCodeRefId = paymentCodeRefId;
	}
	public String getPaymentChannel() {
		return paymentChannel;
	}
	public void setPaymentChannel(String paymentChannel) {
		this.paymentChannel = paymentChannel;
	}
}
