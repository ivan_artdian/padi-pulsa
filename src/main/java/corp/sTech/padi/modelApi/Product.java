package corp.sTech.padi.modelApi;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Product {
	@JsonProperty("product_id")
	private String productId;
	private String description;
	private String operator;
	private String amount;
	@JsonProperty("rs_price")
	private String rsPrice;
	private String status;
	
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getOperator() {
		return operator;
	}
	public void setOperator(String operator) {
		this.operator = operator;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getRsPrice() {
		return rsPrice;
	}
	public void setRsPrice(String rsPrice) {
		this.rsPrice = rsPrice;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
}
