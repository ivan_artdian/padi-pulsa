package corp.sTech.padi.modelApi;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MRequest {
	private String operator;
	private String credentialCode;
	private String credentialPass;
	private String mdn;
	private String userID;
	private String email;
	private String amount;
	private String category;
	private String plan;
	private String productId;
	private String trxID;
	private String languageVer;
	private String paymentAmount;
	private Product product;
	private String padipaySignature;
	private String transactionTime;
	private String expirationTime;
	private String merchantCode;
	private String invoiceCode;
	private String padipayCode;
	// private String amount;
	private String miscFee;
	private String totalPayments;
	private String resultCode;
	private String resultDesc;
	private String paymentCodeRefId;
	private String paymentChannelId;
	private String paymentChannel;
	private String addInfo1;
	private String addInfo2;
	private String deviceType;
	@JsonProperty("id_pelanggan")
	private String idPelanggan;
	private String type;
	private List<Bill> bills;
	private String discount;
	private String pembayaran;
	private String totalTagihan;
	private String favType;
	private String favContent;
	private String favID;
	
	public String getOperator() {
		return operator;
	}
	public void setOperator(String operator) {
		this.operator = operator;
	}
	public String getCredentialCode() {
		return credentialCode;
	}
	public void setCredentialCode(String credentialCode) {
		this.credentialCode = credentialCode;
	}
	public String getCredentialPass() {
		return credentialPass;
	}
	public void setCredentialPass(String credentialPass) {
		this.credentialPass = credentialPass;
	}
	public String getMdn() {
		return mdn;
	}
	public void setMdn(String mdn) {
		this.mdn = mdn;
	}
	public String getUserID() {
		return userID;
	}
	public void setUserID(String userID) {
		this.userID = userID;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getPlan() {
		return plan;
	}
	public void setPlan(String plan) {
		this.plan = plan;
	}
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	public String getTrxID() {
		return trxID;
	}
	public void setTrxID(String trxID) {
		this.trxID = trxID;
	}
	public String getLanguageVer() {
		return languageVer;
	}
	public void setLanguageVer(String languageVer) {
		this.languageVer = languageVer;
	}
	public String getPaymentAmount() {
		return paymentAmount;
	}
	public void setPaymentAmount(String paymentAmount) {
		this.paymentAmount = paymentAmount;
	}
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
	public String getPadipaySignature() {
		return padipaySignature;
	}
	public void setPadipaySignature(String padipaySignature) {
		this.padipaySignature = padipaySignature;
	}
	public String getTransactionTime() {
		return transactionTime;
	}
	public void setTransactionTime(String transactionTime) {
		this.transactionTime = transactionTime;
	}
	public String getExpirationTime() {
		return expirationTime;
	}
	public void setExpirationTime(String expirationTime) {
		this.expirationTime = expirationTime;
	}
	public String getMerchantCode() {
		return merchantCode;
	}
	public void setMerchantCode(String merchantCode) {
		this.merchantCode = merchantCode;
	}
	public String getInvoiceCode() {
		return invoiceCode;
	}
	public void setInvoiceCode(String invoiceCode) {
		this.invoiceCode = invoiceCode;
	}
	public String getPadipayCode() {
		return padipayCode;
	}
	public void setPadipayCode(String padipayCode) {
		this.padipayCode = padipayCode;
	}
	public String getMiscFee() {
		return miscFee;
	}
	public void setMiscFee(String miscFee) {
		this.miscFee = miscFee;
	}
	public String getTotalPayments() {
		return totalPayments;
	}
	public void setTotalPayments(String totalPayments) {
		this.totalPayments = totalPayments;
	}
	public String getResultCode() {
		return resultCode;
	}
	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}
	public String getResultDesc() {
		return resultDesc;
	}
	public void setResultDesc(String resultDesc) {
		this.resultDesc = resultDesc;
	}
	public String getPaymentCodeRefId() {
		return paymentCodeRefId;
	}
	public void setPaymentCodeRefId(String paymentCodeRefId) {
		this.paymentCodeRefId = paymentCodeRefId;
	}
	public String getPaymentChannelId() {
		return paymentChannelId;
	}
	public void setPaymentChannelId(String paymentChannelId) {
		this.paymentChannelId = paymentChannelId;
	}
	public String getPaymentChannel() {
		return paymentChannel;
	}
	public void setPaymentChannel(String paymentChannel) {
		this.paymentChannel = paymentChannel;
	}
	public String getAddInfo1() {
		return addInfo1;
	}
	public void setAddInfo1(String addInfo1) {
		this.addInfo1 = addInfo1;
	}
	public String getAddInfo2() {
		return addInfo2;
	}
	public void setAddInfo2(String addInfo2) {
		this.addInfo2 = addInfo2;
	}
	public String getDeviceType() {
		return deviceType;
	}
	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}
	public String getIdPelanggan() {
		return idPelanggan;
	}
	public void setIdPelanggan(String idPelanggan) {
		this.idPelanggan = idPelanggan;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public List<Bill> getBills() {
		return bills;
	}
	public void setBills(List<Bill> bills) {
		this.bills = bills;
	}
	public String getDiscount() {
		return discount;
	}
	public void setDiscount(String discount) {
		this.discount = discount;
	}
	public String getPembayaran() {
		return pembayaran;
	}
	public void setPembayaran(String pembayaran) {
		this.pembayaran = pembayaran;
	}
	public String getTotalTagihan() {
		return totalTagihan;
	}
	public void setTotalTagihan(String totalTagihan) {
		this.totalTagihan = totalTagihan;
	}
	public String getFavType() {
		return favType;
	}
	public void setFavType(String favType) {
		this.favType = favType;
	}
	public String getFavContent() {
		return favContent;
	}
	public void setFavContent(String favContent) {
		this.favContent = favContent;
	}
	public String getFavID() {
		return favID;
	}
	public void setFavID(String favID) {
		this.favID = favID;
	}
	
}
