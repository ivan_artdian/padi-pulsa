package corp.sTech.padi.mobileprocessor;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.TimeZone;

import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.mysql.fabric.xmlrpc.base.Array;

import corp.sTech.padi.api.EmailAPI;
import corp.sTech.padi.api.Mcash;
import corp.sTech.padi.api.Padipay;
import corp.sTech.padi.model.Agent;
import corp.sTech.padi.model.BillerProduct;
import corp.sTech.padi.model.BookingIpl;
import corp.sTech.padi.model.BookingPulsa;
import corp.sTech.padi.model.Discount;
import corp.sTech.padi.model.Favourite;
import corp.sTech.padi.model.Payments;
import corp.sTech.padi.model.TIbsBillDetail;
import corp.sTech.padi.model.TIbsInvoice;
import corp.sTech.padi.model.TrnReservation;
import corp.sTech.padi.model.UniqueNumber;
import corp.sTech.padi.model.Users;
import corp.sTech.padi.modelApi.Bill;
import corp.sTech.padi.modelApi.MRequest;
import corp.sTech.padi.modelApi.MResponse;
import corp.sTech.padi.modelApi.Product;
import corp.sTech.padi.modelApi.TxnProductHistory;
import corp.sTech.padi.modelProcessors.MMetadata;
import corp.sTech.padi.parameter.ActiveParam;
import corp.sTech.padi.parameter.ResponseCode;
import corp.sTech.padi.parameter.ResponseMessage;
import corp.sTech.padi.parameter.SystemParameter;
import corp.sTech.padi.repository.BillerProductRepository;
import corp.sTech.padi.repository.BookingIplRepository;
import corp.sTech.padi.repository.BookingPulsaRepository;
import corp.sTech.padi.repository.DiscountRepository;
import corp.sTech.padi.repository.FavouriteRepository;
import corp.sTech.padi.repository.PaymentsRepository;
import corp.sTech.padi.repository.SystemParameterRepository;
import corp.sTech.padi.repository.TIbsBillDetailRepository;
import corp.sTech.padi.repository.TIbsInvoiceRepository;
import corp.sTech.padi.repository.TrnReservationRepository;
import corp.sTech.padi.repository.UniqueNumberRepository;
import corp.sTech.padi.repository.UsersRepository;
import corp.sTech.padi.util.DateUtil;
import corp.sTech.padi.util.DateUtils;
import corp.sTech.padi.util.JSONUtils;
import corp.sTech.padi.util.MappingService;
import corp.sTech.padi.util.NumberFormatUtil;
import corp.sTech.padi.util.RandomString;
import corp.sTech.padi.util.XMLUtil;

public class MTransaction {
	@Autowired
	private BookingPulsaRepository bookingPulsaRepository;

	@Autowired
	private BookingIplRepository bookingIplRepository;
	
	@Autowired
	private TrnReservationRepository trnReservationRepository;
	
	@Autowired
	private PaymentsRepository paymentsRepository;
	
	@Autowired
	private SystemParameterRepository systemParameterRepository;
	
	@Autowired
	private UniqueNumberRepository uniqueNumberRepository;
	
	@Autowired
	private TIbsInvoiceRepository ibsInvoiceRepository;
	
	@Autowired
	private TIbsBillDetailRepository ibsBillDetailRepository;
	
	@Autowired
	private UsersRepository usersRepository;
	
	@Autowired
	private DiscountRepository discountRepository;
	
	@Autowired
	private FavouriteRepository favouriteRepository;
	
	@Autowired
	private BillerProductRepository billerProductRepository;

	private String url = "https://cyrusku.cyruspad.com/interkoneksi/interkoneksicyrusku.asp";

	public MMetadata productList(MMetadata metadata) {
		MResponse mResponse = new MResponse();
		
		String sourceBiller = systemParameterRepository.findByParamName("SourceBiller").getParamValue();

		List<BillerProduct> products = (List<BillerProduct>) billerProductRepository.findBySourceBiller(sourceBiller);
		List<Product> products2 = new ArrayList<Product>();
		
		mResponse.setTrxID(RandomString.generateRandomString(8));
		if (products != null) {
			mResponse.setResponseCode(ResponseCode.success);
			mResponse.setResponseMessage(ResponseMessage.success);
			
			for (int i = 0; i < products.size(); i++) {
				Product product = new Product();
				
				product.setProductId(products.get(i).getProductId());
				product.setDescription(products.get(i).getDescription());
				product.setOperator(products.get(i).getOperator());
				product.setAmount(products.get(i).getAmount());
				product.setRsPrice(products.get(i).getRsPrice());
				product.setStatus(products.get(i).getStatus());
				
				products2.add(product);
			}
			mResponse.setProducts(products2);
		} else {
			mResponse.setResponseCode(ResponseCode.failedProductList);
			mResponse.setResponseMessage(ResponseMessage.failedProductList);
		}

		metadata.setmResponse(mResponse);
		return metadata;
	}
	
	public MMetadata productListDirectMCash(MMetadata metadata) {
		MRequest mRequest = metadata.getmRequest();
		MResponse mResponse = new MResponse();

		String operator = mRequest.getOperator();
		String username = systemParameterRepository.paramValueOf(SystemParameter.MCASH_USER);
		String password = systemParameterRepository.paramValueOf(SystemParameter.MCASH_PASSWORD);
		String response = Mcash.cekHargaDanStatus(null, username, password, operator);

		List<Product> products = (List<Product>) JSONUtils.jsonStringToListObject(response, Product.class);
		mResponse.setTrxID(RandomString.generateRandomString(8));
		if (products != null) {
			mResponse.setResponseCode(ResponseCode.success);
			mResponse.setResponseMessage(ResponseMessage.success);
		} else {
			mResponse.setResponseCode(ResponseCode.failedProductList);
			mResponse.setResponseMessage(ResponseMessage.failedProductList);
		}
		mResponse.setProducts(products);

		metadata.setmResponse(mResponse);
		return metadata;
	}

	public MMetadata transactionInquiry(MMetadata metadata) {
		MRequest mRequest = metadata.getmRequest();
		MResponse mResponse = new MResponse();

		String credentialCode = mRequest.getCredentialCode();
		String credentialPass = mRequest.getCredentialPass();
		if (systemParameterRepository.paramValueOf(SystemParameter.CREDENTIAL_CODE).equals(credentialCode)
				&& systemParameterRepository.paramValueOf(SystemParameter.CREDENTIAL_PASS).equals(credentialPass)) {
			String mdn = mRequest.getMdn();
			String userID = mRequest.getUserID();
			String email = mRequest.getEmail();
			String productId = mRequest.getProductId();

			String username = systemParameterRepository.paramValueOf(SystemParameter.MCASH_USER);
			String password = systemParameterRepository.paramValueOf(SystemParameter.MCASH_PASSWORD);
			String response = Mcash.cekHargaDanStatus(productId, username, password, null);

			List<Product> products = (List<Product>) JSONUtils.jsonStringToListObject(response, Product.class);
			Product product = (products != null && products.size() > 0) ? products.get(0) : null;
			String trxId = String.valueOf(RandomString.generateRandom(8));
			if (product != null) {
				if ("active".equalsIgnoreCase(product.getStatus())) {
					if ("tagihan".equalsIgnoreCase(product.getOperator())) {
						// postpaid

						String responseCekTagihan = Mcash.cekTagihan(productId, username, password, mdn, trxId, url);

						mResponse.setTrxID(trxId);
						Map<String, String> responseMap = XMLUtil.xmlStringToMap(responseCekTagihan);
						String rc = responseMap.get("result");
						String rm = responseMap.get("msg");
						String amount = responseMap.get("amount");
						if (ResponseCode.mcashResponseSuccess.equals(rc)) {
							mResponse.setResponseCode(ResponseCode.success);
							mResponse.setResponseMessage(ResponseMessage.success);
							product.setAmount(amount);
						} else {
							mResponse.setResponseCode(ResponseCode.TransactionInquiryNotFound);
							mResponse.setResponseMessage(ResponseMessage.TransactionInquiryNotFound);
						}

						mResponse.setProduct(product);

						metadata.setmResponse(mResponse);
						return metadata;

					} else {
						// prepaid

						mResponse.setTrxID(trxId);
						mResponse.setResponseCode(ResponseCode.success);
						mResponse.setResponseMessage(ResponseMessage.success);
						mResponse.setProduct(product);

						metadata.setmResponse(mResponse);
						return metadata;
					}
				} else {
					mResponse.setTrxID(trxId);
					mResponse.setResponseCode(ResponseCode.TransactionInquiryNotActive);
					mResponse.setResponseMessage(ResponseMessage.TransactionInquiryNotActive);
					mResponse.setProduct(product);

					metadata.setmResponse(mResponse);
					return metadata;
				}
			} else {
				mResponse.setTrxID(trxId);
				mResponse.setResponseCode(ResponseCode.TransactionInquiryNotFound);
				mResponse.setResponseMessage(ResponseMessage.TransactionInquiryNotFound);
				mResponse.setProduct(product);

				metadata.setmResponse(mResponse);
				return metadata;
			}
		} else {
			mResponse.setResponseCode(ResponseCode.CredentialInvalid);
			mResponse.setResponseMessage(ResponseMessage.CredentialInvalid);

			metadata.setmResponse(mResponse);
			return metadata;
		}
	}
	
	public MMetadata iplBillInquiry(MMetadata metadata) {
		MRequest mRequest = metadata.getmRequest();
		MResponse mResponse = new MResponse();
		
		String credentialCode = mRequest.getCredentialCode();
		String credentialPass = mRequest.getCredentialPass();
		if (systemParameterRepository.paramValueOf(SystemParameter.CREDENTIAL_CODE).equals(credentialCode)
				&& systemParameterRepository.paramValueOf(SystemParameter.CREDENTIAL_PASS).equals(credentialPass)) {
			String idPelanggan = mRequest.getIdPelanggan();
			String userID = mRequest.getUserID();
			
			String id = null;
			
			String type = mRequest.getType();
			
			if("idPelanggan".equals(type)) {
				id = idPelanggan;
			}else {
				Users users = usersRepository.findOne(new BigInteger(userID));
				id = users.getUserName();
			}
			
			String email = mRequest.getEmail();

			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/yyyy");
//			List<TIbsInvoice> ibsInvoices = ibsInvoiceRepository.findByIbsBillDetail_IdPelangganAndPaidStatusAndValid(id, "UnPaid", ActiveParam.ACTIVE);
			List<TIbsInvoice> ibsInvoices = ibsInvoiceRepository.findByIbsBillDetail_IdPelangganAndIbsBillDetail_ActiveAndPaidStatusAndValid(id, ActiveParam.ACTIVE, "UnPaid", ActiveParam.ACTIVE);
			
			String trxId = String.valueOf(RandomString.generateRandom(8));
			List<Bill> bills ;
			if(ibsInvoices != null) {
				int totalBillAmount = 0;
				int totalDenda = 0;
				bills = new ArrayList<>();
				
				mResponse.setTrxID(trxId);
				mResponse.setResponseCode(ResponseCode.TransactionInquiryNotFound);
				mResponse.setResponseMessage(ResponseMessage.TransactionInquiryNotFound);
				mResponse.setIdPelanggan(id);
				for (TIbsInvoice ibsInvoice : ibsInvoices) {
					mResponse.setResponseCode(ResponseCode.success);
					mResponse.setResponseMessage(ResponseMessage.success);
					
					TIbsBillDetail ibsBillDetail = ibsInvoice.getIbsBillDetail();
					Bill bill = new Bill();
					bill.setNama(ibsBillDetail.getNamaPelanggan());
					String billPeriode = ibsInvoice.getBillPeriode();
					bill.setPeriode(billPeriode);
					bill.setAlamat(ibsBillDetail.getAlamat());
					bill.setIplName(ibsBillDetail.getNamaPerumahan());
					bill.setKodeBlok(ibsBillDetail.getNoBlock());
					bill.setLuasTanah(ibsBillDetail.getLuasTanah());
					String amount = ibsInvoice.getAmount();
					totalBillAmount = totalBillAmount + NumberFormatUtil.parseNumberToInt(amount);
					
					bill.setAmount(amount);
					bill.setStatus(ibsInvoice.getPaidStatus());
					bill.setBillId(ibsInvoice.getBillId());
					
					Date periode = null;
					Date now = new Date();
					try {
						periode = simpleDateFormat.parse(billPeriode);
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					String denda = "0";
					if (periode != null &&
							(
									periode.getYear() > now.getYear() ||
									(periode.getYear() == now.getYear() && periode.getMonth() >= now.getMonth())
							)
						) {
						 if(ibsInvoice.isDenda()) {
							Agent agent = ibsBillDetail.getIbsBill().getAgent();
							corp.sTech.padi.model.SystemParameter spDenda = systemParameterRepository.findByParamNameAndAgent(SystemParameter.IPL_PARAMATER_DENDA, agent);
							if(spDenda != null) {
								denda = spDenda.getParamValue();
							}
						 }
					}else {
						Agent agent = ibsBillDetail.getIbsBill().getAgent();
						corp.sTech.padi.model.SystemParameter spDenda = systemParameterRepository.findByParamNameAndAgent(SystemParameter.IPL_PARAMATER_DENDA, agent);
						if(spDenda != null) {
							denda = spDenda.getParamValue();
						}
					}
					totalDenda = totalDenda + NumberFormatUtil.parseNumberToInt(denda);
					
					bill.setDenda(denda);
					bills.add(bill);
				}
				
				//discount
				String discount = "0";
				if (bills.size()>0) {
					discount = String.valueOf(getDiscount(Integer.parseInt(bills.get(0).getAmount())));
				}
				
				String discountCriteria = String.valueOf(getDiscountCriteria());
				
				mResponse.setBills(bills);
				mResponse.setTotalTagihan(String.valueOf(totalBillAmount));
				mResponse.setDenda(String.valueOf(totalDenda));
				mResponse.setDiscount(discount);
				mResponse.setDiscountCriteria(discountCriteria);
				
				String totalAmount = String.valueOf(totalBillAmount + totalDenda);
				mResponse.setTotalAmount(totalAmount);
				
				
				metadata.setmResponse(mResponse);
				return metadata;
				
			}else {
				mResponse.setResponseCode(ResponseCode.TransactionInquiryNotFound);
				mResponse.setResponseMessage(ResponseMessage.TransactionInquiryNotFound);
				
				metadata.setmResponse(mResponse);
				return metadata;
			}
		} else {
			mResponse.setResponseCode(ResponseCode.CredentialInvalid);
			mResponse.setResponseMessage(ResponseMessage.CredentialInvalid);
			
			metadata.setmResponse(mResponse);
			return metadata;
		}
	}

	public MMetadata transactionConfirm(MMetadata metadata) {
		MRequest mRequest = metadata.getmRequest();
		MResponse mResponse = new MResponse();

		String credentialCode = mRequest.getCredentialCode();
		String credentialPass = mRequest.getCredentialPass();

		if (systemParameterRepository.paramValueOf(SystemParameter.CREDENTIAL_CODE).equals(credentialCode)
				&& systemParameterRepository.paramValueOf(SystemParameter.CREDENTIAL_PASS).equals(credentialPass)) {

			String trxID = mRequest.getTrxID();
			String mdn = mRequest.getMdn();
			String languageVer = mRequest.getLanguageVer();
			String userID = mRequest.getUserID();
			String email = mRequest.getEmail();
			Product product = mRequest.getProduct();
			String amount = product.getRsPrice();
			int adminFee = Integer.parseInt(systemParameterRepository.paramValueOf(SystemParameter.ADMIN_FEE));
			int amountInt = Integer.parseInt(amount);
			int paymentAmountInt = amountInt + adminFee;

			String merchantCode = systemParameterRepository.paramValueOf(SystemParameter.PADIPAY_MERCHANT_CODE);
			String merchantPass = systemParameterRepository.paramValueOf(SystemParameter.PADIPAY_MERCHANT_PASSWORD);
			String invoiceCode = trxID;

			TimeZone zone = TimeZone.getTimeZone("Asia/Jakarta");
			Calendar calendar = Calendar.getInstance(zone);
			DateFormat dateFormat = new SimpleDateFormat(SystemParameter.PADIPAY_DATE_FORMAT);
			dateFormat.setTimeZone(zone);
			Date now = calendar.getTime();
			calendar.add(Calendar.HOUR, Integer.parseInt(systemParameterRepository.paramValueOf(SystemParameter.PADIPAY_HOUR_EXPIRED)));
			Date expirated = calendar.getTime();
			String transactionTime = dateFormat.format(now);
			String expirationTime = dateFormat.format(expirated);
			String merchantReturnUrl = systemParameterRepository.paramValueOf(SystemParameter.PADIPAY_MERCHANT_URL);

			String callBackUrl = systemParameterRepository.paramValueOf(SystemParameter.PADIPAY_HOME_URL);
			String url = systemParameterRepository.paramValueOf(SystemParameter.PADIPAY_URL);
			String paymentAmount = String.valueOf(paymentAmountInt);
			String urlRedirectForward = Padipay.getUrlRedirectForward(merchantCode, merchantPass, invoiceCode, paymentAmount,
					transactionTime, expirationTime, merchantReturnUrl, userID, email, mdn, "Indonesia", callBackUrl,
					url);
			String signature = Padipay.constructPadipaySignature(merchantCode, merchantPass, invoiceCode, paymentAmount,
					expirationTime);

			String responseCode = ResponseCode.success;
			String responseMessage = ResponseMessage.success;
			
			String transactionCode = getTransactionCode().getNumber();

			if (urlRedirectForward != null) {
				// construct response
				mResponse.setPadipaySignature(signature);
				mResponse.setMerchantCode(merchantCode);
				mResponse.setMerchantPass(merchantPass);
				mResponse.setAmount(paymentAmount);
				mResponse.setInvoiceCode(transactionCode);
				mResponse.setTransactionTime(transactionTime);
				mResponse.setExpirationTime(expirationTime);
				mResponse.setMerchantReturnURL(merchantReturnUrl);
				mResponse.setPadipayReqURL(urlRedirectForward);
			} else {
				responseCode = ResponseCode.TransactionConfirmFailConstructUrl;
				responseMessage = ResponseMessage.TransactionConfirmFailConstructUrl;
			}

			// save booking_pulsa
			BookingPulsa bookingPulsa = new BookingPulsa();
			bookingPulsa.setBooking_id(invoiceCode);
			bookingPulsa.setTxn_state("transaction_confirm");
			bookingPulsa.setTxn_date(now);
			bookingPulsa.setUser_id(userID);
			bookingPulsa.setUser_email(email);
			bookingPulsa.setTxn_amount(amount);
			bookingPulsa.setMdn(mdn);
			bookingPulsa.setProduct_id(product.getProductId());
			bookingPulsa.setProduct_name(product.getDescription());
			bookingPulsa.setOperator(product.getOperator());
			bookingPulsa.setResponse_code(responseCode);
			bookingPulsa.setResponse_message(responseMessage);
			
			//save trn_reservation
			TrnReservation trnReservation = new TrnReservation();
			trnReservation.setTransactionCode(transactionCode);
			trnReservation.setTrnStatus("N");
			trnReservation.setUserId(Integer.parseInt(bookingPulsa.getUser_id()));
			
			calendar.setTime(now);
			calendar.add(Calendar.HOUR, Integer.parseInt(systemParameterRepository.paramValueOf(SystemParameter.PAYMENT_LIMIT_DATE)));
			Date paymentLimitDate = calendar.getTime();
			trnReservation.setPaymentLimitDate(paymentLimitDate);
			
			trnReservation.setTransactionDate(now);
			trnReservation.setBookingLimitDate(paymentLimitDate);
			trnReservation.setReservationChannel(MappingService.reservationChannelOf(bookingPulsa.getOperator()));
			trnReservation.setResellerId("1");
			trnReservation.setDeviceType(mRequest.getDeviceType());
			TrnReservation trnReservationSaved = trnReservationRepository.save(trnReservation);
			
			bookingPulsa.setTrnReservation(trnReservationSaved);
			bookingPulsaRepository.save(bookingPulsa);

			//save payments
			BigDecimal zeroBD = new BigDecimal(0);

			Payments payments = new Payments();
			payments.setPaymentId((int) RandomString.generateRandom(7));
			payments.setPaymentAmount(new BigDecimal(paymentAmountInt));
			payments.setAdminFeeTotal(new BigDecimal(adminFee));
			payments.setExtraFeeTotal(zeroBD);
			payments.setNormalPriceTotal(new BigDecimal(amountInt));
			payments.setServiceFeeTotal(zeroBD);
			payments.setBookBalanceTotal(new BigDecimal(amountInt));
			payments.setDiscountExtraTotal(zeroBD);
			payments.setDiscountTotal(zeroBD);
			payments.setNtaPriceTotal(zeroBD);
			payments.setTotDiscPaymentChannel(zeroBD);
			payments.setTotDiscPoint(zeroBD);
			payments.setTot_disc_Product(zeroBD);
			payments.setVoucherValue(zeroBD);
			payments.setPayment3party_charge_tot(zeroBD);
			payments.setTrnReservation(trnReservation);
			paymentsRepository.save(payments);
			
			metadata.setmResponse(mResponse);
			return metadata;
		} else {
			mResponse.setResponseCode(ResponseCode.CredentialInvalid);
			mResponse.setResponseMessage(ResponseMessage.CredentialInvalid);

			metadata.setmResponse(mResponse);
			return metadata;
		}
	}
	
	public MMetadata iplBillConfirm(MMetadata metadata) {
		MRequest mRequest = metadata.getmRequest();
		MResponse mResponse = new MResponse();
		
		String credentialCode = mRequest.getCredentialCode();
		String credentialPass = mRequest.getCredentialPass();
		
		if (systemParameterRepository.paramValueOf(SystemParameter.CREDENTIAL_CODE).equals(credentialCode)
				&& systemParameterRepository.paramValueOf(SystemParameter.CREDENTIAL_PASS).equals(credentialPass)) {
			String idPelanggan = mRequest.getIdPelanggan();
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/yyyy");
			List<TIbsInvoice> ibsInvoices = ibsInvoiceRepository.findByIbsBillDetail_IdPelangganAndPaidStatusAndValid(idPelanggan, "UnPaid", ActiveParam.ACTIVE);
			
			String reference1 = "";
			for (TIbsInvoice tIbsInvoice : ibsInvoices) {
				reference1 = reference1 +";"+ tIbsInvoice.getBillPeriode()+"-"+tIbsInvoice.getAmount();
			}
			reference1 = reference1.replace(";", "");
			TIbsBillDetail ibsBillDetail = ibsInvoices.get(0).getIbsBillDetail();
			
			String trxID = mRequest.getTrxID();
			String languageVer = mRequest.getLanguageVer();
			String userID = mRequest.getUserID();
			String email = mRequest.getEmail();
//			String paymentAmountRequest = mRequest.getPaymentAmount();
//			String paymentAmountRequest = mRequest.getPaymentAmount();
			int amountInt = 0;
			int totalDenda = 0;
			List<Long> invoiceIds = new ArrayList<>();
			
			List<Bill> bills = mRequest.getBills();
			for (Bill bill : bills) {
				int denda = NumberFormatUtil.parseNumberToInt(bill.getDenda());
				amountInt = amountInt + NumberFormatUtil.parseNumberToInt(bill.getAmount()) + denda;
				totalDenda = totalDenda + denda;
				invoiceIds.add(bill.getBillId());
			}
			
			int adminFee = Integer.parseInt(systemParameterRepository.paramValueOf(SystemParameter.ADMIN_FEE));
			int paymentAmountInt = amountInt + adminFee;
			
			String merchantCode = systemParameterRepository.paramValueOf(SystemParameter.PADIPAY_MERCHANT_CODE);
			String merchantPass = systemParameterRepository.paramValueOf(SystemParameter.PADIPAY_MERCHANT_PASSWORD);
			String transactionCode = getTransactionCode().getNumber();
			String invoiceCode = transactionCode;
			
			TimeZone zone = TimeZone.getTimeZone("Asia/Jakarta");
			Calendar calendar = Calendar.getInstance(zone);
			DateFormat dateFormat = new SimpleDateFormat(SystemParameter.PADIPAY_DATE_FORMAT);
			dateFormat.setTimeZone(zone);
			Date now = calendar.getTime();
			calendar.add(Calendar.HOUR, Integer.parseInt(systemParameterRepository.paramValueOf(SystemParameter.PADIPAY_HOUR_EXPIRED)));
			Date expirated = calendar.getTime();
			String transactionTime = dateFormat.format(now);
			String expirationTime = dateFormat.format(expirated);
			String merchantReturnUrl = systemParameterRepository.paramValueOf(SystemParameter.PADIPAY_MERCHANT_URL);
			
			String callBackUrl = systemParameterRepository.paramValueOf(SystemParameter.PADIPAY_HOME_URL);
			String url = systemParameterRepository.paramValueOf(SystemParameter.PADIPAY_URL);
			String paymentAmount = String.valueOf(paymentAmountInt);
			String mdn = ibsBillDetail.getNoHP();
			String noBlock = ibsBillDetail.getNoBlock();
			String urlRedirectForward = Padipay.getUrlRedirectForward(merchantCode, merchantPass, invoiceCode, paymentAmount,
					transactionTime, expirationTime, merchantReturnUrl, noBlock, email, mdn, "Indonesia", callBackUrl,
					url);
			String signature = Padipay.constructPadipaySignature(merchantCode, merchantPass, invoiceCode, paymentAmount,
					expirationTime);
			
			String responseCode = ResponseCode.onProcess;
			String responseMessage = "On Process";
			
			
			if (urlRedirectForward != null) {
				// construct response
				mResponse.setPadipaySignature(signature);
				mResponse.setMerchantCode(merchantCode);
				mResponse.setMerchantPass(merchantPass);
				mResponse.setAmount(paymentAmount);
				mResponse.setInvoiceCode(transactionCode);
				mResponse.setTransactionTime(transactionTime);
				mResponse.setExpirationTime(expirationTime);
				mResponse.setMerchantReturnURL(merchantReturnUrl);
				mResponse.setPadipayReqURL(urlRedirectForward);
				mResponse.setResponseCode(ResponseCode.success);
				mResponse.setResponseMessage(ResponseMessage.success);

				ibsInvoiceRepository.updateInvoiceId(invoiceIds, invoiceCode);
			} else {
				responseCode = ResponseCode.TransactionConfirmFailConstructUrl;
				responseMessage = ResponseMessage.TransactionConfirmFailConstructUrl;
			}
			
			// save booking_ipl
			BookingIpl bookingIpl = new BookingIpl();
			bookingIpl.setBookingId(invoiceCode);
			bookingIpl.setTIbsBilldetail(ibsBillDetail);
			bookingIpl.setTxnState("Process");
			bookingIpl.setTxnDate(now);
			
			bookingIpl.setUserId(userID);
			bookingIpl.setUserEmail(email);
			bookingIpl.setTxnAmount(paymentAmount);
			bookingIpl.setResponseCode(responseCode);
			bookingIpl.setResponseMessage(responseMessage);
			bookingIpl.setReference1(reference1);
			Agent agent = ibsBillDetail.getIbsBill().getAgent();
			bookingIpl.setAgent(agent);
			
			/*String denda = "0";
			for (TIbsInvoice ibsInvoice : ibsInvoices) {
				String billPeriode = ibsInvoice.getBillPeriode();
				Date periode = null;
				Date todate = new Date();
				try {
					periode = simpleDateFormat.parse(billPeriode);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				if (periode != null &&
						(
								periode.getYear() > todate.getYear() ||
								(periode.getYear() == todate.getYear() && periode.getMonth() >= todate.getMonth())
						)
					) {
					 if(ibsInvoice.isDenda()) {
						corp.sTech.padi.model.SystemParameter spDenda = systemParameterRepository.findByParamNameAndAgent(SystemParameter.IPL_PARAMATER_DENDA, agent);
						if(spDenda != null) {
							denda = spDenda.getParamValue();
						}
					 }
				}else {
					corp.sTech.padi.model.SystemParameter spDenda = systemParameterRepository.findByParamNameAndAgent(SystemParameter.IPL_PARAMATER_DENDA, agent);
					if(spDenda != null) {
						denda = spDenda.getParamValue();
					}
				}
			}
			totalDenda = totalDenda + NumberFormatUtil.parseNumberToInt(denda);
			*/
			
			bookingIpl.setDenda(String.valueOf(totalDenda));
			bookingIpl.setIdPelanggan(idPelanggan);
//			String multiPayment = (ibsInvoices != null && ibsInvoices.size() > 1) ? "Yes" : "No";
			String multiPayment = (bills != null && bills.size() > 1) ? "Yes" : "No";
			bookingIpl.setMultiPayment(multiPayment);
			
			//save trn_reservation
			TrnReservation trnReservation = new TrnReservation();
			trnReservation.setTransactionCode(transactionCode);
			trnReservation.setTrnStatus("N");
			trnReservation.setUserId(Integer.parseInt(bookingIpl.getUserId()));
			
			calendar.setTime(now);
			calendar.add(Calendar.HOUR, Integer.parseInt(systemParameterRepository.paramValueOf(SystemParameter.PAYMENT_LIMIT_DATE)));
			Date paymentLimitDate = calendar.getTime();
			trnReservation.setPaymentLimitDate(paymentLimitDate);
			
			trnReservation.setTransactionDate(now);
			trnReservation.setBookingLimitDate(paymentLimitDate);
			String reservationChannel = "ipl";
			trnReservation.setReservationChannel(reservationChannel);
			trnReservation.setResellerId("1");
			trnReservation.setDeviceType(mRequest.getDeviceType());
			TrnReservation trnReservationSaved = trnReservationRepository.save(trnReservation);
			
			bookingIpl.setTrnReservation(trnReservationSaved);
			bookingIplRepository.save(bookingIpl);
			
			//save payments
			BigDecimal zeroBD = new BigDecimal(0);
			
			Payments payments = new Payments();
			payments.setPaymentId((int) RandomString.generateRandom(7));
			payments.setPaymentAmount(new BigDecimal(paymentAmountInt));
			payments.setAdminFeeTotal(new BigDecimal(adminFee));
			payments.setExtraFeeTotal(zeroBD);
			payments.setNormalPriceTotal(new BigDecimal(amountInt));
			payments.setServiceFeeTotal(zeroBD);
			payments.setBookBalanceTotal(new BigDecimal(amountInt));
			payments.setDiscountExtraTotal(zeroBD);
			payments.setDiscountTotal(zeroBD);
			payments.setNtaPriceTotal(zeroBD);
			payments.setTotDiscPaymentChannel(zeroBD);
			payments.setTotDiscPoint(zeroBD);
			payments.setTot_disc_Product(zeroBD);
			payments.setVoucherValue(zeroBD);
			payments.setPayment3party_charge_tot(zeroBD);
			payments.setTrnReservation(trnReservation);
			paymentsRepository.save(payments);
			
			metadata.setmResponse(mResponse);
			return metadata;
		} else {
			mResponse.setResponseCode(ResponseCode.CredentialInvalid);
			mResponse.setResponseMessage(ResponseMessage.CredentialInvalid);
			
			metadata.setmResponse(mResponse);
			return metadata;
		}
	}

	public MMetadata transactionConfirmResponse(MMetadata metadata) {
		MRequest mRequest = metadata.getmRequest();
		MResponse mResponse = new MResponse();

		mResponse.setResponseCode(ResponseCode.TransactionFailed);
		mResponse.setResponseMessage(mRequest.getResultDesc());

		if (ResponseCode.padipayResponseSuccess.equals(mRequest.getResultCode())) {
			String invoiceCode = mRequest.getInvoiceCode();
			BookingPulsa bookingPulsa = bookingPulsaRepository.findOne(invoiceCode);
			if (bookingPulsa != null) {
				String response = null;

				String productId = bookingPulsa.getProduct_id();
				String username = systemParameterRepository.paramValueOf(SystemParameter.MCASH_USER);
				String password = systemParameterRepository.paramValueOf(SystemParameter.MCASH_PASSWORD);
				String mdn = bookingPulsa.getMdn();
				String trxId = RandomString.generateRandomString(8);
				if ("tagihan".equalsIgnoreCase(bookingPulsa.getOperator())) {
					// postpaid

					String amount = bookingPulsa.getTxn_amount();
					response = Mcash.bayarTagihan(productId, username, password, mdn, trxId, amount, url);
				} else {
					// prepaid

					response = Mcash.topup(productId, username, password, mdn, trxId, url);
				}
				Map<String, String> responseMap = XMLUtil.xmlStringToMap(response);
				String rc = responseMap.get("result");
				String rm = responseMap.get("msg");
				
				if(SystemParameter.PADIPAY_RESULT_SUCCESS.equals(rc)) {
					trnReservationRepository.updateTrnReservationStatus("Y", invoiceCode);
				}

				mResponse.setResponseCode(rc);
				mResponse.setResponseMessage(rm);

				String template = "<div style=\"background-color: #6f62ff; padding-right:50px; padding-left: 50px;padding-top: 25px; padding-bottom:25px\">\n"
						+ "  <div style=\"color: #f0f0f0;margin:0px;font-size: 20px; font-weight:bolder\">" + rm
						+ "</div>\n" + "</div>";
				EmailAPI.sendMail(bookingPulsa.getUser_email(), systemParameterRepository.paramValueOf(SystemParameter.TRANSACTION_CONFIRM_EMAIL_SUBJECT),
						template, null);
			}
			
			
			BookingIpl bookingIpl = bookingIplRepository.findOne(invoiceCode);
			if (bookingIpl != null) {
				String paymentCodeRefId = mRequest.getPaymentCodeRefId();
				String paymentChannel = mRequest.getPaymentChannel();
				String response = null;
				String trxId = RandomString.generateRandomString(8);
				trnReservationRepository.updateTrnReservationStatus("Y", invoiceCode);
				
				/*TIbsBillDetail ibsBillDetail = ibsBillDetailRepository.findByIdPelanggan(bookingIpl.getIdPelanggan());
				ibsInvoiceRepository.updatePaidStatus(ibsBillDetail, new Date());*/
				ibsInvoiceRepository.updatePaidStatus(invoiceCode, new Date());
				
				bookingIpl.setTxnState("Finish");
				String rm = "Success";
				if(!ResponseCode.padipayResponseSuccess.equals(mRequest.getResultCode())) {
					rm = "Failed";
				}
				bookingIpl.setResponseMessage(rm);
				bookingIpl.setPaymentCodeRefId(paymentCodeRefId);
				bookingIpl.setPaymentChannel(paymentChannel);
				bookingIplRepository.save(bookingIpl);
				
				mResponse.setResponseCode(ResponseCode.success);
				mResponse.setResponseMessage(rm);
				
				String template = "<div style=\"background-color: #6f62ff; padding-right:50px; padding-left: 50px;padding-top: 25px; padding-bottom:25px\">\n"
						+ "  <div style=\"color: #f0f0f0;margin:0px;font-size: 20px; font-weight:bolder\">" + rm
						+ "</div>\n" + "</div>";
				EmailAPI.sendMail(bookingIpl.getUserEmail(), systemParameterRepository.paramValueOf(SystemParameter.TRANSACTION_CONFIRM_EMAIL_SUBJECT),
						template, null);
			}
		}

		metadata.setmResponse(mResponse);
		return metadata;
	}

	public MMetadata iplBillHistory(MMetadata metadata) {
		MRequest mRequest = metadata.getmRequest();
		MResponse mResponse = new MResponse();
		String userId = mRequest.getIdPelanggan();
		
		/*Users users = usersRepository.findOne(new BigInteger(userId));
		String idPelanggan = users.getUserName();*/

		/*List<TIbsInvoice> ibsInvoices = ibsInvoiceRepository.findByIbsBillDetail_IdPelangganAndPaidStatusAndValid(idPelanggan, "Paid", ActiveParam.ACTIVE);
		
//		mResponse.setTrxID(trxId);
		mResponse.setResponseCode(ResponseCode.TransactionHistoryEmpty);
		mResponse.setResponseMessage(ResponseMessage.TransactionHistoryEmpty);
		mResponse.setIdPelanggan(idPelanggan);
		
		List<Bill> bills = new ArrayList<>();
		List<String> trxIds = new ArrayList<>();
		if(ibsInvoices != null && ibsInvoices.size() >0) {
			for (TIbsInvoice ibsInvoice : ibsInvoices) {
				trxIds.add(""+ibsInvoice.getBillId());
				
				TIbsBillDetail ibsBillDetail = ibsInvoice.getIbsBillDetail();
				mResponse.setResponseCode(ResponseCode.success);
				mResponse.setResponseMessage(ResponseMessage.success);
				
				Bill bill = new Bill();
				bill.setNama(ibsBillDetail.getNamaPelanggan());
				String billPeriode = ibsInvoice.getBillPeriode();
				bill.setPeriode(billPeriode);
				bill.setAlamat(ibsBillDetail.getAlamat());
				bill.setIplName(ibsBillDetail.getNamaPerumahan());
				bill.setKodeBlok(ibsBillDetail.getNoBlock());
				bill.setLuasTanah(ibsBillDetail.getLuasTanah());
				String amount = ibsInvoice.getAmount();
				bill.setAmount(amount);
				bill.setStatus(ibsInvoice.getPaidStatus());
				bills.add(bill);
			}
			
			mResponse.setTrxIDs(trxIds);
			mResponse.setBills(bills);
			
			metadata.setmResponse(mResponse);
			return metadata;
		}else {
			mResponse.setResponseCode(ResponseCode.TransactionHistoryEmpty);
			mResponse.setResponseMessage(ResponseMessage.TransactionHistoryEmpty);
			
			metadata.setmResponse(mResponse);
			return metadata;
		}*/
		
		
//		List<BookingIpl> bookingIpls = bookingIplRepository.findByIdPelanggan(idPelanggan);
		List<BookingIpl> bookingIpls = bookingIplRepository.findByUserId(userId);
		
		mResponse.setResponseCode(ResponseCode.TransactionHistoryEmpty);
		mResponse.setResponseMessage(ResponseMessage.TransactionHistoryEmpty);
//		mResponse.setIdPelanggan(idPelanggan);
		
		List<Bill> bills = new ArrayList<>();
		List<String> trxIds = new ArrayList<>();
		if(bookingIpls != null && bookingIpls.size() >0) {
			for (BookingIpl bookingIpl : bookingIpls) {
				trxIds.add(""+bookingIpl.getBookingId());
				
				TIbsBillDetail ibsBillDetail = bookingIpl.getTIbsBilldetail();
				mResponse.setResponseCode(ResponseCode.success);
				mResponse.setResponseMessage(ResponseMessage.success);
				
				Bill bill = new Bill();
				/*bill.setNama(ibsBillDetail.getNamaPelanggan());
				String billPeriode = bookingIpl.getBillPeriode();
				bill.setPeriode(billPeriode)*/;
				bill.setNama(ibsBillDetail.getNamaPelanggan());
				
				String[] reference1s = bookingIpl.getReference1().split(";");
				
				bill.setReference1s(Arrays.asList(reference1s));
				bill.setAlamat(ibsBillDetail.getAlamat());
				bill.setIplName(ibsBillDetail.getNamaPerumahan());
				bill.setKodeBlok(ibsBillDetail.getNoBlock());
				bill.setLuasTanah(ibsBillDetail.getLuasTanah());
				/*String amount = bookingIpl.getAmount();
				bill.setAmount(amount);*/
				bill.setStatus(bookingIpl.getResponseMessage());
				bill.setPaymentCodeRefId(bookingIpl.getPaymentCodeRefId());
				bill.setPaymentChannel(bookingIpl.getPaymentChannel());
				bills.add(bill);
			}
			
			mResponse.setTrxIDs(trxIds);
			mResponse.setBills(bills);
			
			metadata.setmResponse(mResponse);
			return metadata;
		}else {
			mResponse.setResponseCode(ResponseCode.TransactionHistoryEmpty);
			mResponse.setResponseMessage(ResponseMessage.TransactionHistoryEmpty);
			
			metadata.setmResponse(mResponse);
			return metadata;
		}
	}
	
	public MMetadata transactionBillHistory(MMetadata metadata) {

		MRequest mRequest = metadata.getmRequest();
		MResponse mResponse = new MResponse();
		String userId = mRequest.getIdPelanggan();
		
		List<BookingPulsa> bookingPulsas = bookingPulsaRepository.findByUserId(userId);
		
		mResponse.setResponseCode(ResponseCode.TransactionHistoryEmpty);
		mResponse.setResponseMessage(ResponseMessage.TransactionHistoryEmpty);
		
		List<TxnProductHistory> txnProductHistories = new ArrayList<TxnProductHistory>();
		List<String> trxIds = new ArrayList<>();
		if(bookingPulsas != null && bookingPulsas.size() >0) {
			for (BookingPulsa bookingPulsa : bookingPulsas) {
				trxIds.add(""+bookingPulsa.getBooking_id());
				
				TxnProductHistory txnProductHistory = new TxnProductHistory();
				txnProductHistory.setTxnDate(DateUtil.dateToString(bookingPulsa.getTxn_date()));
				txnProductHistory.setAmount(bookingPulsa.getTxn_amount());
				txnProductHistory.setProductName(bookingPulsa.getProduct_name());
				txnProductHistory.setOperator(bookingPulsa.getOperator());
				txnProductHistory.setMdn(bookingPulsa.getMdn());
				txnProductHistory.setState(bookingPulsa.getTxn_state());
				
				txnProductHistories.add(txnProductHistory);
			}
			
			mResponse.setResponseCode(ResponseCode.success);
			mResponse.setResponseMessage(ResponseMessage.success);
			
			mResponse.setTrxIDs(trxIds);
			mResponse.setTxnProductHistories(txnProductHistories);
			
			metadata.setmResponse(mResponse);
			return metadata;
		}else {
			mResponse.setResponseCode(ResponseCode.TransactionHistoryEmpty);
			mResponse.setResponseMessage(ResponseMessage.TransactionHistoryEmpty);
			
			metadata.setmResponse(mResponse);
			return metadata;
		}
	
	}
	
	public MMetadata listFavourite(MMetadata metadata) {
		MRequest mRequest = metadata.getmRequest();
		MResponse mResponse = new MResponse();

		String userId = mRequest.getIdPelanggan();

		List<Favourite> favourites = favouriteRepository.findByUserId(userId);
		
		if (favourites != null) {
			
			for (Favourite favourite : favourites) {
				Favourite listOfFavourite = new Favourite();
				
				listOfFavourite.setFav_id(favourite.getFav_id());
				listOfFavourite.setType(favourite.getType());
				listOfFavourite.setContent(favourite.getContent());
				
				favourites.add(listOfFavourite);
			}
			
			mResponse.setFavourites(favourites);
			mResponse.setResponseCode(ResponseCode.success);
			mResponse.setResponseMessage(ResponseMessage.success);
		} else {
			mResponse.setResponseCode(ResponseCode.failedFavouritesNull);
			mResponse.setResponseMessage(ResponseMessage.failedProductList);
		}

		metadata.setmResponse(mResponse);
		return metadata;
	}
	
	public MMetadata addFavourite(MMetadata metadata) {
		MRequest mRequest = metadata.getmRequest();
		MResponse mResponse = new MResponse();

		String userId = mRequest.getIdPelanggan();
		String favType= mRequest.getFavType();
		String favData= mRequest.getFavContent();
		
		Favourite favourite = new Favourite();
		favourite.setUser_id(userId);
		favourite.setType(favType);
		favourite.setContent(favData);
		
		try {
			favouriteRepository.save(favourite);
			
			mResponse.setResponseCode(ResponseCode.success);
			mResponse.setResponseMessage(ResponseMessage.success);
		} catch (Exception e) {
			mResponse.setResponseCode(ResponseCode.failedAddFavourite);
			mResponse.setResponseMessage(ResponseMessage.failedAddFavourite);// TODO: handle exception
		}

		metadata.setmResponse(mResponse);
		return metadata;
	}
	
	public MMetadata updateFavourite(MMetadata metadata) {
		MRequest mRequest = metadata.getmRequest();
		MResponse mResponse = new MResponse();

		String favId = mRequest.getFavID();

		Favourite favourite = favouriteRepository.findByFavId(favId);
		
		if (favourite != null) {
			
			favourite.setContent(mRequest.getFavContent());
			
			favouriteRepository.save(favourite);
			
			mResponse.setResponseCode(ResponseCode.success);
			mResponse.setResponseMessage(ResponseMessage.success);
		} else {
			mResponse.setResponseCode(ResponseCode.failedFavouritesNull);
			mResponse.setResponseMessage(ResponseMessage.failedProductList);
		}

		metadata.setmResponse(mResponse);
		return metadata;
	}
	
	public MMetadata deleteFavourite(MMetadata metadata) {
		MRequest mRequest = metadata.getmRequest();
		MResponse mResponse = new MResponse();

		String favId = mRequest.getFavID();

		Favourite favourite = favouriteRepository.findByFavId(favId);
		
		if (favourite != null) {
			
			favouriteRepository.delete(favourite);
			
			mResponse.setResponseCode(ResponseCode.success);
			mResponse.setResponseMessage(ResponseMessage.success);
		} else {
			mResponse.setResponseCode(ResponseCode.failedFavouritesNull);
			mResponse.setResponseMessage(ResponseMessage.failedProductList);
		}

		metadata.setmResponse(mResponse);
		return metadata;
	}
	
	public UniqueNumber getTransactionCode() {
		String thnBlnTgl = DateUtils.dateToyyyyMMddString(new Date());

		UniqueNumber un = new UniqueNumber();
		List<UniqueNumber> uniqNumberLst = uniqueNumberRepository.findByTrnDate(thnBlnTgl);

		Integer counter = 0;

		if (uniqNumberLst.size() > 0) {
			un = uniqNumberLst.get(0);
			counter = un.getCounter() + 1;

			un.setCounter(counter);

		} else {
			counter = 1;
		}

		String thnBlnTglyymmdd = DateUtils.dateToyyyyMMddhhmmssString(new Date());

		String idUn = counter.toString();
		String idNumber = "0000" + idUn;

		Random aRandom = new Random(System.currentTimeMillis());
		long numberRandom = aRandom.nextInt(900) + 100;

		String random = String.valueOf(numberRandom);

		String number = thnBlnTglyymmdd.substring(2, 8) + random + idNumber.substring(idNumber.length() - 4);

		un.setCounter(counter);
		un.setNumber(number);
		
		System.out.println(" getNumber : "+JSONUtils.objectToJSONString(un));
		
		uniqueNumberRepository.save(un);
		return un;
	}

	private int getDiscount(int billAmount) {
		
		try {
			List<Discount> discounts = discountRepository.findByStatus(1);
			if (discounts.size()>0) {
				int totDiscount = 0;
				for (int i = 0; i < discounts.size(); i++) {
					if (discounts.get(i).equals("Nominal")) {
						totDiscount = totDiscount + discounts.get(i).getValue();
					} else {
						int discBillAmount = billAmount * discounts.get(i).getValue();
						
						totDiscount = totDiscount + discBillAmount;
					}
				}
				return totDiscount;
			} else {
				return 0;
			}
		} catch (Exception e) {
			System.out.println("Exception Discount "+e.getMessage());
			return 0;
		}
		
	}
	
	private int getDiscountCriteria() {
		
		try {
			int c = 0;
			List<Discount> discounts = discountRepository.findByStatus(1);
			for (int i = 0; i < discounts.size(); i++) {
				String a = discounts.get(i).getCriteria();
				int b = Integer.parseInt(a);
				c = c + b;
			}
			return c;
		} catch (Exception e) {
			return 0;
		}
		
	}

	public BillerProductRepository getBillerProductRepository() {
		return billerProductRepository;
	}

	public void setBillerProductRepository(BillerProductRepository billerProductRepository) {
		this.billerProductRepository = billerProductRepository;
	}

}
