package corp.sTech.padi.mobileprocessor;

import org.springframework.beans.factory.annotation.Autowired;

import corp.sTech.padi.modelApi.MRequest;
import corp.sTech.padi.modelProcessors.MMetadata;
import corp.sTech.padi.parameter.MobileServiceName;
import corp.sTech.padi.util.JSONUtils;
import corp.sTech.padi.util.TransactionUtils;

public class MProcessor {
	@Autowired
	private MTransaction mTransaction;

	public MMetadata mobileProcess(MMetadata metadata) {

		// processing request
		if (metadata.getmRawData() != null) {
			String rawRequest = metadata.getmRawData().getRawRequest();
			// debugTransaction(metadata.getTxnName(), "In", rawRequest);

			// decrypt
			String request = rawRequest;
			debugTransaction(metadata.getTxnName(), "In", request);

			// extract request
			MRequest mRequest = (MRequest) JSONUtils.jsonStringToObject(request, MRequest.class);
			metadata.setmRequest(mRequest);

		}

		// processing service
		String txnName = metadata.getTxnName();
		if (txnName.equals(MobileServiceName.productList)) {
			metadata = mTransaction.productList(metadata);
		} else if (txnName.equals(MobileServiceName.transactionInquiry)) {
			metadata = mTransaction.transactionInquiry(metadata);
		} else if (txnName.equals(MobileServiceName.iplBillInquiry)) {
			metadata = mTransaction.iplBillInquiry(metadata);
		} else if (txnName.equals(MobileServiceName.transactionConfirm)) {
			metadata = mTransaction.transactionConfirm(metadata);
		} else if (txnName.equals(MobileServiceName.iplBillConfirm)) {
			metadata = mTransaction.iplBillConfirm(metadata);
		} else if (txnName.equals(MobileServiceName.transactionConfirmResponse)) {
			metadata = mTransaction.transactionConfirmResponse(metadata);
		} else if (txnName.equals(MobileServiceName.iplBillHistory)) {
			metadata = mTransaction.iplBillHistory(metadata);
		} else if (txnName.equals(MobileServiceName.transactionBillHistory)) {
			metadata = mTransaction.transactionBillHistory(metadata);
		} else if (txnName.equals(MobileServiceName.favList)) {
			metadata = mTransaction.listFavourite(metadata);
		} else if (txnName.equals(MobileServiceName.favAdd)) {
			metadata = mTransaction.addFavourite(metadata);
		} else if (txnName.equals(MobileServiceName.favUpdate)) {
			metadata = mTransaction.updateFavourite(metadata);
		} else if (txnName.equals(MobileServiceName.favDelete)) {
			metadata = mTransaction.deleteFavourite(metadata);
		}

		// processing response
		metadata = TransactionUtils.constructResponse(metadata);
		debugTransaction(metadata.getTxnName(), "Out", metadata.getmRawData().getRawResponse());

		return metadata;
	}

	private void debugTransaction(String txnName, String txnMode, String rawData) {
		System.out.println("[" + txnName + "]" + "[" + txnMode + "]" + "[" + rawData + "]");
	}
}
