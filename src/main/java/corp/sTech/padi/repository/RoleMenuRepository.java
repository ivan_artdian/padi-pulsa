package corp.sTech.padi.repository;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import corp.sTech.padi.model.Menu;
import corp.sTech.padi.model.Role;
import corp.sTech.padi.model.RoleMenu;

public interface RoleMenuRepository extends BaseRepository<RoleMenu, Long> {
	List<RoleMenu> findByRole(Role role);

	List<RoleMenu> findTop1ByOrderByIdDesc();

	List<RoleMenu> findByRoleAndMenu(Role role, Menu menu);

	@Transactional
	void deleteByRole(Role role);

	@Transactional
	void deleteByMenu(Menu menu);
}
