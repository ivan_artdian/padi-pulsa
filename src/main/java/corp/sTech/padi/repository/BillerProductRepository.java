package corp.sTech.padi.repository;

import java.util.List;

import corp.sTech.padi.model.BillerProduct;

public interface BillerProductRepository extends BaseRepository<BillerProduct, String> {

	List<BillerProduct> findBySourceBiller(String sourceBiller);
}
