package corp.sTech.padi.repository;

import java.util.List;

import corp.sTech.padi.model.UniqueNumber;

public interface UniqueNumberRepository extends BaseRepository<UniqueNumber, String> {
	List<UniqueNumber> findByTrnDate(String trnDate);
}
