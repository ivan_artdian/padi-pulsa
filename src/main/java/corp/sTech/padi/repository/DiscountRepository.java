package corp.sTech.padi.repository;

import java.util.List;

import corp.sTech.padi.model.Discount;

public interface DiscountRepository extends BaseRepository<Discount, String> {

	List<Discount> findByStatus(int status);
}
