package corp.sTech.padi.repository;

import java.util.List;

import corp.sTech.padi.model.BookingPulsa;

public interface BookingPulsaRepository extends BaseRepository<BookingPulsa, String> {

	List<BookingPulsa> findByUserId(String userId);
}
