package corp.sTech.padi.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import corp.sTech.padi.model.Group;
import corp.sTech.padi.model.User;

public interface UserRepository extends BaseRepository<User, String> {
	User findByUsername(String username);
	User findByUsernameAndPassword(String username, String password);
	User findByUsernameAndPasswordAndGroups(String username, String password, Group group);

	@Query("select u from User u LEFT JOIN FETCH u.role where u.username = (:username) and u.password = (:password)")
	User findRoleByUsernameAndPasswordEagerLy(@Param("username") String username, @Param("password") String password);

	@Query("select u from User u LEFT JOIN FETCH u.role")
	List<User> findAllWithRoleEagerly();

	@Query("select u from User u LEFT JOIN FETCH u.role where u.username = (:username)")
	User findByUserWithRoleEagerly(@Param("username") String username);
}