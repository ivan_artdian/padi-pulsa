package corp.sTech.padi.repository;

import java.util.List;

import corp.sTech.padi.model.Group;

public interface GroupRepository extends BaseRepository<Group, String> {
	Group findByUsernameAndPassword(String username, String password);

	List<Group> findByUsername(String username);

	/*@Query("select g from Group g LEFT JOIN FETCH g.groupPreferences where g.username = (:username)")
	Group findGroupWithGroupPreferenceEagerly(@Param("username") String username);*/
}
