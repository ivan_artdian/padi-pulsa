package corp.sTech.padi.repository;

import java.util.List;

import org.springframework.data.jpa.repository.EntityGraph;

import corp.sTech.padi.model.Agent;
import corp.sTech.padi.model.TIbsBill;

public interface TIbsBillRepository extends BaseRepository<TIbsBill, Long> {
	@EntityGraph(value = "TIbsBill.billDetails")
	List<TIbsBill> findByAgent(Agent agent);
	
	@EntityGraph(value = "TIbsBill.billDetails")
	TIbsBill findTop1ByAgent(Agent agent);

	@EntityGraph(value = "TIbsBill.billDetails")
	TIbsBill findOneWithBillInfosByBillId(long billId);
}
