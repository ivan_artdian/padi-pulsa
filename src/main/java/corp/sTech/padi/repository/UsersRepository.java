package corp.sTech.padi.repository;

import java.math.BigInteger;
import java.util.List;

import corp.sTech.padi.model.Users;

public interface UsersRepository extends BaseRepository<Users, BigInteger> {
	List<Users> findByEmail(String email);
}