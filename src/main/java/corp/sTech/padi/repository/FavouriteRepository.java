package corp.sTech.padi.repository;

import java.util.List;

import corp.sTech.padi.model.Favourite;

public interface FavouriteRepository extends BaseRepository<Favourite, String> {

	List<Favourite> findByUserId(String userId);
	Favourite findByFavId(String fav_id);
}
