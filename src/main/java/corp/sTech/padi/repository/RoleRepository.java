package corp.sTech.padi.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import corp.sTech.padi.model.Role;

public interface RoleRepository extends BaseRepository<Role, Long> {
	@Query("select r from Role r LEFT JOIN FETCH r.roleMenus rm where r.roleId = (:id)")
	public Role findWithRoleMenuByRoleIdEagerly(@Param ("id")long roleId);

	public List<Role> findByRoleId(long roleId);
}
