package corp.sTech.padi.repository;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import corp.sTech.padi.model.Agent;
import corp.sTech.padi.model.TIbsBillDetail;
import corp.sTech.padi.model.TIbsInvoice;

public interface TIbsInvoiceRepository extends BaseRepository<TIbsInvoice, Long> {
	List<TIbsInvoice> findByIbsBillDetail_IbsBill_Agent(Agent agent);
	List<TIbsInvoice> findByOrderByCreateDateDesc();
	List<TIbsInvoice> findByIbsBillDetail_IbsBill_AgentOrderByCreateDateDesc(Agent agent);
	TIbsInvoice findTop1ByIbsBillDetail_IdPelangganAndBillPeriode(String idPleanggan, String billPeriode);
	List<TIbsInvoice> findByIbsBillDetailAndBillPeriode(TIbsBillDetail ibsBillDetail, String billPeriode);
	List<TIbsInvoice> findByBillPeriode(String billPeriode);
	List<TIbsInvoice> findByIbsBillDetail_IdPelangganAndPaidStatusAndValid(String idPleanggan, String paidStatus, boolean valid);
	List<TIbsInvoice> findByIbsBillDetail_IdPelangganAndIbsBillDetail_ActiveAndPaidStatusAndValid(String idPleanggan, boolean billdetailsActive, String paidStatus, boolean valid);
	
	@Transactional
	@Modifying
	@Query("update TIbsInvoice ti set ti.paidStatus = 'Paid', ti.paidDate = ?2 where ti.ibsBillDetail = ?1")
	void updatePaidStatus(TIbsBillDetail ibsBillDetail, Date date);
	
	@Transactional
	@Modifying
	@Query("update TIbsInvoice ti set ti.paidStatus = 'Paid', ti.paidDate = ?2 where ti.invoiceCode = ?1")
	void updatePaidStatus(String invoiceCode, Date date);
	
	@Transactional
	@Modifying
	@Query("update TIbsInvoice ti set ti.invoiceCode = ?2 where ti.billId IN ?1")
	void updateInvoiceId(List<Long> billIds, String invoiceCode);
}
