package corp.sTech.padi.repository;

import corp.sTech.padi.model.Payments;

public interface PaymentsRepository extends BaseRepository<Payments, String> {
	
}
