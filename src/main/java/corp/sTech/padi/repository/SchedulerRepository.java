package corp.sTech.padi.repository;

import corp.sTech.padi.model.Scheduler;

public interface SchedulerRepository extends BaseRepository<Scheduler, String> {
}
