package corp.sTech.padi.repository;

import java.util.List;

import corp.sTech.padi.model.Menu;

public interface MenuRepository extends BaseRepository<Menu, Long> {
	public List<Menu> findByMenuId(long menuId);

	public Menu findByUrlLike(String url);
}
