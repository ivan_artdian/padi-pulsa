package corp.sTech.padi.repository;

import java.util.List;

import corp.sTech.padi.model.Agent;
import corp.sTech.padi.model.TIbsBillDetail;

public interface TIbsBillDetailRepository extends BaseRepository<TIbsBillDetail, Long> {
	List<TIbsBillDetail> findByIbsBill_Agent(Agent agent);
	List<TIbsBillDetail> findByIdPelangganIn(List<String> idPelanggan);
	TIbsBillDetail findByIdPelanggan(String idPelanggan);
}
