package corp.sTech.padi.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import corp.sTech.padi.model.TrnReservation;

public interface TrnReservationRepository extends BaseRepository<TrnReservation, String> {
	
	@Transactional
	@Modifying
	@Query("update TrnReservation set trnStatus = ?1 where transactionCode = ?2")
	void updateTrnReservationStatus(String trnStatus, String transactionCode);
}
