package corp.sTech.padi.repository;

import java.util.List;

import corp.sTech.padi.model.Agent;
import corp.sTech.padi.model.BookingIpl;

public interface BookingIplRepository extends BaseRepository<BookingIpl, String> {
	List<BookingIpl> findByIdPelanggan(String idPelanggan);
	List<BookingIpl> findByUserId(String userId);
	
	List<BookingIpl> findByAgent(Agent agent);
	List<BookingIpl> findByResponseCode(String responseCode);
}
