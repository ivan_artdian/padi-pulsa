package corp.sTech.padi.repository;

import java.util.List;

import corp.sTech.padi.model.GroupMenu;

public interface GroupMenuRepository extends BaseRepository<GroupMenu, Long> {
	List<GroupMenu> findById(long id);
}
