package corp.sTech.padi.repository;

import java.util.List;

import corp.sTech.padi.model.Agent;


public interface AgentRepository extends BaseRepository<Agent, String> {
	List<Agent> findByStatus(boolean status);

	List<Agent> findByAgentId(String agentId);
}