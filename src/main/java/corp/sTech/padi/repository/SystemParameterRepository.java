package corp.sTech.padi.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;

import corp.sTech.padi.model.Agent;
import corp.sTech.padi.model.SystemParameter;

public interface SystemParameterRepository extends BaseRepository<SystemParameter, Long> {
	@Query("select s.paramValue from SystemParameter s where s.paramName = ?1")
	String paramValueOf(String paramname);
	
	public SystemParameter findByParamName(String paramName);
	public SystemParameter findByParamNameAndAgent(String paramName, Agent agent);
	
	public List<SystemParameter> findByAgent(Agent agent);
}
